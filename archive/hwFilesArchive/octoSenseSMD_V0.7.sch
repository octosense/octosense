<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="5" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Mechanical" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Gehäuse" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="blind_vias" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="card" color="13" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="_Dimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="6" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="_tanames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="_banames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="_taDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="_baDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="160" name="O_Dim" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="252" name="Panel_note" color="13" fill="1" visible="yes" active="yes"/>
<layer number="253" name="panel" color="13" fill="1" visible="yes" active="yes"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="xfer" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+05V" urn="urn:adsk.eagle:symbol:26987/1">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" urn="urn:adsk.eagle:component:27032/1" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_R&amp;D" urn="urn:adsk.eagle:library:32975949">
<packages>
<package name="DIL08" urn="urn:adsk.eagle:footprint:32975984/1" library_version="1">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="5.08" y1="2.921" x2="-5.08" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="4.318" x2="-5.334" y2="4.064" width="0.127" layer="21"/>
<wire x1="-5.334" y1="4.064" x2="-5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="4.318" x2="5.334" y2="4.064" width="0.127" layer="21"/>
<wire x1="5.334" y1="4.064" x2="5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="1.016" y1="5.08" x2="1.27" y2="4.826" width="0.127" layer="21"/>
<wire x1="1.27" y1="4.826" x2="1.524" y2="5.08" width="0.127" layer="21"/>
<wire x1="-1.524" y1="5.08" x2="-1.27" y2="4.826" width="0.127" layer="21"/>
<wire x1="-1.27" y1="4.826" x2="-1.016" y2="5.08" width="0.127" layer="21"/>
<wire x1="-1.524" y1="-4.826" x2="-1.27" y2="-4.572" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-4.572" x2="-1.016" y2="-4.826" width="0.127" layer="21"/>
<wire x1="1.016" y1="-4.826" x2="1.27" y2="-4.572" width="0.127" layer="21"/>
<wire x1="1.27" y1="-4.572" x2="1.524" y2="-4.826" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.556" x2="-5.334" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-5.334" y1="-3.81" x2="-5.08" y2="-4.064" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.556" x2="5.334" y2="-3.81" width="0.127" layer="21"/>
<wire x1="5.334" y1="-3.81" x2="5.08" y2="-4.064" width="0.127" layer="21"/>
<pad name="1" x="-3.937" y="-3.81" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="2" x="-1.27" y="-3.683" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="3" x="1.27" y="-3.683" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="4" x="3.937" y="-3.81" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="5" x="3.937" y="3.81" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="6" x="1.27" y="3.683" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="7" x="-1.27" y="3.683" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="8" x="-3.937" y="3.81" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<text x="-5.334" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="DIL08" urn="urn:adsk.eagle:package:32976017/1" type="box" library_version="1">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="DIL08"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
</symbols>
<devicesets>
<deviceset name="DIL8" urn="urn:adsk.eagle:component:32976038/1" library_version="1">
<gates>
</gates>
<devices>
<device name="" package="DIL08">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32976017/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_PINHEADERS">
<packages>
<package name="HEADER_1X3_FEMALE">
<wire x1="3.81" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<pad name="1" x="-2.7178" y="0" drill="0.8128" rot="R90"/>
<pad name="2" x="0" y="0" drill="0.8128" rot="R90"/>
<pad name="3" x="2.7178" y="0" drill="0.8128"/>
<text x="0" y="2.54" size="1.016" layer="51" font="fixed" rot="R180" align="center">&gt;NAME</text>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
</package>
<package name="1X04">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<wire x1="-4.3" y1="6.9" x2="26.8" y2="6.9" width="0.2032" layer="21"/>
<wire x1="26.8" y1="6.9" x2="26.8" y2="-6.6" width="0.2032" layer="21"/>
<wire x1="26.8" y1="-6.6" x2="-4.3" y2="-6.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-6.6" x2="-4.3" y2="6.9" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.4" shape="offset" rot="R90"/>
<pad name="2" x="7.62" y="0" drill="1.4" shape="offset" rot="R90"/>
<pad name="3" x="15.24" y="0" drill="1.4" shape="offset" rot="R90"/>
<pad name="4" x="22.86" y="0" drill="1.4" shape="offset" rot="R90"/>
<text x="-1.27" y="5.04" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="3.77" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="0" y1="6.85" x2="6.85" y2="6.85" width="0.127" layer="21"/>
<wire x1="3" y1="7" x2="3" y2="-6.5" width="0.127" layer="21"/>
<wire x1="10.5" y1="7" x2="10.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="18.5" y1="7" x2="18.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="-3" y1="7" x2="-3" y2="-6.5" width="0.127" layer="21"/>
<wire x1="25.5" y1="7" x2="25.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="4" y1="7" x2="4" y2="-6.5" width="0.127" layer="21"/>
<wire x1="11.5" y1="7" x2="11.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="19.5" y1="7" x2="19.5" y2="-6.5" width="0.127" layer="21"/>
</package>
<package name="1X04-1.27MM">
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="-0.127" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
</package>
<package name="MOLEX-1X4_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
</package>
<package name="1X04-SMD">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="1X04_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-4-PTH">
<wire x1="-4.5" y1="-5" x2="-5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5" x2="-5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-6.3" x2="-6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-6" y1="-6.3" x2="-6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-6" y1="1.1" x2="6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6" y1="1.1" x2="6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="6" y1="-6.3" x2="5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-6.3" x2="5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-5" x2="4.5" y2="-5" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-5" drill="0.7"/>
<pad name="2" x="-1" y="-5" drill="0.7"/>
<pad name="3" x="1" y="-5" drill="0.7"/>
<pad name="4" x="3" y="-5" drill="0.7"/>
<text x="-2.27" y="0.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.27" y="-1" size="0.4064" layer="27">&gt;Value</text>
<text x="-3.4" y="-4.3" size="1.27" layer="51">+</text>
<text x="-1.4" y="-4.3" size="1.27" layer="51">-</text>
<text x="0.7" y="-4.1" size="0.8" layer="51">S</text>
<text x="2.7" y="-4.1" size="0.8" layer="51">S</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1MM-RA">
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X04_SMD_STRAIGHT_COMBO">
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-SMD_LONG">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
</package>
<package name="1X04_SMALLPADS">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0.127" y="0" drill="0.85" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.85" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.85" rot="R90"/>
<pad name="4" x="7.493" y="0" drill="0.85" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X6">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="MOLEX-1X6-RA">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SMD">
<wire x1="7.62" y1="1.25" x2="-7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="1.25" x2="-7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-1.25" x2="-6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="7.62" y1="-1.25" x2="7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="6.35" y2="-7.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-6.35" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-5.08" y="0" drill="1.4"/>
<hole x="5.08" y="0" drill="1.4"/>
</package>
<package name="1X06_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="0.508" x2="-0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.143" x2="0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.143" x2="1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.905" y2="1.143" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.143" x2="3.175" y2="1.143" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.143" x2="3.81" y2="0.508" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="4.445" y2="1.143" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.143" x2="5.715" y2="1.143" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.143" x2="6.35" y2="0.508" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.985" y2="1.143" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.143" x2="8.255" y2="1.143" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.143" x2="8.89" y2="0.508" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="9.525" y2="1.143" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.143" x2="10.795" y2="1.143" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.143" x2="11.43" y2="0.508" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="12.065" y2="1.143" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.143" x2="13.335" y2="1.143" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.143" x2="13.97" y2="0.508" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.508" x2="13.97" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.762" x2="13.335" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.397" x2="12.065" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.397" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.762" x2="10.795" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.397" x2="9.525" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.397" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.762" x2="8.255" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.397" x2="6.985" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.397" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="5.715" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.397" x2="4.445" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.397" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.175" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.397" x2="1.905" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.397" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="octagon"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="octagon"/>
<pad name="5" x="10.16" y="0" drill="1.016" shape="octagon"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" shape="octagon"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="9.144" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="11.684" y1="-0.127" x2="11.176" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.716" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.1176" x2="13.6906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.8636" x2="13.6906" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6-RA_LOCK">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SIP_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-2.921" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_FEMALE_LOCK.010">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="13.97" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="1.27" x2="13.97" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3175" y1="-0.1905" x2="0.3175" y2="0.1905" layer="51"/>
<rectangle x1="2.2225" y1="-0.1905" x2="2.8575" y2="0.1905" layer="51"/>
<rectangle x1="4.7625" y1="-0.1905" x2="5.3975" y2="0.1905" layer="51"/>
<rectangle x1="7.3025" y1="-0.1905" x2="7.9375" y2="0.1905" layer="51"/>
<rectangle x1="9.8425" y1="-0.1905" x2="10.4775" y2="0.1905" layer="51"/>
<rectangle x1="12.3825" y1="-0.1905" x2="13.0175" y2="0.1905" layer="51"/>
</package>
<package name="1X06_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06-SMD_EDGE">
<wire x1="13.97" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-11.176" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-11.176" x2="13.97" y2="-11.176" width="0.127" layer="51"/>
<wire x1="13.97" y1="-11.176" x2="13.97" y2="-2.54" width="0.127" layer="51"/>
<smd name="4" x="7.62" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="10.16" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="12.7" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<text x="0" y="-6.35" size="0.4064" layer="51">thru-hole vertical Female Header</text>
<text x="0" y="-7.62" size="0.4064" layer="51">used as an SMD part </text>
<text x="0" y="-8.89" size="0.4064" layer="51">(placed horizontally, at board's edge)</text>
<rectangle x1="-0.381" y1="-2.4892" x2="0.381" y2="0.2794" layer="51"/>
<rectangle x1="2.159" y1="-2.4892" x2="2.921" y2="0.2794" layer="51"/>
<rectangle x1="4.699" y1="-2.4892" x2="5.461" y2="0.2794" layer="51"/>
<rectangle x1="7.239" y1="-2.4892" x2="8.001" y2="0.2794" layer="51"/>
<rectangle x1="9.779" y1="-2.4892" x2="10.541" y2="0.2794" layer="51"/>
<rectangle x1="12.319" y1="-2.4892" x2="13.081" y2="0.2794" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-6">
<wire x1="-2.3" y1="3.4" x2="19.76" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.76" y1="3.4" x2="19.76" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-2.8" x2="19.76" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="19.76" y1="3.15" x2="20.16" y2="3.15" width="0.2032" layer="51"/>
<wire x1="20.16" y1="3.15" x2="20.16" y2="2.15" width="0.2032" layer="51"/>
<wire x1="20.16" y1="2.15" x2="19.76" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="5" x="14" y="0" drill="1.2" diameter="2.032"/>
<pad name="6" x="17.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X06-SMD-FEMALE">
<wire x1="-7.62" y1="4.05" x2="7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="4.05" x2="7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="-4.05" x2="-7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="-4.05" x2="-7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="-6.85" y1="-3" x2="-6.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-6.85" y1="-2" x2="-5.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-5.85" y1="-2" x2="-5.85" y2="-3" width="0.2032" layer="21"/>
<wire x1="-7.1" y1="4" x2="-7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="4" x2="-7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="-4" x2="-7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.1" y1="4" x2="7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="4" x2="7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="-4" x2="7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="4" x2="0.3" y2="4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="4" x2="-2.2" y2="4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="4" x2="-4.8" y2="4" width="0.2032" layer="21"/>
<wire x1="2.3" y1="4" x2="2.9" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="4" x2="5.4" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-4" x2="5.4" y2="-4" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-4" x2="2.8" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-4" x2="0.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-4" x2="-2.2" y2="-4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="-4" x2="-4.8" y2="-4" width="0.2032" layer="21"/>
<smd name="6" x="6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="5" x="3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="4" x="1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="3" x="-1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="2" x="-3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="1" x="-6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A1" x="-6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A2" x="-3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A3" x="-1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A4" x="1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A5" x="3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A6" x="6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
</package>
<package name="1X06-SMD-FEMALE-V2">
<description>Package for 4UCONN part #19686 *UNPROVEN*</description>
<wire x1="-7.5" y1="0.45" x2="-7.5" y2="-8.05" width="0.127" layer="21"/>
<wire x1="7.5" y1="0.45" x2="-7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="7.5" y1="-8.05" x2="7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-8.05" x2="7.5" y2="-8.05" width="0.127" layer="21"/>
<smd name="4" x="-1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="5" x="-3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="6" x="-6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="3" x="1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="7.6" y="-8.3" size="1" layer="27" rot="R180">&gt;Value</text>
<text x="-7.4" y="-9.3" size="1" layer="25">&gt;Name</text>
</package>
<package name="1X06_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<hole x="7.62" y="0" drill="1.4732"/>
<hole x="10.16" y="0" drill="1.4732"/>
<hole x="12.7" y="0" drill="1.4732"/>
</package>
<package name="1X06_SMD_STRAIGHT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="1.25" x2="-10.883" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="-1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="-1.817" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.377" y1="1.25" x2="-0.703" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.457" y1="1.25" x2="-5.783" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.329" y1="-1.25" x2="-6.831" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.409" y1="-1.25" x2="-11.911" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="-12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_ALT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="1.25" x2="-1.817" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-10.883" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.323" y1="1.25" x2="-11.997" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.243" y1="1.25" x2="-6.917" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.371" y1="-1.25" x2="-5.869" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.291" y1="-1.25" x2="-0.789" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="-12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="-2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_COMBO">
<wire x1="12.7" y1="1.27" x2="12.7" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="1.25" x2="14.07" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="-1.25" x2="13.4" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.4" y1="1.25" x2="14.07" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="1.25" x2="11.911" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="-1.29" x2="11.911" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="8.409" y1="1.25" x2="9.371" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.409" y1="-1.29" x2="9.371" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="5" x="10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5-2" x="10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6-2" x="12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="BTA">
<description>British Telecom connector, used for Vernier sensors (analog)</description>
<hole x="6.45" y="0" drill="3.2"/>
<hole x="-6.45" y="0" drill="3.2"/>
<pad name="1" x="4.75" y="9" drill="1.4" diameter="2.54" shape="square" rot="R90"/>
<pad name="2" x="3.25" y="6" drill="1.4" diameter="2.54"/>
<pad name="3" x="1.75" y="9" drill="1.4" diameter="2.54"/>
<pad name="5" x="-1.25" y="9" drill="1.4" diameter="2.54"/>
<pad name="4" x="0.25" y="6" drill="1.4" diameter="2.54"/>
<pad name="6" x="-2.75" y="6" drill="1.4" diameter="2.54"/>
<wire x1="-8" y1="-7.5" x2="-8" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-3.8" x2="-7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-8" y1="-7.5" x2="7.9" y2="-7.5" width="0.2032" layer="21"/>
<wire x1="7.9" y1="-7.5" x2="7.9" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="9.9" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-8.925" y1="-3.8" x2="-8.925" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="8.925" y1="-3.8" x2="8.925" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-8.925" y1="-5.1" x2="-8" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.9" y1="-5.1" x2="8.925" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-8.925" y1="-3.8" x2="-7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-3.8" x2="7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="8.925" y2="-3.8" width="0.2032" layer="21"/>
<text x="-2.92" y="-7.07" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-8" y1="-5.1" x2="7.9" y2="-5.1" width="0.2032" layer="21"/>
</package>
<package name="BTD">
<description>British Telecom connector, used for Vernier sensors (digital)</description>
<hole x="6.45" y="0" drill="3.2"/>
<hole x="-6.45" y="0" drill="3.2"/>
<wire x1="-7.95" y1="-3.8" x2="-7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-7.5" x2="7.95" y2="-7.5" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="9.9" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-8.975" y1="-3.8" x2="-8.975" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="8.875" y1="-3.8" x2="8.875" y2="-5.1" width="0.2032" layer="21"/>
<pad name="2" x="-3.25" y="9" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="1" x="-4.75" y="6" drill="1.4" diameter="2.54" shape="square" rot="R180"/>
<pad name="4" x="-0.25" y="9" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="6" x="2.75" y="9" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="3" x="-1.75" y="6" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="5" x="1.25" y="6" drill="1.4" diameter="2.54" rot="R180"/>
<wire x1="-8.975" y1="-5.1" x2="-7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-5.1" x2="7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-5.1" x2="8.875" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-8.975" y1="-3.8" x2="-7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-3.8" x2="7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-7.5" x2="7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-7.5" x2="-7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="8.875" y2="-3.8" width="0.2032" layer="21"/>
<text x="-3" y="-7" size="1.27" layer="25">&gt;Name</text>
</package>
<package name="1X06_SMD_MALE">
<text x="-1" y="3.321" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1" y="-4.591" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="13.97" y2="-1.25" width="0.127" layer="51"/>
<wire x1="13.97" y1="-1.25" x2="13.97" y2="1.25" width="0.127" layer="51"/>
<wire x1="13.97" y1="1.25" x2="-1.27" y2="1.25" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="0" x2="0.32" y2="2.75" layer="51"/>
<rectangle x1="4.76" y1="0" x2="5.4" y2="2.75" layer="51"/>
<rectangle x1="9.84" y1="0" x2="10.48" y2="2.75" layer="51"/>
<rectangle x1="2.22" y1="-2.75" x2="2.86" y2="0" layer="51" rot="R180"/>
<rectangle x1="7.3" y1="-2.75" x2="7.94" y2="0" layer="51" rot="R180"/>
<rectangle x1="12.38" y1="-2.75" x2="13.02" y2="0" layer="51" rot="R180"/>
<smd name="1" x="0" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="3" x="5.08" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="4" x="7.62" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="5" x="10.16" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="6" x="12.7" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="-1.25" x2="-0.635" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="1.25" x2="-0.635" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.762" y1="1.25" x2="1.778" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.302" y1="1.25" x2="4.318" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.842" y1="1.25" x2="6.858" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.382" y1="1.25" x2="9.398" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.922" y1="1.25" x2="11.938" y2="1.25" width="0.1778" layer="21"/>
<wire x1="1.778" y1="-1.25" x2="0.762" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="4.318" y1="-1.25" x2="3.302" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="6.858" y1="-1.25" x2="5.842" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="9.398" y1="-1.25" x2="8.382" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="11.938" y1="-1.25" x2="10.922" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="-1.25" x2="13.97" y2="1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="-1.25" x2="13.335" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="1.25" x2="13.335" y2="1.25" width="0.1778" layer="21"/>
</package>
<package name="1X06-1MM">
<description>Works with part #579 (https://www.sparkfun.com/products/579). Commonly used on GPS modules EM408, EM406 and GP-635T. Also compatible with cable 9123 (https://www.sparkfun.com/products/9123) and cable 574 (https://www.sparkfun.com/products/574).</description>
<circle x="-3.6" y="1.2" radius="0.1047" width="0.4064" layer="51"/>
<wire x1="-2.54" y1="-1.651" x2="2.54" y2="-1.651" width="0.254" layer="21"/>
<wire x1="-4.318" y1="0.508" x2="-4.318" y2="1.905" width="0.254" layer="21"/>
<wire x1="3.302" y1="1.905" x2="4.318" y2="1.905" width="0.254" layer="21"/>
<wire x1="4.318" y1="1.905" x2="4.318" y2="0.508" width="0.254" layer="21"/>
<wire x1="-4.318" y1="1.905" x2="-3.302" y2="1.905" width="0.254" layer="21"/>
<smd name="1" x="-2.54" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-1.54" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-0.54" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="0.46" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.46" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="2.46" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="P$1" x="-3.84" y="-0.955" dx="1.2" dy="1.8" layer="1"/>
<smd name="P$2" x="3.76" y="-0.955" dx="1.2" dy="1.8" layer="1"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06_LOCK_SMALLPADS">
<pad name="1" x="0.1524" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="6" x="12.5476" y="0" drill="0.9" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<wire x1="-1.27" y1="0.6270625" x2="-0.635" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.2620625" x2="0.635" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.2620625" x2="1.27" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.6270625" x2="1.905" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.2620625" x2="3.175" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.2620625" x2="3.81" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.6270625" x2="4.445" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.2620625" x2="5.715" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.2620625" x2="6.35" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.6270625" x2="6.985" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.2620625" x2="8.255" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.2620625" x2="8.89" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.6270625" x2="9.525" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.2620625" x2="10.795" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.2620625" x2="11.43" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.6270625" x2="12.065" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.2620625" x2="13.335" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.2620625" x2="13.97" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.6270625" x2="13.97" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.6429375" x2="13.335" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.2779375" x2="12.065" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.2779375" x2="11.43" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.6429375" x2="10.795" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.2779375" x2="9.525" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.2779375" x2="8.89" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.6429375" x2="8.255" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.2779375" x2="6.985" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.2779375" x2="6.35" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.6429375" x2="5.715" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.2779375" x2="4.445" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.2779375" x2="3.81" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.6429375" x2="3.175" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.2779375" x2="1.905" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.2779375" x2="1.27" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.6429375" x2="0.635" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.2779375" x2="-0.635" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.2779375" x2="-1.27" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.6429375" x2="-1.27" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.6270625" x2="1.27" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.6270625" x2="3.81" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.6270625" x2="6.35" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.6270625" x2="8.89" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.6270625" x2="11.43" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="-0.714375" y1="-1.6271875" x2="0.635" y2="-1.6271875" width="0.1524" layer="21"/>
</package>
<package name="1X06_POGGO">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01_LONGPAD">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1" diameter="1.6764" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="LUXEON-PAD">
<smd name="P$1" x="0" y="0" dx="3.9" dy="2.4" layer="1" roundness="25"/>
<text x="-1.5" y="2" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SMTSO-256-ET">
<wire x1="-2.286" y1="1.016" x2="-1.016" y2="2.286" width="1.016" layer="31" curve="-42.075022"/>
<wire x1="1.016" y1="2.286" x2="2.286" y2="1.016" width="1.016" layer="31" curve="-42.075022"/>
<wire x1="2.286" y1="-1.016" x2="1.016" y2="-2.286" width="1.016" layer="31" curve="-42.075022"/>
<wire x1="-1.016" y1="-2.286" x2="-2.286" y2="-1.016" width="1.016" layer="31" curve="-42.075022"/>
<circle x="0" y="0" radius="1.016" width="0.127" layer="51"/>
<pad name="P$1" x="0" y="0" drill="3.81" diameter="6.1976"/>
</package>
<package name="SMTRA-256-8-6">
<pad name="P$1" x="-1.9939" y="0" drill="1.3462"/>
<pad name="P$2" x="1.9939" y="0" drill="1.3462"/>
<smd name="P$3" x="0" y="0" dx="6.6548" dy="4.3434" layer="1" cream="no"/>
<text x="1.27" y="2.54" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.54" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<rectangle x1="-3.302" y1="0.762" x2="3.302" y2="2.032" layer="31"/>
<rectangle x1="-1.016" y1="0.508" x2="1.016" y2="0.762" layer="31"/>
<rectangle x1="-1.016" y1="-1.016" x2="1.016" y2="-0.762" layer="31"/>
<rectangle x1="-3.302" y1="-2.032" x2="3.302" y2="-0.762" layer="31"/>
<rectangle x1="-1.016" y1="-0.762" x2="1.016" y2="-0.508" layer="31"/>
<rectangle x1="2.794" y1="0.508" x2="3.302" y2="0.762" layer="31"/>
<rectangle x1="2.794" y1="-0.762" x2="3.302" y2="-0.508" layer="31"/>
<rectangle x1="-3.302" y1="-0.762" x2="-2.794" y2="-0.508" layer="31"/>
<rectangle x1="-3.302" y1="0.508" x2="-2.794" y2="0.762" layer="31"/>
</package>
<package name="1X01NS">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
</package>
<package name="1X01">
<wire x1="1.27" y1="0.635" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="1.1938" y="1.8288" size="1.27" layer="25" font="fixed">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27" font="fixed">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01_2MM">
<pad name="1" x="0" y="0" drill="2" diameter="3.302" rot="R90"/>
<text x="-1.3462" y="1.8288" size="0.8" layer="25" font="fixed">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="0.8" layer="27" font="fixed">&gt;VALUE</text>
</package>
<package name="1X01_OFFSET">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<text x="1.1938" y="1.8288" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="PAD-1.5X4.5">
<smd name="P$1" x="0" y="0" dx="1.5" dy="2" layer="1"/>
</package>
<package name="1X01_POGOPIN_HOLE_LARGE">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90" thermals="no"/>
<hole x="0" y="0" drill="1.5494"/>
</package>
<package name="1X01_POGOPIN_HOLE_0.58">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90" thermals="no"/>
<hole x="0" y="0" drill="1.4732"/>
</package>
<package name="SNAP-FEMALE">
<pad name="1" x="0" y="0" drill="2.921" diameter="4.572"/>
<polygon width="0.254" layer="1">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="29">
<vertex x="-4.0005" y="0" curve="-90.002865"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="31">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="41">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
</package>
<package name="SNAP-MALE">
<smd name="2" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100"/>
<text x="-1.397" y="0.889" size="0.8128" layer="21" font="vector" ratio="15">&gt;NAME</text>
</package>
<package name="SPRING-CONNECTOR">
<smd name="P$2" x="0" y="0" dx="7.112" dy="7.112" layer="1" roundness="100"/>
<text x="-1.3462" y="5.6388" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X01NS-KIT">
<pad name="1" x="0" y="0" drill="1.016" shape="octagon" rot="R90" stop="no"/>
<circle x="0" y="0" radius="0.508" width="0" layer="29"/>
<circle x="0" y="0" radius="0.9398" width="0" layer="30"/>
</package>
<package name="1X01_POGO_SMALL">
<pad name="1" x="0" y="0" drill="0.6" shape="octagon" rot="R90" stop="no"/>
<circle x="0" y="0" radius="0.3" width="0" layer="29"/>
<circle x="0" y="0" radius="0.7" width="0" layer="30"/>
</package>
<package name="1X03">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X3">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3">
<wire x1="-2.3" y1="3.4" x2="9.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="3.4" x2="9.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="9.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="9.3" y1="3.15" x2="9.7" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="3.15" x2="9.7" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="2.15" x2="9.3" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_LOCK">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0.1666875" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0.03175" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="4.92125" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.1176" x2="6.0706" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.8636" x2="6.0706" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X3_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<wire x1="-2.3" y1="3.4" x2="9.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="3.4" x2="9.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="9.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="9.3" y1="3.15" x2="9.7" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="3.15" x2="9.7" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="2.15" x2="9.3" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7.1778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-3-PTH">
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6256"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6256"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6256"/>
<text x="-1.27" y="5.24" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="3.97" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
</package>
<package name="1X03_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-5MM-3">
<wire x1="-3.1" y1="4.2" x2="13.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="4.2" x2="13.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="13.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="13.1" y1="4" x2="13.7" y2="4" width="0.2032" layer="51"/>
<wire x1="13.7" y1="4" x2="13.7" y2="3" width="0.2032" layer="51"/>
<wire x1="13.7" y1="3" x2="13.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.413" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.413"/>
<pad name="3" x="10" y="0" drill="1.3" diameter="2.413"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_NO_SILK">
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-3-SMD">
<wire x1="-4.99" y1="-2.07" x2="-4.99" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.99" y1="-5.57" x2="-4.19" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-5.57" x2="-4.19" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-3.07" x2="-2.99" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="-3.07" x2="4.21" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-3.07" x2="4.21" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-5.57" x2="5.01" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="5.01" y1="-5.57" x2="5.01" y2="-2.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="1.93" x2="-2.99" y2="1.93" width="0.2032" layer="21"/>
<smd name="1" x="-1.99" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="3" x="2.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-4.39" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="4.41" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="0.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<text x="-2.26" y="0.2" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.26" y="-1.07" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="1X03-1MM-RA">
<wire x1="-1" y1="-4.6" x2="1" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="1.75" y1="-0.35" x2="2.4997" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.4997" y1="-0.35" x2="2.4997" y2="-2" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-0.35" x2="-1.75" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="0" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="1" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_FEMALE">
<wire x1="-3.935" y1="4.25" x2="-3.935" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="4.25" x2="-3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="-4.25" x2="3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-3.935" y1="-4.25" x2="3.935" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-0.32" y1="6.8" x2="0.32" y2="7.65" layer="51"/>
<rectangle x1="2.22" y1="6.8" x2="2.86" y2="7.65" layer="51"/>
<rectangle x1="-2.86" y1="6.8" x2="-2.22" y2="7.65" layer="51"/>
<smd name="3" x="2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="0" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-3.155" y="2.775" size="1" layer="27">&gt;Value</text>
<text x="-2.955" y="-3.395" size="1" layer="25">&gt;Name</text>
</package>
<package name="1X03_SMD_RA_MALE">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
</package>
<package name="1X03_SMD_RA_MALE_POST">
<description>&lt;h3&gt;SMD 3-Pin Male Right-Angle Header w/ Alignment posts&lt;/h3&gt;

Matches 4UCONN part # 11026&lt;br&gt;
&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt;http://www.4uconnector.com/online/object/4udrawing/11026.pdf&lt;/a&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.6"/>
<hole x="1.27" y="0" drill="1.6"/>
</package>
<package name="JST-3-PTH-VERT">
<description>This 3-pin connector mates with the JST cable sold on SparkFun.</description>
<wire x1="-3.95" y1="-2.25" x2="-3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="2.25" x2="3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="2.25" x2="3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="0" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="3" x="2" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.4" y="0.75" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.75" size="1.27" layer="51">-</text>
<text x="1.7" y="0.95" size="0.8" layer="51">S</text>
</package>
<package name="2X3_SMD">
<circle x="0" y="1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-2.54" y="1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-2.54" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="0" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="2.54" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="2.54" y="1.27" radius="0.7" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-2.5" x2="-3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.5" x2="3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.5" x2="3.81" y2="-2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.5" x2="-3.81" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-0.3" y1="2.55" x2="0.3" y2="3.35" layer="51"/>
<rectangle x1="-2.84" y1="2.55" x2="-2.24" y2="3.35" layer="51"/>
<rectangle x1="2.24" y1="2.55" x2="2.84" y2="3.35" layer="51"/>
<rectangle x1="-2.84" y1="-3.35" x2="-2.24" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="-0.3" y1="-3.35" x2="0.3" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="2.24" y1="-3.35" x2="2.84" y2="-2.55" layer="51" rot="R180"/>
<smd name="1" x="-2.54" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="2" x="-2.54" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="3" x="0" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="4" x="0" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="5" x="2.54" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="6" x="2.54" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<text x="-3.502" y="0.1" size="0.4064" layer="25">&gt;Name</text>
<text x="-3.502" y="-0.408" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="2X3">
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.605" x2="-0.635" y2="-1.605" width="0.2032" layer="21"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<text x="-1.27" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2X3-NS">
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.2032" layer="51"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.2032" layer="51"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.2032" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<pad name="1" x="-2.38125" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="2" x="-2.38125" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="5" x="2.38125" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="6" x="2.38125" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2X3_POGO">
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.127" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.127" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.127" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.127" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="1.905" width="0.127" layer="21"/>
<wire x1="6.35" y1="1.905" x2="5.715" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.127" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$2" x="0" y="2.54" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$3" x="2.54" y="0" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$4" x="2.54" y="2.54" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$5" x="5.08" y="0" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$6" x="5.08" y="2.54" drill="1.2" diameter="1.8288" shape="octagon"/>
<text x="-0.508" y="4.064" size="0.4064" layer="25">&gt;NAME</text>
<text x="1.778" y="4.064" size="0.4064" layer="25">&gt;VALUE</text>
</package>
<package name="2X3-SHROUDED">
<wire x1="-2.775" y1="3.175" x2="-2.775" y2="1.905" width="0.2032" layer="21"/>
<wire x1="4.5" y1="7.56" x2="4.5" y2="-7.56" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-7.56" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="7.56" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="7.56" x2="4.4" y2="7.56" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-7.56" x2="-4.5" y2="-7.56" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="6.46" x2="3.4" y2="6.46" width="0.2032" layer="51"/>
<wire x1="3.4" y1="6.46" x2="3.4" y2="-6.46" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-6.46" x2="3.4" y2="-6.46" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="6.46" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-6.46" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<pad name="1" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<text x="-2.921" y="7.874" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<text x="-2.921" y="-8.382" size="0.4064" layer="104">&gt;NAME</text>
</package>
<package name="2X3_SMT_POSTS">
<description>4UCON 15881&lt;br&gt;
Male .1" spaced SMT&lt;br&gt;
Keying posts into board</description>
<wire x1="-1.778" y1="2.54" x2="-0.762" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.762" y1="2.54" x2="1.778" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-2.54" x2="-0.889" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.762" y1="-2.54" x2="1.778" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.175" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="3.175" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.429" y1="-4.064" x2="-3.429" y2="-2.794" width="0.127" layer="21"/>
<smd name="1" x="-2.54" y="-2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="3" x="0" y="-2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="4" x="0" y="2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="5" x="2.54" y="-2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="6" x="2.54" y="2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<text x="0" y="-5.08" size="0.6096" layer="27">&gt;VALUE</text>
<text x="0" y="-6.35" size="0.6096" layer="25">&gt;NAME</text>
<hole x="-1.27" y="0" drill="1.8"/>
<hole x="1.27" y="0" drill="1.8"/>
</package>
<package name="2X3_LOCK">
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.2032" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.2032" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.2032" layer="21"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<pad name="1" x="-2.3415625" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="2" x="-2.3415625" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="5" x="2.3415625" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="6" x="2.3415625" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="1X3_FEMALE">
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="-2.54" size="1.778" layer="95" rot="R90">&gt;name</text>
</symbol>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M06">
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M01">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="off" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M03">
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M03X2">
<circle x="-1.778" y="2.54" radius="1.016" width="0.1524" layer="94"/>
<circle x="1.778" y="2.54" radius="1.016" width="0.1524" layer="94"/>
<circle x="1.778" y="0" radius="1.016" width="0.1524" layer="94"/>
<circle x="1.778" y="-2.54" radius="1.016" width="0.1524" layer="94"/>
<circle x="-1.778" y="-2.54" radius="1.016" width="0.1524" layer="94"/>
<circle x="-1.778" y="0" radius="1.016" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-2.794" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-2.794" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-2.794" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="2.794" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="2.794" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="2.794" y2="2.54" width="0.1524" layer="94"/>
<pin name="1" x="-7.62" y="2.54" visible="off" length="short"/>
<pin name="2" x="7.62" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="3" x="-7.62" y="0" visible="off" length="short"/>
<pin name="4" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="5" x="-7.62" y="-2.54" visible="off" length="short"/>
<pin name="6" x="7.62" y="-2.54" visible="off" length="short" rot="R180"/>
<text x="-5.08" y="5.588" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.112" size="1.27" layer="96">&gt;VALUE</text>
<text x="-4.318" y="2.794" size="1.4224" layer="95">1</text>
<text x="3.302" y="2.794" size="1.4224" layer="95">2</text>
<text x="-4.318" y="0.254" size="1.4224" layer="95">3</text>
<text x="3.302" y="0.254" size="1.4224" layer="95">4</text>
<text x="-4.318" y="-2.286" size="1.4224" layer="95">5</text>
<text x="3.302" y="-2.286" size="1.4224" layer="95">6</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1X3_FEMALE" prefix="JP">
<gates>
<gate name="G$1" symbol="1X3_FEMALE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HEADER_1X3_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M04" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 4&lt;/b&gt;
Standard 4-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.27MM" package="1X04-1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X04-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_LONG" package="1X04-SMD_LONG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1X04_SMALLPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="SMALL_PADS"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M06" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 6&lt;/b&gt;&lt;br&gt;
Standard 6-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08094 with associated crimp pins and housings.&lt;p&gt;



NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS...
This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place. You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers. Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<gates>
<gate name="G$1" symbol="M06" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SIP" package="1X06">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA" package="MOLEX-1X6-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X06-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X06_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X06_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X6_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA_LOCK" package="MOLEX-1X6-RA_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIP_LOCK" package="1X06-SIP_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE_LOCK" package="1X06_FEMALE_LOCK.010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X06_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X06-SMD_EDGE_FEMALE" package="1X06-SMD_EDGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-6" package="SCREWTERMINAL-3.5MM-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMDF" package="1X06-SMD-FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-FEMALE-V2" package="1X06-SMD-FEMALE-V2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLES_ONLY" package="1X06_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT" package="1X06_SMD_STRAIGHT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-ALT" package="1X06_SMD_STRAIGHT_ALT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-COMBO" package="1X06_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VERNIER-ANALOG" package="BTA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VERNIER-DIGITAL" package="BTD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_MALE" package="1X06_SMD_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11293"/>
</technology>
</technologies>
</device>
<device name="SMD-1MM" package="1X06-1MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_SILK" package="1X06_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_SMALLPADS" package="1X06_LOCK_SMALLPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGGO" package="1X06_POGGO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M01" prefix="JP">
<description>&lt;b&gt;Header 1&lt;/b&gt;
Standard 1-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115).</description>
<gates>
<gate name="G$1" symbol="M01" x="0" y="0"/>
</gates>
<devices>
<device name="PTH_LONGPAD" package="1X01_LONGPAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="LUXEON-PAD">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMTSO-256-ET" package="SMTSO-256-ET">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMTRA-256-8-6" package="SMTRA-256-8-6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMDNS" package="1X01NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_2MM" package="1X01_2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="OFFSET" package="1X01_OFFSET">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-4.5X1.5" package="PAD-1.5X4.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLE_LARGE" package="1X01_POGOPIN_HOLE_LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLE_0.58" package="1X01_POGOPIN_HOLE_0.58">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SNAP-FEMALE" package="SNAP-FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SNAP-MALE" package="SNAP-MALE">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-CONN" package="SPRING-CONNECTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NOSILK-KIT" package="1X01NS-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1X01_POGO_SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name="POGO_SMALL">
<attribute name="VARIANT" value="POGO_SMALL" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M03" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 3&lt;/b&gt;
Standard 3-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08232 with associated crimp pins and housings.</description>
<gates>
<gate name="G$1" symbol="M03" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X03_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X03_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X3_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X03_NO_SILK" package="1X03_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X03_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-3-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09915" constant="no"/>
</technology>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X03_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SCREW-5MM" package="SCREWTERMINAL-5MM-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X03_LOCK_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-SMD" package="JST-3-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X03-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X03_SMD_RA_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10926"/>
<attribute name="VALUE" value="1x3 RA Female .1&quot;"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE" package="1X03_SMD_RA_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10925"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE_POST" package="1X03_SMD_RA_MALE_POST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-3-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M03X2" prefix="JP" uservalue="yes">
<description>2x3 .1" header.&lt;br&gt;
Shrouded, with keying- CONN-10681&lt;br&gt;
SMT- CONN-11415&lt;br&gt;
Pogo pins- HW-11044</description>
<gates>
<gate name="G$1" symbol="M03X2" x="0" y="0"/>
</gates>
<devices>
<device name="&quot;" package="2X3_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2X3_SILK_SM" package="2X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE_SMD" package="2X3_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11290"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="2X3-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PINS" package="2X3_POGO">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="HW-11044"/>
</technology>
</technologies>
</device>
<device name="SHROUD" package="2X3-SHROUDED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10681"/>
</technology>
</technologies>
</device>
<device name="SMT" package="2X3_SMT_POSTS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11415"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="2X3_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_CONNECTORS">
<packages>
<package name="THONKICONN">
<description>3.5mm socket for Eurorack modular synths</description>
<pad name="P$3_TIP" x="0" y="4.84" drill="1.55" shape="square"/>
<pad name="P$2_SWITCH" x="0" y="-3.38" drill="1.55" shape="square"/>
<pad name="P$1_SLEEVE" x="0" y="-6.48" drill="1.35" shape="square"/>
<wire x1="-4.5" y1="6" x2="4.5" y2="6" width="0.127" layer="21"/>
<wire x1="4.5" y1="6" x2="4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4.5" x2="-4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4.5" x2="-4.5" y2="6" width="0.127" layer="21"/>
<text x="-3.8" y="1.5" size="1" layer="21" font="vector">&gt;NAME</text>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.08" layer="21"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.08" layer="21"/>
</package>
<package name="WQP-PJ301M-12_JACK">
<wire x1="-4.5" y1="6" x2="-1.5" y2="6" width="0.127" layer="51"/>
<wire x1="-1.5" y1="6" x2="1.5" y2="6" width="0.127" layer="51"/>
<wire x1="1.5" y1="6" x2="4.5" y2="6" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6" x2="-4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-4.5" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<wire x1="0.2" y1="-4.5" x2="4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="4.5" y1="-4.5" x2="4.5" y2="6" width="0.127" layer="51"/>
<circle x="0" y="0" radius="3.162275" width="0.127" layer="253"/>
<wire x1="-1.5" y1="6" x2="-1.5" y2="4" width="0.127" layer="51"/>
<wire x1="-1.5" y1="4" x2="1.5" y2="4" width="0.127" layer="51"/>
<wire x1="1.5" y1="4" x2="1.5" y2="6" width="0.127" layer="51"/>
<pad name="P$1_TIP" x="0" y="5" drill="1.1" thermals="no"/>
<pad name="P$2_SWITCH" x="0" y="-3.5" drill="1.1"/>
<pad name="P$3_SLEEVE" x="0" y="-6.5" drill="1.1"/>
<circle x="0" y="0" radius="2.690721875" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-4.6" x2="-0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="-0.2" y1="-6.7" x2="0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="0.2" y1="-6.7" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<rectangle x1="-2.8" y1="-2.8" x2="2.8" y2="2.8" layer="39"/>
<text x="4.5212" y="-4.7244" size="1.016" layer="21" rot="SR180">&gt;NAME</text>
<wire x1="-4.5" y1="5.5" x2="-4.5" y2="6" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6" x2="-4" y2="6" width="0.127" layer="21"/>
<wire x1="4" y1="6" x2="4.5" y2="6" width="0.127" layer="21"/>
<wire x1="4.5" y1="6" x2="4.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4" x2="4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4.5" x2="4" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4" x2="-4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4.5" x2="-4" y2="-4.5" width="0.127" layer="21"/>
</package>
<package name="THONKICONN_TIGHT">
<wire x1="-4.5" y1="6" x2="-1.5" y2="6" width="0.127" layer="51"/>
<wire x1="-1.5" y1="6" x2="1.5" y2="6" width="0.127" layer="51"/>
<wire x1="1.5" y1="6" x2="4.5" y2="6" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6" x2="-4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-4.5" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<wire x1="0.2" y1="-4.5" x2="4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="4.5" y1="-4.5" x2="4.5" y2="6" width="0.127" layer="51"/>
<circle x="0" y="0" radius="3.162275" width="0.127" layer="253"/>
<wire x1="-1.5" y1="6" x2="-1.5" y2="4" width="0.127" layer="51"/>
<wire x1="-1.5" y1="4" x2="1.5" y2="4" width="0.127" layer="51"/>
<wire x1="1.5" y1="4" x2="1.5" y2="6" width="0.127" layer="51"/>
<pad name="P$1_TIP" x="0" y="5" drill="1.1" thermals="no"/>
<pad name="P$2_SWITCH" x="0" y="-3.5" drill="1.1"/>
<pad name="P$3_SLEEVE" x="0" y="-6.1063" drill="1.1"/>
<circle x="0" y="0" radius="2.690721875" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-4.6" x2="-0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="-0.2" y1="-6.7" x2="0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="0.2" y1="-6.7" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<rectangle x1="-2.8" y1="-2.8" x2="2.8" y2="2.8" layer="39"/>
<text x="-2.3622" y="6.2738" size="1.016" layer="21" rot="SR0">&gt;NAME</text>
<wire x1="-4.5" y1="5.5" x2="-4.5" y2="6" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6" x2="-4" y2="6" width="0.127" layer="21"/>
<wire x1="4" y1="6" x2="4.5" y2="6" width="0.127" layer="21"/>
<wire x1="4.5" y1="6" x2="4.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4" x2="4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4.5" x2="4" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4" x2="-4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4.5" x2="-4" y2="-4.5" width="0.127" layer="21"/>
</package>
<package name="MICROPHONE">
<circle x="0" y="0" radius="4.8" width="0.1524" layer="21"/>
<pad name="P$1" x="-1.27" y="2" drill="0.7"/>
<pad name="P$2" x="1.27" y="2" drill="0.7"/>
<circle x="0" y="0" radius="4.57905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="21"/>
<circle x="0" y="-1.27" radius="1.27" width="0.1524" layer="21"/>
</package>
<package name="PWR_CON_WITHOUT_LABLES">
<wire x1="-1.905" y1="6.35" x2="-2.54" y2="5.715" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="4.445" x2="-1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="3.81" x2="-2.54" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="6.35" x2="1.905" y2="6.35" width="0.1524" layer="21"/>
<wire x1="1.905" y1="6.35" x2="2.54" y2="5.715" width="0.1524" layer="21"/>
<wire x1="2.54" y1="5.715" x2="2.54" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.54" y1="4.445" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="2.54" y2="3.175" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="-1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="3.175" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-3.81" x2="-2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-5.715" x2="-1.905" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-4.445" x2="2.54" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-5.715" x2="1.905" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="-1.905" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-4.445" x2="-2.54" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-6.985" x2="2.54" y2="-6.985" width="0.6096" layer="21"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<pad name="1" x="-1.27" y="4.9037875" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="4.9037875" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="9" x="-1.27" y="-4.9037875" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="10" x="1.27" y="-4.9037875" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
</package>
<package name="POWER_JACK">
<wire x1="-4.445" y1="-5.207" x2="-4.445" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-6.35" x2="-2.032" y2="-6.35" width="0.127" layer="21"/>
<wire x1="2.032" y1="-6.35" x2="4.445" y2="-6.35" width="0.127" layer="21"/>
<wire x1="4.445" y1="-6.35" x2="4.445" y2="7.366" width="0.127" layer="21"/>
<wire x1="4.445" y1="7.366" x2="-4.445" y2="7.366" width="0.127" layer="21"/>
<wire x1="-4.445" y1="7.366" x2="-4.445" y2="-1.143" width="0.127" layer="21"/>
<pad name="PIN" x="0" y="-6.0452" drill="0.5" diameter="1.6764" shape="long"/>
<pad name="RING" x="0" y="-0.3048" drill="0.5" diameter="1.7" shape="long" rot="R180"/>
<pad name="SW" x="-4.445" y="-3.175" drill="0.5" diameter="1.6764" shape="long" rot="R270"/>
<polygon width="0.0508" layer="46">
<vertex x="-1.166" y="-0.0508"/>
<vertex x="1.166" y="-0.0508" curve="-90"/>
<vertex x="1.42" y="-0.3048" curve="-90"/>
<vertex x="1.166" y="-0.5588"/>
<vertex x="-1.166" y="-0.5588" curve="-90"/>
<vertex x="-1.42" y="-0.3048" curve="-90"/>
</polygon>
<polygon width="0.0508" layer="46">
<vertex x="-1.016" y="-5.7912"/>
<vertex x="1.016" y="-5.7912" curve="-90"/>
<vertex x="1.27" y="-6.0452" curve="-90"/>
<vertex x="1.016" y="-6.2992"/>
<vertex x="-1.016" y="-6.2992" curve="-90"/>
<vertex x="-1.27" y="-6.0452" curve="-90"/>
</polygon>
<polygon width="0.0508" layer="46">
<vertex x="-4.699" y="-4.191"/>
<vertex x="-4.699" y="-2.159" curve="-90"/>
<vertex x="-4.445" y="-1.905" curve="-90"/>
<vertex x="-4.191" y="-2.159"/>
<vertex x="-4.191" y="-4.191" curve="-90"/>
<vertex x="-4.445" y="-4.445" curve="-90"/>
</polygon>
<text x="0" y="-2.54" size="0.8" layer="21" font="fixed" ratio="15" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-3.81" size="0.8" layer="21" font="fixed" ratio="15" rot="R180" align="center">&gt;VALUE</text>
<polygon width="0.1524" layer="39">
<vertex x="-4.445" y="7.366"/>
<vertex x="4.445" y="7.366"/>
<vertex x="4.445" y="-6.35"/>
<vertex x="-4.445" y="-6.35"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="THONKICONN">
<description>pj301-b vertical 3.5mm jack socket</description>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="1.524" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.524" y1="1.016" x2="2.286" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="2.286" x2="-1.016" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.524" x2="-0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="-0.762" y2="2.286" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="4.572" y2="-2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="4.064" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="2.286" y1="-0.508" x2="7.874" y2="0.508" layer="94" rot="R90"/>
<pin name="SLEEVE" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="TIP" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="SWITCH" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="MICROHONE">
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="5.08" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" length="middle"/>
<pin name="P$2" x="5.08" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="PWR_CON">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="8.382" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="PWR_JACK">
<wire x1="-5.715" y1="1.27" x2="-5.715" y2="3.81" width="0.1524" layer="94" curve="-180"/>
<wire x1="-5.715" y1="3.81" x2="-1.27" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="4.445" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="-1.27" y2="4.445" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.715" y1="-2.54" x2="-6.35" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="-6.985" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<pin name="PIN" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="RING" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="SW" x="5.08" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<text x="-7.62" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="0" y="-2.54"/>
<vertex x="-0.508" y="-1.27"/>
<vertex x="0.508" y="-1.27"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="THONKICONN" prefix="U">
<description>3.5mm socket for Eurorack modular synths</description>
<gates>
<gate name="G$1" symbol="THONKICONN" x="0" y="0"/>
</gates>
<devices>
<device name="OLD" package="THONKICONN">
<connects>
<connect gate="G$1" pin="SLEEVE" pad="P$1_SLEEVE"/>
<connect gate="G$1" pin="SWITCH" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="TIP" pad="P$3_TIP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NEW" package="WQP-PJ301M-12_JACK">
<connects>
<connect gate="G$1" pin="SLEEVE" pad="P$3_SLEEVE"/>
<connect gate="G$1" pin="SWITCH" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="TIP" pad="P$1_TIP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="THONKICONN_TIGHT">
<connects>
<connect gate="G$1" pin="SLEEVE" pad="P$3_SLEEVE"/>
<connect gate="G$1" pin="SWITCH" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="TIP" pad="P$1_TIP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICROPHONE" prefix="M">
<gates>
<gate name="G$1" symbol="MICROHONE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MICROPHONE">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value=""/>
<attribute name="ODOO" value=""/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWR_CON_WITHOUT_LABLES" prefix="J">
<gates>
<gate name="G$1" symbol="PWR_CON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PWR_CON_WITHOUT_LABLES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value="M2X5"/>
<attribute name="ODOO" value=""/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWR_JACK" prefix="J">
<gates>
<gate name="G$1" symbol="PWR_JACK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="POWER_JACK">
<connects>
<connect gate="G$1" pin="PIN" pad="PIN"/>
<connect gate="G$1" pin="RING" pad="RING"/>
<connect gate="G$1" pin="SW" pad="SW"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value=""/>
<attribute name="ODOO" value="PC-GK2.1"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_POTS" urn="urn:adsk.eagle:library:32898787">
<packages>
<package name="POT_ALPHA" urn="urn:adsk.eagle:footprint:32898807/1" library_version="1">
<wire x1="3.73" y1="0" x2="5.08" y2="0" width="0.127" layer="21"/>
<wire x1="5.08" y1="0" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="8.89" x2="5.08" y2="12.5" width="0.127" layer="21"/>
<wire x1="5.08" y1="12.5" x2="5" y2="12.5" width="0.127" layer="21"/>
<wire x1="5" y1="12.5" x2="-5" y2="12.5" width="0.127" layer="21"/>
<wire x1="-5" y1="12.5" x2="-5.08" y2="12.5" width="0.127" layer="21"/>
<wire x1="-5.08" y1="12.5" x2="-5.08" y2="8.89" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.127" layer="21"/>
<wire x1="-5.08" y1="0" x2="-3.73" y2="0" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="9.2" x2="2.2225" y2="9.2" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="2" x="0" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="3" x="2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="P$1" x="-5.5" y="7" drill="0.6" diameter="1.6" shape="long" rot="R90"/>
<pad name="P$2" x="5.5" y="7" drill="0.6" diameter="1.6" shape="long" rot="R90"/>
<text x="0.072" y="11.7065" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0.072" y="2.523" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<polygon width="0.01" layer="46">
<vertex x="-5" y="6.2"/>
<vertex x="-5" y="7.7" curve="90"/>
<vertex x="-5.5" y="8.2" curve="90"/>
<vertex x="-6" y="7.7"/>
<vertex x="-6" y="6.2" curve="90"/>
<vertex x="-5.5" y="5.7" curve="90"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="6" y="6.2"/>
<vertex x="6" y="7.7" curve="90"/>
<vertex x="5.5" y="8.2" curve="90"/>
<vertex x="5" y="7.7"/>
<vertex x="5" y="6.2" curve="90"/>
<vertex x="5.5" y="5.7" curve="90"/>
</polygon>
<circle x="0" y="7" radius="3.5" width="0.1524" layer="21"/>
<circle x="0" y="7" radius="3.190834375" width="0.1524" layer="21"/>
<circle x="0" y="7" radius="3.4" width="0.127" layer="253"/>
<rectangle x1="-5.1308" y1="-0.0254" x2="5.1054" y2="12.5476" layer="39"/>
<rectangle x1="-3.81" y1="-0.9144" x2="3.81" y2="-0.0254" layer="39"/>
<rectangle x1="-5.3594" y1="9.017" x2="-5.1308" y2="10.2362" layer="39"/>
<rectangle x1="5.1054" y1="9.017" x2="5.334" y2="10.2362" layer="39"/>
<rectangle x1="5.1054" y1="3.7592" x2="5.334" y2="4.9784" layer="39"/>
<rectangle x1="-5.3594" y1="3.7592" x2="-5.1308" y2="4.9784" layer="39"/>
</package>
<package name="POT_APHA_NO_SILK" urn="urn:adsk.eagle:footprint:32898801/1" library_version="1">
<wire x1="3.73" y1="0" x2="5.08" y2="0" width="0.127" layer="51"/>
<wire x1="5.08" y1="0" x2="5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="5.08" y1="8.89" x2="5.08" y2="12.5" width="0.127" layer="51"/>
<wire x1="5.08" y1="12.5" x2="5" y2="12.5" width="0.127" layer="51"/>
<wire x1="5" y1="12.5" x2="-5" y2="12.5" width="0.127" layer="51"/>
<wire x1="-5" y1="12.5" x2="-5.08" y2="12.5" width="0.127" layer="51"/>
<wire x1="-5.08" y1="12.5" x2="-5.08" y2="8.89" width="0.127" layer="51"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.127" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.73" y2="0" width="0.127" layer="51"/>
<wire x1="-2.2225" y1="9.2" x2="2.2225" y2="9.2" width="0.1524" layer="51"/>
<pad name="1" x="-2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="2" x="0" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="3" x="2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="P$1" x="-5.5" y="7" drill="0.6" diameter="1.6" shape="long" rot="R90"/>
<pad name="P$2" x="5.5" y="7" drill="0.6" diameter="1.6" shape="long" rot="R90"/>
<text x="0.072" y="11.7065" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0.072" y="2.523" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<polygon width="0.01" layer="46">
<vertex x="-5" y="6.2"/>
<vertex x="-5" y="7.7" curve="90"/>
<vertex x="-5.5" y="8.2" curve="90"/>
<vertex x="-6" y="7.7"/>
<vertex x="-6" y="6.2" curve="90"/>
<vertex x="-5.5" y="5.7" curve="90"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="6" y="6.2"/>
<vertex x="6" y="7.7" curve="90"/>
<vertex x="5.5" y="8.2" curve="90"/>
<vertex x="5" y="7.7"/>
<vertex x="5" y="6.2" curve="90"/>
<vertex x="5.5" y="5.7" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-5.333" y="9.061"/>
<vertex x="-5.087" y="9.061"/>
<vertex x="-5.087" y="10.188"/>
<vertex x="-5.333" y="10.188"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="5.067" y="9.061"/>
<vertex x="5.313" y="9.061"/>
<vertex x="5.313" y="10.188"/>
<vertex x="5.067" y="10.188"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-5.333" y="3.811"/>
<vertex x="-5.087" y="3.811"/>
<vertex x="-5.087" y="4.938"/>
<vertex x="-5.333" y="4.938"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="5.067" y="3.811"/>
<vertex x="5.313" y="3.811"/>
<vertex x="5.313" y="4.938"/>
<vertex x="5.067" y="4.938"/>
</polygon>
<polygon width="0.127" layer="39">
<vertex x="-5.08" y="12.4968"/>
<vertex x="-5.08" y="0"/>
<vertex x="-3.81" y="0"/>
<vertex x="-3.81" y="-1.27"/>
<vertex x="3.81" y="-1.27"/>
<vertex x="3.81" y="0"/>
<vertex x="5.08" y="0"/>
<vertex x="5.08" y="12.4968"/>
</polygon>
<circle x="0" y="7" radius="3.5" width="0.1524" layer="51"/>
<circle x="0" y="7" radius="3.190834375" width="0.1524" layer="51"/>
<circle x="0" y="7" radius="3.4" width="0.127" layer="253"/>
</package>
<package name="POT_ALPHA_METAL" urn="urn:adsk.eagle:footprint:32898799/1" library_version="1">
<wire x1="3.73" y1="0.5" x2="5.08" y2="0.5" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.5" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="8.89" x2="5.08" y2="12" width="0.127" layer="21"/>
<wire x1="5.08" y1="12" x2="-5.08" y2="12" width="0.127" layer="21"/>
<wire x1="-5.08" y1="12" x2="-5.08" y2="8.89" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0.5" width="0.127" layer="21"/>
<wire x1="-5.08" y1="0.5" x2="-3.73" y2="0.5" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="9.2" x2="2.2225" y2="9.2" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="-0.5" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="2" x="0" y="-0.5" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="3" x="2.5" y="-0.5" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="P$1" x="-5.5" y="7" drill="0.6" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$2" x="5.5" y="7" drill="0.6" diameter="1.27" shape="long" rot="R90"/>
<text x="0.072" y="11.2065" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0.072" y="2.023" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<circle x="0" y="7" radius="3.5" width="0.1524" layer="21"/>
<circle x="0" y="7" radius="3.190834375" width="0.1524" layer="21"/>
<circle x="0" y="7" radius="3.5" width="0.127" layer="253"/>
<polygon width="0.127" layer="39">
<vertex x="-5.08" y="11.9968"/>
<vertex x="-5.08" y="0.5"/>
<vertex x="-3.81" y="0.5"/>
<vertex x="-3.81" y="-1.77"/>
<vertex x="3.81" y="-1.77"/>
<vertex x="3.81" y="0.5"/>
<vertex x="5.08" y="0.5"/>
<vertex x="5.08" y="11.9968"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="-5" y="6.7"/>
<vertex x="-5" y="7.2" curve="90"/>
<vertex x="-5.5" y="7.7" curve="90"/>
<vertex x="-6" y="7.2"/>
<vertex x="-6" y="6.7" curve="90"/>
<vertex x="-5.5" y="6.2" curve="90"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="6" y="6.7"/>
<vertex x="6" y="7.2" curve="90"/>
<vertex x="5.5" y="7.7" curve="90"/>
<vertex x="5" y="7.2"/>
<vertex x="5" y="6.7" curve="90"/>
<vertex x="5.5" y="6.2" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="12"/>
<vertex x="1" y="12"/>
<vertex x="1" y="11.5"/>
<vertex x="-1" y="11.5"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="2.5"/>
<vertex x="1" y="2.5"/>
<vertex x="1" y="2"/>
<vertex x="-1" y="2"/>
</polygon>
</package>
<package name="POT_ALPHA_METAL_NO_SILK" urn="urn:adsk.eagle:footprint:32898798/1" library_version="1">
<pad name="1" x="-2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="2" x="0" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="3" x="2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="P$1" x="-5.5" y="7" drill="0.6" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$2" x="5.5" y="7" drill="0.6" diameter="1.27" shape="long" rot="R90"/>
<text x="0.072" y="11.2065" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0.072" y="2.523" size="0.8" layer="21" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<circle x="0" y="7" radius="3.5" width="0.127" layer="253"/>
<polygon width="0.127" layer="39">
<vertex x="-5.08" y="11.9968"/>
<vertex x="-5.08" y="0"/>
<vertex x="-3.81" y="0"/>
<vertex x="-3.81" y="-1.27"/>
<vertex x="3.81" y="-1.27"/>
<vertex x="3.81" y="0"/>
<vertex x="5.08" y="0"/>
<vertex x="5.08" y="11.9968"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="-5" y="6.7"/>
<vertex x="-5" y="7.2" curve="90"/>
<vertex x="-5.5" y="7.7" curve="90"/>
<vertex x="-6" y="7.2"/>
<vertex x="-6" y="6.7" curve="90"/>
<vertex x="-5.5" y="6.2" curve="90"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="6" y="6.7"/>
<vertex x="6" y="7.2" curve="90"/>
<vertex x="5.5" y="7.7" curve="90"/>
<vertex x="5" y="7.2"/>
<vertex x="5" y="6.7" curve="90"/>
<vertex x="5.5" y="6.2" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="12"/>
<vertex x="1" y="12"/>
<vertex x="1" y="11.5"/>
<vertex x="-1" y="11.5"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="2.5"/>
<vertex x="1" y="2.5"/>
<vertex x="1" y="2"/>
<vertex x="-1" y="2"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="POT_ALPHA" urn="urn:adsk.eagle:package:32898840/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="POT_ALPHA"/>
</packageinstances>
</package3d>
<package3d name="POT_APHA_NO_SILK" urn="urn:adsk.eagle:package:32898834/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="POT_APHA_NO_SILK"/>
</packageinstances>
</package3d>
<package3d name="POT_ALPHA_METAL" urn="urn:adsk.eagle:package:32898832/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="POT_ALPHA_METAL"/>
</packageinstances>
</package3d>
<package3d name="POT_ALPHA_METAL_NO_SILK" urn="urn:adsk.eagle:package:32898831/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="POT_ALPHA_METAL_NO_SILK"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="POT_ALPHA" urn="urn:adsk.eagle:symbol:32898816/1" library_version="1">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="-0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-3.048" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-2.032" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="4.826" y1="0" x2="2.54" y2="0" width="0.127" layer="94"/>
<wire x1="2.54" y1="0" x2="1.778" y2="0" width="0.127" layer="94"/>
<wire x1="1.778" y1="0" x2="1.778" y2="1.524" width="0.127" layer="94"/>
<wire x1="1.778" y1="1.524" x2="1.016" y2="1.524" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.524" x2="1.27" y2="1.016" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.524" x2="1.27" y2="2.032" width="0.127" layer="94"/>
<wire x1="2.54" y1="-1.778" x2="2.54" y2="-1.524" width="0.127" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="-0.508" width="0.127" layer="94"/>
<wire x1="2.54" y1="0.254" x2="2.54" y2="0.508" width="0.127" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="1.524" width="0.127" layer="94"/>
<wire x1="2.54" y1="2.286" x2="2.54" y2="2.54" width="0.127" layer="94"/>
<pin name="1" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="P$1" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<text x="-5.969" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="POT_ALPHA" urn="urn:adsk.eagle:component:32898855/1" prefix="P" library_version="1">
<description>"A/B""value"_"height"_"D(centrer detent)"_"W(white mark)"</description>
<gates>
<gate name="G$1" symbol="POT_ALPHA" x="0" y="0"/>
</gates>
<devices>
<device name="POT_ALPHA" package="POT_ALPHA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="P$1" pad="P$1 P$2" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32898840/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="POT_APHA_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="P$1" pad="P$1 P$2" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32898834/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
</technology>
</technologies>
</device>
<device name="METAL" package="POT_ALPHA_METAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="P$1" pad="P$1 P$2" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32898832/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
</technology>
</technologies>
</device>
<device name="METAL_NO_SILK" package="POT_ALPHA_METAL_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="P$1" pad="P$1 P$2" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32898831/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_ICS">
<packages>
<package name="PS9117-F3-A">
<description>Original name &lt;b&gt;PC_PS9117-F3-A_#_TBR_LB&lt;/b&gt;&lt;p&gt;</description>
<circle x="-0.95" y="-1.5" radius="0.15" width="0.2" layer="21"/>
<circle x="-0.95" y="-1.5" radius="0.15" width="0.2" layer="21"/>
<wire x1="2.032" y1="-2.50025" x2="2.032" y2="2.49975" width="0.01" layer="51"/>
<wire x1="-2.032" y1="-2.50025" x2="-2.032" y2="2.49975" width="0.01" layer="51"/>
<wire x1="-1.258853125" y1="-0.762221875" x2="-1.258853125" y2="0.761721875" width="0.2" layer="21" curve="159.200165"/>
<wire x1="-2.032" y1="2.49975" x2="2.032" y2="2.49975" width="0.01" layer="51"/>
<wire x1="-2.032" y1="-2.50025" x2="2.032" y2="-2.50025" width="0.01" layer="51"/>
<wire x1="-1.258853125" y1="-0.762221875" x2="-1.258853125" y2="0.761721875" width="0.2" layer="21" curve="159.200165"/>
<wire x1="1.4" y1="-1.90525" x2="1.4" y2="1.90475" width="0.2" layer="21"/>
<wire x1="-1.386" y1="1.90475" x2="1.4" y2="1.90475" width="0.2" layer="21"/>
<wire x1="-1.386" y1="0.76175" x2="-1.386" y2="1.90475" width="0.2" layer="21"/>
<wire x1="-1.386" y1="-0.76225" x2="-1.259" y2="-0.76225" width="0.2" layer="21"/>
<wire x1="-1.386" y1="-1.90525" x2="-1.386" y2="-0.76225" width="0.2" layer="21"/>
<wire x1="-1.386" y1="0.76175" x2="-1.259" y2="0.76175" width="0.2" layer="21"/>
<wire x1="-1.386" y1="-1.90525" x2="1.4" y2="-1.90525" width="0.2" layer="21"/>
<smd name="1" x="-1.39" y="-3.357" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="2" x="1.39" y="-3.357" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.39" y="3.358" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="4" x="0" y="3.358" dx="1.8" dy="0.8" layer="1" rot="R90"/>
<smd name="5" x="-1.39" y="3.358" dx="1.8" dy="1" layer="1" rot="R90"/>
<text x="-2.037" y="4.612" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-2.037" y="4.358" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
<text x="-2.037" y="4.612" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-2.037" y="4.358" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="LB_K_PC">
<circle x="3.175" y="1.27" radius="0.635" width="0.1524" layer="94"/>
<wire x1="-5.715" y1="2.54" x2="-6.985" y2="0" width="0.254" layer="94"/>
<wire x1="-6.985" y1="0" x2="-8.255" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.715" y1="0" x2="-6.985" y2="0" width="0.254" layer="94"/>
<wire x1="-6.985" y1="0" x2="-8.255" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="1.905" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-4.191" y2="2.159" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-4.191" y2="1.651" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0.635" x2="-4.191" y2="0.889" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0.635" x2="-4.191" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-5.715" y1="2.54" x2="-6.985" y2="2.54" width="0.254" layer="94"/>
<wire x1="-6.985" y1="2.54" x2="-6.985" y2="0" width="0.254" layer="94"/>
<wire x1="-6.985" y1="2.54" x2="-8.255" y2="2.54" width="0.254" layer="94"/>
<wire x1="-6.985" y1="2.54" x2="-6.985" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="-6.985" y2="5.08" width="0.1524" layer="94"/>
<wire x1="7.747" y1="1.27" x2="10.16" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-6.985" y1="-2.54" x2="-6.985" y2="0" width="0.1524" layer="94"/>
<wire x1="-9.525" y1="7.62" x2="-9.525" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="9.525" y1="7.62" x2="9.525" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="9.525" y1="7.62" x2="-9.525" y2="7.62" width="0.4064" layer="94"/>
<wire x1="9.525" y1="-5.08" x2="-9.525" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="6.477" y1="5.08" x2="10.16" y2="5.08" width="0.1524" layer="94"/>
<wire x1="3.81" y1="1.27" x2="7.62" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="5.08" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-0.635" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="4.445" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="10.16" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="2.54" y2="1.27" width="0.4064" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<pin name="A" x="-12.7" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="C" x="-12.7" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="GND" x="12.7" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="VCC" x="12.7" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="VO" x="12.7" y="1.27" visible="pad" length="short" direction="pas" rot="R180"/>
<text x="-9.525" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-9.525" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="6.477" y="5.334" size="0.8128" layer="93">Vcc</text>
<text x="6.477" y="-2.286" size="0.8128" layer="93">GND</text>
<text x="-7.62" y="5.334" size="0.8128" layer="93">A</text>
<text x="-7.62" y="-3.556" size="0.8128" layer="93">C</text>
<text x="6.477" y="1.524" size="0.8128" layer="93">Vo</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PS9117A" prefix="IC">
<gates>
<gate name="G$1" symbol="LB_K_PC" x="10.16" y="5.08"/>
</gates>
<devices>
<device name="" package="PS9117-F3-A">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="VO" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="CEL"/>
<attribute name="MPN" value="PS9117A-AX"/>
<attribute name="PRICE" value="38.48" constant="no"/>
<attribute name="SIZEX" value="3.7"/>
<attribute name="SIZEY" value="7"/>
<attribute name="SIZEZ" value="2.1"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="VENDOR" value="MOUSER" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_RESISTORS">
<packages>
<package name="RESISTOR0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.770625" y="0" dx="0.9" dy="1" layer="1"/>
<smd name="2" x="0.770625" y="0" dx="0.9" dy="1" layer="1"/>
<text x="-0.0127" y="1.184275" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0.0127" y="-1.1985625" size="0.8" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.6858" x2="1.397" y2="0.6858" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR0603" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESISTOR0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="PRICE" value="0.1" constant="no"/>
<attribute name="SIZEX" value="0.85"/>
<attribute name="SIZEY" value="1.55"/>
<attribute name="SIZEZ" value="0.5"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="arduino-micro">
<packages>
<package name="ARDUINO-MICRO">
<pad name="D12" x="1.27" y="1.27" drill="0.9"/>
<pad name="D11" x="1.27" y="3.81" drill="0.9"/>
<pad name="D10" x="1.27" y="6.35" drill="0.9"/>
<pad name="D9" x="1.27" y="8.89" drill="0.9"/>
<pad name="D8" x="1.27" y="11.43" drill="0.9"/>
<pad name="D7" x="1.27" y="13.97" drill="0.9"/>
<pad name="D6" x="1.27" y="16.51" drill="0.9"/>
<pad name="D5" x="1.27" y="19.05" drill="0.9"/>
<pad name="D4" x="1.27" y="21.59" drill="0.9"/>
<pad name="D3" x="1.27" y="24.13" drill="0.9"/>
<pad name="D2" x="1.27" y="26.67" drill="0.9"/>
<pad name="GND" x="1.27" y="29.21" drill="0.9"/>
<pad name="RST" x="1.27" y="31.75" drill="0.9"/>
<pad name="RX" x="1.27" y="34.29" drill="0.9"/>
<pad name="TX" x="1.27" y="36.83" drill="0.9"/>
<pad name="SS" x="1.27" y="39.37" drill="0.9"/>
<pad name="MOSI" x="1.27" y="41.91" drill="0.9"/>
<pad name="D13" x="16.51" y="1.27" drill="0.9"/>
<pad name="3V3" x="16.51" y="3.81" drill="0.9"/>
<pad name="REF" x="16.51" y="6.35" drill="0.9"/>
<pad name="A0" x="16.51" y="8.89" drill="0.9"/>
<pad name="A1" x="16.51" y="11.43" drill="0.9"/>
<pad name="A2" x="16.51" y="13.97" drill="0.9"/>
<pad name="A3" x="16.51" y="16.51" drill="0.9"/>
<pad name="A4" x="16.51" y="19.05" drill="0.9"/>
<pad name="A5" x="16.51" y="21.59" drill="0.9"/>
<pad name="NC8" x="16.51" y="24.13" drill="0.9"/>
<pad name="NC7" x="16.51" y="26.67" drill="0.9"/>
<pad name="5V" x="16.51" y="29.21" drill="0.9"/>
<pad name="RST1" x="16.51" y="31.75" drill="0.9"/>
<pad name="GND1" x="16.51" y="34.29" drill="0.9"/>
<pad name="VIN" x="16.51" y="36.83" drill="0.9"/>
<pad name="MISO" x="16.51" y="39.37" drill="0.9"/>
<pad name="SCK" x="16.51" y="41.91" drill="0.9"/>
<wire x1="0" y1="-2.54" x2="17.78" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="17.78" y1="-2.54" x2="17.78" y2="45.72" width="0.4064" layer="21"/>
<wire x1="17.78" y1="45.72" x2="0" y2="45.72" width="0.4064" layer="21"/>
<wire x1="0" y1="45.72" x2="0" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="5.5245" y1="-2.032" x2="5.5245" y2="-3.302" width="0.4064" layer="51"/>
<wire x1="5.5245" y1="-3.302" x2="12.6365" y2="-3.302" width="0.4064" layer="51"/>
<wire x1="12.6365" y1="-3.302" x2="12.6365" y2="-2.032" width="0.4064" layer="51"/>
<wire x1="12.6365" y1="-2.032" x2="13.3985" y2="-2.032" width="0.4064" layer="51"/>
<wire x1="13.3985" y1="-2.032" x2="13.3985" y2="0" width="0.4064" layer="51"/>
<wire x1="13.3985" y1="0" x2="12.6365" y2="0" width="0.4064" layer="51"/>
<wire x1="12.6365" y1="0" x2="12.6365" y2="2.032" width="0.4064" layer="51"/>
<wire x1="12.6365" y1="2.032" x2="5.5245" y2="2.032" width="0.4064" layer="51"/>
<wire x1="5.5245" y1="2.032" x2="5.5245" y2="0" width="0.4064" layer="51"/>
<wire x1="5.5245" y1="0" x2="4.7625" y2="0" width="0.4064" layer="51"/>
<wire x1="4.7625" y1="0" x2="4.7625" y2="-2.032" width="0.4064" layer="51"/>
<wire x1="4.7625" y1="-2.032" x2="5.5245" y2="-2.032" width="0.4064" layer="51"/>
<text x="0.9525" y="46.4185" size="1.27" layer="25">&gt;NAME</text>
<text x="0.1905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ARDUINO-MICRO">
<text x="-12.065" y="25.273" size="1.27" layer="95">&gt;NAME</text>
<text x="-12.5095" y="-25.0825" size="1.27" layer="95">&gt;VALUE</text>
<pin name="IO12*" x="-17.78" y="-20.32" length="middle"/>
<pin name="IO11*" x="-17.78" y="-17.78" length="middle"/>
<pin name="IO10*" x="-17.78" y="-15.24" length="middle"/>
<pin name="IO9*" x="-17.78" y="-12.7" length="middle"/>
<pin name="IO8" x="-17.78" y="-10.16" length="middle"/>
<pin name="D7" x="-17.78" y="-7.62" length="middle"/>
<pin name="D6*" x="-17.78" y="-5.08" length="middle"/>
<pin name="D5*" x="-17.78" y="-2.54" length="middle"/>
<pin name="D4*" x="-17.78" y="0" length="middle"/>
<pin name="D3/SCL" x="-17.78" y="2.54" length="middle"/>
<pin name="D2/SDA" x="-17.78" y="5.08" length="middle"/>
<pin name="GND" x="-17.78" y="7.62" length="middle"/>
<pin name="RESET" x="-17.78" y="10.16" length="middle"/>
<pin name="D0/RX" x="-17.78" y="12.7" length="middle"/>
<pin name="D1/TX" x="-17.78" y="15.24" length="middle"/>
<pin name="RXLED/SS" x="-17.78" y="17.78" length="middle"/>
<pin name="MOSI" x="-17.78" y="20.32" length="middle"/>
<wire x1="12.7" y1="-22.86" x2="12.7" y2="22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="-12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-22.86" x2="-12.7" y2="22.86" width="0.254" layer="94"/>
<wire x1="-12.7" y1="22.86" x2="12.7" y2="22.86" width="0.254" layer="94"/>
<pin name="IO13*" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="3V" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="AREF" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="A0" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="A1" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="A2" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="A3" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="A4" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="A5" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="NC8" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="NC7" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="5V" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="RESET1" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="GND1" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="VIN" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="MISO" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="SCK" x="17.78" y="20.32" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARDUINO_MICRO">
<gates>
<gate name="G$1" symbol="ARDUINO-MICRO" x="0" y="0"/>
</gates>
<devices>
<device name="FOOTPRINT" package="ARDUINO-MICRO">
<connects>
<connect gate="G$1" pin="3V" pad="3V3"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="A0" pad="A0"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="AREF" pad="REF"/>
<connect gate="G$1" pin="D0/RX" pad="RX"/>
<connect gate="G$1" pin="D1/TX" pad="TX"/>
<connect gate="G$1" pin="D2/SDA" pad="D2"/>
<connect gate="G$1" pin="D3/SCL" pad="D3"/>
<connect gate="G$1" pin="D4*" pad="D4"/>
<connect gate="G$1" pin="D5*" pad="D5"/>
<connect gate="G$1" pin="D6*" pad="D6"/>
<connect gate="G$1" pin="D7" pad="D7"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="IO10*" pad="D10"/>
<connect gate="G$1" pin="IO11*" pad="D11"/>
<connect gate="G$1" pin="IO12*" pad="D12"/>
<connect gate="G$1" pin="IO13*" pad="D13"/>
<connect gate="G$1" pin="IO8" pad="D8"/>
<connect gate="G$1" pin="IO9*" pad="D9"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="NC7" pad="NC7"/>
<connect gate="G$1" pin="NC8" pad="NC8"/>
<connect gate="G$1" pin="RESET" pad="RST"/>
<connect gate="G$1" pin="RESET1" pad="RST1"/>
<connect gate="G$1" pin="RXLED/SS" pad="SS"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_SYMBOLS" urn="urn:adsk.eagle:library:32975873">
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:32975880/1" library_version="1">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="+05V" urn="urn:adsk.eagle:symbol:32975885/1" library_version="1">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:32975892/1" prefix="SUPPLY" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:32975897/1" prefix="SUPPLY" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_JUMPERS" urn="urn:adsk.eagle:library:32977727">
<packages>
<package name="SOLDERJUMPER" urn="urn:adsk.eagle:footprint:32977729/1" library_version="5">
<wire x1="0.8" y1="-1" x2="-0.8" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="1" x2="1" y2="0.7" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1" y1="0.7" x2="-0.8" y2="1" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1" y1="-0.7" x2="-0.8" y2="-1" width="0.2032" layer="21" curve="90"/>
<wire x1="0.8" y1="-1" x2="1" y2="-0.7" width="0.2032" layer="21" curve="90"/>
<wire x1="-0.8" y1="1" x2="0.8" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.45" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0.45" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-0.019" y="1.651" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="-0.019" y="-1.651" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SOLDERJUMPER" urn="urn:adsk.eagle:package:32977733/2" type="empty" library_version="5">
<packageinstances>
<packageinstance name="SOLDERJUMPER"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SOLDERJUMPER" urn="urn:adsk.eagle:symbol:32977731/1" library_version="5">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SOLDERJUMPER" urn="urn:adsk.eagle:component:32977735/2" prefix="SJ" library_version="5">
<gates>
<gate name="G$1" symbol="SOLDERJUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOLDERJUMPER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977733/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TYPE" value="SOLDER"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Teensy_3_and_LC_Series_Boards_v1.4">
<packages>
<package name="TEENSY_3.0-3.2&amp;LC_DIL">
<pad name="GND" x="-7.62" y="16.51" drill="0.9652"/>
<pad name="0" x="-7.62" y="13.97" drill="0.9652"/>
<pad name="1" x="-7.62" y="11.43" drill="0.9652"/>
<pad name="2" x="-7.62" y="8.89" drill="0.9652"/>
<pad name="3" x="-7.62" y="6.35" drill="0.9652"/>
<pad name="4" x="-7.62" y="3.81" drill="0.9652"/>
<pad name="5" x="-7.62" y="1.27" drill="0.9652"/>
<pad name="6" x="-7.62" y="-1.27" drill="0.9652"/>
<pad name="7" x="-7.62" y="-3.81" drill="0.9652"/>
<pad name="8" x="-7.62" y="-6.35" drill="0.9652"/>
<pad name="9" x="-7.62" y="-8.89" drill="0.9652"/>
<pad name="10" x="-7.62" y="-11.43" drill="0.9652"/>
<pad name="11" x="-7.62" y="-13.97" drill="0.9652"/>
<pad name="12" x="-7.62" y="-16.51" drill="0.9652"/>
<pad name="13" x="7.62" y="-16.51" drill="0.9652"/>
<pad name="14/A0" x="7.62" y="-13.97" drill="0.9652"/>
<pad name="15/A1" x="7.62" y="-11.43" drill="0.9652"/>
<pad name="16/A2" x="7.62" y="-8.89" drill="0.9652"/>
<pad name="17/A3" x="7.62" y="-6.35" drill="0.9652"/>
<pad name="18/A4" x="7.62" y="-3.81" drill="0.9652"/>
<pad name="19/A5" x="7.62" y="-1.27" drill="0.9652"/>
<pad name="20/A6" x="7.62" y="1.27" drill="0.9652"/>
<pad name="21/A7" x="7.62" y="3.81" drill="0.9652"/>
<pad name="22/A8" x="7.62" y="6.35" drill="0.9652"/>
<pad name="23/A9" x="7.62" y="8.89" drill="0.9652"/>
<pad name="3.3V" x="7.62" y="11.43" drill="0.9652"/>
<pad name="AGND" x="7.62" y="13.97" drill="0.9652"/>
<pad name="VIN" x="7.62" y="16.51" drill="0.9652"/>
<wire x1="-8.89" y1="17.78" x2="8.89" y2="17.78" width="0.127" layer="51"/>
<wire x1="8.89" y1="17.78" x2="8.89" y2="-17.78" width="0.127" layer="51"/>
<wire x1="8.89" y1="-17.78" x2="-8.89" y2="-17.78" width="0.127" layer="51"/>
<wire x1="-8.89" y1="-17.78" x2="-8.89" y2="17.78" width="0.127" layer="51"/>
<wire x1="-1.27" y1="16.51" x2="1.27" y2="16.51" width="0.2032" layer="21"/>
<wire x1="1.27" y1="16.51" x2="1.27" y2="17.78" width="0.2032" layer="21"/>
<wire x1="1.27" y1="17.78" x2="8.89" y2="17.78" width="0.2032" layer="21"/>
<wire x1="8.89" y1="17.78" x2="8.89" y2="-17.78" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-17.78" x2="-8.89" y2="-17.78" width="0.2032" layer="21"/>
<wire x1="-8.89" y1="-17.78" x2="-8.89" y2="17.78" width="0.2032" layer="21"/>
<wire x1="-8.89" y1="17.78" x2="-1.27" y2="17.78" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="17.78" x2="-1.27" y2="16.51" width="0.2032" layer="21"/>
<text x="-3.81" y="13.97" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.81" y="-13.97" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TEENSY_3.1-3.2_DIL">
<wire x1="-17.78" y1="-38.1" x2="17.78" y2="-38.1" width="0.254" layer="94"/>
<wire x1="17.78" y1="-38.1" x2="17.78" y2="30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="30.48" x2="-17.78" y2="30.48" width="0.254" layer="94"/>
<wire x1="-17.78" y1="30.48" x2="-17.78" y2="-38.1" width="0.254" layer="94"/>
<pin name="12/MISO" x="-22.86" y="-5.08" visible="pin" length="middle"/>
<pin name="11/MOSI" x="-22.86" y="-2.54" visible="pin" length="middle"/>
<pin name="10/TX2/PWM" x="-22.86" y="0" visible="pin" length="middle"/>
<pin name="9/RX2/PWM" x="-22.86" y="2.54" visible="pin" length="middle"/>
<pin name="8/TX3" x="-22.86" y="5.08" visible="pin" length="middle"/>
<pin name="7/RX3" x="-22.86" y="7.62" visible="pin" length="middle"/>
<pin name="6/PWM" x="-22.86" y="10.16" visible="pin" length="middle"/>
<pin name="5/PWM" x="-22.86" y="12.7" visible="pin" length="middle"/>
<pin name="4/CAN-RX/PWM" x="-22.86" y="15.24" visible="pin" length="middle"/>
<pin name="3/CAN-TX/PWM" x="-22.86" y="17.78" visible="pin" length="middle"/>
<pin name="2" x="-22.86" y="20.32" visible="pin" length="middle"/>
<pin name="1/TX1/T" x="-22.86" y="22.86" visible="pin" length="middle"/>
<pin name="0/RX1/T" x="-22.86" y="25.4" visible="pin" length="middle"/>
<pin name="GND" x="22.86" y="17.78" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="AGND" x="22.86" y="5.08" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="3.3V" x="22.86" y="22.86" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="23/A9/T/PWM" x="-22.86" y="-33.02" visible="pin" length="middle"/>
<pin name="22/A8/T/PWM" x="-22.86" y="-30.48" visible="pin" length="middle"/>
<pin name="21/A7/PWM" x="-22.86" y="-27.94" visible="pin" length="middle"/>
<pin name="20/A6/PWM" x="-22.86" y="-25.4" visible="pin" length="middle"/>
<pin name="19/A5/T/SCL0" x="-22.86" y="-22.86" visible="pin" length="middle"/>
<pin name="18/A4/T/SDA0" x="-22.86" y="-20.32" visible="pin" length="middle"/>
<pin name="17/A3/T" x="-22.86" y="-17.78" visible="pin" length="middle"/>
<pin name="16/A2/T" x="-22.86" y="-15.24" visible="pin" length="middle"/>
<pin name="15/A1/T" x="-22.86" y="-12.7" visible="pin" length="middle"/>
<pin name="14/A1" x="-22.86" y="-10.16" visible="pin" length="middle"/>
<pin name="13/SCK/LED" x="-22.86" y="-7.62" visible="pin" length="middle"/>
<text x="-5.588" y="31.75" size="1.27" layer="95" font="vector" ratio="15">&gt;NAME</text>
<pin name="VIN" x="22.86" y="25.4" visible="pin" length="middle" direction="pwr" rot="R180"/>
<text x="-7.112" y="-40.894" size="1.27" layer="96" font="vector" ratio="15">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEENSY_3.1-3.2_DIL">
<description>Teensy 3.1 or 3.2 in a DIL Layout.</description>
<gates>
<gate name="G$1" symbol="TEENSY_3.1-3.2_DIL" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="TEENSY_3.0-3.2&amp;LC_DIL">
<connects>
<connect gate="G$1" pin="0/RX1/T" pad="0"/>
<connect gate="G$1" pin="1/TX1/T" pad="1"/>
<connect gate="G$1" pin="10/TX2/PWM" pad="10"/>
<connect gate="G$1" pin="11/MOSI" pad="11"/>
<connect gate="G$1" pin="12/MISO" pad="12"/>
<connect gate="G$1" pin="13/SCK/LED" pad="13"/>
<connect gate="G$1" pin="14/A1" pad="14/A0"/>
<connect gate="G$1" pin="15/A1/T" pad="15/A1"/>
<connect gate="G$1" pin="16/A2/T" pad="16/A2"/>
<connect gate="G$1" pin="17/A3/T" pad="17/A3"/>
<connect gate="G$1" pin="18/A4/T/SDA0" pad="18/A4"/>
<connect gate="G$1" pin="19/A5/T/SCL0" pad="19/A5"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20/A6/PWM" pad="20/A6"/>
<connect gate="G$1" pin="21/A7/PWM" pad="21/A7"/>
<connect gate="G$1" pin="22/A8/T/PWM" pad="22/A8"/>
<connect gate="G$1" pin="23/A9/T/PWM" pad="23/A9"/>
<connect gate="G$1" pin="3.3V" pad="3.3V"/>
<connect gate="G$1" pin="3/CAN-TX/PWM" pad="3"/>
<connect gate="G$1" pin="4/CAN-RX/PWM" pad="4"/>
<connect gate="G$1" pin="5/PWM" pad="5"/>
<connect gate="G$1" pin="6/PWM" pad="6"/>
<connect gate="G$1" pin="7/RX3" pad="7"/>
<connect gate="G$1" pin="8/TX3" pad="8"/>
<connect gate="G$1" pin="9/RX2/PWM" pad="9"/>
<connect gate="G$1" pin="AGND" pad="AGND"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_CONNECTORS" urn="urn:adsk.eagle:library:32904192">
<packages>
<package name="WQP-PJ301M-12_JACK" urn="urn:adsk.eagle:footprint:32904247/1" library_version="15">
<wire x1="-4.5" y1="6" x2="-1.5" y2="6" width="0.127" layer="51"/>
<wire x1="-1.5" y1="6" x2="1.5" y2="6" width="0.127" layer="51"/>
<wire x1="1.5" y1="6" x2="4.5" y2="6" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6" x2="-4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-4.5" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<wire x1="0.2" y1="-4.5" x2="4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="4.5" y1="-4.5" x2="4.5" y2="6" width="0.127" layer="51"/>
<wire x1="-1.5" y1="6" x2="-1.5" y2="4" width="0.127" layer="51"/>
<wire x1="-1.5" y1="4" x2="1.5" y2="4" width="0.127" layer="51"/>
<wire x1="1.5" y1="4" x2="1.5" y2="6" width="0.127" layer="51"/>
<wire x1="-0.2" y1="-4.6" x2="-0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="-0.2" y1="-6.7" x2="0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="0.2" y1="-6.7" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="5.5" x2="-4.5" y2="6" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6" x2="-4" y2="6" width="0.127" layer="21"/>
<wire x1="4" y1="6" x2="4.5" y2="6" width="0.127" layer="21"/>
<wire x1="4.5" y1="6" x2="4.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4" x2="4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4.5" x2="4" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4" x2="-4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4.5" x2="-4" y2="-4.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.162275" width="0.127" layer="253"/>
<circle x="0" y="0" radius="2.690721875" width="0.127" layer="21"/>
<pad name="P$1_TIP" x="0" y="5" drill="1.1" thermals="no"/>
<pad name="P$2_SWITCH" x="0" y="-3.5" drill="1.1"/>
<pad name="P$3_SLEEVE" x="0" y="-6.5" drill="1.1"/>
<rectangle x1="-2.8" y1="-2.8" x2="2.8" y2="2.8" layer="39"/>
<text x="4.5212" y="-4.7244" size="1.016" layer="21" rot="SR180">&gt;NAME</text>
</package>
<package name="THONKICONN_TIGHT" urn="urn:adsk.eagle:footprint:32904248/1" library_version="15">
<wire x1="-4.5" y1="6" x2="-1.5" y2="6" width="0.127" layer="51"/>
<wire x1="-1.5" y1="6" x2="1.5" y2="6" width="0.127" layer="51"/>
<wire x1="1.5" y1="6" x2="4.5" y2="6" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6" x2="-4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-4.5" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<wire x1="0.2" y1="-4.5" x2="4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="4.5" y1="-4.5" x2="4.5" y2="6" width="0.127" layer="51"/>
<wire x1="-1.5" y1="6" x2="-1.5" y2="4" width="0.127" layer="51"/>
<wire x1="-1.5" y1="4" x2="1.5" y2="4" width="0.127" layer="51"/>
<wire x1="1.5" y1="4" x2="1.5" y2="6" width="0.127" layer="51"/>
<wire x1="-0.2" y1="-4.6" x2="-0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="-0.2" y1="-6.7" x2="0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="0.2" y1="-6.7" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="5.5" x2="-4.5" y2="6" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6" x2="-4" y2="6" width="0.127" layer="21"/>
<wire x1="4" y1="6" x2="4.5" y2="6" width="0.127" layer="21"/>
<wire x1="4.5" y1="6" x2="4.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4" x2="4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4.5" x2="4" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4" x2="-4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4.5" x2="-4" y2="-4.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.162275" width="0.127" layer="253"/>
<circle x="0" y="0" radius="2.690721875" width="0.127" layer="21"/>
<pad name="P$1_TIP" x="0" y="5" drill="1.1" thermals="no"/>
<pad name="P$2_SWITCH" x="0" y="-3.5" drill="1.1"/>
<pad name="P$3_SLEEVE" x="0" y="-6.1063" drill="1.1"/>
<rectangle x1="-2.8" y1="-2.8" x2="2.8" y2="2.8" layer="39"/>
<text x="-2.3622" y="6.2738" size="1.016" layer="21" rot="SR0">&gt;NAME</text>
</package>
<package name="WQP-PJ366ST_JACK" urn="urn:adsk.eagle:footprint:32904249/1" library_version="15">
<wire x1="-5" y1="6" x2="-1.5" y2="6" width="0.0508" layer="51"/>
<wire x1="-1.5" y1="6" x2="1.5" y2="6" width="0.0508" layer="51"/>
<wire x1="1.5" y1="6" x2="5" y2="6" width="0.0508" layer="51"/>
<wire x1="-5" y1="6" x2="-5" y2="-4.5" width="0.0508" layer="51"/>
<wire x1="-5" y1="-4.5" x2="0.2" y2="-4.5" width="0.0508" layer="51"/>
<wire x1="0.2" y1="-4.5" x2="5" y2="-4.5" width="0.0508" layer="51"/>
<wire x1="5" y1="-4.5" x2="5" y2="6" width="0.0508" layer="51"/>
<wire x1="-1.5" y1="6" x2="-1.5" y2="4" width="0.0508" layer="51"/>
<wire x1="-1.5" y1="4" x2="1.5" y2="4" width="0.0508" layer="51"/>
<wire x1="1.5" y1="4" x2="1.5" y2="6" width="0.0508" layer="51"/>
<wire x1="-0.2" y1="-4.6" x2="-0.2" y2="-6.7" width="0.0508" layer="51"/>
<wire x1="-0.2" y1="-6.7" x2="0.2" y2="-6.7" width="0.0508" layer="51"/>
<wire x1="0.2" y1="-6.7" x2="0.2" y2="-4.5" width="0.0508" layer="51"/>
<wire x1="-5" y1="-4.5" x2="-5" y2="-4" width="0.127" layer="21"/>
<wire x1="-5" y1="-4" x2="-5" y2="-2" width="0.127" layer="21"/>
<wire x1="-5" y1="-2" x2="-5" y2="0" width="0.127" layer="21"/>
<wire x1="-5" y1="0" x2="-5" y2="2" width="0.127" layer="21"/>
<wire x1="-5" y1="2" x2="-5" y2="4" width="0.127" layer="21"/>
<wire x1="-5" y1="4" x2="-5" y2="6" width="0.127" layer="21"/>
<wire x1="3" y1="6" x2="5" y2="6" width="0.127" layer="21"/>
<wire x1="5" y1="6" x2="5" y2="4" width="0.127" layer="21"/>
<wire x1="5" y1="4" x2="5" y2="2" width="0.127" layer="21"/>
<wire x1="5" y1="2" x2="5" y2="0" width="0.127" layer="21"/>
<wire x1="5" y1="0" x2="5" y2="-2" width="0.127" layer="21"/>
<wire x1="5" y1="-2" x2="5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="5" y1="-4.5" x2="-5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-5" y1="6" x2="-3" y2="6" width="0.127" layer="21"/>
<wire x1="-3" y1="6" x2="-1" y2="6" width="0.127" layer="21"/>
<wire x1="-1" y1="6" x2="3" y2="6" width="0.127" layer="21"/>
<wire x1="3" y1="6" x2="1" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="4" x2="5" y2="6" width="0.127" layer="21"/>
<wire x1="-5" y1="4" x2="-3" y2="6" width="0.127" layer="21"/>
<wire x1="3" y1="2" x2="5" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="0" x2="5" y2="2" width="0.127" layer="21"/>
<wire x1="3" y1="-2" x2="5" y2="0" width="0.127" layer="21"/>
<wire x1="3" y1="-4" x2="5" y2="-2" width="0.127" layer="21"/>
<wire x1="-1" y1="6" x2="-5" y2="2" width="0.127" layer="21"/>
<wire x1="-3" y1="2" x2="-5" y2="0" width="0.127" layer="21"/>
<wire x1="-3" y1="0" x2="-5" y2="-2" width="0.127" layer="21"/>
<wire x1="-3" y1="-2" x2="-5" y2="-4" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.162275" width="0.127" layer="253"/>
<circle x="0" y="0" radius="2.690721875" width="0.127" layer="21"/>
<pad name="P$1_TIP" x="0" y="5" drill="1.1" thermals="no"/>
<pad name="P$2_SWITCH" x="0" y="-3.5" drill="1.1"/>
<pad name="P$3_SLEEVE" x="0" y="-6.5" drill="1.1"/>
<rectangle x1="-2.8" y1="-2.8" x2="2.8" y2="2.8" layer="39"/>
<text x="4.5212" y="-4.7244" size="1.016" layer="21" rot="SR180">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="WQP-PJ301M-12_JACK" urn="urn:adsk.eagle:package:32904283/2" type="model" library_version="15">
<packageinstances>
<packageinstance name="WQP-PJ301M-12_JACK"/>
</packageinstances>
</package3d>
<package3d name="THONKICONN_TIGHT" urn="urn:adsk.eagle:package:32904284/2" type="model" library_version="15">
<packageinstances>
<packageinstance name="THONKICONN_TIGHT"/>
</packageinstances>
</package3d>
<package3d name="WQP-PJ366ST_JACK" urn="urn:adsk.eagle:package:32904285/2" type="model" library_version="15">
<packageinstances>
<packageinstance name="WQP-PJ366ST_JACK"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="THONKICONN" urn="urn:adsk.eagle:symbol:32904200/1" library_version="15">
<description>pj301-b vertical 3.5mm jack socket</description>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="1.524" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.524" y1="1.016" x2="2.286" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="2.286" x2="-1.016" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.524" x2="-0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="-0.762" y2="2.286" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="4.572" y2="-2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="4.064" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="2.286" y1="-0.508" x2="7.874" y2="0.508" layer="94" rot="R90"/>
<pin name="SLEEVE" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="TIP" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="SWITCH" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="STEREO-AUDIO-JACK" urn="urn:adsk.eagle:symbol:32904199/1" library_version="15">
<wire x1="-1.27" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="3.048" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.286" y="-2.286" size="1.016" layer="94" ratio="15">L</text>
<text x="2.286" y="0.254" size="1.016" layer="94" ratio="15">R</text>
<rectangle x1="-7.62" y1="-0.762" x2="-2.54" y2="0.762" layer="94" rot="R90"/>
<pin name="RIGHT" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="LEFT" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="SLEEVE" x="5.08" y="2.54" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="THONKICONN" urn="urn:adsk.eagle:component:32904292/5" prefix="J" library_version="15">
<description>3.5mm socket for Eurorack modular synths</description>
<gates>
<gate name="G$1" symbol="THONKICONN" x="0" y="0"/>
</gates>
<devices>
<device name="NEW" package="WQP-PJ301M-12_JACK">
<connects>
<connect gate="G$1" pin="SLEEVE" pad="P$3_SLEEVE"/>
<connect gate="G$1" pin="SWITCH" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="TIP" pad="P$1_TIP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32904283/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="THONKICONN_TIGHT">
<connects>
<connect gate="G$1" pin="SLEEVE" pad="P$3_SLEEVE"/>
<connect gate="G$1" pin="SWITCH" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="TIP" pad="P$1_TIP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32904284/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="THONKICONN-STEREO" urn="urn:adsk.eagle:component:32904291/4" prefix="J" library_version="15">
<description>3.5mm socket for Eurorack modular synths</description>
<gates>
<gate name="G$1" symbol="STEREO-AUDIO-JACK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WQP-PJ366ST_JACK">
<connects>
<connect gate="G$1" pin="LEFT" pad="P$1_TIP"/>
<connect gate="G$1" pin="RIGHT" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="SLEEVE" pad="P$3_SLEEVE"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32904285/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_ICS" urn="urn:adsk.eagle:library:32977748">
<packages>
<package name="SO8" urn="urn:adsk.eagle:footprint:33387862/1" library_version="8">
<rectangle x1="-2.79515" y1="1.35495" x2="-2.30495" y2="2.45505" layer="51" rot="R270"/>
<rectangle x1="-2.79515" y1="0.08495" x2="-2.30495" y2="1.18505" layer="51" rot="R270"/>
<rectangle x1="-2.79515" y1="-1.18505" x2="-2.30495" y2="-0.08495" layer="51" rot="R270"/>
<rectangle x1="-2.79515" y1="-2.45505" x2="-2.30495" y2="-1.35495" layer="51" rot="R270"/>
<rectangle x1="2.30495" y1="-2.45505" x2="2.79515" y2="-1.35495" layer="51" rot="R270"/>
<rectangle x1="2.30495" y1="-1.18505" x2="2.79515" y2="-0.08495" layer="51" rot="R270"/>
<rectangle x1="2.30495" y1="0.08495" x2="2.79515" y2="1.18505" layer="51" rot="R270"/>
<rectangle x1="2.30495" y1="1.35495" x2="2.79515" y2="2.45505" layer="51" rot="R270"/>
<rectangle x1="-3.683" y1="-2.667" x2="3.683" y2="2.667" layer="39"/>
<smd name="1" x="-2.6" y="1.905" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="2" x="-2.6" y="0.635" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="3" x="-2.6" y="-0.635" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="4" x="-2.6" y="-1.905" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="5" x="2.6" y="-1.905" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="6" x="2.6" y="-0.635" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="7" x="2.6" y="0.635" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="8" x="2.6" y="1.905" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<text x="0" y="3.14325" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="16" rot="SR270" align="center">&gt;VALUE</text>
<wire x1="1.9558" y1="-2.159" x2="1.9558" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-1.9558" y1="-2.159" x2="-1.5748" y2="-2.54" width="0.1524" layer="51" curve="90"/>
<wire x1="1.5748" y1="2.54" x2="1.9558" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="1.9558" y1="-2.159" x2="1.5748" y2="-2.54" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.5748" y1="2.54" x2="-1.9558" y2="2.159" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.9558" y1="2.159" x2="-1.9558" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-1.5748" y1="-2.54" x2="-1.4478" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.4478" y1="-2.54" x2="1.5748" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.5748" y1="2.54" x2="0.508" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.54" x2="-0.508" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="2.54" x2="-1.4478" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.4478" y1="2.54" x2="-1.5748" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.54" x2="-0.508" y2="2.54" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.4478" y1="2.54" x2="-1.4478" y2="-2.54" width="0.127" layer="21"/>
</package>
<package name="MSOP8" urn="urn:adsk.eagle:footprint:32977812/1" library_version="8">
<description>&lt;b&gt;8M, 8-Lead, 0.118" Wide, Miniature Small Outline Package&lt;/b&gt;&lt;p&gt;
MSOP&lt;br&gt;
8M-Package doc1097.pdf</description>
<wire x1="-1.48" y1="1.23" x2="-1.23" y2="1.48" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="1.23" y1="1.48" x2="1.48" y2="1.23" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.23" y1="-1.49" x2="1.48" y2="-1.24" width="0.1524" layer="21" curve="90"/>
<wire x1="-1.48" y1="-1.24" x2="-1.23" y2="-1.49" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.24" y1="-1.49" x2="-1.22" y2="-1.49" width="0.1524" layer="21"/>
<wire x1="-1.22" y1="1.48" x2="1.24" y2="1.48" width="0.1524" layer="21"/>
<wire x1="-1.48" y1="1.23" x2="-1.48" y2="-1.23" width="0.1524" layer="21"/>
<wire x1="1.48" y1="-1.24" x2="1.48" y2="1.23" width="0.1524" layer="21"/>
<circle x="-0.65" y="-0.65" radius="0.325" width="0.254" layer="21"/>
<smd name="1" x="-0.975" y="-2.2" dx="0.4" dy="1.1" layer="1"/>
<smd name="2" x="-0.325" y="-2.2" dx="0.4" dy="1.1" layer="1"/>
<smd name="3" x="0.325" y="-2.2" dx="0.4" dy="1.1" layer="1"/>
<smd name="4" x="0.975" y="-2.2" dx="0.4" dy="1.1" layer="1"/>
<smd name="5" x="0.975" y="2.2" dx="0.4" dy="1.1" layer="1"/>
<smd name="6" x="0.325" y="2.2" dx="0.4" dy="1.1" layer="1"/>
<smd name="7" x="-0.325" y="2.2" dx="0.4" dy="1.1" layer="1"/>
<smd name="8" x="-0.975" y="2.2" dx="0.4" dy="1.1" layer="1"/>
<text x="-2.159" y="0" size="0.8" layer="25" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="2.032" y="0" size="0.8" layer="25" ratio="15" rot="R90" align="center">&gt;VALUE</text>
<rectangle x1="-1.175" y1="-2.45" x2="-0.775" y2="-1.55" layer="51"/>
<rectangle x1="-0.525" y1="-2.45" x2="-0.125" y2="-1.55" layer="51"/>
<rectangle x1="0.125" y1="-2.45" x2="0.525" y2="-1.55" layer="51"/>
<rectangle x1="0.775" y1="-2.45" x2="1.175" y2="-1.55" layer="51"/>
<rectangle x1="0.775" y1="1.55" x2="1.175" y2="2.45" layer="51"/>
<rectangle x1="0.125" y1="1.55" x2="0.525" y2="2.45" layer="51"/>
<rectangle x1="-0.525" y1="1.55" x2="-0.125" y2="2.45" layer="51"/>
<rectangle x1="-1.175" y1="1.55" x2="-0.775" y2="2.45" layer="51"/>
<rectangle x1="-1.778" y1="-3.048" x2="1.778" y2="3.048" layer="39"/>
</package>
<package name="SO16" urn="urn:adsk.eagle:footprint:32977804/1" library_version="8">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="51" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="51" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="51" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="51" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="-1.4478" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.4478" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="-1.4478" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.4478" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.4478" x2="5.08" y2="-1.4478" width="0.0508" layer="21"/>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
<rectangle x1="-5.334" y1="-3.81" x2="5.334" y2="3.81" layer="39"/>
<smd name="1" x="-4.445" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="2" x="-3.175" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="3" x="-1.905" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="4" x="-0.635" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="5" x="0.635" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="6" x="1.905" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="7" x="3.175" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="8" x="4.445" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="9" x="4.445" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="10" x="3.175" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="11" x="1.905" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="12" x="0.635" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="13" x="-0.635" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="14" x="-1.905" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="15" x="-3.175" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="16" x="-4.445" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<text x="-5.715" y="0" size="0.8" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="15" rot="SR0" align="center">&gt;VALUE</text>
</package>
<package name="TSSOP-16" urn="urn:adsk.eagle:footprint:32977813/1" library_version="8">
<description>8-Bit Shift Registers With 3-State Output Registers</description>
<text x="0.0076" y="3.4572" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="25" ratio="15" rot="R90" align="center">&gt;VALUE</text>
<smd name="1" x="-2.8" y="2.275" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="2" x="-2.8" y="1.625" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="3" x="-2.8" y="0.975" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="4" x="-2.8" y="0.325" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="5" x="-2.8" y="-0.325" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="6" x="-2.8" y="-0.975" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="7" x="-2.8" y="-1.625" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="8" x="-2.8" y="-2.275" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="9" x="2.8" y="-2.275" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="10" x="2.8" y="-1.625" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="11" x="2.8" y="-0.975" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="12" x="2.8" y="-0.325" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="13" x="2.8" y="0.325" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="14" x="2.8" y="0.975" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="15" x="2.8" y="1.625" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="16" x="2.8" y="2.275" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<circle x="-1.5" y="2.1208" radius="0.325" width="0" layer="21"/>
<wire x1="2.1" y1="-0.0792" x2="2.1" y2="2.7208" width="0.2032" layer="21"/>
<wire x1="2.1" y1="2.7208" x2="-2.1" y2="2.7208" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="2.7208" x2="-2.1" y2="-0.0792" width="0.2032" layer="21"/>
<wire x1="-2.413" y1="2.7178" x2="-3.175" y2="2.7178" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-2.6954" x2="2.1" y2="-2.6954" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-2.6954" x2="2.1" y2="0.1046" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="0.1046" x2="-2.1" y2="-2.6954" width="0.2032" layer="21"/>
<rectangle x1="-2.725" y1="1.7958" x2="-2.475" y2="2.7958" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="1.1458" x2="-2.475" y2="2.1458" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="0.4958" x2="-2.475" y2="1.4958" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="-0.1542" x2="-2.475" y2="0.8458" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-0.1542" x2="2.725" y2="0.8458" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="0.4958" x2="2.725" y2="1.4958" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="1.1458" x2="2.725" y2="2.1458" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="1.7958" x2="2.725" y2="2.7958" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="-0.8204" x2="-2.475" y2="0.1796" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="-1.4704" x2="-2.475" y2="-0.4704" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="-2.1204" x2="-2.475" y2="-1.1204" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="-2.7704" x2="-2.475" y2="-1.7704" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-2.7704" x2="2.725" y2="-1.7704" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-2.1204" x2="2.725" y2="-1.1204" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-1.4704" x2="2.725" y2="-0.4704" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-0.8204" x2="2.725" y2="0.1796" layer="51" rot="R270"/>
<rectangle x1="-3.81" y1="-3.048" x2="3.81" y2="3.048" layer="39"/>
</package>
<package name="DIL16" urn="urn:adsk.eagle:footprint:32977808/1" library_version="8">
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<text x="-10.922" y="0" size="0.8" layer="25" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SO8" urn="urn:adsk.eagle:package:32977849/3" type="model" library_version="8">
<packageinstances>
<packageinstance name="SO8"/>
</packageinstances>
</package3d>
<package3d name="MSOP8" urn="urn:adsk.eagle:package:32977835/1" type="box" library_version="8">
<description>&lt;b&gt;8M, 8-Lead, 0.118" Wide, Miniature Small Outline Package&lt;/b&gt;&lt;p&gt;
MSOP&lt;br&gt;
8M-Package doc1097.pdf</description>
<packageinstances>
<packageinstance name="MSOP8"/>
</packageinstances>
</package3d>
<package3d name="SO16" urn="urn:adsk.eagle:package:32977843/2" type="model" library_version="8">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<packageinstances>
<packageinstance name="SO16"/>
</packageinstances>
</package3d>
<package3d name="TSSOP-16" urn="urn:adsk.eagle:package:32977834/1" type="box" library_version="8">
<description>8-Bit Shift Registers With 3-State Output Registers</description>
<packageinstances>
<packageinstance name="TSSOP-16"/>
</packageinstances>
</package3d>
<package3d name="DIL16" urn="urn:adsk.eagle:package:32977839/1" type="box" library_version="8">
<packageinstances>
<packageinstance name="DIL16"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="OPAMP" urn="urn:adsk.eagle:symbol:32977774/1" library_version="8">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="7.62" y2="0" width="0.4064" layer="94"/>
<wire x1="7.62" y1="0" x2="-2.54" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="94"/>
<pin name="+IN" x="-5.08" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="-IN" x="-5.08" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="10.16" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<text x="7.62" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="7.62" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="OPAMP_PWR" urn="urn:adsk.eagle:symbol:32977773/1" library_version="8">
<pin name="V+" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="V-" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<text x="-1.905" y="-0.762" size="1.778" layer="95">&gt;NAME</text>
<text x="0.635" y="6.985" size="1.016" layer="95" rot="R270">V+</text>
<text x="1.5875" y="-6.985" size="1.016" layer="95" rot="R90">V-</text>
</symbol>
<symbol name="74595" urn="urn:adsk.eagle:symbol:32977775/1" library_version="8">
<wire x1="-7.62" y1="-15.24" x2="7.62" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="7.62" y2="12.7" width="0.4064" layer="94"/>
<wire x1="7.62" y1="12.7" x2="-7.62" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-15.24" width="0.4064" layer="94"/>
<pin name="G" x="-12.7" y="-12.7" length="middle" direction="in" function="dot"/>
<pin name="QA" x="12.7" y="10.16" length="middle" direction="hiz" rot="R180"/>
<pin name="QB" x="12.7" y="7.62" length="middle" direction="hiz" rot="R180"/>
<pin name="QC" x="12.7" y="5.08" length="middle" direction="hiz" rot="R180"/>
<pin name="QD" x="12.7" y="2.54" length="middle" direction="hiz" rot="R180"/>
<pin name="QE" x="12.7" y="0" length="middle" direction="hiz" rot="R180"/>
<pin name="QF" x="12.7" y="-2.54" length="middle" direction="hiz" rot="R180"/>
<pin name="QG" x="12.7" y="-5.08" length="middle" direction="hiz" rot="R180"/>
<pin name="QH" x="12.7" y="-7.62" length="middle" direction="hiz" rot="R180"/>
<pin name="QH*" x="12.7" y="-12.7" length="middle" direction="hiz" rot="R180"/>
<pin name="RCK" x="-12.7" y="-2.54" length="middle" direction="in" function="clk"/>
<pin name="SCK" x="-12.7" y="5.08" length="middle" direction="in" function="clk"/>
<pin name="SCL" x="-12.7" y="2.54" length="middle" direction="in" function="dot"/>
<pin name="SER" x="-12.7" y="10.16" length="middle" direction="in"/>
<text x="-7.62" y="13.335" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-7.62" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PWRN" urn="urn:adsk.eagle:symbol:32977767/1" library_version="8">
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCP6002" urn="urn:adsk.eagle:component:32977859/3" prefix="IC" library_version="8">
<gates>
<gate name="A" symbol="OPAMP" x="7.62" y="15.24"/>
<gate name="B" symbol="OPAMP" x="7.62" y="-15.24"/>
<gate name="P" symbol="OPAMP_PWR" x="-12.7" y="0"/>
</gates>
<devices>
<device name="CD" package="SO8">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977849/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="Microchip"/>
<attribute name="MPN" value="MCP6002T-I/SN"/>
<attribute name="ODOO" value="IC_MCP6002_SO8" constant="no"/>
<attribute name="PRICE" value="5.82" constant="no"/>
<attribute name="SIZEX" value="6.2"/>
<attribute name="SIZEY" value="7.8"/>
<attribute name="SIZEZ" value="1.5"/>
<attribute name="TYPE" value="SMT"/>
</technology>
</technologies>
</device>
<device name="CP" package="MSOP8">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977835/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="Microchip" constant="no"/>
<attribute name="MPN" value="MCP6002T-I/MS"/>
<attribute name="ODOO" value="IC_MCP6002_MSOP8" constant="no"/>
<attribute name="SIZEX" value="6.4"/>
<attribute name="SIZEY" value="3"/>
<attribute name="SIZEZ" value="1.2"/>
<attribute name="TYPE" value="SMT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="74*595" urn="urn:adsk.eagle:component:32977871/2" prefix="IC" library_version="8">
<gates>
<gate name="A" symbol="74595" x="22.86" y="0"/>
<gate name="P" symbol="PWRN" x="-5.08" y="0" addlevel="request"/>
</gates>
<devices>
<device name="D" package="SO16">
<connects>
<connect gate="A" pin="G" pad="13"/>
<connect gate="A" pin="QA" pad="15"/>
<connect gate="A" pin="QB" pad="1"/>
<connect gate="A" pin="QC" pad="2"/>
<connect gate="A" pin="QD" pad="3"/>
<connect gate="A" pin="QE" pad="4"/>
<connect gate="A" pin="QF" pad="5"/>
<connect gate="A" pin="QG" pad="6"/>
<connect gate="A" pin="QH" pad="7"/>
<connect gate="A" pin="QH*" pad="9"/>
<connect gate="A" pin="RCK" pad="12"/>
<connect gate="A" pin="SCK" pad="11"/>
<connect gate="A" pin="SCL" pad="10"/>
<connect gate="A" pin="SER" pad="14"/>
<connect gate="P" pin="GND" pad="8"/>
<connect gate="P" pin="VCC" pad="16"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977843/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="Texas Instruments"/>
<attribute name="MPN" value="SN74HC595DR"/>
<attribute name="ODOO" value="IC_74HC595_SO16" constant="no"/>
<attribute name="SIZEX" value="10.8"/>
<attribute name="SIZEY" value="6"/>
<attribute name="SIZEZ" value="1.5"/>
<attribute name="TYPE" value="SMT"/>
</technology>
</technologies>
</device>
<device name="P" package="TSSOP-16">
<connects>
<connect gate="A" pin="G" pad="13"/>
<connect gate="A" pin="QA" pad="15"/>
<connect gate="A" pin="QB" pad="1"/>
<connect gate="A" pin="QC" pad="2"/>
<connect gate="A" pin="QD" pad="3"/>
<connect gate="A" pin="QE" pad="4"/>
<connect gate="A" pin="QF" pad="5"/>
<connect gate="A" pin="QG" pad="6"/>
<connect gate="A" pin="QH" pad="7"/>
<connect gate="A" pin="QH*" pad="9"/>
<connect gate="A" pin="RCK" pad="12"/>
<connect gate="A" pin="SCK" pad="11"/>
<connect gate="A" pin="SCL" pad="10"/>
<connect gate="A" pin="SER" pad="14"/>
<connect gate="P" pin="GND" pad="8"/>
<connect gate="P" pin="VCC" pad="16"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977834/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="Texas Instruments"/>
<attribute name="MPN" value="SN74HC595DBR"/>
<attribute name="ODOO" value="IC_74HC595_TSSOP16"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
<device name="" package="DIL16">
<connects>
<connect gate="A" pin="G" pad="13"/>
<connect gate="A" pin="QA" pad="15"/>
<connect gate="A" pin="QB" pad="1"/>
<connect gate="A" pin="QC" pad="2"/>
<connect gate="A" pin="QD" pad="3"/>
<connect gate="A" pin="QE" pad="4"/>
<connect gate="A" pin="QF" pad="5"/>
<connect gate="A" pin="QG" pad="6"/>
<connect gate="A" pin="QH" pad="7"/>
<connect gate="A" pin="QH*" pad="9"/>
<connect gate="A" pin="RCK" pad="12"/>
<connect gate="A" pin="SCK" pad="11"/>
<connect gate="A" pin="SCL" pad="10"/>
<connect gate="A" pin="SER" pad="14"/>
<connect gate="P" pin="GND" pad="8"/>
<connect gate="P" pin="VCC" pad="16"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977839/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_RESISTORS" urn="urn:adsk.eagle:library:32975925">
<packages>
<package name="RESISTOR0603" urn="urn:adsk.eagle:footprint:32975930/1" library_version="3">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<rectangle x1="-1.397" y1="-0.6858" x2="1.397" y2="0.6858" layer="39"/>
<smd name="1" x="-0.770625" y="0" dx="0.9" dy="1" layer="1"/>
<smd name="2" x="0.770625" y="0" dx="0.9" dy="1" layer="1"/>
<text x="-0.0127" y="1.184275" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0.0127" y="-1.1985625" size="0.8" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="RESISTOR0603" urn="urn:adsk.eagle:package:32975936/2" type="model" library_version="3">
<description>Chip, 1.60 X 0.80 X 0.50 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESISTOR0603"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RESISTOR" urn="urn:adsk.eagle:symbol:32975932/1" library_version="3">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR0603" urn="urn:adsk.eagle:component:32975941/2" prefix="R" uservalue="yes" library_version="3">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESISTOR0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32975936/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="PRICE" value="0.1" constant="no"/>
<attribute name="SIZEX" value="0.85"/>
<attribute name="SIZEY" value="1.55"/>
<attribute name="SIZEZ" value="0.5"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_CAPACITORS" urn="urn:adsk.eagle:library:32978038">
<packages>
<package name="CAPACITOR0603" urn="urn:adsk.eagle:footprint:32978050/1" library_version="4">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<rectangle x1="-1.397" y1="-0.6858" x2="1.397" y2="0.6858" layer="39"/>
<smd name="1" x="-0.770625" y="0" dx="0.9" dy="1" layer="1"/>
<smd name="2" x="0.770625" y="0" dx="0.9" dy="1" layer="1"/>
<text x="-0.0635" y="1.2065" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-0.0635" y="-1.2065" size="0.8" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="CAPACITOR0805" urn="urn:adsk.eagle:footprint:32978049/1" library_version="4">
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
<rectangle x1="-1.8034" y1="-1.016" x2="1.8034" y2="1.016" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.2" dy="1.55" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.2" dy="1.55" layer="1"/>
<text x="0" y="1.524" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="-0.127" y="-1.524" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="CAPACITOR0603" urn="urn:adsk.eagle:package:32978072/2" type="model" library_version="4">
<description>Chip, 1.60 X 0.80 X 0.80 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.80 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPACITOR0603"/>
</packageinstances>
</package3d>
<package3d name="CAPACITOR0805" urn="urn:adsk.eagle:package:32978071/2" type="model" library_version="4">
<description>Chip, 2.05 X 1.27 X 1.25 mm body
&lt;p&gt;Chip package with body size 2.05 X 1.27 X 1.25 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPACITOR0805"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:32978053/1" library_version="4">
<rectangle x1="-2.032" y1="-0.762" x2="2.032" y2="-0.254" layer="94"/>
<rectangle x1="-2.032" y1="0.254" x2="2.032" y2="0.762" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="1.651" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="0" y1="0.762" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.762" x2="0" y2="-5.08" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR0603" urn="urn:adsk.eagle:component:32978083/2" prefix="C" uservalue="yes" library_version="4">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="1.27"/>
</gates>
<devices>
<device name="" package="CAPACITOR0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32978072/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value=""/>
<attribute name="ODOO" value="0.1"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value="0.85"/>
<attribute name="SIZEY" value="1.55"/>
<attribute name="SIZEZ" value="0.8"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR0805" urn="urn:adsk.eagle:component:32978082/2" prefix="C" library_version="4">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="1.27"/>
</gates>
<devices>
<device name="" package="CAPACITOR0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32978071/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value=""/>
<attribute name="ODOO" value="0.1"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value="1.2"/>
<attribute name="SIZEY" value="2"/>
<attribute name="SIZEZ" value="1.2"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_DIODES" urn="urn:adsk.eagle:library:32977972">
<packages>
<package name="MULTI(0603+0805+MICROMELF)" urn="urn:adsk.eagle:footprint:32977980/1" library_version="2">
<wire x1="0.92" y1="0.435375" x2="-0.92" y2="0.435375" width="0.1524" layer="51"/>
<wire x1="-0.92" y1="-0.435375" x2="0.92" y2="-0.435375" width="0.1524" layer="51"/>
<wire x1="-0.18415" y1="0.238125" x2="0.200025" y2="0" width="0.127" layer="51"/>
<wire x1="0.200025" y1="0" x2="-0.18415" y2="-0.238125" width="0.127" layer="51"/>
<wire x1="-0.18415" y1="-0.238125" x2="-0.18415" y2="0.238125" width="0.127" layer="51"/>
<rectangle x1="-1" y1="-0.4318" x2="-0.6" y2="0.4318" layer="51" rot="R180"/>
<rectangle x1="0.6" y1="-0.4318" x2="1" y2="0.4318" layer="51" rot="R180"/>
<rectangle x1="-0.2253" y1="-0.4001" x2="0.1745" y2="0.4001" layer="35" rot="R180"/>
<rectangle x1="-1.5494" y1="-0.9144" x2="1.5494" y2="0.9144" layer="39"/>
<smd name="K" x="0.9" y="0" dx="0.9" dy="1.45" layer="1" rot="R180"/>
<smd name="A" x="-0.9" y="0" dx="0.9" dy="1.45" layer="1" rot="R180"/>
<text x="0" y="1.397" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<circle x="1.7526" y="0" radius="0.1016" width="0.2032" layer="21"/>
</package>
<package name="DIODE_0603" urn="urn:adsk.eagle:footprint:32977982/1" library_version="2">
<wire x1="-0.90825" y1="-0.435375" x2="0.90825" y2="-0.435375" width="0.1524" layer="51"/>
<wire x1="0.90825" y1="0.435375" x2="-0.90825" y2="0.435375" width="0.1524" layer="51"/>
<wire x1="0.15875" y1="-0.238125" x2="-0.238125" y2="0" width="0.127" layer="51"/>
<wire x1="-0.238125" y1="0" x2="0.15875" y2="0.238125" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0.238125" x2="0.15875" y2="-0.238125" width="0.127" layer="51"/>
<rectangle x1="0.59055" y1="-0.4318" x2="0.99695" y2="0.4318" layer="51"/>
<rectangle x1="-0.99695" y1="-0.4318" x2="-0.59055" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<rectangle x1="-1.4986" y1="-0.6858" x2="1.4986" y2="0.6858" layer="39"/>
<smd name="K" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="A" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="0" y="1.27" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<circle x="-1.651" y="0" radius="0.1016" width="0.2032" layer="21"/>
</package>
<package name="SOD123" urn="urn:adsk.eagle:footprint:32977983/1" library_version="2">
<wire x1="-1.1" y1="0.7" x2="1.1" y2="0.7" width="0.254" layer="51"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.7" width="0.254" layer="51"/>
<wire x1="1.1" y1="-0.7" x2="-1.1" y2="-0.7" width="0.254" layer="51"/>
<wire x1="-1.1" y1="-0.7" x2="-1.1" y2="0.7" width="0.254" layer="51"/>
<rectangle x1="-1.95" y1="-0.45" x2="-1.2" y2="0.4" layer="51"/>
<rectangle x1="1.2" y1="-0.45" x2="1.95" y2="0.4" layer="51"/>
<rectangle x1="-1.05" y1="-0.65" x2="-0.15" y2="0.7" layer="51"/>
<rectangle x1="-2.4638" y1="-0.9652" x2="2.4638" y2="0.9652" layer="39"/>
<smd name="A" x="1.6539375" y="0" dx="1.05" dy="1.2" layer="1"/>
<smd name="C" x="-1.6539375" y="0" dx="1.05" dy="1.2" layer="1"/>
<text x="-0.0078" y="1.5207" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="-0.0078" y="-1.5888" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<circle x="-2.54" y="0" radius="0.1016" width="0.2032" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="MULTI(0603+0805+MICROMELF)" urn="urn:adsk.eagle:package:32977997/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="MULTI(0603+0805+MICROMELF)"/>
</packageinstances>
</package3d>
<package3d name="DIODE_0603" urn="urn:adsk.eagle:package:32977999/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="DIODE_0603"/>
</packageinstances>
</package3d>
<package3d name="SOD123" urn="urn:adsk.eagle:package:32978000/2" type="model" library_version="2">
<description>SOD, 3.75 mm span, 2.50 X 1.50 X 1.35 mm body
&lt;p&gt;SOD package with 3.75 mm span with body size 2.50 X 1.50 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOD123"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DIODE" urn="urn:adsk.eagle:symbol:32977992/1" library_version="2">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DIODE_ZENER" urn="urn:adsk.eagle:symbol:32977990/1" library_version="2">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.508" y2="-1.27" width="0.254" layer="94"/>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N4148" urn="urn:adsk.eagle:component:32978013/1" prefix="D" library_version="2">
<description>multifootprint covering 0603 + 0805 + microMELF</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIODE_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977999/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="DC COMPONENTS "/>
<attribute name="MPN" value="CD4148WT(0603C) "/>
<attribute name="ODOO" value="D_1N4148_0603" constant="no"/>
<attribute name="PRICE" value="0.4"/>
<attribute name="SIZEX" value="0.85"/>
<attribute name="SIZEY" value="1.55"/>
<attribute name="SIZEZ" value="0.5"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/1n4148-0603/univerzalni-diody-smd/dc-components/cd4148wt0603c/"/>
</technology>
</technologies>
</device>
<device name="MULTI" package="MULTI(0603+0805+MICROMELF)">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977997/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="DC COMPONENTS "/>
<attribute name="MPN" value="CD4148WT(0603C) "/>
<attribute name="ODOO" value="D_1N4148_0603" constant="no"/>
<attribute name="PRICE" value="0.4"/>
<attribute name="SIZEX" value="0.85"/>
<attribute name="SIZEY" value="1.55"/>
<attribute name="SIZEZ" value="0.5"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/1n4148-0603/univerzalni-diody-smd/dc-components/cd4148wt0603c/"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER_DIODE" urn="urn:adsk.eagle:component:32978010/2" prefix="ZD" uservalue="yes" library_version="2">
<gates>
<gate name="G$1" symbol="DIODE_ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32978000/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
<technology name="10V">
<attribute name="MAN" value=" DIOTEC SEMICONDUCTOR "/>
<attribute name="MPN" value="BZT52C10"/>
<attribute name="ODOO" value="DZ_10V_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c10-dio/zenerovy-diody-smd/diotec-semiconductor/bzt52c10/"/>
</technology>
<technology name="13V">
<attribute name="MAN" value=" DIOTEC SEMICONDUCTOR "/>
<attribute name="MPN" value="BZT52C13"/>
<attribute name="ODOO" value="DZ_13V_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c13-dio/zenerovy-diody-smd/diotec-semiconductor/bzt52c13/"/>
</technology>
<technology name="20V">
<attribute name="MAN" value="DIOTEC SEMICONDUCTOR "/>
<attribute name="MPN" value="BZT52C20"/>
<attribute name="ODOO" value="DZ_20V_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c20-dio/zenerovy-diody-smd/diotec-semiconductor/bzt52c20/"/>
</technology>
<technology name="2V4">
<attribute name="MAN" value="DIOTEC SEMICONDUCTOR "/>
<attribute name="MPN" value="BZT52C2V4"/>
<attribute name="ODOO" value="DZ_2.4V_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c2v4-dio/zenerovy-diody-smd/diotec-semiconductor/bzt52c2v4/#"/>
</technology>
<technology name="3V">
<attribute name="MAN" value="DIOTEC SEMICONDUCTOR"/>
<attribute name="MPN" value="BZT52C3V0"/>
<attribute name="ODOO" value="DZ_3V_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c3v0-dio/zenerovy-diody-smd/diotec-semiconductor/bzt52c3v0/#"/>
</technology>
<technology name="3V3">
<attribute name="MAN" value="DIODES INCORPORATED "/>
<attribute name="MPN" value="BZT52C3V3-7-F "/>
<attribute name="ODOO" value="DZ_3V3_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c3v3-7-f/zenerovy-diody-smd/diodes-incorporated/"/>
</technology>
<technology name="3V9">
<attribute name="MAN" value="DIODES INCORPORATED"/>
<attribute name="MPN" value="BZT52C3V9-7-F"/>
<attribute name="ODOO" value="DZ_3.9V_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c3v9-7-f/zenerovy-diody-smd/diodes-incorporated/#"/>
</technology>
<technology name="4V7">
<attribute name="MAN" value=" ON SEMICONDUCTOR"/>
<attribute name="MPN" value="MMSZ4V7T1G"/>
<attribute name="ODOO" value="DZ_4.7V_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/mmsz4v7t1g/zenerovy-diody-smd/on-semiconductor/"/>
</technology>
<technology name="5V1">
<attribute name="MAN" value=" DIOTEC SEMICONDUCTOR "/>
<attribute name="MPN" value="BZT52C5V1-DIO"/>
<attribute name="ODOO" value="DZ_5.1V_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c5v1-dio/zenerovy-diody-smd/diotec-semiconductor/bzt52c5v1/"/>
</technology>
<technology name="5V6">
<attribute name="MAN" value=" DIOTEC SEMICONDUCTOR "/>
<attribute name="MPN" value="BZT52C5V6"/>
<attribute name="ODOO" value="DZ_5.6V_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c5v6-dio/zenerovy-diody-smd/diotec-semiconductor/bzt52c5v6/"/>
</technology>
<technology name="9V1">
<attribute name="MAN" value="DIODES INCORPORATED"/>
<attribute name="MPN" value="BZT52C9V1-7-F"/>
<attribute name="ODOO" value="DZ_9V1_SOD123" constant="no"/>
<attribute name="PRICE" value="0.3"/>
<attribute name="SIZEX" value="0.5"/>
<attribute name="SIZEY" value="3.7"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/bzt52c9v1-7-f/zenerovy-diody-smd/diodes-incorporated/"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1N5819" urn="urn:adsk.eagle:component:32978005/2" prefix="D" library_version="2">
<description>diode instead of 1N4007</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32978000/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MPN" value="1N5819HW-7-F "/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="0.5"/>
<attribute name="SIZEX" value="1.55"/>
<attribute name="SIZEY" value="2.65"/>
<attribute name="SIZEZ" value="0.9"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/1n5819hw-7-f/diody-schottky-smd/diodes-incorporated/"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_FUSES" urn="urn:adsk.eagle:library:32977952">
<packages>
<package name="PTC1210" urn="urn:adsk.eagle:footprint:32977954/1" library_version="3">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.1468" x2="1.55" y2="1.1468" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.1468" x2="1.55" y2="-1.1468" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.1468" x2="-1.55" y2="-1.1468" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.1468" x2="-1.55" y2="1.1468" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.5" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.5" layer="1"/>
<text x="0" y="1.9304" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.1336" size="0.8" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="-1.5748" y1="-1.1176" x2="-0.6096" y2="1.1684" layer="21"/>
<rectangle x1="0.6096" y1="-1.1176" x2="1.5748" y2="1.1684" layer="21"/>
<rectangle x1="-2.54" y1="-1.524" x2="2.54" y2="1.524" layer="39"/>
</package>
<package name="PTC0805" urn="urn:adsk.eagle:footprint:32977956/1" library_version="3">
<wire x1="-0.5822" y1="0.5844" x2="0.5314" y2="0.5844" width="0.1016" layer="51"/>
<wire x1="-0.5822" y1="-0.5714" x2="0.5314" y2="-0.5714" width="0.1016" layer="51"/>
<rectangle x1="-1.1644" y1="-0.6325" x2="-0.5643" y2="0.6325" layer="51"/>
<rectangle x1="0.5056" y1="-0.6325" x2="1.1057" y2="0.6325" layer="51"/>
<rectangle x1="-0.3254" y1="-0.3" x2="0.2746" y2="0.3" layer="35"/>
<rectangle x1="-1.7018" y1="-0.9906" x2="1.651" y2="0.9906" layer="39"/>
<smd name="2" x="0.9214" y="0" dx="1" dy="1.5" layer="1"/>
<smd name="3" x="-0.9722" y="0" dx="1" dy="1.5" layer="1"/>
<text x="0.0151" y="1.6002" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0.0151" y="-1.5875" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="PTC1210" urn="urn:adsk.eagle:package:32977960/2" type="model" library_version="3">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<packageinstances>
<packageinstance name="PTC1210"/>
</packageinstances>
</package3d>
<package3d name="PTC0805" urn="urn:adsk.eagle:package:32977962/4" type="model" library_version="3">
<description>Chip, 2.15 X 1.35 X 1.00 mm body
&lt;p&gt;Chip package with body size 2.15 X 1.35 X 1.00 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="PTC0805"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PTC" urn="urn:adsk.eagle:symbol:32977958/1" library_version="3">
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.794" y1="-2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<pin name="1" x="-6.35" y="0" visible="off" length="short"/>
<pin name="2" x="6.35" y="0" visible="off" length="short" rot="R180"/>
<text x="0" y="3.683" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="-0.127" y="-3.81" size="1.778" layer="96" align="center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PTC" urn="urn:adsk.eagle:component:32977964/3" prefix="F" library_version="3">
<gates>
<gate name="G$1" symbol="PTC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PTC0805">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977962/4"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value="1.2"/>
<attribute name="SIZEY" value="2"/>
<attribute name="SIZEZ" value="0.5"/>
<attribute name="TYPE" value="SMT" constant="no"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
<technology name="100MA">
<attribute name="MAN" value="ECE"/>
<attribute name="MPN" value="SR010-15"/>
<attribute name="ODOO" value="F_PTC_100MA_0805" constant="no"/>
<attribute name="PRICE" value="3.2"/>
<attribute name="SIZEX" value="1.2"/>
<attribute name="SIZEY" value="2"/>
<attribute name="SIZEZ" value="0.5"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/sr010-15/polymerove-pojistky-smd/ece/"/>
</technology>
<technology name="200MA">
<attribute name="MAN" value="ECE"/>
<attribute name="MPN" value="SR020-09"/>
<attribute name="ODOO" value="F_PTC_200MA_0805" constant="no"/>
<attribute name="PRICE" value="3.2"/>
<attribute name="SIZEX" value="1.2"/>
<attribute name="SIZEY" value="2"/>
<attribute name="SIZEZ" value="0.5"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/sr020-09/polymerove-pojistky-smd/ece/"/>
</technology>
</technologies>
</device>
<device name="1210" package="PTC1210">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977960/2"/>
</package3dinstances>
<technologies>
<technology name="300MA">
<attribute name="MAN" value="BELL" constant="no"/>
<attribute name="MPN" value="0ZCJ0025AF2E" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="SMT" constant="no"/>
<attribute name="WEB" value="https://cz.mouser.com/ProductDetail/Bel-Fuse/0ZCJ0025AF2E?qs=%2Fha2pyFadugaoNql5zaJPiD2ngDcsWcEUPNvBo1cVae3%2F9LGXPV2cw%3D%3D&amp;utm_source=octopart&amp;utm_medium=aggregator&amp;utm_campaign=530-0ZCJ0025AF2E&amp;utm_content=Bel" constant="no"/>
</technology>
<technology name="500MA">
<attribute name="MAN" value="ECE "/>
<attribute name="MPN" value="SM050-16"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/sm050-16/polymerove-pojistky-smd/ece/"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_SWITCHES" urn="urn:adsk.eagle:library:32903743">
<packages>
<package name="SWITCH_SPDT_TOGGLE" urn="urn:adsk.eagle:footprint:33626216/2" library_version="10">
<description>&lt;b&gt;TINY SWITCH&lt;/b&gt;&lt;p&gt;
Source: http://www2.produktinfo.conrad.com/datenblaetter/700000-724999/705152-da-01-de-Subminiaturschalter_TL_36YO.pdf</description>
<wire x1="-4.064" y1="2.54" x2="4.064" y2="2.54" width="0.2032" layer="21"/>
<wire x1="4.064" y1="2.54" x2="4.064" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-2.54" x2="-4.064" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-4.064" y1="-2.54" x2="-4.064" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="0" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="0" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.2032" layer="51" curve="-180"/>
<wire x1="-1.375" y1="1.275" x2="-1.35" y2="-1.3" width="0.2032" layer="51" curve="-273.242292"/>
<circle x="-3.175" y="0" radius="1.27" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="2.425" width="0.127" layer="253"/>
<pad name="1" x="-2.54" y="0" drill="0.8" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="2" x="0" y="0" drill="0.8" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="0.8" diameter="1.4224" shape="octagon" rot="R90"/>
<text x="-5.08" y="3.175" size="1.778" layer="25">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="27">&gt;VALUE</text>
<text x="-2.667" y="-0.127" size="0.4064" layer="21">1</text>
<text x="-0.127" y="-0.127" size="0.4064" layer="21">2</text>
<text x="2.413" y="-0.127" size="0.4064" layer="21">3</text>
</package>
<package name="PUSH_320.02E11.08BLK" urn="urn:adsk.eagle:footprint:32903780/1" library_version="10">
<description>&lt;b&gt;320.02E11.08BLK-1&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="-3.81" y="3.81" drill="1.22" diameter="1.83"/>
<pad name="3" x="1.27" y="-1.27" drill="1.22" diameter="1.83"/>
<text x="5.9" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="5.9" y="-7.62" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-6.25" y1="6.25" x2="6.25" y2="6.25" width="0.001" layer="51"/>
<wire x1="6.25" y1="6.25" x2="6.25" y2="-6.25" width="0.001" layer="51"/>
<wire x1="6.25" y1="-6.25" x2="-6.25" y2="-6.25" width="0.001" layer="51"/>
<wire x1="-6.25" y1="-6.25" x2="-6.25" y2="6.25" width="0.001" layer="51"/>
<wire x1="-6.25" y1="6.25" x2="6.25" y2="6.25" width="0.001" layer="21"/>
<wire x1="6.25" y1="6.25" x2="6.25" y2="-6.25" width="0.001" layer="21"/>
<wire x1="6.25" y1="-6.25" x2="-6.25" y2="-6.25" width="0.01" layer="21"/>
<wire x1="-6.25" y1="-6.25" x2="-6.25" y2="6.25" width="0.001" layer="21"/>
<hole x="3.81" y="1.27" drill="1"/>
<hole x="-3.81" y="-3.81" drill="1"/>
<circle x="0" y="0" radius="6.1" width="0.1524" layer="253"/>
</package>
<package name="TACTILE-PTH" urn="urn:adsk.eagle:footprint:32903755/1" library_version="10">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.181" width="0.127" layer="253"/>
<circle x="0" y="0" radius="1.55" width="0.127" layer="253"/>
<circle x="0" y="0" radius="2.55" width="0.1524" layer="253"/>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.778"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.778"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.778"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.778"/>
<text x="-5.08" y="0" size="0.8" layer="25" font="fixed" ratio="15" rot="R270" align="center">&gt;NAME</text>
<text x="5.08" y="0" size="0.8" layer="27" font="fixed" ratio="15" rot="R90" align="center">&gt;VALUE</text>
<rectangle x1="-3.1242" y1="-3.1242" x2="3.1242" y2="3.1242" layer="39"/>
</package>
<package name="TACTILE-PTH-12MM" urn="urn:adsk.eagle:footprint:32903756/1" library_version="10">
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="0" y="0" radius="4.75" width="0.1524" layer="253"/>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.703125" y1="5.375" x2="4.703125" y2="5.375" width="0.127" layer="253"/>
<wire x1="5.375" y1="4.703125" x2="5.375" y2="-4.703125" width="0.127" layer="253"/>
<wire x1="4.703125" y1="-5.375" x2="-4.703125" y2="-5.375" width="0.127" layer="253"/>
<wire x1="-5.375" y1="-4.703125" x2="-5.375" y2="4.703125" width="0.127" layer="253"/>
<wire x1="-5.375" y1="4.703125" x2="-4.703125" y2="5.375" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="5.375" x2="5.375" y2="4.703125" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="-5.375" x2="5.375" y2="-4.703125" width="0.127" layer="253" curve="90"/>
<wire x1="-4.703125" y1="-5.375" x2="-5.375" y2="-4.703125" width="0.127" layer="253" curve="-90"/>
<wire x1="-6.1" y1="6.1" x2="6.1" y2="6.1" width="0.0762" layer="253"/>
<wire x1="6.1" y1="6.1" x2="6.1" y2="-6.1" width="0.0762" layer="253"/>
<wire x1="6.1" y1="-6.1" x2="-6.1" y2="-6.1" width="0.0762" layer="253"/>
<wire x1="-6.1" y1="-6.1" x2="-6.1" y2="6.1" width="0.0762" layer="253"/>
<wire x1="0" y1="6.1" x2="4.7" y2="8" width="0.0762" layer="252"/>
<wire x1="5.2" y1="5.1" x2="8.8" y2="5.8" width="0.0762" layer="252"/>
<wire x1="4.8" y1="0.5" x2="8.1" y2="0.6" width="0.0762" layer="252"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
<text x="0" y="7.62" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-7.62" size="0.8" layer="27" font="fixed" ratio="15" rot="R180" align="center">&gt;VALUE</text>
<text x="5" y="7.8" size="0.8" layer="252">thyme_rect</text>
<text x="8.4" y="0.3" size="0.8" layer="252">thyme_round</text>
<text x="9.1" y="5.8" size="0.8" layer="252">cv_trin</text>
<rectangle x1="-6.096" y1="-6.096" x2="6.096" y2="6.096" layer="39"/>
</package>
<package name="TACTILE-PTH-SIDEEZ" urn="urn:adsk.eagle:footprint:32903754/1" library_version="10">
<circle x="2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<circle x="2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="-2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<wire x1="1.5" y1="-3.8" x2="-1.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-3.65" y2="3.5" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="3.5" x2="-3" y2="3.5" width="0.2032" layer="51"/>
<wire x1="3" y1="3.5" x2="3.65" y2="3.5" width="0.2032" layer="51"/>
<wire x1="3.65" y1="3.5" x2="3.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2" x2="3" y2="2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2" x2="-3" y2="3.5" width="0.2032" layer="51"/>
<wire x1="3" y1="2" x2="3" y2="3.5" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-1.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2" x2="1.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-2" x2="3.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="1" x2="-3.65" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="-2" x2="3.65" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.65" y1="-2" x2="3.65" y2="1" width="0.2032" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="ANCHOR1" x="-3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="ANCHOR2" x="3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<text x="-5.08" y="0" size="0.8" layer="25" font="fixed" ratio="15" rot="R270" align="center">&gt;NAME</text>
<text x="5.08" y="0" size="0.8" layer="27" font="fixed" ratio="15" rot="R90" align="center">&gt;VALUE</text>
</package>
<package name="TACTILE_SWITCH_TALL" urn="urn:adsk.eagle:footprint:32903752/1" library_version="10">
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<circle x="0" y="0" radius="3.181" width="0.127" layer="253"/>
<circle x="0" y="0" radius="1.55" width="0.127" layer="253"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.254" layer="21"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.254" layer="21"/>
<smd name="A1" x="-4.0258" y="-2.25" dx="1.2" dy="1.6" layer="1" rot="R90"/>
<smd name="A2" x="4.0258" y="-2.25" dx="1.2" dy="1.56" layer="1" rot="R90"/>
<smd name="B1" x="-4.0258" y="2.25" dx="1.2" dy="1.6" layer="1" rot="R90"/>
<smd name="B2" x="4.0258" y="2.25" dx="1.2" dy="1.56" layer="1" rot="R90"/>
<text x="0" y="3.81" size="0.8" layer="25" font="fixed" ratio="15" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-3.81" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="TACTILE-SMD-12MM" urn="urn:adsk.eagle:footprint:32903751/1" library_version="10">
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-4.703125" y1="5.375" x2="4.703125" y2="5.375" width="0.127" layer="253"/>
<wire x1="5.375" y1="4.703125" x2="5.375" y2="-4.703125" width="0.127" layer="253"/>
<wire x1="4.703125" y1="-5.375" x2="-4.703125" y2="-5.375" width="0.127" layer="253"/>
<wire x1="-5.375" y1="-4.703125" x2="-5.375" y2="4.703125" width="0.127" layer="253"/>
<wire x1="-5.375" y1="4.703125" x2="-4.703125" y2="5.375" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="5.375" x2="5.375" y2="4.703125" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="-5.375" x2="5.375" y2="-4.703125" width="0.127" layer="253" curve="90"/>
<wire x1="-4.703125" y1="-5.375" x2="-5.375" y2="-4.703125" width="0.127" layer="253" curve="-90"/>
<smd name="1" x="6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="2" x="-6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="3" x="6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="4" x="-6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<text x="0" y="7.62" size="0.8" layer="25" font="fixed" ratio="15" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-7.62" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="TACTICLE-PTH-12MM-WITHHOLES" urn="urn:adsk.eagle:footprint:32903775/1" library_version="10">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<text x="0" y="6.985" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-7.112" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
<text x="4.064" y="-4.699" size="1.27" layer="21" ratio="10">4</text>
<hole x="0" y="-4.4958" drill="1.8034"/>
<hole x="0" y="4.4958" drill="1.8034"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="0" y="0" radius="4.75" width="0.127" layer="253"/>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.703125" y1="5.375" x2="4.703125" y2="5.375" width="0.127" layer="253"/>
<wire x1="5.375" y1="4.703125" x2="5.375" y2="-4.703125" width="0.127" layer="253"/>
<wire x1="4.703125" y1="-5.375" x2="-4.703125" y2="-5.375" width="0.127" layer="253"/>
<wire x1="-5.375" y1="-4.703125" x2="-5.375" y2="4.703125" width="0.127" layer="253"/>
<wire x1="-5.375" y1="4.703125" x2="-4.703125" y2="5.375" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="5.375" x2="5.375" y2="4.703125" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="-5.375" x2="5.375" y2="-4.703125" width="0.127" layer="253" curve="90"/>
<wire x1="-4.703125" y1="-5.375" x2="-5.375" y2="-4.703125" width="0.127" layer="253" curve="-90"/>
<wire x1="-6.096" y1="6.096" x2="-6.096" y2="-6.096" width="0.127" layer="253"/>
<wire x1="-6.096" y1="-6.096" x2="6.096" y2="-6.096" width="0.127" layer="253"/>
<wire x1="6.096" y1="-6.096" x2="6.096" y2="6.096" width="0.127" layer="253"/>
<wire x1="6.096" y1="6.096" x2="-6.096" y2="6.096" width="0.127" layer="253"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
</package>
</packages>
<packages3d>
<package3d name="SWITCH_SPDT_TOGGLE" urn="urn:adsk.eagle:package:33626219/3" type="model" library_version="10">
<description>&lt;b&gt;TINY SWITCH&lt;/b&gt;&lt;p&gt;
Source: http://www2.produktinfo.conrad.com/datenblaetter/700000-724999/705152-da-01-de-Subminiaturschalter_TL_36YO.pdf</description>
<packageinstances>
<packageinstance name="SWITCH_SPDT_TOGGLE"/>
</packageinstances>
</package3d>
<package3d name="PUSH_320.02E11.08BLK" urn="urn:adsk.eagle:package:32903785/2" type="model" library_version="10">
<description>&lt;b&gt;320.02E11.08BLK-1&lt;/b&gt;&lt;br&gt;</description>
<packageinstances>
<packageinstance name="PUSH_320.02E11.08BLK"/>
</packageinstances>
</package3d>
<package3d name="TACTILE-PTH" urn="urn:adsk.eagle:package:32903802/1" type="box" library_version="10">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TACTILE-PTH"/>
</packageinstances>
</package3d>
<package3d name="TACTILE-PTH-12MM" urn="urn:adsk.eagle:package:32903803/1" type="box" library_version="10">
<packageinstances>
<packageinstance name="TACTILE-PTH-12MM"/>
</packageinstances>
</package3d>
<package3d name="TACTILE-PTH-SIDEEZ" urn="urn:adsk.eagle:package:32903801/1" type="box" library_version="10">
<packageinstances>
<packageinstance name="TACTILE-PTH-SIDEEZ"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_TALL" urn="urn:adsk.eagle:package:32903799/1" type="box" library_version="10">
<packageinstances>
<packageinstance name="TACTILE_SWITCH_TALL"/>
</packageinstances>
</package3d>
<package3d name="TACTILE-SMD-12MM" urn="urn:adsk.eagle:package:32903798/1" type="box" library_version="10">
<packageinstances>
<packageinstance name="TACTILE-SMD-12MM"/>
</packageinstances>
</package3d>
<package3d name="TACTICLE-PTH-12MM-WITHHOLES" urn="urn:adsk.eagle:package:32903789/1" type="box" library_version="10">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TACTICLE-PTH-12MM-WITHHOLES"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ON_OFF_ON" urn="urn:adsk.eagle:symbol:32903771/1" library_version="10">
<wire x1="0" y1="-3.175" x2="0" y2="3.302" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="3.175" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.175" width="0.254" layer="94"/>
<text x="5.08" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="7.62" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S" x="2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="O" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="OFF_ON_MOM" urn="urn:adsk.eagle:symbol:32903768/1" library_version="10">
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-2.54" y1="1.016" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.016" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="1.016" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="2.032" x2="0.508" y2="2.032" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SWITCH_SP3T_TOGGLE" urn="urn:adsk.eagle:component:32903810/4" library_version="10">
<gates>
<gate name="G$1" symbol="ON_OFF_ON" x="0" y="5.08"/>
</gates>
<devices>
<device name="" package="SWITCH_SPDT_TOGGLE">
<connects>
<connect gate="G$1" pin="O" pad="3"/>
<connect gate="G$1" pin="P" pad="2"/>
<connect gate="G$1" pin="S" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:33626219/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PUSHBUTTON_MOM_320.02" urn="urn:adsk.eagle:component:32903808/2" library_version="10">
<gates>
<gate name="G$1" symbol="OFF_ON_MOM" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="PUSH_320.02E11.08BLK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32903785/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PUSH_MOMENTARY" urn="urn:adsk.eagle:component:32903820/1" prefix="S" library_version="10">
<description>Various NO switches- pushbuttons, reed, etc</description>
<gates>
<gate name="G$2" symbol="OFF_ON_MOM" x="0" y="0"/>
</gates>
<devices>
<device name="12MM" package="TACTILE-PTH-12MM">
<connects>
<connect gate="G$2" pin="1" pad="1 2" route="any"/>
<connect gate="G$2" pin="2" pad="3 4" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32903803/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_THT_6X6MM" package="TACTILE-PTH">
<connects>
<connect gate="G$2" pin="1" pad="1 2" route="any"/>
<connect gate="G$2" pin="2" pad="3 4" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32903802/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
<technology name="_17MM">
<attribute name="MAN" value="WEALTHMETAL"/>
<attribute name="MPN" value="TC-0111-T"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value="https://www.gme.cz/tc-0111-t"/>
</technology>
</technologies>
</device>
<device name="SIDE_EZ" package="TACTILE-PTH-SIDEEZ">
<connects>
<connect gate="G$2" pin="1" pad="1"/>
<connect gate="G$2" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32903801/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_SMT_6X6MM" package="TACTILE_SWITCH_TALL">
<connects>
<connect gate="G$2" pin="1" pad="A1 A2" route="any"/>
<connect gate="G$2" pin="2" pad="B1 B2" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32903799/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD-12MM" package="TACTILE-SMD-12MM">
<connects>
<connect gate="G$2" pin="1" pad="1 2" route="any"/>
<connect gate="G$2" pin="2" pad="3 4" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32903798/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_12MM_HOLES" package="TACTICLE-PTH-12MM-WITHHOLES">
<connects>
<connect gate="G$2" pin="1" pad="1 2" route="any"/>
<connect gate="G$2" pin="2" pad="3 4" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32903789/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_DIODES">
<packages>
<package name="DIODE_0603">
<wire x1="-0.90825" y1="-0.435375" x2="0.90825" y2="-0.435375" width="0.1524" layer="51"/>
<wire x1="0.90825" y1="0.435375" x2="-0.90825" y2="0.435375" width="0.1524" layer="51"/>
<rectangle x1="0.59055" y1="-0.4318" x2="0.99695" y2="0.4318" layer="51"/>
<rectangle x1="-0.99695" y1="-0.4318" x2="-0.59055" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="K" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="A" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="0" y="1.27" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<wire x1="0.15875" y1="-0.238125" x2="-0.238125" y2="0" width="0.127" layer="51"/>
<wire x1="-0.238125" y1="0" x2="0.15875" y2="0.238125" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0.238125" x2="0.15875" y2="-0.238125" width="0.127" layer="51"/>
<circle x="-1.651" y="0" radius="0.1016" width="0.2032" layer="21"/>
<rectangle x1="-1.4986" y1="-0.6858" x2="1.4986" y2="0.6858" layer="39"/>
</package>
<package name="MULTI(0603+0805+MICROMELF)">
<wire x1="0.92" y1="0.435375" x2="-0.92" y2="0.435375" width="0.1524" layer="51"/>
<wire x1="-0.92" y1="-0.435375" x2="0.92" y2="-0.435375" width="0.1524" layer="51"/>
<rectangle x1="-1" y1="-0.4318" x2="-0.6" y2="0.4318" layer="51" rot="R180"/>
<rectangle x1="0.6" y1="-0.4318" x2="1" y2="0.4318" layer="51" rot="R180"/>
<rectangle x1="-0.2253" y1="-0.4001" x2="0.1745" y2="0.4001" layer="35" rot="R180"/>
<smd name="K" x="0.9" y="0" dx="0.9" dy="1.45" layer="1" rot="R180"/>
<smd name="A" x="-0.9" y="0" dx="0.9" dy="1.45" layer="1" rot="R180"/>
<text x="0" y="1.397" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<wire x1="-0.18415" y1="0.238125" x2="0.200025" y2="0" width="0.127" layer="51"/>
<wire x1="0.200025" y1="0" x2="-0.18415" y2="-0.238125" width="0.127" layer="51"/>
<wire x1="-0.18415" y1="-0.238125" x2="-0.18415" y2="0.238125" width="0.127" layer="51"/>
<circle x="1.7526" y="0" radius="0.1016" width="0.2032" layer="21"/>
<rectangle x1="-1.5494" y1="-0.9144" x2="1.5494" y2="0.9144" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N4148" prefix="D">
<description>multifootprint covering 0603 + 0805 + microMELF</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIODE_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value="DC COMPONENTS "/>
<attribute name="MPN" value="CD4148WT(0603C) "/>
<attribute name="ODOO" value="D_1N4148_0603" constant="no"/>
<attribute name="PRICE" value="0.4"/>
<attribute name="SIZEX" value="0.85"/>
<attribute name="SIZEY" value="1.55"/>
<attribute name="SIZEZ" value="0.5"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/1n4148-0603/univerzalni-diody-smd/dc-components/cd4148wt0603c/"/>
</technology>
</technologies>
</device>
<device name="MULTI" package="MULTI(0603+0805+MICROMELF)">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value="DC COMPONENTS "/>
<attribute name="MPN" value="CD4148WT(0603C) "/>
<attribute name="ODOO" value="D_1N4148_0603" constant="no"/>
<attribute name="PRICE" value="0.4"/>
<attribute name="SIZEX" value="0.85"/>
<attribute name="SIZEY" value="1.55"/>
<attribute name="SIZEZ" value="0.5"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/1n4148-0603/univerzalni-diody-smd/dc-components/cd4148wt0603c/"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_VOLTAGE REGULATORS" urn="urn:adsk.eagle:library:32975822">
<packages>
<package name="DPAK" urn="urn:adsk.eagle:footprint:32975826/1" library_version="7">
<wire x1="3.2766" y1="3.7354" x2="3.277" y2="-2.459" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-2.459" x2="-3.2766" y2="3.7354" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="3.735" x2="3.2774" y2="3.7346" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="3.837" x2="-2.5654" y2="4.5482" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="4.5482" x2="-2.1082" y2="5.0054" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="5.0054" x2="2.1082" y2="5.0054" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="5.0054" x2="2.5654" y2="4.5482" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="4.5482" x2="2.5654" y2="3.837" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.837" x2="-2.5654" y2="3.837" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.2032" layer="21"/>
<smd name="1" x="-2.28" y="-4.04" dx="1.4" dy="3" layer="1"/>
<smd name="3" x="2.28" y="-4.04" dx="1.4" dy="3" layer="1"/>
<smd name="4" x="0" y="2.858" dx="4.826" dy="5.715" layer="1"/>
<rectangle x1="-2.7178" y1="-5.4562" x2="-1.8542" y2="-2.5606" layer="51"/>
<rectangle x1="1.8542" y1="-5.4562" x2="2.7178" y2="-2.5606" layer="51"/>
<rectangle x1="-0.4318" y1="-3.3226" x2="0.4318" y2="-2.5606" layer="21"/>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="3.837"/>
<vertex x="-2.5654" y="4.5482"/>
<vertex x="-2.1082" y="5.0054"/>
<vertex x="2.1082" y="5.0054"/>
<vertex x="2.5654" y="4.5482"/>
<vertex x="2.5654" y="3.837"/>
</polygon>
<text x="-3.81" y="0" size="0.8" layer="25" font="fixed" ratio="15" rot="R270" align="center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="SOT89" urn="urn:adsk.eagle:footprint:32975827/1" library_version="7">
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-0.7874" y1="1.5748" x2="-0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="-0.3556" y1="2.0066" x2="0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="0.3556" y1="2.0066" x2="0.7874" y2="1.5748" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.5748" x2="0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="-0.7874" y1="1.2954" x2="-0.7874" y2="1.5748" width="0.1998" layer="51"/>
<wire x1="-1.6" y1="1.2" x2="-2.3" y2="1.2" width="0.127" layer="21"/>
<wire x1="-2.3" y1="1.2" x2="-2.3" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.3" x2="-2.2" y2="-1.3" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.2" x2="2.3" y2="1.2" width="0.127" layer="21"/>
<wire x1="2.3" y1="1.2" x2="2.3" y2="-1.2" width="0.127" layer="21"/>
<wire x1="2.3" y1="-1.2" x2="2.2" y2="-1.2" width="0.127" layer="21"/>
<smd name="1" x="-1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="3" x="1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.727" dx="0.8" dy="1.9" layer="1"/>
<smd name="4" x="0" y="0.94" dx="2.032" dy="3.65" layer="1" roundness="75"/>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<polygon width="0.1998" layer="51">
<vertex x="-0.7874" y="1.3208"/>
<vertex x="-0.7874" y="1.5748"/>
<vertex x="-0.3556" y="2.0066"/>
<vertex x="0.3048" y="2.0066"/>
<vertex x="0.3556" y="2.0066"/>
<vertex x="0.7874" y="1.5748"/>
<vertex x="0.7874" y="1.2954"/>
<vertex x="-0.7874" y="1.2954"/>
</polygon>
<text x="-3.048" y="0" size="0.8" layer="25" font="fixed" ratio="15" rot="R270" align="center">&gt;NAME</text>
<text x="3.048" y="0" size="0.8" layer="27" font="fixed" ratio="15" rot="R90" align="center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="DPAK" urn="urn:adsk.eagle:package:32975835/1" type="box" library_version="7">
<packageinstances>
<packageinstance name="DPAK"/>
</packageinstances>
</package3d>
<package3d name="SOT89" urn="urn:adsk.eagle:package:32975836/2" type="model" library_version="7">
<description>4-TO, DPAK, 1.50 mm pitch, 4.10 mm span, 4.45 X 2.50 X 1.40 mm body
&lt;p&gt;4-pin TO, DPAK package with 1.50 mm pitch, 4.10 mm span with body size 4.45 X 2.50 X 1.40 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT89"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LDO" urn="urn:adsk.eagle:symbol:32975831/1" library_version="7">
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="2.54" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">GND</text>
<text x="-4.445" y="-0.635" size="1.524" layer="95">IN</text>
<text x="0.635" y="-0.635" size="1.524" layer="95">OUT</text>
<pin name="IN" x="-7.62" y="0" visible="off" length="short" direction="in"/>
<pin name="GND" x="0" y="-7.62" visible="off" length="short" direction="in" rot="R90"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="78" urn="urn:adsk.eagle:component:32975844/2" prefix="U" library_version="7">
<gates>
<gate name="G$1" symbol="LDO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT89">
<connects>
<connect gate="G$1" pin="GND" pad="2 4"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32975836/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value="4.1"/>
<attribute name="SIZEY" value="4.5"/>
<attribute name="SIZEZ" value="1.5"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
<technology name="05">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="TEXAS INSTRUMENTS"/>
<attribute name="MPN" value="UA78L05ACPK"/>
<attribute name="ODOO" value="VR_78L05_SOT89" constant="no"/>
<attribute name="PRICE" value="5.2" constant="no"/>
<attribute name="SIZEX" value="4.1"/>
<attribute name="SIZEY" value="4.5"/>
<attribute name="SIZEZ" value="1.5"/>
<attribute name="TYPE" value="SMT" constant="no"/>
<attribute name="VENDOR" value="TME" constant="no"/>
</technology>
<technology name="06">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="ST MICROELECTRONICS"/>
<attribute name="MPN" value="L78L06ABUTR"/>
<attribute name="ODOO" value="VR_78L06_SOT89" constant="no"/>
<attribute name="PRICE" value="3.33" constant="no"/>
<attribute name="SIZEX" value="4.1"/>
<attribute name="SIZEY" value="4.5"/>
<attribute name="SIZEZ" value="1.5"/>
<attribute name="TYPE" value="SMT" constant="no"/>
<attribute name="VENDOR" value="TME" constant="no"/>
</technology>
<technology name="33">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="ST MICROELECTRONICS"/>
<attribute name="MPN" value="L78L33ACUTR"/>
<attribute name="ODOO" value="VR_78L33_SOT89" constant="no"/>
<attribute name="PRICE" value="2" constant="no"/>
<attribute name="SIZEX" value="4.1"/>
<attribute name="SIZEY" value="4.5"/>
<attribute name="SIZEZ" value="1.5"/>
<attribute name="TYPE" value="SMT" constant="no"/>
<attribute name="VENDOR" value="TME" constant="no"/>
</technology>
</technologies>
</device>
<device name="D" package="DPAK">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32975835/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="SIZEX" value="6.5"/>
<attribute name="SIZEY" value="9.8"/>
<attribute name="SIZEZ" value="2.3"/>
<attribute name="TYPE" value="SMT" constant="no"/>
<attribute name="VENDOR" value="" constant="no"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
<technology name="05">
<attribute name="MAN" value="ON Semiconductor"/>
<attribute name="MPN" value="MC7805BDTRKG"/>
<attribute name="SIZEX" value="6.5"/>
<attribute name="SIZEY" value="9.8"/>
<attribute name="SIZEZ" value="2.3"/>
<attribute name="TYPE" value="SMT" constant="no"/>
<attribute name="VENDOR" value="TME" constant="no"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
<technology name="06">
<attribute name="MAN" value="ON Semiconductor"/>
<attribute name="MPN" value="MC78M06CDTRKG"/>
<attribute name="SIZEX" value="6.5"/>
<attribute name="SIZEY" value="9.8"/>
<attribute name="SIZEZ" value="2.3"/>
<attribute name="TYPE" value="SMT" constant="no"/>
<attribute name="VENDOR" value="MOUSER"/>
<attribute name="WEB" value="https://cz.mouser.com/ProductDetail/ON-Semiconductor/MC78M06CDTRKG?qs=sGAEpiMZZMtUqDgmOWBjgFMGuATcbJe5enXcPMluhLw%3D"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LF33CDT-TR" urn="urn:adsk.eagle:component:32975838/1" prefix="U" library_version="7">
<description>PM-REG-L4940-SOT3</description>
<gates>
<gate name="G$1" symbol="LDO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DPAK">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32975835/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="STM" constant="no"/>
<attribute name="MPN" value="LF33CDT-TR" constant="no"/>
<attribute name="ODOO" value="VR_LF33CDT_DPAK" constant="no"/>
<attribute name="SIZEX" value="6.5" constant="no"/>
<attribute name="SIZEY" value="9.8" constant="no"/>
<attribute name="SIZEZ" value="2.3" constant="no"/>
<attribute name="TYPE" value="SMT" constant="no"/>
<attribute name="WEB" value="https://www.digikey.com/product-detail/en/LF33CDT-TR/497-1532-1-ND/592050?utm_campaign=buynow&amp;WT.z_cid=ref_octopart_dkc_buynow&amp;utm_medium=aggregator&amp;curr=usd&amp;site=us&amp;utm_source=octopart" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_LEDS" urn="urn:adsk.eagle:library:32977677">
<packages>
<package name="LED_1206-BOTTOM_2.1D" urn="urn:adsk.eagle:footprint:32977679/1" library_version="10">
<wire x1="-0.889" y1="-1.524" x2="-0.254" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.524" x2="-0.254" y2="-1.143" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="0.127" y2="-1.524" width="0.127" layer="21"/>
<wire x1="0.127" y1="-1.524" x2="-0.254" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.905" x2="-0.254" y2="-1.524" width="0.127" layer="21"/>
<wire x1="0.127" y1="-1.524" x2="0.127" y2="-1.143" width="0.127" layer="21"/>
<wire x1="0.127" y1="-1.524" x2="0.127" y2="-1.905" width="0.127" layer="21"/>
<wire x1="0.127" y1="-1.524" x2="0.889" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-0.9271" y1="1.05" x2="0.9271" y2="1.05" width="0.002540625" layer="20"/>
<wire x1="0.9271" y1="1.05" x2="1.05" y2="0.9271" width="0.002540625" layer="20" curve="-90"/>
<wire x1="1.05" y1="0.9271" x2="1.05" y2="-0.9271" width="0.002540625" layer="20"/>
<wire x1="1.05" y1="-0.9271" x2="0.9271" y2="-1.05" width="0.002540625" layer="20" curve="-90"/>
<wire x1="0.9271" y1="-1.05" x2="-0.9271" y2="-1.05" width="0.002540625" layer="20"/>
<wire x1="-0.9271" y1="-1.05" x2="-1.05" y2="-0.9271" width="0.002540625" layer="20" curve="-90"/>
<wire x1="-1.05" y1="-0.9271" x2="-1.05" y2="0.9271" width="0.002540625" layer="20"/>
<wire x1="-1.05" y1="0.9271" x2="-0.9271" y2="1.05" width="0.002540625" layer="20" curve="-90"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<rectangle x1="-2.8702" y1="-1.0922" x2="2.8702" y2="1.0922" layer="39"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<text x="0" y="1.905" size="0.8" layer="21" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="3.175" size="0.8" layer="21" font="vector" ratio="15" align="center">&gt;VALUE</text>
<text x="-1.778" y="-1.905" size="0.8128" layer="21" font="vector" ratio="15">A</text>
<text x="1.27" y="-1.905" size="0.8128" layer="21" font="vector" ratio="15">C</text>
<circle x="0" y="0" radius="1.5" width="0" layer="253"/>
</package>
<package name="LED_0603" urn="urn:adsk.eagle:footprint:32977688/1" library_version="10">
<wire x1="-0.90825" y1="-0.435375" x2="0.90825" y2="-0.435375" width="0.1524" layer="51"/>
<wire x1="0.90825" y1="0.435375" x2="-0.90825" y2="0.435375" width="0.1524" layer="51"/>
<wire x1="0.15875" y1="-0.238125" x2="-0.238125" y2="0" width="0.127" layer="51"/>
<wire x1="-0.238125" y1="0" x2="0.15875" y2="0.238125" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0.238125" x2="0.15875" y2="-0.238125" width="0.127" layer="51"/>
<rectangle x1="0.59055" y1="-0.4318" x2="0.99695" y2="0.4318" layer="51"/>
<rectangle x1="-0.99695" y1="-0.4318" x2="-0.59055" y2="0.4318" layer="51"/>
<rectangle x1="-1.524" y1="-0.7112" x2="1.524" y2="0.7112" layer="39"/>
<smd name="K" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="A" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.0635" y="1.0795" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="-0.0635" y="-0.9525" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<circle x="0" y="0" radius="1.501" width="0.127" layer="253"/>
</package>
</packages>
<packages3d>
<package3d name="LED_1206-BOTTOM_2.1D" urn="urn:adsk.eagle:package:32977700/2" type="model" library_version="10">
<packageinstances>
<packageinstance name="LED_1206-BOTTOM_2.1D"/>
</packageinstances>
</package3d>
<package3d name="LED_0603" urn="urn:adsk.eagle:package:32977709/2" type="model" library_version="10">
<packageinstances>
<packageinstance name="LED_0603"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LED" urn="urn:adsk.eagle:symbol:32977698/2" library_version="10">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_SMT" urn="urn:adsk.eagle:component:32977721/4" prefix="LD" library_version="10">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977709/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
<technology name="-WW-WIS190TS-G">
<attribute name="MAN" value="WAH WANG HOLDING "/>
<attribute name="MPN" value="WW-WIS190TS-G "/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="1.5"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/ww-wis190ts-g/diody-led-smd-bile/wah-wang-holding/"/>
</technology>
</technologies>
</device>
<device name="_1206_BOTTOM" package="LED_1206-BOTTOM_2.1D">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977700/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value="1.6"/>
<attribute name="SIZEY" value="3.4"/>
<attribute name="SIZEZ" value="1.4"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
<technology name="_BLUE">
<attribute name="MAN" value="KINGBRIGHT ELECTRONIC "/>
<attribute name="MPN" value="KPTL-3216QBC-D-01 "/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="2.7"/>
<attribute name="SIZEX" value="1.6"/>
<attribute name="SIZEY" value="3.4"/>
<attribute name="SIZEZ" value="1.4"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/kptl-3216qbc-d-01/diody-led-smd-barevne/kingbright-electronic/"/>
</technology>
<technology name="_COLDWHITE">
<attribute name="MAN" value="LITEON "/>
<attribute name="MPN" value="LTW-C230DS "/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="1.7"/>
<attribute name="SIZEX" value="1.6"/>
<attribute name="SIZEY" value="3.4"/>
<attribute name="SIZEZ" value="1.4"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/ltw-c230ds/diody-led-smd-bile/liteon/"/>
</technology>
<technology name="_GREEN">
<attribute name="MAN" value="OPTOSUPPLY "/>
<attribute name="MPN" value="OSG51206E1N "/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="0.9"/>
<attribute name="SIZEX" value="1.6"/>
<attribute name="SIZEY" value="3.4"/>
<attribute name="SIZEZ" value="1.4"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/osg51206e1n/diody-led-smd-barevne/optosupply/"/>
</technology>
<technology name="_RED">
<attribute name="MAN" value="KINGBRIGHT ELECTRONIC "/>
<attribute name="MPN" value="KPTL-3216SURCK-01 "/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="PRICE" value="1.5"/>
<attribute name="SIZEX" value="1.6"/>
<attribute name="SIZEY" value="3.4"/>
<attribute name="SIZEZ" value="1.4"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="https://www.tme.eu/cz/details/kptl-3216surck-01/diody-led-smd-barevne/kingbright-electronic/"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_PINHEADERS" urn="urn:adsk.eagle:library:32976504">
<packages>
<package name="1X02" urn="urn:adsk.eagle:footprint:32976895/1" library_version="4">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X2" urn="urn:adsk.eagle:footprint:32976897/1" library_version="4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2" urn="urn:adsk.eagle:footprint:32976899/1" library_version="4">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-SMD" urn="urn:adsk.eagle:footprint:32976901/1" library_version="4">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="0" size="0.4064" layer="27">&gt;Value</text>
<text x="2.159" y="-4.445" size="1.27" layer="51">+</text>
<text x="-2.921" y="-4.445" size="1.27" layer="51">-</text>
</package>
<package name="1X02_BIG" urn="urn:adsk.eagle:footprint:32976902/1" library_version="4">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
</package>
<package name="JST-2-SMD-VERT" urn="urn:adsk.eagle:footprint:32976904/1" library_version="4">
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="2.54" y="-5.08" size="1.27" layer="25">&gt;Name</text>
<text x="2.24" y="3.48" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="R_SW_TH" urn="urn:adsk.eagle:footprint:32976906/1" library_version="4">
<wire x1="-1.651" y1="19.2532" x2="-1.651" y2="-1.3716" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-1.3716" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="19.2532" x2="13.589" y2="19.2532" width="0.2032" layer="21"/>
<wire x1="13.589" y1="19.2532" x2="13.589" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="13.589" y1="-2.2352" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.6002"/>
<pad name="P$2" x="0" y="16.9926" drill="1.6002"/>
<pad name="P$3" x="12.0904" y="15.494" drill="1.6002"/>
<pad name="P$4" x="12.0904" y="8.4582" drill="1.6002"/>
</package>
<package name="SCREWTERMINAL-5MM-2" urn="urn:adsk.eagle:footprint:32976908/1" library_version="4">
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LOCK" urn="urn:adsk.eagle:footprint:32976910/1" library_version="4">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.123825" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="2.663825" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="MOLEX-1X2_LOCK" urn="urn:adsk.eagle:footprint:32976912/1" library_version="4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="1X02_LOCK_LONGPADS" urn="urn:adsk.eagle:footprint:32976914/1" library_version="4">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK" urn="urn:adsk.eagle:footprint:32976916/1" library_version="4">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS" urn="urn:adsk.eagle:footprint:32976918/1" library_version="4">
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
</package>
<package name="1X02_NO_SILK" urn="urn:adsk.eagle:footprint:32976920/1" library_version="4">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-2-PTH" urn="urn:adsk.eagle:footprint:32976922/1" library_version="4">
<wire x1="-2" y1="0" x2="-2" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.6" x2="-2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.6" x2="2" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="1X02_XTRA_BIG" urn="urn:adsk.eagle:footprint:32976924/1" library_version="4">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
</package>
<package name="1X02_PP_HOLES_ONLY" urn="urn:adsk.eagle:footprint:32976926/1" library_version="4">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS" urn="urn:adsk.eagle:footprint:32976928/1" library_version="4">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS" urn="urn:adsk.eagle:footprint:32976930/1" library_version="4">
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT" urn="urn:adsk.eagle:footprint:32976932/1" library_version="4">
<description>&lt;H3&gt;JST-2-PTH-KIT&lt;/h3&gt;
2-Pin JST, through-hole connector&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2" urn="urn:adsk.eagle:footprint:32976934/1" library_version="4">
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM" urn="urn:adsk.eagle:footprint:32976936/1" library_version="4">
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.127" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.127" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST-2-PTH-VERT" urn="urn:adsk.eagle:footprint:32976938/1" library_version="4">
<wire x1="-2.95" y1="-2.25" x2="-2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="2.25" x2="2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="2.25" x2="2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="1" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-1.984" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="2.016" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="0.616" y="0.75" size="1.27" layer="51">+</text>
<text x="-1.384" y="0.75" size="1.27" layer="51">-</text>
</package>
</packages>
<packages3d>
<package3d name="1X02" urn="urn:adsk.eagle:package:32977286/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="1X02"/>
</packageinstances>
</package3d>
<package3d name="MOLEX-1X2" urn="urn:adsk.eagle:package:32977285/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="MOLEX-1X2"/>
</packageinstances>
</package3d>
<package3d name="SCREWTERMINAL-3.5MM-2" urn="urn:adsk.eagle:package:32977284/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="SCREWTERMINAL-3.5MM-2"/>
</packageinstances>
</package3d>
<package3d name="JST-2-SMD" urn="urn:adsk.eagle:package:32977283/1" type="box" library_version="4">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<packageinstances>
<packageinstance name="JST-2-SMD"/>
</packageinstances>
</package3d>
<package3d name="1X02_BIG" urn="urn:adsk.eagle:package:32977282/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="1X02_BIG"/>
</packageinstances>
</package3d>
<package3d name="JST-2-SMD-VERT" urn="urn:adsk.eagle:package:32977281/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="JST-2-SMD-VERT"/>
</packageinstances>
</package3d>
<package3d name="R_SW_TH" urn="urn:adsk.eagle:package:32977280/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="R_SW_TH"/>
</packageinstances>
</package3d>
<package3d name="SCREWTERMINAL-5MM-2" urn="urn:adsk.eagle:package:32977279/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="SCREWTERMINAL-5MM-2"/>
</packageinstances>
</package3d>
<package3d name="1X02_LOCK" urn="urn:adsk.eagle:package:32977278/2" type="model" library_version="4">
<description>Single-row, 2-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 5.08 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X2), 2-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 5.08 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="1X02_LOCK"/>
</packageinstances>
</package3d>
<package3d name="MOLEX-1X2_LOCK" urn="urn:adsk.eagle:package:32977308/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="MOLEX-1X2_LOCK"/>
</packageinstances>
</package3d>
<package3d name="1X02_LOCK_LONGPADS" urn="urn:adsk.eagle:package:32977309/1" type="box" library_version="4">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<packageinstances>
<packageinstance name="1X02_LOCK_LONGPADS"/>
</packageinstances>
</package3d>
<package3d name="SCREWTERMINAL-3.5MM-2_LOCK" urn="urn:adsk.eagle:package:32977310/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="SCREWTERMINAL-3.5MM-2_LOCK"/>
</packageinstances>
</package3d>
<package3d name="1X02_LONGPADS" urn="urn:adsk.eagle:package:32977311/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="1X02_LONGPADS"/>
</packageinstances>
</package3d>
<package3d name="1X02_NO_SILK" urn="urn:adsk.eagle:package:32977312/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="1X02_NO_SILK"/>
</packageinstances>
</package3d>
<package3d name="JST-2-PTH" urn="urn:adsk.eagle:package:32977313/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="JST-2-PTH"/>
</packageinstances>
</package3d>
<package3d name="1X02_XTRA_BIG" urn="urn:adsk.eagle:package:32977314/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="1X02_XTRA_BIG"/>
</packageinstances>
</package3d>
<package3d name="1X02_PP_HOLES_ONLY" urn="urn:adsk.eagle:package:32977315/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="1X02_PP_HOLES_ONLY"/>
</packageinstances>
</package3d>
<package3d name="SCREWTERMINAL-3.5MM-2-NS" urn="urn:adsk.eagle:package:32977316/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="SCREWTERMINAL-3.5MM-2-NS"/>
</packageinstances>
</package3d>
<package3d name="JST-2-PTH-NS" urn="urn:adsk.eagle:package:32977317/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="JST-2-PTH-NS"/>
</packageinstances>
</package3d>
<package3d name="JST-2-PTH-KIT" urn="urn:adsk.eagle:package:32977318/1" type="box" library_version="4">
<description>&lt;H3&gt;JST-2-PTH-KIT&lt;/h3&gt;
2-Pin JST, through-hole connector&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<packageinstances>
<packageinstance name="JST-2-PTH-KIT"/>
</packageinstances>
</package3d>
<package3d name="SPRINGTERMINAL-2.54MM-2" urn="urn:adsk.eagle:package:32977319/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="SPRINGTERMINAL-2.54MM-2"/>
</packageinstances>
</package3d>
<package3d name="1X02_2.54_SCREWTERM" urn="urn:adsk.eagle:package:32977320/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="1X02_2.54_SCREWTERM"/>
</packageinstances>
</package3d>
<package3d name="JST-2-PTH-VERT" urn="urn:adsk.eagle:package:32977321/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="JST-2-PTH-VERT"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="M02" urn="urn:adsk.eagle:symbol:32976834/1" library_version="4">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M02" urn="urn:adsk.eagle:component:32977579/2" prefix="JP" library_version="4">
<description>Standard 2-pin 0.1" header. Use with &lt;br&gt;
- straight break away headers ( PRT-00116)&lt;br&gt;
- right angle break away headers (PRT-00553)&lt;br&gt;
- swiss pins (PRT-00743)&lt;br&gt;
- machine pins (PRT-00117)&lt;br&gt;
- female headers (PRT-00115)&lt;br&gt;&lt;br&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08433</description>
<gates>
<gate name="G$1" symbol="M02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977286/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977285/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977284/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977283/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08352"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977282/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977281/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROCKER" package="R_SW_TH">
<connects>
<connect gate="G$1" pin="1" pad="P$3"/>
<connect gate="G$1" pin="2" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977280/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977279/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977278/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977308/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977309/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977310/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977311/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977312/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977313/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977314/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977315/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977316/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977317/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977318/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977319/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977320/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-2-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977321/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_FIDUCIALS" urn="urn:adsk.eagle:library:32977965">
<packages>
<package name="FIDUCIAL" urn="urn:adsk.eagle:footprint:32977966/1" library_version="3">
<text x="0" y="-1.74625" size="0.8" layer="21" font="vector" ratio="18" align="center">&gt;NAME</text>
<smd name="2" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" cream="no"/>
<circle x="0" y="0" radius="1" width="0" layer="39"/>
<circle x="0" y="0" radius="1" width="0" layer="41"/>
<circle x="0" y="0" radius="1" width="0" layer="29"/>
</package>
</packages>
<packages3d>
<package3d name="FIDUCIAL" urn="urn:adsk.eagle:package:32977968/2" type="empty" library_version="3">
<packageinstances>
<packageinstance name="FIDUCIAL"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="FIDUCIAL" urn="urn:adsk.eagle:symbol:32977967/1" library_version="3">
<circle x="0" y="0" radius="1.27" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIAL" urn="urn:adsk.eagle:component:32977970/2" prefix="FID" library_version="3">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32977968/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TYPE" value="FID"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="R8" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R9" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="AMP_B" library="BASTL_ICS" library_urn="urn:adsk.eagle:library:32977748" deviceset="MCP6002" device="CD" package3d_urn="urn:adsk.eagle:package:32977849/3" value="MCP6002"/>
<part name="R15" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R17" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R18" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1M"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R19" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R7" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="100"/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IN_5" library="BASTL_CONNECTORS" deviceset="THONKICONN" device="NEW"/>
<part name="IN_4" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="IN_3" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="IN_2" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="IN_1" library="BASTL_CONNECTORS" deviceset="THONKICONN" device="NEW"/>
<part name="MIDI_OUT" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN-STEREO" device="" package3d_urn="urn:adsk.eagle:package:32904285/2"/>
<part name="MIDI_IN" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN-STEREO" device="" package3d_urn="urn:adsk.eagle:package:32904285/2"/>
<part name="SYNTH/CV_B" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN-STEREO" device="" package3d_urn="urn:adsk.eagle:package:32904285/2"/>
<part name="B_CALIBRATE" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="B_INVERT" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="B_SHIFT" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="B_SYNTH_MODE" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="B_MUTE" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="B_8" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="B_7" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="IC4" library="BASTL_VOLTAGE REGULATORS" library_urn="urn:adsk.eagle:library:32975822" deviceset="78" device="D" package3d_urn="urn:adsk.eagle:package:32975835/1" technology="05" value="7805D"/>
<part name="IC5" library="BASTL_ICS" library_urn="urn:adsk.eagle:library:32977748" deviceset="74*595" device="D" package3d_urn="urn:adsk.eagle:package:32977843/2"/>
<part name="IC6" library="BASTL_ICS" library_urn="urn:adsk.eagle:library:32977748" deviceset="74*595" device="D" package3d_urn="urn:adsk.eagle:package:32977843/2"/>
<part name="IC7" library="BASTL_ICS" library_urn="urn:adsk.eagle:library:32977748" deviceset="74*595" device="D" package3d_urn="urn:adsk.eagle:package:32977843/2"/>
<part name="L_0" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_RED" value="LED_SMT_RED_1206_BOTTOM"/>
<part name="L_1" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_2" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_3" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_4" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_5" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_F" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_E" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_D" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_C" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_B" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_A" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="M1" library="BASTL_CONNECTORS" deviceset="MICROPHONE" device=""/>
<part name="PULLUP_2" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="SWITCH_SP3T_TOGGLE" device="" package3d_urn="urn:adsk.eagle:package:33626219/3" value="3_WAY"/>
<part name="PULLUP_3" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="SWITCH_SP3T_TOGGLE" device="" package3d_urn="urn:adsk.eagle:package:33626219/3"/>
<part name="PULLUP_4" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="SWITCH_SP3T_TOGGLE" device="" package3d_urn="urn:adsk.eagle:package:33626219/3"/>
<part name="PULLUP_5" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="SWITCH_SP3T_TOGGLE" device="" package3d_urn="urn:adsk.eagle:package:33626219/3"/>
<part name="GAIN_1" library="BASTL_POTS" library_urn="urn:adsk.eagle:library:32898787" deviceset="POT_ALPHA" device="POT_ALPHA" package3d_urn="urn:adsk.eagle:package:32898840/1" value="B100k"/>
<part name="THR" library="BASTL_POTS" library_urn="urn:adsk.eagle:library:32898787" deviceset="POT_ALPHA" device="POT_ALPHA" package3d_urn="urn:adsk.eagle:package:32898840/1" value="B100k"/>
<part name="OFFSET_3" library="BASTL_POTS" library_urn="urn:adsk.eagle:library:32898787" deviceset="POT_ALPHA" device="POT_ALPHA" package3d_urn="urn:adsk.eagle:package:32898840/1" value="B100k"/>
<part name="OFFSET_2" library="BASTL_POTS" library_urn="urn:adsk.eagle:library:32898787" deviceset="POT_ALPHA" device="POT_ALPHA" package3d_urn="urn:adsk.eagle:package:32898840/1" value="B100k"/>
<part name="GAIN_3" library="BASTL_POTS" library_urn="urn:adsk.eagle:library:32898787" deviceset="POT_ALPHA" device="POT_ALPHA" package3d_urn="urn:adsk.eagle:package:32898840/1" value="B100k"/>
<part name="GAIN_2" library="BASTL_POTS" library_urn="urn:adsk.eagle:library:32898787" deviceset="POT_ALPHA" device="POT_ALPHA" package3d_urn="urn:adsk.eagle:package:32898840/1" value="B100k"/>
<part name="IN_PIN_2" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="IN_PIN_3" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="IN_PIN_4" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="IN_PIN_5" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="AMP_M" library="BASTL_ICS" library_urn="urn:adsk.eagle:library:32977748" deviceset="MCP6002" device="CD" package3d_urn="urn:adsk.eagle:package:32977849/3" value="MCP6002"/>
<part name="R1" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R2" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1M"/>
<part name="R3" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R4" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R20" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R21" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R22" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1M"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R23" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R24" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="100"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R25" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R26" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1M"/>
<part name="R27" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R28" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1M"/>
<part name="R29" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="100"/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R30" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="100"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R31" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R32" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1M"/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R34" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="C1" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="R35" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="C2" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R36" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R37" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C3" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="C4" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="GND27" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R40" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="100"/>
<part name="R41" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="100"/>
<part name="SUPPLY22" library="supply2" deviceset="+5V" device=""/>
<part name="D2" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D3" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D4" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D5" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D6" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D7" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D8" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D9" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D10" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D11" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D12" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D13" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D14" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="SUPPLY23" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY24" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY25" library="supply2" deviceset="+5V" device=""/>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND28" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND29" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J1" library="BASTL_CONNECTORS" deviceset="PWR_CON_WITHOUT_LABLES" device=""/>
<part name="GND30" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="PTC" library="BASTL_FUSES" library_urn="urn:adsk.eagle:library:32977952" deviceset="PTC" device="1210" package3d_urn="urn:adsk.eagle:package:32977960/2" technology="500MA" value="PTC500MA1210"/>
<part name="D15" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N5819" device="" package3d_urn="urn:adsk.eagle:package:32978000/2"/>
<part name="C6" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="GND31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C9" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="U$1" library="arduino-micro" deviceset="ARDUINO_MICRO" device="FOOTPRINT"/>
<part name="R42" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R43" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R44" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="J2" library="BASTL_CONNECTORS" deviceset="PWR_JACK" device=""/>
<part name="GND35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R45" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="B_GATE_A" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="B_CV_A" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="L_G" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_H" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_INV" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="L_GATE" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R46" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="GND33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R47" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R48" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="GND36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R49" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R51" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R52" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="D16" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="ZENER_DIODE" device="" package3d_urn="urn:adsk.eagle:package:32978000/2" technology="5V1" value="5V1"/>
<part name="GND38" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R53" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="D18" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="ZENER_DIODE" device="" package3d_urn="urn:adsk.eagle:package:32978000/2" technology="5V1" value="5V1"/>
<part name="GND39" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C12" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="C13" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="C14" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="C15" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="R33" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R54" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R55" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R56" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R57" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R58" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R59" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R60" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R61" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R62" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R63" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R64" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R65" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R66" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R67" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND44" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND45" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND46" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND47" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND50" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND51" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND52" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND53" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D17" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D19" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="D20" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="I2C_HEADER" library="BASTL_PINHEADERS" deviceset="M04" device="LOCK"/>
<part name="GND54" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY28" library="supply2" deviceset="+5V" device=""/>
<part name="GND55" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY29" library="supply2" deviceset="+5V" device=""/>
<part name="GND56" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND57" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY30" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY31" library="supply2" deviceset="+5V" device=""/>
<part name="R68" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1M"/>
<part name="R69" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1M"/>
<part name="GND58" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND59" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND60" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC1" library="BASTL_ICS" library_urn="urn:adsk.eagle:library:32977748" deviceset="74*595" device="D" package3d_urn="urn:adsk.eagle:package:32977843/2"/>
<part name="GND61" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY35" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY36" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY37" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY38" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY39" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY2" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY6" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY10" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY1" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY7" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY9" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="4_CUT_FOR_1M" library="BASTL_JUMPERS" library_urn="urn:adsk.eagle:library:32977727" deviceset="SOLDERJUMPER" device="" package3d_urn="urn:adsk.eagle:package:32977733/2"/>
<part name="5_CUT_FOR_1M" library="BASTL_JUMPERS" library_urn="urn:adsk.eagle:library:32977727" deviceset="SOLDERJUMPER" device="" package3d_urn="urn:adsk.eagle:package:32977733/2"/>
<part name="SUPPLY11" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY12" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY15" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY16" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY26" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="GND24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY18" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY19" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="GND64" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY27" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="IC2" library="BASTL_VOLTAGE REGULATORS" library_urn="urn:adsk.eagle:library:32975822" deviceset="LF33CDT-TR" device="" package3d_urn="urn:adsk.eagle:package:32975835/1" value="78L33"/>
<part name="C16" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="C17" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="GND65" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY32" library="supply2" deviceset="+5V" device=""/>
<part name="GND68" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D24" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="1N4148" device="MULTI" package3d_urn="urn:adsk.eagle:package:32977997/1" value="1N4148"/>
<part name="R71" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="R72" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="10k"/>
<part name="SUPPLY33" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="R73" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="R70" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="SPI_HEADER" library="BASTL_PINHEADERS" deviceset="M06" device="LOCK"/>
<part name="U$5" library="Teensy_3_and_LC_Series_Boards_v1.4" deviceset="TEENSY_3.1-3.2_DIL" device=""/>
<part name="GND69" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND70" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY17" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY34" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="C19" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="C20" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="GND66" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND67" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="JP2" library="BASTL_PINHEADERS" deviceset="M01" device="PTH"/>
<part name="C21" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="B_1" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSHBUTTON_MOM_320.02" device="" package3d_urn="urn:adsk.eagle:package:32903785/2"/>
<part name="B_2" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSHBUTTON_MOM_320.02" device="" package3d_urn="urn:adsk.eagle:package:32903785/2"/>
<part name="B_3" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSHBUTTON_MOM_320.02" device="" package3d_urn="urn:adsk.eagle:package:32903785/2"/>
<part name="B_4" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSHBUTTON_MOM_320.02" device="" package3d_urn="urn:adsk.eagle:package:32903785/2"/>
<part name="B_5" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSHBUTTON_MOM_320.02" device="" package3d_urn="urn:adsk.eagle:package:32903785/2"/>
<part name="B_6" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSHBUTTON_MOM_320.02" device="" package3d_urn="urn:adsk.eagle:package:32903785/2"/>
<part name="L_6" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_RED" value="LED_SMT_RED_1206_BOTTOM"/>
<part name="R50" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="JP4" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="GND72" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY5" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY40" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY4" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="SUPPLY13" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+3V3" device=""/>
<part name="VOLTAGERANGE" library="BASTL_PINHEADERS" deviceset="M03" device="LOCK"/>
<part name="EXP" library="BASTL_PINHEADERS" deviceset="M03X2" device="LOCK"/>
<part name="GND73" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="1_READ" library="BASTL_PINHEADERS" deviceset="M03" device="LOCK"/>
<part name="R74" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="100k"/>
<part name="GND74" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C18" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="GND75" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C23" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="47p"/>
<part name="R75" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="C24" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="47p"/>
<part name="C25" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="47p"/>
<part name="C26" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C27" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0603" device="" package3d_urn="urn:adsk.eagle:package:32978072/2" value="100n"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D23" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="ZENER_DIODE" device="" package3d_urn="urn:adsk.eagle:package:32978000/2" technology="5V1" value="5V1"/>
<part name="D25" library="BASTL_DIODES" library_urn="urn:adsk.eagle:library:32977972" deviceset="ZENER_DIODE" device="" package3d_urn="urn:adsk.eagle:package:32978000/2" technology="5V1" value="5V1"/>
<part name="GATE_2" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="GATE_3" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="GATE_A" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="GATE_B" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="1_EF" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="R150" library="BASTL_RESISTORS" deviceset="RESISTOR0603" device="" value="1k"/>
<part name="R151" library="BASTL_RESISTORS" deviceset="RESISTOR0603" device="" value="100"/>
<part name="IC16" library="BASTL_ICS" deviceset="PS9117A" device="" value="TLP2368"/>
<part name="D21" library="BASTL_DIODES" deviceset="1N4148" device="" value="1N4148"/>
<part name="R152" library="BASTL_RESISTORS" deviceset="RESISTOR0603" device="" value="100"/>
<part name="SUPPLY97" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+5V" device=""/>
<part name="SUPPLY98" library="BASTL_SYMBOLS" library_urn="urn:adsk.eagle:library:32975873" deviceset="+5V" device=""/>
<part name="SUPPLY99" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R5" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="100k"/>
<part name="R6" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="100k"/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="CV_A" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="DIR_CV_3" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="DIR_CV_2" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="GND62" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND63" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND76" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="1_PREAMP" library="BASTL_CONNECTORS" library_urn="urn:adsk.eagle:library:32904192" deviceset="THONKICONN" device="NEW" package3d_urn="urn:adsk.eagle:package:32904283/2"/>
<part name="GND77" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R10" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="SUPPLY3" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY8" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY14" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY20" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY21" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY41" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY42" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY43" library="supply2" deviceset="+5V" device=""/>
<part name="L_GATE_B" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R11" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L_GATE_C" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R12" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND40" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L_OUT_D" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R13" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND41" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L_GATE_D" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R14" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND42" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L_OUT_E" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R16" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND43" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L_GATE_E" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R38" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND48" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L_OUT_C" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R39" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND49" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L_OUT_B" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R76" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND71" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L_EF" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R77" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND78" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L_AMP" library="BASTL_LEDS" library_urn="urn:adsk.eagle:library:32977677" deviceset="LED_SMT" device="_1206_BOTTOM" package3d_urn="urn:adsk.eagle:package:32977700/2" technology="_GREEN" value="LED_SMT_GREEN_1206_BOTTOM"/>
<part name="R78" library="BASTL_RESISTORS" library_urn="urn:adsk.eagle:library:32975925" deviceset="RESISTOR0603" device="" package3d_urn="urn:adsk.eagle:package:32975936/2" value="1k"/>
<part name="GND79" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="B_MIDI" library="BASTL_SWITCHES" library_urn="urn:adsk.eagle:library:32903743" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM" package3d_urn="urn:adsk.eagle:package:32903802/1" technology="_17MM" value="PUSH_MOMENTARY_17MM_THT_6X6MM"/>
<part name="LDR" library="BASTL_PINHEADERS" library_urn="urn:adsk.eagle:library:32976504" deviceset="M02" device="LOCK" package3d_urn="urn:adsk.eagle:package:32977278/2" value="LDR"/>
<part name="GND80" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="FID1" library="BASTL_FIDUCIALS" library_urn="urn:adsk.eagle:library:32977965" deviceset="FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:32977968/2"/>
<part name="FID2" library="BASTL_FIDUCIALS" library_urn="urn:adsk.eagle:library:32977965" deviceset="FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:32977968/2"/>
<part name="FID3" library="BASTL_FIDUCIALS" library_urn="urn:adsk.eagle:library:32977965" deviceset="FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:32977968/2"/>
<part name="FID4" library="BASTL_FIDUCIALS" library_urn="urn:adsk.eagle:library:32977965" deviceset="FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:32977968/2"/>
<part name="C5" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0805" device="" package3d_urn="urn:adsk.eagle:package:32978071/2" value="10uF"/>
<part name="C11" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0805" device="" package3d_urn="urn:adsk.eagle:package:32978071/2" value="1uF"/>
<part name="C29" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0805" device="" package3d_urn="urn:adsk.eagle:package:32978071/2" value="10uF"/>
<part name="C30" library="BASTL_CAPACITORS" library_urn="urn:adsk.eagle:library:32978038" deviceset="CAPACITOR0805" device="" package3d_urn="urn:adsk.eagle:package:32978071/2" value="10uF"/>
<part name="GND81" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND82" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND83" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND84" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="370.84" y="309.88" size="5.08" layer="91">connect everything to the microcontroller</text>
<text x="22.86" y="259.08" size="5.08" layer="91">is it possible to runn at 3.3V?</text>
<text x="370.84" y="375.92" size="5.08" layer="91">8-analog pins</text>
<text x="370.84" y="368.3" size="5.08" layer="91">3+2 pins = UI</text>
<text x="370.84" y="360.68" size="5.08" layer="91">2 CV/audio PWM pins</text>
<text x="370.84" y="317.5" size="5.08" layer="91">3 gate outs</text>
<text x="370.84" y="345.44" size="5.08" layer="91">2 pins for MIDI</text>
<text x="370.84" y="337.82" size="5.08" layer="91">3 pins SPI</text>
<text x="370.84" y="330.2" size="5.08" layer="91">2 pins I2C</text>
<text x="370.84" y="322.58" size="5.08" layer="91">22-25 pins</text>
</plain>
<instances>
<instance part="R8" gate="G$1" x="53.34" y="175.26" smashed="yes">
<attribute name="NAME" x="49.53" y="176.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="171.958" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="30.48" y="185.42" smashed="yes" rot="R270">
<attribute name="NAME" x="31.9786" y="189.23" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="27.178" y="189.23" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND10" gate="1" x="30.48" y="165.1" smashed="yes">
<attribute name="VALUE" x="27.94" y="162.56" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="7.62" y="160.02" smashed="yes">
<attribute name="NAME" x="3.81" y="161.5186" size="1.778" layer="95"/>
<attribute name="VALUE" x="3.81" y="156.718" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="-2.54" y="152.4" smashed="yes">
<attribute name="VALUE" x="-5.08" y="149.86" size="1.778" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="86.36" y="170.18" smashed="yes" rot="R180">
<attribute name="NAME" x="90.17" y="168.6814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="90.17" y="173.482" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND14" gate="1" x="91.44" y="162.56" smashed="yes">
<attribute name="VALUE" x="88.9" y="160.02" size="1.778" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="53.34" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="51.8414" y="151.13" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="56.642" y="151.13" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND15" gate="1" x="53.34" y="147.32" smashed="yes">
<attribute name="VALUE" x="50.8" y="144.78" size="1.778" layer="96"/>
</instance>
<instance part="R19" gate="G$1" x="43.18" y="175.26" smashed="yes" rot="R180">
<attribute name="NAME" x="46.99" y="173.7614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="46.99" y="178.562" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R7" gate="G$1" x="7.62" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="11.43" y="138.2014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="11.43" y="143.002" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND7" gate="1" x="2.54" y="134.62" smashed="yes">
<attribute name="VALUE" x="0" y="132.08" size="1.778" layer="96"/>
</instance>
<instance part="IN_5" gate="G$1" x="58.42" y="27.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="60.96" y="32.004" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="IN_4" gate="G$1" x="0" y="27.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="2.54" y="32.004" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="IN_3" gate="G$1" x="-5.08" y="88.9" smashed="yes" rot="MR0">
<attribute name="NAME" x="-2.54" y="92.964" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="IN_2" gate="G$1" x="-7.62" y="157.48" smashed="yes" rot="MR0">
<attribute name="NAME" x="-5.08" y="161.544" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="IN_1" gate="G$1" x="38.1" y="-38.1" smashed="yes" rot="MR0">
<attribute name="NAME" x="40.64" y="-34.036" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="MIDI_OUT" gate="G$1" x="279.4" y="165.1" smashed="yes" rot="R180">
<attribute name="NAME" x="284.48" y="162.052" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="MIDI_IN" gate="G$1" x="157.48" y="172.72" smashed="yes" rot="MR180">
<attribute name="NAME" x="152.4" y="169.672" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SYNTH/CV_B" gate="G$1" x="200.66" y="33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="205.74" y="29.972" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="B_CALIBRATE" gate="G$2" x="419.1" y="-20.32" smashed="yes">
<attribute name="NAME" x="416.56" y="-17.78" size="1.778" layer="95"/>
</instance>
<instance part="B_INVERT" gate="G$2" x="419.1" y="-27.94" smashed="yes">
<attribute name="NAME" x="416.56" y="-25.4" size="1.778" layer="95"/>
</instance>
<instance part="B_SHIFT" gate="G$2" x="419.1" y="-35.56" smashed="yes">
<attribute name="NAME" x="416.56" y="-33.02" size="1.778" layer="95"/>
</instance>
<instance part="B_SYNTH_MODE" gate="G$2" x="419.1" y="-43.18" smashed="yes">
<attribute name="NAME" x="416.56" y="-40.64" size="1.778" layer="95"/>
</instance>
<instance part="B_MUTE" gate="G$2" x="419.1" y="-50.8" smashed="yes">
<attribute name="NAME" x="416.56" y="-48.26" size="1.778" layer="95"/>
</instance>
<instance part="B_8" gate="G$2" x="419.1" y="-58.42" smashed="yes">
<attribute name="NAME" x="416.56" y="-55.88" size="1.778" layer="95"/>
</instance>
<instance part="B_7" gate="G$2" x="419.1" y="-66.04" smashed="yes">
<attribute name="NAME" x="416.56" y="-63.5" size="1.778" layer="95"/>
</instance>
<instance part="IC4" gate="G$1" x="30.48" y="241.3" smashed="yes">
<attribute name="NAME" x="33.02" y="233.68" size="1.778" layer="95"/>
<attribute name="VALUE" x="33.02" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="IC5" gate="A" x="360.68" y="111.76" smashed="yes">
<attribute name="NAME" x="353.06" y="125.095" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="353.06" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="IC6" gate="A" x="360.68" y="63.5" smashed="yes">
<attribute name="NAME" x="353.06" y="76.835" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="353.06" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="IC7" gate="A" x="360.68" y="17.78" smashed="yes">
<attribute name="NAME" x="353.06" y="31.115" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="358.14" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="L_0" gate="G$1" x="398.78" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="163.576" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="163.195" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_1" gate="G$1" x="398.78" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="153.416" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="155.575" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_2" gate="G$1" x="398.78" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="145.796" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="147.955" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_3" gate="G$1" x="398.78" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="138.176" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="140.335" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_4" gate="G$1" x="403.86" y="127" smashed="yes" rot="R270">
<attribute name="NAME" x="399.288" y="123.444" size="1.778" layer="95"/>
<attribute name="VALUE" x="399.288" y="121.285" size="1.778" layer="96"/>
</instance>
<instance part="L_5" gate="G$1" x="403.86" y="119.38" smashed="yes" rot="R270">
<attribute name="NAME" x="399.288" y="115.824" size="1.778" layer="95"/>
<attribute name="VALUE" x="399.288" y="113.665" size="1.778" layer="96"/>
</instance>
<instance part="L_F" gate="G$1" x="398.78" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="394.208" y="47.244" size="1.778" layer="95"/>
<attribute name="VALUE" x="394.208" y="45.085" size="1.778" layer="96"/>
</instance>
<instance part="L_E" gate="G$1" x="398.78" y="58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="394.208" y="54.864" size="1.778" layer="95"/>
<attribute name="VALUE" x="394.208" y="52.705" size="1.778" layer="96"/>
</instance>
<instance part="L_D" gate="G$1" x="398.78" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="69.596" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="71.755" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_C" gate="G$1" x="398.78" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="77.216" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="79.375" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_B" gate="G$1" x="398.78" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="84.836" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="86.995" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_A" gate="G$1" x="398.78" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="92.456" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="94.615" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="M1" gate="G$1" x="55.88" y="-45.72" smashed="yes" rot="R90"/>
<instance part="PULLUP_2" gate="G$1" x="17.78" y="172.72" smashed="yes">
<attribute name="NAME" x="10.16" y="170.815" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="PULLUP_3" gate="G$1" x="17.78" y="101.6" smashed="yes">
<attribute name="NAME" x="22.86" y="99.695" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="PULLUP_4" gate="G$1" x="10.16" y="35.56" smashed="yes">
<attribute name="NAME" x="15.24" y="33.655" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="PULLUP_5" gate="G$1" x="68.58" y="35.56" smashed="yes">
<attribute name="NAME" x="73.66" y="33.655" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="GAIN_1" gate="G$1" x="96.52" y="-25.4" smashed="yes" rot="MR90">
<attribute name="NAME" x="92.71" y="-31.369" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="92.71" y="-29.21" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="THR" gate="G$1" x="180.34" y="2.54" smashed="yes">
<attribute name="NAME" x="174.371" y="-1.27" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="176.53" y="-1.27" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="OFFSET_3" gate="G$1" x="30.48" y="106.68" smashed="yes">
<attribute name="NAME" x="24.511" y="102.87" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.67" y="102.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="OFFSET_2" gate="G$1" x="30.48" y="175.26" smashed="yes">
<attribute name="NAME" x="27.051" y="179.07" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.67" y="171.45" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GAIN_3" gate="G$1" x="63.5" y="106.68" smashed="yes" rot="MR90">
<attribute name="NAME" x="59.69" y="100.711" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="59.69" y="102.87" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GAIN_2" gate="G$1" x="63.5" y="175.26" smashed="yes" rot="MR90">
<attribute name="NAME" x="59.69" y="169.291" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="59.69" y="171.45" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="IN_PIN_2" gate="G$1" x="-5.08" y="139.7" smashed="yes">
<attribute name="NAME" x="-7.62" y="137.16" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="IN_PIN_3" gate="G$1" x="-2.54" y="71.12" smashed="yes">
<attribute name="NAME" x="-5.08" y="68.58" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="IN_PIN_4" gate="G$1" x="2.54" y="12.7" smashed="yes">
<attribute name="NAME" x="0" y="10.16" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="IN_PIN_5" gate="G$1" x="60.96" y="12.7" smashed="yes">
<attribute name="NAME" x="58.42" y="10.16" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="AMP_M" gate="P" x="162.56" y="238.76" smashed="yes">
<attribute name="NAME" x="160.655" y="237.998" size="1.778" layer="95"/>
</instance>
<instance part="R1" gate="G$1" x="15.24" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="13.7414" y="179.07" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="13.462" y="184.15" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="20.32" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="18.8214" y="179.07" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="18.542" y="184.15" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="53.34" y="106.68" smashed="yes">
<attribute name="NAME" x="49.53" y="108.1786" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="103.378" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="30.48" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="31.9786" y="120.65" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="27.178" y="120.65" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND1" gate="1" x="30.48" y="96.52" smashed="yes">
<attribute name="VALUE" x="27.94" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="R20" gate="G$1" x="12.7" y="91.44" smashed="yes">
<attribute name="NAME" x="8.89" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.81" y="88.138" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="0" y="83.82" smashed="yes">
<attribute name="VALUE" x="-2.54" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="R21" gate="G$1" x="86.36" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="90.17" y="100.1014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="90.17" y="104.902" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND4" gate="1" x="91.44" y="93.98" smashed="yes">
<attribute name="VALUE" x="88.9" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="48.26" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="46.7614" y="82.55" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="51.562" y="82.55" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="53.34" y="78.74" smashed="yes">
<attribute name="VALUE" x="50.8" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="43.18" y="106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="46.99" y="105.1814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="46.99" y="109.982" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R24" gate="G$1" x="10.16" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="13.97" y="69.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="13.97" y="74.422" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND6" gate="1" x="5.08" y="66.04" smashed="yes">
<attribute name="VALUE" x="2.54" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="15.24" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="13.7414" y="107.95" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="18.542" y="107.95" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R26" gate="G$1" x="20.32" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="18.8214" y="107.95" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="23.622" y="107.95" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R27" gate="G$1" x="7.62" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="6.1214" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.922" y="41.91" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R28" gate="G$1" x="12.7" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="11.2014" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="16.002" y="41.91" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R29" gate="G$1" x="15.24" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="19.05" y="11.2014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="19.05" y="16.002" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND8" gate="1" x="10.16" y="7.62" smashed="yes">
<attribute name="VALUE" x="7.62" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="5.08" y="22.86" smashed="yes">
<attribute name="VALUE" x="2.54" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="63.5" y="22.86" smashed="yes">
<attribute name="VALUE" x="60.96" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="73.66" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="77.47" y="11.2014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="77.47" y="16.002" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND16" gate="1" x="68.58" y="7.62" smashed="yes">
<attribute name="VALUE" x="66.04" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="R31" gate="G$1" x="66.04" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="64.5414" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="69.342" y="41.91" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R32" gate="G$1" x="71.12" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="69.6214" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="74.422" y="46.99" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND19" gate="1" x="180.34" y="-5.08" smashed="yes">
<attribute name="VALUE" x="177.8" y="-7.62" size="1.778" layer="96"/>
</instance>
<instance part="R34" gate="G$1" x="175.26" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="179.07" y="28.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="179.07" y="33.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="180.34" y="25.4" smashed="yes">
<attribute name="NAME" x="181.864" y="25.781" size="1.778" layer="95"/>
<attribute name="VALUE" x="181.864" y="20.701" size="1.778" layer="96"/>
</instance>
<instance part="R35" gate="G$1" x="180.34" y="76.2" smashed="yes" rot="R180">
<attribute name="NAME" x="184.15" y="74.7014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="184.15" y="79.502" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C2" gate="G$1" x="190.5" y="71.12" smashed="yes">
<attribute name="NAME" x="192.024" y="71.501" size="1.778" layer="95"/>
<attribute name="VALUE" x="192.024" y="66.421" size="1.778" layer="96"/>
</instance>
<instance part="GND20" gate="1" x="190.5" y="63.5" smashed="yes">
<attribute name="VALUE" x="187.96" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND21" gate="1" x="180.34" y="17.78" smashed="yes">
<attribute name="VALUE" x="177.8" y="15.24" size="1.778" layer="96"/>
</instance>
<instance part="R36" gate="G$1" x="185.42" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="189.23" y="28.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="189.23" y="33.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R37" gate="G$1" x="185.42" y="35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="189.23" y="34.0614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="189.23" y="38.862" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND22" gate="1" x="200.66" y="66.04" smashed="yes">
<attribute name="VALUE" x="198.12" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="GND23" gate="1" x="195.58" y="27.94" smashed="yes">
<attribute name="VALUE" x="193.04" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="AMP_B" gate="P" x="152.4" y="238.76" smashed="yes">
<attribute name="NAME" x="150.495" y="237.998" size="1.778" layer="95"/>
</instance>
<instance part="C3" gate="G$1" x="144.78" y="238.76" smashed="yes">
<attribute name="NAME" x="146.304" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="146.304" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="157.48" y="238.76" smashed="yes">
<attribute name="NAME" x="159.004" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="159.004" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="GND27" gate="1" x="274.32" y="160.02" smashed="yes">
<attribute name="VALUE" x="271.78" y="157.48" size="1.778" layer="96"/>
</instance>
<instance part="R40" gate="G$1" x="269.24" y="165.1" smashed="yes" rot="R180">
<attribute name="NAME" x="273.05" y="163.6014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="273.05" y="168.402" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R41" gate="G$1" x="269.24" y="177.8" smashed="yes" rot="R180">
<attribute name="NAME" x="273.05" y="176.3014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="273.05" y="181.102" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY22" gate="+5V" x="264.16" y="167.64" smashed="yes">
<attribute name="VALUE" x="262.255" y="170.815" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="429.26" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="27.4574" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="30.2514" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D3" gate="G$1" x="429.26" y="20.32" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="19.8374" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="22.6314" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D4" gate="G$1" x="429.26" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="12.2174" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="15.0114" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D5" gate="G$1" x="429.26" y="5.08" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="4.5974" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="7.3914" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D6" gate="G$1" x="429.26" y="-2.54" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-3.0226" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-0.2286" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D7" gate="G$1" x="429.26" y="-10.16" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-10.6426" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-7.8486" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D8" gate="G$1" x="429.26" y="-20.32" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-20.8026" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-18.0086" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D9" gate="G$1" x="429.26" y="-27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-28.4226" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-25.6286" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D10" gate="G$1" x="429.26" y="-35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-36.0426" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-33.2486" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D11" gate="G$1" x="429.26" y="-43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-43.6626" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-40.8686" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D12" gate="G$1" x="429.26" y="-50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-51.2826" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-48.4886" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D13" gate="G$1" x="429.26" y="-58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-58.9026" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-56.1086" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D14" gate="G$1" x="429.26" y="-66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-66.5226" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-63.7286" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY23" gate="+5V" x="345.44" y="66.04" smashed="yes" rot="R90">
<attribute name="VALUE" x="342.265" y="64.135" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY24" gate="+5V" x="345.44" y="20.32" smashed="yes" rot="R90">
<attribute name="VALUE" x="342.265" y="18.415" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY25" gate="+5V" x="345.44" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="342.265" y="112.395" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND25" gate="1" x="347.98" y="96.52" smashed="yes">
<attribute name="VALUE" x="345.44" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="GND28" gate="1" x="347.98" y="48.26" smashed="yes">
<attribute name="VALUE" x="345.44" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="GND29" gate="1" x="347.98" y="2.54" smashed="yes">
<attribute name="VALUE" x="345.44" y="0" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="-12.7" y="236.22" smashed="yes">
<attribute name="VALUE" x="-30.48" y="226.06" size="1.778" layer="96"/>
<attribute name="NAME" x="-15.24" y="244.602" size="1.778" layer="95"/>
</instance>
<instance part="GND30" gate="1" x="0" y="233.68" smashed="yes">
<attribute name="VALUE" x="-2.54" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="PTC" gate="G$1" x="0" y="241.3" smashed="yes">
<attribute name="NAME" x="-2.54" y="244.348" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.302" y="236.22" size="1.778" layer="96"/>
</instance>
<instance part="D15" gate="G$1" x="7.62" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="7.1374" y="241.3" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="9.9314" y="241.3" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="12.7" y="238.76" smashed="yes">
<attribute name="NAME" x="14.224" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="14.224" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="GND31" gate="1" x="30.48" y="231.14" smashed="yes">
<attribute name="VALUE" x="27.94" y="228.6" size="1.778" layer="96"/>
</instance>
<instance part="GND32" gate="1" x="7.62" y="233.68" smashed="yes">
<attribute name="VALUE" x="5.08" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="43.18" y="238.76" smashed="yes">
<attribute name="NAME" x="44.704" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="44.704" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="U$1" gate="G$1" x="454.66" y="276.86" smashed="yes">
<attribute name="NAME" x="442.595" y="302.133" size="1.27" layer="95"/>
<attribute name="VALUE" x="442.1505" y="251.7775" size="1.27" layer="95"/>
</instance>
<instance part="R42" gate="G$1" x="81.28" y="152.4" smashed="yes" rot="R180">
<attribute name="NAME" x="85.09" y="150.9014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="85.09" y="155.702" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R43" gate="G$1" x="86.36" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="90.17" y="82.3214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="90.17" y="87.122" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R44" gate="G$1" x="190.5" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="194.31" y="56.9214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="194.31" y="61.722" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J2" gate="G$1" x="-20.32" y="259.08" smashed="yes">
<attribute name="NAME" x="-27.94" y="264.16" size="1.778" layer="95"/>
<attribute name="VALUE" x="-27.94" y="254" size="1.778" layer="96"/>
</instance>
<instance part="GND35" gate="1" x="-15.24" y="254" smashed="yes">
<attribute name="VALUE" x="-17.78" y="251.46" size="1.778" layer="96"/>
</instance>
<instance part="R45" gate="G$1" x="408.94" y="157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="155.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="410.21" y="163.322" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="B_GATE_A" gate="G$2" x="419.1" y="-73.66" smashed="yes">
<attribute name="NAME" x="416.56" y="-71.12" size="1.778" layer="95"/>
</instance>
<instance part="B_CV_A" gate="G$2" x="419.1" y="-81.28" smashed="yes">
<attribute name="NAME" x="416.56" y="-78.74" size="1.778" layer="95"/>
</instance>
<instance part="L_G" gate="G$1" x="398.78" y="43.18" smashed="yes" rot="R270">
<attribute name="NAME" x="394.208" y="39.624" size="1.778" layer="95"/>
<attribute name="VALUE" x="394.208" y="37.465" size="1.778" layer="96"/>
</instance>
<instance part="L_H" gate="G$1" x="398.78" y="35.56" smashed="yes" rot="R270">
<attribute name="NAME" x="394.208" y="32.004" size="1.778" layer="95"/>
<attribute name="VALUE" x="394.208" y="29.845" size="1.778" layer="96"/>
</instance>
<instance part="L_INV" gate="G$1" x="388.62" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="393.192" y="186.436" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="393.192" y="188.595" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_GATE" gate="G$1" x="403.86" y="104.14" smashed="yes" rot="R270">
<attribute name="NAME" x="399.288" y="100.584" size="1.778" layer="95"/>
<attribute name="VALUE" x="399.288" y="98.425" size="1.778" layer="96"/>
</instance>
<instance part="R46" gate="G$1" x="48.26" y="-33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="46.7614" y="-36.83" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="51.562" y="-36.83" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND33" gate="1" x="43.18" y="-43.18" smashed="yes">
<attribute name="VALUE" x="40.64" y="-45.72" size="1.778" layer="96"/>
</instance>
<instance part="GND34" gate="1" x="55.88" y="-55.88" smashed="yes">
<attribute name="VALUE" x="53.34" y="-58.42" size="1.778" layer="96"/>
</instance>
<instance part="R47" gate="G$1" x="73.66" y="-50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="77.47" y="-52.2986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="77.47" y="-47.498" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R48" gate="G$1" x="83.82" y="-50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="87.63" y="-52.2986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="87.63" y="-47.498" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND36" gate="1" x="88.9" y="-53.34" smashed="yes">
<attribute name="VALUE" x="86.36" y="-55.88" size="1.778" layer="96"/>
</instance>
<instance part="R49" gate="G$1" x="68.58" y="-35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="72.39" y="-37.0586" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="72.39" y="-32.258" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R51" gate="G$1" x="106.68" y="-38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="105.41" y="-39.5986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="110.49" y="-34.798" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND37" gate="1" x="137.16" y="-45.72" smashed="yes">
<attribute name="VALUE" x="134.62" y="-48.26" size="1.778" layer="96"/>
</instance>
<instance part="R52" gate="G$1" x="22.86" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="26.67" y="28.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="26.67" y="33.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D16" gate="G$1" x="30.48" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="28.575" y="26.162" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="33.909" y="26.162" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND38" gate="1" x="30.48" y="22.86" smashed="yes">
<attribute name="VALUE" x="27.94" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="R53" gate="G$1" x="81.28" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="85.09" y="28.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="85.09" y="33.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D18" gate="G$1" x="86.36" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="84.455" y="26.162" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="89.789" y="26.162" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND39" gate="1" x="86.36" y="22.86" smashed="yes">
<attribute name="VALUE" x="83.82" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="497.84" y="269.24" smashed="yes">
<attribute name="NAME" x="499.364" y="269.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="499.364" y="264.541" size="1.778" layer="96"/>
</instance>
<instance part="IC5" gate="P" x="66.04" y="233.68" smashed="yes">
<attribute name="NAME" x="65.405" y="233.045" size="1.778" layer="95"/>
</instance>
<instance part="IC6" gate="P" x="76.2" y="233.68" smashed="yes">
<attribute name="NAME" x="75.565" y="233.045" size="1.778" layer="95"/>
</instance>
<instance part="IC7" gate="P" x="86.36" y="233.68" smashed="yes">
<attribute name="NAME" x="85.725" y="233.045" size="1.778" layer="95"/>
</instance>
<instance part="C13" gate="G$1" x="60.96" y="236.22" smashed="yes">
<attribute name="NAME" x="62.484" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="62.484" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="71.12" y="236.22" smashed="yes">
<attribute name="NAME" x="72.644" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.644" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="C15" gate="G$1" x="81.28" y="236.22" smashed="yes">
<attribute name="NAME" x="82.804" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="82.804" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="R33" gate="G$1" x="408.94" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="148.3614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="153.162" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R54" gate="G$1" x="408.94" y="142.24" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="140.7414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="145.542" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R55" gate="G$1" x="408.94" y="134.62" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="133.1214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="137.922" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R56" gate="G$1" x="416.56" y="127" smashed="yes" rot="R180">
<attribute name="VALUE" x="420.37" y="130.302" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R57" gate="G$1" x="416.56" y="119.38" smashed="yes" rot="R180"/>
<instance part="R58" gate="G$1" x="408.94" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="87.4014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="410.21" y="92.202" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R59" gate="G$1" x="408.94" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="79.7814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="84.582" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R60" gate="G$1" x="408.94" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="72.1614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="76.962" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R61" gate="G$1" x="408.94" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="64.5414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="69.342" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R62" gate="G$1" x="408.94" y="58.42" smashed="yes" rot="R180">
<attribute name="VALUE" x="412.75" y="61.722" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R63" gate="G$1" x="408.94" y="50.8" smashed="yes" rot="R180"/>
<instance part="R64" gate="G$1" x="408.94" y="43.18" smashed="yes" rot="R180"/>
<instance part="R65" gate="G$1" x="408.94" y="35.56" smashed="yes" rot="R180"/>
<instance part="R66" gate="G$1" x="416.56" y="104.14" smashed="yes" rot="R180">
<attribute name="VALUE" x="415.29" y="109.982" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R67" gate="G$1" x="398.78" y="182.88" smashed="yes" rot="R180">
<attribute name="NAME" x="402.59" y="181.3814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="402.59" y="186.182" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND17" gate="1" x="403.86" y="180.34" smashed="yes">
<attribute name="VALUE" x="401.32" y="177.8" size="1.778" layer="96"/>
</instance>
<instance part="GND44" gate="1" x="414.02" y="63.5" smashed="yes">
<attribute name="VALUE" x="411.48" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND45" gate="1" x="414.02" y="71.12" smashed="yes">
<attribute name="VALUE" x="411.48" y="68.58" size="1.778" layer="96"/>
</instance>
<instance part="GND46" gate="1" x="414.02" y="78.74" smashed="yes">
<attribute name="VALUE" x="411.48" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="GND47" gate="1" x="414.02" y="86.36" smashed="yes">
<attribute name="VALUE" x="411.48" y="83.82" size="1.778" layer="96"/>
</instance>
<instance part="GND50" gate="1" x="414.02" y="132.08" smashed="yes">
<attribute name="VALUE" x="411.48" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND51" gate="1" x="414.02" y="139.7" smashed="yes">
<attribute name="VALUE" x="411.48" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="GND52" gate="1" x="414.02" y="147.32" smashed="yes">
<attribute name="VALUE" x="411.48" y="144.78" size="1.778" layer="96"/>
</instance>
<instance part="GND53" gate="1" x="414.02" y="154.94" smashed="yes">
<attribute name="VALUE" x="411.48" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="D17" gate="G$1" x="429.26" y="-73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-74.1426" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-71.3486" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D19" gate="G$1" x="429.26" y="-81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-81.7626" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-78.9686" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D20" gate="G$1" x="429.26" y="-88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-89.3826" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-86.5886" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="I2C_HEADER" gate="G$1" x="185.42" y="238.76" smashed="yes">
<attribute name="VALUE" x="180.34" y="231.14" size="1.778" layer="96"/>
<attribute name="NAME" x="180.34" y="247.142" size="1.778" layer="95"/>
</instance>
<instance part="GND54" gate="1" x="190.5" y="205.74" smashed="yes">
<attribute name="VALUE" x="187.96" y="203.2" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY28" gate="+5V" x="198.12" y="210.82" smashed="yes" rot="R270">
<attribute name="VALUE" x="201.295" y="212.725" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND55" gate="1" x="190.5" y="233.68" smashed="yes">
<attribute name="VALUE" x="187.96" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY29" gate="+5V" x="198.12" y="238.76" smashed="yes" rot="R270">
<attribute name="VALUE" x="201.295" y="240.665" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND56" gate="1" x="434.34" y="284.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="431.8" y="287.02" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND57" gate="1" x="474.98" y="289.56" smashed="yes" rot="R90">
<attribute name="VALUE" x="477.52" y="287.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY30" gate="+5V" x="490.22" y="284.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="493.395" y="286.385" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY31" gate="+5V" x="502.92" y="271.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="506.095" y="273.685" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R68" gate="G$1" x="185.42" y="-35.56" smashed="yes" rot="R270">
<attribute name="NAME" x="186.9186" y="-31.75" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="182.118" y="-31.75" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R69" gate="G$1" x="185.42" y="-66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="186.9186" y="-62.23" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="182.118" y="-62.23" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND58" gate="1" x="185.42" y="-48.26" smashed="yes">
<attribute name="VALUE" x="182.88" y="-50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND59" gate="1" x="185.42" y="-78.74" smashed="yes">
<attribute name="VALUE" x="182.88" y="-81.28" size="1.778" layer="96"/>
</instance>
<instance part="GND60" gate="1" x="497.84" y="261.62" smashed="yes">
<attribute name="VALUE" x="495.3" y="259.08" size="1.778" layer="96"/>
</instance>
<instance part="IC1" gate="A" x="358.14" y="172.72" smashed="yes">
<attribute name="NAME" x="350.52" y="186.055" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="355.6" y="175.26" size="1.778" layer="96"/>
</instance>
<instance part="GND61" gate="1" x="345.44" y="157.48" smashed="yes">
<attribute name="VALUE" x="342.9" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY35" gate="+5V" x="340.36" y="175.26" smashed="yes" rot="R90">
<attribute name="VALUE" x="337.185" y="173.355" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY36" gate="G$1" x="66.04" y="55.88" smashed="yes">
<attribute name="VALUE" x="64.135" y="59.055" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY37" gate="G$1" x="71.12" y="63.5" smashed="yes">
<attribute name="VALUE" x="69.215" y="66.675" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY38" gate="G$1" x="7.62" y="55.88" smashed="yes">
<attribute name="VALUE" x="5.715" y="59.055" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY39" gate="G$1" x="12.7" y="63.5" smashed="yes">
<attribute name="VALUE" x="10.795" y="66.675" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="15.24" y="127" smashed="yes">
<attribute name="VALUE" x="13.335" y="130.175" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="20.32" y="127" smashed="yes">
<attribute name="VALUE" x="18.415" y="130.175" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY10" gate="G$1" x="30.48" y="127" smashed="yes">
<attribute name="VALUE" x="28.575" y="130.175" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="15.24" y="193.04" smashed="yes">
<attribute name="VALUE" x="13.335" y="196.215" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="G$1" x="20.32" y="195.58" smashed="yes">
<attribute name="VALUE" x="18.415" y="198.755" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="G$1" x="30.48" y="195.58" smashed="yes">
<attribute name="VALUE" x="28.575" y="198.755" size="1.778" layer="96"/>
</instance>
<instance part="4_CUT_FOR_1M" gate="G$1" x="22.86" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="20.32" y="43.18" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="27.94" y="27.94" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="5_CUT_FOR_1M" gate="G$1" x="81.28" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="78.74" y="43.18" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="86.36" y="43.18" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY11" gate="G$1" x="48.26" y="-20.32" smashed="yes">
<attribute name="VALUE" x="46.355" y="-17.145" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY12" gate="G$1" x="68.58" y="-45.72" smashed="yes">
<attribute name="VALUE" x="66.675" y="-42.545" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY15" gate="G$1" x="185.42" y="-25.4" smashed="yes">
<attribute name="VALUE" x="183.515" y="-22.225" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY16" gate="G$1" x="185.42" y="-55.88" smashed="yes">
<attribute name="VALUE" x="183.515" y="-52.705" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY26" gate="G$1" x="180.34" y="12.7" smashed="yes">
<attribute name="VALUE" x="178.435" y="15.875" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="81.28" y="223.52" smashed="yes">
<attribute name="VALUE" x="78.74" y="220.98" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY18" gate="+5V" x="81.28" y="243.84" smashed="yes">
<attribute name="VALUE" x="79.375" y="247.015" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY19" gate="G$1" x="144.78" y="248.92" smashed="yes">
<attribute name="VALUE" x="147.955" y="252.095" size="1.778" layer="96"/>
</instance>
<instance part="GND64" gate="1" x="144.78" y="228.6" smashed="yes">
<attribute name="VALUE" x="142.24" y="226.06" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY27" gate="G$1" x="490.22" y="261.62" smashed="yes" rot="R270">
<attribute name="VALUE" x="493.395" y="263.525" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="IC2" gate="G$1" x="114.3" y="243.84" smashed="yes">
<attribute name="NAME" x="116.84" y="236.22" size="1.778" layer="95"/>
<attribute name="VALUE" x="116.84" y="233.68" size="1.778" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="106.68" y="238.76" smashed="yes">
<attribute name="NAME" x="108.204" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="108.204" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="121.92" y="238.76" smashed="yes">
<attribute name="NAME" x="123.444" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="123.444" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="GND65" gate="1" x="114.3" y="231.14" smashed="yes">
<attribute name="VALUE" x="111.76" y="228.6" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY32" gate="+5V" x="139.7" y="259.08" smashed="yes">
<attribute name="VALUE" x="137.795" y="262.255" size="1.778" layer="96"/>
</instance>
<instance part="GND68" gate="1" x="106.68" y="-78.74" smashed="yes">
<attribute name="VALUE" x="104.14" y="-81.28" size="1.778" layer="96"/>
</instance>
<instance part="D24" gate="G$1" x="99.06" y="-66.04" smashed="yes">
<attribute name="NAME" x="101.6" y="-65.5574" size="1.778" layer="95"/>
<attribute name="VALUE" x="101.6" y="-68.3514" size="1.778" layer="96"/>
</instance>
<instance part="R71" gate="G$1" x="111.76" y="-55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="115.57" y="-57.3786" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="115.57" y="-52.578" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R72" gate="G$1" x="101.6" y="-55.88" smashed="yes">
<attribute name="NAME" x="97.79" y="-54.3814" size="1.778" layer="95"/>
<attribute name="VALUE" x="97.79" y="-59.182" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY33" gate="G$1" x="96.52" y="-48.26" smashed="yes">
<attribute name="VALUE" x="94.615" y="-45.085" size="1.778" layer="96"/>
</instance>
<instance part="R73" gate="G$1" x="137.16" y="-63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="143.51" y="-64.9986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="140.97" y="-60.198" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R70" gate="G$1" x="93.98" y="-60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="95.4786" y="-57.15" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="90.678" y="-57.15" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SPI_HEADER" gate="G$1" x="185.42" y="213.36" smashed="yes">
<attribute name="VALUE" x="180.34" y="203.2" size="1.778" layer="96"/>
<attribute name="NAME" x="180.34" y="224.282" size="1.778" layer="95"/>
</instance>
<instance part="U$5" gate="G$1" x="360.68" y="271.78" smashed="yes">
<attribute name="NAME" x="355.092" y="303.53" size="1.27" layer="95" font="vector" ratio="15"/>
<attribute name="VALUE" x="353.568" y="230.886" size="1.27" layer="96" font="vector" ratio="15"/>
</instance>
<instance part="GND69" gate="1" x="383.54" y="274.32" smashed="yes">
<attribute name="VALUE" x="381" y="271.78" size="1.778" layer="96"/>
</instance>
<instance part="GND70" gate="1" x="383.54" y="287.02" smashed="yes">
<attribute name="VALUE" x="381" y="284.48" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY17" gate="+5V" x="396.24" y="297.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="399.415" y="299.085" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY34" gate="G$1" x="401.32" y="294.64" smashed="yes" rot="R270">
<attribute name="VALUE" x="404.495" y="296.545" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C19" gate="G$1" x="388.62" y="294.64" smashed="yes">
<attribute name="NAME" x="390.144" y="295.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="390.144" y="289.941" size="1.778" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="393.7" y="292.1" smashed="yes">
<attribute name="NAME" x="395.224" y="292.481" size="1.778" layer="95"/>
<attribute name="VALUE" x="395.224" y="287.401" size="1.778" layer="96"/>
</instance>
<instance part="GND66" gate="1" x="388.62" y="287.02" smashed="yes">
<attribute name="VALUE" x="386.08" y="284.48" size="1.778" layer="96"/>
</instance>
<instance part="GND67" gate="1" x="393.7" y="284.48" smashed="yes">
<attribute name="VALUE" x="391.16" y="281.94" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="G$1" x="375.92" y="264.16" smashed="yes">
<attribute name="VALUE" x="373.38" y="259.08" size="1.778" layer="96"/>
<attribute name="NAME" x="373.38" y="267.462" size="1.778" layer="95"/>
</instance>
<instance part="IC1" gate="P" x="96.52" y="233.68" smashed="yes">
<attribute name="NAME" x="95.885" y="233.045" size="1.778" layer="95"/>
</instance>
<instance part="C21" gate="G$1" x="91.44" y="236.22" smashed="yes">
<attribute name="NAME" x="92.964" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.964" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="B_1" gate="G$1" x="419.1" y="27.94" smashed="yes">
<attribute name="NAME" x="416.56" y="30.48" size="1.778" layer="95"/>
</instance>
<instance part="B_2" gate="G$1" x="419.1" y="20.32" smashed="yes">
<attribute name="NAME" x="416.56" y="22.86" size="1.778" layer="95"/>
</instance>
<instance part="B_3" gate="G$1" x="419.1" y="12.7" smashed="yes">
<attribute name="NAME" x="416.56" y="15.24" size="1.778" layer="95"/>
</instance>
<instance part="B_4" gate="G$1" x="419.1" y="5.08" smashed="yes">
<attribute name="NAME" x="416.56" y="7.62" size="1.778" layer="95"/>
</instance>
<instance part="B_5" gate="G$1" x="419.1" y="-2.54" smashed="yes">
<attribute name="NAME" x="416.56" y="0" size="1.778" layer="95"/>
</instance>
<instance part="B_6" gate="G$1" x="419.1" y="-10.16" smashed="yes">
<attribute name="NAME" x="416.56" y="-7.62" size="1.778" layer="95"/>
</instance>
<instance part="L_6" gate="G$1" x="403.86" y="111.76" smashed="yes" rot="R270">
<attribute name="NAME" x="399.288" y="108.204" size="1.778" layer="95"/>
<attribute name="VALUE" x="399.288" y="106.045" size="1.778" layer="96"/>
</instance>
<instance part="R50" gate="G$1" x="416.56" y="111.76" smashed="yes" rot="R180"/>
<instance part="JP4" gate="G$1" x="167.64" y="-43.18" smashed="yes" rot="MR180">
<attribute name="NAME" x="165.1" y="-40.64" size="1.778" layer="95" rot="MR270"/>
</instance>
<instance part="GND72" gate="1" x="177.8" y="-43.18" smashed="yes" rot="R90">
<attribute name="VALUE" x="180.34" y="-45.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="AMP_M" gate="B" x="111.76" y="-63.5" smashed="yes">
<attribute name="NAME" x="119.38" y="-58.42" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="-60.96" size="1.778" layer="96"/>
</instance>
<instance part="AMP_M" gate="A" x="83.82" y="-38.1" smashed="yes">
<attribute name="NAME" x="91.44" y="-33.02" size="1.778" layer="95"/>
<attribute name="VALUE" x="91.44" y="-35.56" size="1.778" layer="96"/>
</instance>
<instance part="AMP_B" gate="B" x="58.42" y="93.98" smashed="yes">
<attribute name="NAME" x="66.04" y="99.06" size="1.778" layer="95"/>
<attribute name="VALUE" x="66.04" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="AMP_B" gate="A" x="58.42" y="162.56" smashed="yes">
<attribute name="NAME" x="66.04" y="167.64" size="1.778" layer="95"/>
<attribute name="VALUE" x="66.04" y="165.1" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="15.24" y="76.2" smashed="yes">
<attribute name="VALUE" x="13.335" y="79.375" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY40" gate="G$1" x="12.7" y="144.78" smashed="yes">
<attribute name="VALUE" x="10.795" y="147.955" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="20.32" y="17.78" smashed="yes">
<attribute name="VALUE" x="18.415" y="20.955" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY13" gate="G$1" x="78.74" y="17.78" smashed="yes">
<attribute name="VALUE" x="76.835" y="20.955" size="1.778" layer="96"/>
</instance>
<instance part="VOLTAGERANGE" gate="G$1" x="132.08" y="246.38" smashed="yes">
<attribute name="VALUE" x="129.54" y="238.76" size="1.778" layer="96"/>
<attribute name="NAME" x="129.54" y="252.222" size="1.778" layer="95"/>
</instance>
<instance part="EXP" gate="G$1" x="406.4" y="170.18" smashed="yes">
<attribute name="NAME" x="401.32" y="175.768" size="1.27" layer="95"/>
<attribute name="VALUE" x="401.32" y="163.068" size="1.27" layer="96"/>
</instance>
<instance part="GND73" gate="1" x="396.24" y="170.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="393.7" y="172.72" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="1_READ" gate="G$1" x="129.54" y="-25.4" smashed="yes" rot="R270">
<attribute name="VALUE" x="121.92" y="-22.86" size="1.778" layer="96" rot="R270"/>
<attribute name="NAME" x="135.382" y="-22.86" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="R74" gate="G$1" x="104.14" y="-71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="102.6414" y="-74.93" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="99.822" y="-74.93" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND74" gate="1" x="104.14" y="-78.74" smashed="yes">
<attribute name="VALUE" x="101.6" y="-81.28" size="1.778" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="78.74" y="-53.34" smashed="yes">
<attribute name="NAME" x="80.264" y="-52.959" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.264" y="-58.039" size="1.778" layer="96"/>
</instance>
<instance part="GND75" gate="1" x="78.74" y="-63.5" smashed="yes">
<attribute name="VALUE" x="76.2" y="-66.04" size="1.778" layer="96"/>
</instance>
<instance part="C23" gate="G$1" x="88.9" y="-7.62" smashed="yes" rot="R90">
<attribute name="NAME" x="88.519" y="-6.096" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="93.599" y="-6.096" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R75" gate="G$1" x="88.9" y="-60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="90.3986" y="-57.15" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="85.598" y="-57.15" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C24" gate="G$1" x="58.42" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="58.039" y="123.444" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="63.119" y="123.444" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C25" gate="G$1" x="55.88" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="55.499" y="189.484" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="60.579" y="189.484" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C26" gate="G$1" x="38.1" y="170.18" smashed="yes">
<attribute name="NAME" x="39.624" y="170.561" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.624" y="165.481" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="38.1" y="162.56" smashed="yes">
<attribute name="VALUE" x="35.56" y="160.02" size="1.778" layer="96"/>
</instance>
<instance part="C27" gate="G$1" x="38.1" y="104.14" smashed="yes">
<attribute name="NAME" x="39.624" y="104.521" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.624" y="99.441" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="38.1" y="96.52" smashed="yes">
<attribute name="VALUE" x="35.56" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="D23" gate="G$1" x="182.88" y="-43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="180.975" y="-44.958" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="186.309" y="-44.958" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D25" gate="G$1" x="182.88" y="-73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="180.975" y="-75.438" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="186.309" y="-75.438" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GATE_2" gate="G$1" x="96.52" y="149.86" smashed="yes">
<attribute name="NAME" x="93.98" y="153.924" size="1.778" layer="95"/>
</instance>
<instance part="GATE_3" gate="G$1" x="96.52" y="81.28" smashed="yes">
<attribute name="NAME" x="93.98" y="85.344" size="1.778" layer="95"/>
</instance>
<instance part="GATE_A" gate="G$1" x="205.74" y="55.88" smashed="yes">
<attribute name="NAME" x="203.2" y="59.944" size="1.778" layer="95"/>
</instance>
<instance part="GATE_B" gate="G$1" x="226.06" y="17.78" smashed="yes">
<attribute name="NAME" x="223.52" y="21.844" size="1.778" layer="95"/>
</instance>
<instance part="1_EF" gate="G$1" x="149.86" y="-66.04" smashed="yes">
<attribute name="NAME" x="147.32" y="-61.976" size="1.778" layer="95"/>
</instance>
<instance part="R150" gate="G$1" x="237.49" y="168.91" smashed="yes">
<attribute name="NAME" x="233.68" y="170.4086" size="1.778" layer="95"/>
<attribute name="VALUE" x="233.68" y="165.608" size="1.778" layer="96"/>
</instance>
<instance part="R151" gate="G$1" x="187.96" y="172.72" smashed="yes">
<attribute name="NAME" x="184.15" y="174.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="184.15" y="169.418" size="1.778" layer="96"/>
</instance>
<instance part="IC16" gate="G$1" x="210.82" y="167.64" smashed="yes">
<attribute name="NAME" x="201.295" y="175.895" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.295" y="160.02" size="1.778" layer="96"/>
</instance>
<instance part="D21" gate="G$1" x="195.58" y="167.64" smashed="yes" rot="R90">
<attribute name="NAME" x="195.0974" y="170.18" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="197.8914" y="170.18" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R152" gate="G$1" x="177.8" y="172.72" smashed="yes">
<attribute name="NAME" x="173.99" y="174.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.99" y="169.418" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY97" gate="+5V" x="242.57" y="173.99" smashed="yes">
<attribute name="VALUE" x="240.665" y="177.165" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY98" gate="+5V" x="227.33" y="176.53" smashed="yes">
<attribute name="VALUE" x="225.425" y="179.705" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY99" gate="GND" x="223.52" y="160.02" smashed="yes">
<attribute name="VALUE" x="221.615" y="156.845" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="81.28" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="79.7814" y="52.07" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="84.582" y="57.15" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R6" gate="G$1" x="22.86" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="21.3614" y="52.07" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.162" y="52.07" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND26" gate="1" x="200.66" y="50.8" smashed="yes">
<attribute name="VALUE" x="198.12" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="CV_A" gate="G$1" x="205.74" y="73.66" smashed="yes">
<attribute name="NAME" x="203.2" y="77.724" size="1.778" layer="95"/>
</instance>
<instance part="DIR_CV_3" gate="G$1" x="96.52" y="99.06" smashed="yes">
<attribute name="NAME" x="93.98" y="103.124" size="1.778" layer="95"/>
</instance>
<instance part="DIR_CV_2" gate="G$1" x="96.52" y="167.64" smashed="yes">
<attribute name="NAME" x="93.98" y="171.704" size="1.778" layer="95"/>
</instance>
<instance part="GND62" gate="1" x="91.44" y="144.78" smashed="yes">
<attribute name="VALUE" x="88.9" y="142.24" size="1.778" layer="96"/>
</instance>
<instance part="GND63" gate="1" x="91.44" y="76.2" smashed="yes">
<attribute name="VALUE" x="88.9" y="73.66" size="1.778" layer="96"/>
</instance>
<instance part="GND76" gate="1" x="144.78" y="-71.12" smashed="yes">
<attribute name="VALUE" x="142.24" y="-73.66" size="1.778" layer="96"/>
</instance>
<instance part="1_PREAMP" gate="G$1" x="142.24" y="-40.64" smashed="yes">
<attribute name="NAME" x="139.7" y="-36.576" size="1.778" layer="95"/>
</instance>
<instance part="GND77" gate="1" x="220.98" y="12.7" smashed="yes">
<attribute name="VALUE" x="218.44" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="215.9" y="20.32" smashed="yes" rot="R180">
<attribute name="NAME" x="219.71" y="18.8214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="219.71" y="23.622" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY3" gate="+5V" x="424.18" y="127" smashed="yes" rot="R270">
<attribute name="VALUE" x="427.355" y="128.905" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY8" gate="+5V" x="424.18" y="119.38" smashed="yes" rot="R270">
<attribute name="VALUE" x="427.355" y="121.285" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY14" gate="+5V" x="424.18" y="111.76" smashed="yes" rot="R270">
<attribute name="VALUE" x="427.355" y="113.665" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY20" gate="+5V" x="424.18" y="104.14" smashed="yes" rot="R270">
<attribute name="VALUE" x="427.355" y="106.045" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY21" gate="+5V" x="416.56" y="58.42" smashed="yes" rot="R270">
<attribute name="VALUE" x="419.735" y="60.325" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY41" gate="+5V" x="416.56" y="50.8" smashed="yes" rot="R270">
<attribute name="VALUE" x="419.735" y="52.705" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY42" gate="+5V" x="416.56" y="43.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="419.735" y="45.085" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY43" gate="+5V" x="416.56" y="35.56" smashed="yes" rot="R270">
<attribute name="VALUE" x="419.735" y="37.465" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="L_GATE_B" gate="G$1" x="76.2" y="149.86" smashed="yes">
<attribute name="NAME" x="79.756" y="145.288" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="81.915" y="145.288" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R11" gate="G$1" x="76.2" y="139.7" smashed="yes" rot="R270">
<attribute name="NAME" x="77.6986" y="143.51" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="72.898" y="143.51" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND18" gate="1" x="76.2" y="132.08" smashed="yes">
<attribute name="VALUE" x="73.66" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="L_GATE_C" gate="G$1" x="81.28" y="81.28" smashed="yes">
<attribute name="NAME" x="84.836" y="76.708" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="86.995" y="76.708" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R12" gate="G$1" x="81.28" y="71.12" smashed="yes" rot="R270">
<attribute name="NAME" x="82.7786" y="74.93" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="77.978" y="74.93" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND40" gate="1" x="81.28" y="63.5" smashed="yes">
<attribute name="VALUE" x="78.74" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="L_OUT_D" gate="G$1" x="175.26" y="73.66" smashed="yes">
<attribute name="NAME" x="178.816" y="69.088" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="180.975" y="69.088" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R13" gate="G$1" x="170.18" y="68.58" smashed="yes">
<attribute name="NAME" x="166.37" y="70.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="166.37" y="65.278" size="1.778" layer="96"/>
</instance>
<instance part="GND41" gate="1" x="165.1" y="66.04" smashed="yes">
<attribute name="VALUE" x="162.56" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="L_GATE_D" gate="G$1" x="185.42" y="55.88" smashed="yes">
<attribute name="NAME" x="188.976" y="51.308" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="191.135" y="51.308" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R14" gate="G$1" x="180.34" y="50.8" smashed="yes">
<attribute name="NAME" x="176.53" y="52.2986" size="1.778" layer="95"/>
<attribute name="VALUE" x="176.53" y="47.498" size="1.778" layer="96"/>
</instance>
<instance part="GND42" gate="1" x="175.26" y="48.26" smashed="yes">
<attribute name="VALUE" x="172.72" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="L_OUT_E" gate="G$1" x="170.18" y="27.94" smashed="yes">
<attribute name="NAME" x="173.736" y="23.368" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="175.895" y="23.368" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R16" gate="G$1" x="165.1" y="22.86" smashed="yes">
<attribute name="NAME" x="161.29" y="24.3586" size="1.778" layer="95"/>
<attribute name="VALUE" x="161.29" y="19.558" size="1.778" layer="96"/>
</instance>
<instance part="GND43" gate="1" x="160.02" y="20.32" smashed="yes">
<attribute name="VALUE" x="157.48" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="L_GATE_E" gate="G$1" x="210.82" y="17.78" smashed="yes">
<attribute name="NAME" x="214.376" y="13.208" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="216.535" y="13.208" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R38" gate="G$1" x="210.82" y="7.62" smashed="yes" rot="R90">
<attribute name="NAME" x="209.3214" y="3.81" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="214.122" y="3.81" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND48" gate="1" x="210.82" y="0" smashed="yes">
<attribute name="VALUE" x="208.28" y="-2.54" size="1.778" layer="96"/>
</instance>
<instance part="L_OUT_C" gate="G$1" x="73.66" y="99.06" smashed="yes">
<attribute name="NAME" x="77.216" y="94.488" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="79.375" y="94.488" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R39" gate="G$1" x="73.66" y="88.9" smashed="yes" rot="R270">
<attribute name="NAME" x="75.1586" y="92.71" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="70.358" y="92.71" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND49" gate="1" x="73.66" y="81.28" smashed="yes">
<attribute name="VALUE" x="71.12" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="L_OUT_B" gate="G$1" x="73.66" y="172.72" smashed="yes" rot="R180">
<attribute name="NAME" x="70.104" y="177.292" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="67.945" y="177.292" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R76" gate="G$1" x="73.66" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="72.1614" y="179.07" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="76.962" y="179.07" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND71" gate="1" x="73.66" y="190.5" smashed="yes" rot="R180">
<attribute name="VALUE" x="76.2" y="193.04" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_EF" gate="G$1" x="127" y="-66.04" smashed="yes">
<attribute name="NAME" x="130.556" y="-70.612" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="132.715" y="-70.612" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R77" gate="G$1" x="127" y="-76.2" smashed="yes" rot="R270">
<attribute name="NAME" x="128.4986" y="-72.39" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="123.698" y="-72.39" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND78" gate="1" x="127" y="-83.82" smashed="yes">
<attribute name="VALUE" x="124.46" y="-86.36" size="1.778" layer="96"/>
</instance>
<instance part="L_AMP" gate="G$1" x="99.06" y="-40.64" smashed="yes">
<attribute name="NAME" x="102.616" y="-45.212" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="104.775" y="-45.212" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R78" gate="G$1" x="104.14" y="-45.72" smashed="yes">
<attribute name="NAME" x="100.33" y="-44.2214" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.33" y="-49.022" size="1.778" layer="96"/>
</instance>
<instance part="GND79" gate="1" x="109.22" y="-48.26" smashed="yes">
<attribute name="VALUE" x="106.68" y="-50.8" size="1.778" layer="96"/>
</instance>
<instance part="B_MIDI" gate="G$2" x="419.1" y="-88.9" smashed="yes">
<attribute name="NAME" x="416.56" y="-86.36" size="1.778" layer="95"/>
</instance>
<instance part="LDR" gate="G$1" x="43.18" y="40.64" smashed="yes">
<attribute name="VALUE" x="40.64" y="35.56" size="1.778" layer="96"/>
<attribute name="NAME" x="40.64" y="46.482" size="1.778" layer="95"/>
</instance>
<instance part="GND80" gate="1" x="50.8" y="38.1" smashed="yes">
<attribute name="VALUE" x="48.26" y="35.56" size="1.778" layer="96"/>
</instance>
<instance part="FID1" gate="G$1" x="121.92" y="60.96" smashed="yes"/>
<instance part="FID2" gate="G$1" x="127" y="60.96" smashed="yes"/>
<instance part="FID3" gate="G$1" x="121.92" y="55.88" smashed="yes"/>
<instance part="FID4" gate="G$1" x="127" y="55.88" smashed="yes"/>
<instance part="C5" gate="G$1" x="55.88" y="-35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="54.229" y="-34.036" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="59.309" y="-34.036" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="106.68" y="-71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="105.156" y="-72.771" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="105.156" y="-67.691" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C29" gate="G$1" x="38.1" y="236.22" smashed="yes" rot="R180">
<attribute name="NAME" x="36.576" y="234.569" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="36.576" y="239.649" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C30" gate="G$1" x="22.86" y="236.22" smashed="yes" rot="R180">
<attribute name="NAME" x="21.336" y="234.569" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="21.336" y="239.649" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND81" gate="1" x="22.86" y="228.6" smashed="yes">
<attribute name="VALUE" x="20.32" y="226.06" size="1.778" layer="96"/>
</instance>
<instance part="GND82" gate="1" x="38.1" y="228.6" smashed="yes">
<attribute name="VALUE" x="35.56" y="226.06" size="1.778" layer="96"/>
</instance>
<instance part="GND83" gate="1" x="43.18" y="231.14" smashed="yes">
<attribute name="VALUE" x="40.64" y="228.6" size="1.778" layer="96"/>
</instance>
<instance part="GND84" gate="1" x="12.7" y="231.14" smashed="yes">
<attribute name="VALUE" x="10.16" y="228.6" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="30.48" y1="167.64" x2="30.48" y2="170.18" width="0.1524" layer="91"/>
<pinref part="OFFSET_2" gate="G$1" pin="1"/>
<pinref part="OFFSET_2" gate="G$1" pin="P$1"/>
<wire x1="33.02" y1="170.18" x2="30.48" y2="170.18" width="0.1524" layer="91"/>
<junction x="30.48" y="170.18"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="IN_PIN_2" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IN_2" gate="G$1" pin="SLEEVE"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="48.26" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="30.48" y1="99.06" x2="30.48" y2="101.6" width="0.1524" layer="91"/>
<pinref part="OFFSET_3" gate="G$1" pin="1"/>
<pinref part="OFFSET_3" gate="G$1" pin="P$1"/>
<wire x1="33.02" y1="101.6" x2="30.48" y2="101.6" width="0.1524" layer="91"/>
<junction x="30.48" y="101.6"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="IN_PIN_3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="IN_3" gate="G$1" pin="SLEEVE"/>
</segment>
<segment>
<pinref part="IN_PIN_4" gate="G$1" pin="1"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IN_4" gate="G$1" pin="SLEEVE"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IN_5" gate="G$1" pin="SLEEVE"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IN_PIN_5" gate="G$1" pin="1"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="THR" gate="G$1" pin="1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="THR" gate="G$1" pin="P$1"/>
<wire x1="182.88" y1="-2.54" x2="180.34" y2="-2.54" width="0.1524" layer="91"/>
<junction x="180.34" y="-2.54"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="SYNTH/CV_B" gate="G$1" pin="SLEEVE"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="MIDI_OUT" gate="G$1" pin="SLEEVE"/>
<pinref part="GND27" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC5" gate="A" pin="G"/>
<pinref part="GND25" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC6" gate="A" pin="G"/>
<pinref part="GND28" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="G"/>
<pinref part="GND29" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="J1" gate="G$1" pin="6"/>
<wire x1="0" y1="236.22" x2="-5.08" y2="236.22" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="8"/>
<wire x1="-5.08" y1="233.68" x2="-5.08" y2="236.22" width="0.1524" layer="91"/>
<junction x="-5.08" y="236.22"/>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="-5.08" y1="236.22" x2="-5.08" y2="238.76" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="-5.08" y1="238.76" x2="-20.32" y2="238.76" width="0.1524" layer="91"/>
<junction x="-5.08" y="238.76"/>
<pinref part="J1" gate="G$1" pin="5"/>
<wire x1="-20.32" y1="238.76" x2="-20.32" y2="236.22" width="0.1524" layer="91"/>
<junction x="-20.32" y="238.76"/>
<pinref part="J1" gate="G$1" pin="7"/>
<wire x1="-20.32" y1="236.22" x2="-20.32" y2="233.68" width="0.1524" layer="91"/>
<junction x="-20.32" y="236.22"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND84" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="GND"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND83" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D15" gate="G$1" pin="A"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="RING"/>
<pinref part="GND35" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IN_1" gate="G$1" pin="SLEEVE"/>
<pinref part="GND33" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R48" gate="G$1" pin="1"/>
<pinref part="GND36" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D16" gate="G$1" pin="A"/>
<pinref part="GND38" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D18" gate="G$1" pin="A"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R67" gate="G$1" pin="1"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<pinref part="GND44" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<pinref part="GND45" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<pinref part="GND46" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R58" gate="G$1" pin="1"/>
<pinref part="GND47" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R55" gate="G$1" pin="1"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R54" gate="G$1" pin="1"/>
<pinref part="GND51" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="GND52" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<pinref part="GND53" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="I2C_HEADER" gate="G$1" pin="1"/>
<pinref part="GND55" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<pinref part="GND56" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND1"/>
<pinref part="GND57" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND58" gate="1" pin="GND"/>
<wire x1="185.42" y1="-45.72" x2="193.04" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="D23" gate="G$1" pin="A"/>
<wire x1="182.88" y1="-45.72" x2="185.42" y2="-45.72" width="0.1524" layer="91"/>
<junction x="185.42" y="-45.72"/>
</segment>
<segment>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="185.42" y1="-76.2" x2="190.5" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="D25" gate="G$1" pin="A"/>
<wire x1="182.88" y1="-76.2" x2="185.42" y2="-76.2" width="0.1524" layer="91"/>
<junction x="185.42" y="-76.2"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND60" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="G"/>
<pinref part="GND61" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC7" gate="P" pin="GND"/>
<wire x1="81.28" y1="226.06" x2="86.36" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="81.28" y1="231.14" x2="81.28" y2="226.06" width="0.1524" layer="91"/>
<junction x="81.28" y="226.06"/>
<pinref part="IC6" gate="P" pin="GND"/>
<wire x1="76.2" y1="226.06" x2="81.28" y2="226.06" width="0.1524" layer="91"/>
<junction x="76.2" y="226.06"/>
<wire x1="71.12" y1="226.06" x2="76.2" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="71.12" y1="231.14" x2="71.12" y2="226.06" width="0.1524" layer="91"/>
<junction x="71.12" y="226.06"/>
<pinref part="IC5" gate="P" pin="GND"/>
<wire x1="66.04" y1="226.06" x2="71.12" y2="226.06" width="0.1524" layer="91"/>
<junction x="66.04" y="226.06"/>
<wire x1="66.04" y1="226.06" x2="60.96" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="60.96" y1="231.14" x2="60.96" y2="226.06" width="0.1524" layer="91"/>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="IC1" gate="P" pin="GND"/>
<wire x1="86.36" y1="226.06" x2="91.44" y2="226.06" width="0.1524" layer="91"/>
<junction x="86.36" y="226.06"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="91.44" y1="226.06" x2="96.52" y2="226.06" width="0.1524" layer="91"/>
<wire x1="91.44" y1="231.14" x2="91.44" y2="226.06" width="0.1524" layer="91"/>
<junction x="91.44" y="226.06"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="144.78" y1="233.68" x2="144.78" y2="231.14" width="0.1524" layer="91"/>
<pinref part="AMP_B" gate="P" pin="V-"/>
<wire x1="144.78" y1="231.14" x2="152.4" y2="231.14" width="0.1524" layer="91"/>
<pinref part="AMP_M" gate="P" pin="V-"/>
<wire x1="152.4" y1="231.14" x2="157.48" y2="231.14" width="0.1524" layer="91"/>
<junction x="152.4" y="231.14"/>
<wire x1="157.48" y1="231.14" x2="162.56" y2="231.14" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="157.48" y1="233.68" x2="157.48" y2="231.14" width="0.1524" layer="91"/>
<junction x="157.48" y="231.14"/>
<pinref part="GND64" gate="1" pin="GND"/>
<junction x="144.78" y="231.14"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="233.68" x2="114.3" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="114.3" y1="236.22" x2="106.68" y2="233.68" width="0.1524" layer="91"/>
<junction x="114.3" y="236.22"/>
<pinref part="GND65" gate="1" pin="GND"/>
<wire x1="114.3" y1="236.22" x2="114.3" y2="233.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND54" gate="1" pin="GND"/>
<pinref part="SPI_HEADER" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="AGND"/>
<pinref part="GND69" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="GND"/>
<pinref part="GND70" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND66" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND67" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="2"/>
<pinref part="GND72" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="3"/>
<pinref part="GND73" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND34" gate="1" pin="GND"/>
<pinref part="M1" gate="G$1" pin="P$1"/>
<wire x1="55.88" y1="-53.34" x2="55.88" y2="-50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R74" gate="G$1" pin="1"/>
<pinref part="GND74" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND75" gate="1" pin="GND"/>
<wire x1="78.74" y1="-58.42" x2="78.74" y2="-60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC16" gate="G$1" pin="GND"/>
<pinref part="SUPPLY99" gate="GND" pin="GND"/>
<wire x1="223.52" y1="165.1" x2="223.52" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GATE_A" gate="G$1" pin="SLEEVE"/>
<pinref part="GND26" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GATE_2" gate="G$1" pin="SLEEVE"/>
<pinref part="GND62" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GATE_3" gate="G$1" pin="SLEEVE"/>
<pinref part="GND63" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="1_EF" gate="G$1" pin="SLEEVE"/>
<pinref part="GND76" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="1_PREAMP" gate="G$1" pin="SLEEVE"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="DIR_CV_3" gate="G$1" pin="SLEEVE"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="DIR_CV_2" gate="G$1" pin="SLEEVE"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="CV_A" gate="G$1" pin="SLEEVE"/>
<wire x1="200.66" y1="71.12" x2="200.66" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GATE_B" gate="G$1" pin="SLEEVE"/>
<pinref part="GND77" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="GND18" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="GND40" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="GND41" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="GND42" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="GND43" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<pinref part="GND48" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R39" gate="G$1" pin="2"/>
<pinref part="GND49" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R76" gate="G$1" pin="2"/>
<pinref part="GND71" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R77" gate="G$1" pin="2"/>
<pinref part="GND78" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R78" gate="G$1" pin="2"/>
<pinref part="GND79" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LDR" gate="G$1" pin="1"/>
<pinref part="GND80" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="GND68" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="1"/>
<pinref part="GND81" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="1"/>
<pinref part="GND82" gate="1" pin="GND"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="SUPPLY22" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="IC6" gate="A" pin="SCL"/>
<pinref part="SUPPLY23" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="SCL"/>
<pinref part="SUPPLY24" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="IC5" gate="A" pin="SCL"/>
<pinref part="SUPPLY25" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="I2C_HEADER" gate="G$1" pin="2"/>
<pinref part="SUPPLY29" gate="+5V" pin="+5V"/>
<wire x1="195.58" y1="238.76" x2="190.5" y2="238.76" width="0.1524" layer="91"/>
<label x="195.58" y="238.76" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="5V"/>
<pinref part="SUPPLY30" gate="+5V" pin="+5V"/>
<wire x1="487.68" y1="284.48" x2="472.44" y2="284.48" width="0.1524" layer="91"/>
<label x="485.14" y="284.48" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY31" gate="+5V" pin="+5V"/>
<wire x1="500.38" y1="271.78" x2="497.84" y2="274.32" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="497.84" y1="274.32" x2="472.44" y2="292.1" width="0.1524" layer="91"/>
<junction x="497.84" y="274.32"/>
<label x="497.84" y="271.78" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="SCL"/>
<pinref part="SUPPLY35" gate="+5V" pin="+5V"/>
<wire x1="342.9" y1="175.26" x2="345.44" y2="175.26" width="0.1524" layer="91"/>
<label x="345.44" y="175.26" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="OUT"/>
<wire x1="38.1" y1="241.3" x2="43.18" y2="243.84" width="0.1524" layer="91"/>
<pinref part="IC5" gate="P" pin="VCC"/>
<pinref part="IC6" gate="P" pin="VCC"/>
<wire x1="60.96" y1="241.3" x2="66.04" y2="241.3" width="0.1524" layer="91"/>
<wire x1="66.04" y1="241.3" x2="71.12" y2="241.3" width="0.1524" layer="91"/>
<junction x="66.04" y="241.3"/>
<pinref part="IC7" gate="P" pin="VCC"/>
<wire x1="71.12" y1="241.3" x2="76.2" y2="241.3" width="0.1524" layer="91"/>
<wire x1="76.2" y1="241.3" x2="81.28" y2="241.3" width="0.1524" layer="91"/>
<junction x="76.2" y="241.3"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="81.28" y1="241.3" x2="86.36" y2="241.3" width="0.1524" layer="91"/>
<junction x="81.28" y="241.3"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="71.12" y="241.3"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="43.18" y1="243.84" x2="60.96" y2="241.3" width="0.1524" layer="91"/>
<junction x="43.18" y="243.84"/>
<junction x="60.96" y="241.3"/>
<pinref part="SUPPLY18" gate="+5V" pin="+5V"/>
<pinref part="IC2" gate="G$1" pin="IN"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="86.36" y1="241.3" x2="91.44" y2="241.3" width="0.1524" layer="91"/>
<junction x="86.36" y="241.3"/>
<junction x="106.68" y="243.84"/>
<pinref part="IC1" gate="P" pin="VCC"/>
<wire x1="91.44" y1="241.3" x2="96.52" y2="241.3" width="0.1524" layer="91"/>
<wire x1="96.52" y1="241.3" x2="106.68" y2="243.84" width="0.1524" layer="91"/>
<junction x="96.52" y="241.3"/>
<pinref part="C21" gate="G$1" pin="1"/>
<junction x="91.44" y="241.3"/>
<label x="101.6" y="241.3" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="C29" gate="G$1" pin="2"/>
<junction x="38.1" y="241.3"/>
</segment>
<segment>
<pinref part="SUPPLY28" gate="+5V" pin="+5V"/>
<pinref part="SPI_HEADER" gate="G$1" pin="2"/>
<wire x1="195.58" y1="210.82" x2="190.5" y2="210.82" width="0.1524" layer="91"/>
<label x="195.58" y="210.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY17" gate="+5V" pin="+5V"/>
<wire x1="393.7" y1="297.18" x2="388.62" y2="299.72" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="388.62" y1="299.72" x2="383.54" y2="297.18" width="0.1524" layer="91"/>
<junction x="388.62" y="299.72"/>
<label x="391.16" y="297.18" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="U$5" gate="G$1" pin="3.3V"/>
<pinref part="SUPPLY34" gate="G$1" pin="+5V"/>
<wire x1="398.78" y1="294.64" x2="393.7" y2="297.18" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="393.7" y1="297.18" x2="383.54" y2="294.64" width="0.1524" layer="91"/>
<junction x="393.7" y="297.18"/>
<label x="398.78" y="294.64" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="VOLTAGERANGE" gate="G$1" pin="3"/>
<pinref part="SUPPLY32" gate="+5V" pin="+5V"/>
<wire x1="139.7" y1="256.54" x2="139.7" y2="248.92" width="0.1524" layer="91"/>
<label x="139.7" y="256.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R150" gate="G$1" pin="2"/>
<wire x1="242.57" y1="171.45" x2="242.57" y2="168.91" width="0.1524" layer="91"/>
<pinref part="SUPPLY97" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="IC16" gate="G$1" pin="VCC"/>
<wire x1="227.33" y1="173.99" x2="223.52" y2="172.72" width="0.1524" layer="91"/>
<pinref part="SUPPLY98" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R56" gate="G$1" pin="1"/>
<pinref part="SUPPLY3" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R57" gate="G$1" pin="1"/>
<pinref part="SUPPLY8" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R50" gate="G$1" pin="1"/>
<pinref part="SUPPLY14" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R66" gate="G$1" pin="1"/>
<pinref part="SUPPLY20" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R62" gate="G$1" pin="1"/>
<pinref part="SUPPLY21" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R63" gate="G$1" pin="1"/>
<pinref part="SUPPLY41" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R64" gate="G$1" pin="1"/>
<pinref part="SUPPLY42" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R65" gate="G$1" pin="1"/>
<pinref part="SUPPLY43" gate="+5V" pin="+5V"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="53.34" y1="165.1" x2="48.26" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="48.26" y1="175.26" x2="48.26" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<junction x="48.26" y="175.26"/>
<pinref part="AMP_B" gate="A" pin="-IN"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="50.8" y1="187.96" x2="48.26" y2="187.96" width="0.1524" layer="91"/>
<wire x1="48.26" y1="187.96" x2="48.26" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_C" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="81.28" y1="101.6" x2="81.28" y2="99.06" width="0.1524" layer="91"/>
<junction x="81.28" y="101.6"/>
<label x="81.28" y="99.06" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="68.58" y1="114.3" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<wire x1="68.58" y1="106.68" x2="68.58" y2="101.6" width="0.1524" layer="91"/>
<pinref part="GAIN_3" gate="G$1" pin="3"/>
<junction x="68.58" y="106.68"/>
<pinref part="GAIN_3" gate="G$1" pin="2"/>
<wire x1="68.58" y1="101.6" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<wire x1="63.5" y1="114.3" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<pinref part="AMP_B" gate="B" pin="OUT"/>
<junction x="68.58" y="101.6"/>
<wire x1="81.28" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<wire x1="73.66" y1="101.6" x2="68.58" y2="101.6" width="0.1524" layer="91"/>
<wire x1="68.58" y1="114.3" x2="68.58" y2="121.92" width="0.1524" layer="91"/>
<junction x="68.58" y="114.3"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="68.58" y1="121.92" x2="63.5" y2="121.92" width="0.1524" layer="91"/>
<pinref part="L_OUT_C" gate="G$1" pin="A"/>
<junction x="73.66" y="101.6"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A2"/>
<wire x1="472.44" y1="269.24" x2="474.98" y2="269.24" width="0.1524" layer="91"/>
<label x="474.98" y="269.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="21/A7/PWM"/>
<wire x1="337.82" y1="243.84" x2="335.28" y2="243.84" width="0.1524" layer="91"/>
<label x="335.28" y="243.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="2.54" y1="142.24" x2="2.54" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="IN_PIN_2" gate="G$1" pin="3"/>
<label x="2.54" y="160.02" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="IN_2" gate="G$1" pin="TIP"/>
<wire x1="-2.54" y1="160.02" x2="2.54" y2="160.02" width="0.1524" layer="91"/>
<junction x="2.54" y="160.02"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="PULLUP_2" gate="G$1" pin="O"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="PULLUP_2" gate="G$1" pin="S"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="OFFSET_2" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="OFFSET_2" gate="G$1" pin="2"/>
<pinref part="C26" gate="G$1" pin="1"/>
<junction x="38.1" y="175.26"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="GAIN_2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="R22" gate="G$1" pin="2"/>
<junction x="48.26" y="91.44"/>
<wire x1="17.78" y1="91.44" x2="48.26" y2="91.44" width="0.1524" layer="91"/>
<wire x1="48.26" y1="91.44" x2="53.34" y2="91.44" width="0.1524" layer="91"/>
<pinref part="AMP_B" gate="B" pin="+IN"/>
<pinref part="PULLUP_3" gate="G$1" pin="P"/>
<wire x1="17.78" y1="91.44" x2="17.78" y2="96.52" width="0.1524" layer="91"/>
<junction x="17.78" y="91.44"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="7.62" y1="73.66" x2="7.62" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="IN_PIN_3" gate="G$1" pin="3"/>
<wire x1="5.08" y1="73.66" x2="7.62" y2="73.66" width="0.1524" layer="91"/>
<pinref part="IN_3" gate="G$1" pin="TIP"/>
<wire x1="0" y1="91.44" x2="7.62" y2="91.44" width="0.1524" layer="91"/>
<junction x="7.62" y="91.44"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="OFFSET_3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<pinref part="OFFSET_3" gate="G$1" pin="2"/>
<pinref part="C27" gate="G$1" pin="1"/>
<junction x="38.1" y="109.22"/>
<wire x1="38.1" y1="106.68" x2="38.1" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="53.34" y1="96.52" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="48.26" y1="106.68" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<junction x="48.26" y="106.68"/>
<pinref part="AMP_B" gate="B" pin="-IN"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="53.34" y1="121.92" x2="48.26" y2="121.92" width="0.1524" layer="91"/>
<wire x1="48.26" y1="121.92" x2="48.26" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="PULLUP_3" gate="G$1" pin="O"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="PULLUP_3" gate="G$1" pin="S"/>
</segment>
</net>
<net name="V_A" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A0"/>
<wire x1="472.44" y1="264.16" x2="474.98" y2="264.16" width="0.1524" layer="91"/>
<label x="474.98" y="264.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="23/A9/T/PWM"/>
<wire x1="337.82" y1="238.76" x2="335.28" y2="233.68" width="0.1524" layer="91"/>
<label x="335.28" y="233.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="129.54" y1="-33.02" x2="129.54" y2="-17.78" width="0.1524" layer="91"/>
<label x="129.54" y="-17.78" size="1.778" layer="95" xref="yes"/>
<pinref part="1_READ" gate="G$1" pin="2"/>
</segment>
</net>
<net name="P_V_D" class="0">
<segment>
<pinref part="IN_4" gate="G$1" pin="TIP"/>
<pinref part="PULLUP_4" gate="G$1" pin="P"/>
<wire x1="5.08" y1="30.48" x2="10.16" y2="30.48" width="0.1524" layer="91"/>
<wire x1="10.16" y1="30.48" x2="15.24" y2="30.48" width="0.1524" layer="91"/>
<junction x="10.16" y="30.48"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="30.48" width="0.1524" layer="91"/>
<junction x="15.24" y="30.48"/>
<wire x1="15.24" y1="30.48" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
<pinref part="IN_PIN_4" gate="G$1" pin="3"/>
<wire x1="10.16" y1="15.24" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="PULLUP_4" gate="G$1" pin="O"/>
<pinref part="R27" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="PULLUP_4" gate="G$1" pin="S"/>
<pinref part="R28" gate="G$1" pin="1"/>
<pinref part="4_CUT_FOR_1M" gate="G$1" pin="1"/>
<wire x1="22.86" y1="40.64" x2="12.7" y2="40.64" width="0.1524" layer="91"/>
<junction x="12.7" y="40.64"/>
<wire x1="22.86" y1="40.64" x2="30.48" y2="40.64" width="0.1524" layer="91"/>
<junction x="22.86" y="40.64"/>
<pinref part="4_CUT_FOR_1M" gate="G$1" pin="2"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="30.48" y1="40.64" x2="30.48" y2="50.8" width="0.1524" layer="91"/>
<wire x1="30.48" y1="50.8" x2="22.86" y2="50.8" width="0.1524" layer="91"/>
<junction x="22.86" y="50.8"/>
</segment>
</net>
<net name="P_V_E" class="0">
<segment>
<pinref part="IN_5" gate="G$1" pin="TIP"/>
<pinref part="PULLUP_5" gate="G$1" pin="P"/>
<wire x1="63.5" y1="30.48" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="68.58" y1="30.48" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<junction x="68.58" y="30.48"/>
<wire x1="73.66" y1="15.24" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<junction x="73.66" y="30.48"/>
<wire x1="73.66" y1="30.48" x2="76.2" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="2"/>
<pinref part="IN_PIN_5" gate="G$1" pin="3"/>
<wire x1="68.58" y1="15.24" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<label x="76.2" y="35.56" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="PULLUP_5" gate="G$1" pin="O"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<pinref part="PULLUP_5" gate="G$1" pin="S"/>
<pinref part="5_CUT_FOR_1M" gate="G$1" pin="1"/>
<wire x1="81.28" y1="40.64" x2="71.12" y2="40.64" width="0.1524" layer="91"/>
<junction x="71.12" y="40.64"/>
<wire x1="81.28" y1="40.64" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<junction x="81.28" y="40.64"/>
<wire x1="88.9" y1="40.64" x2="88.9" y2="50.8" width="0.1524" layer="91"/>
<pinref part="5_CUT_FOR_1M" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="88.9" y1="50.8" x2="81.28" y2="50.8" width="0.1524" layer="91"/>
<junction x="81.28" y="50.8"/>
</segment>
</net>
<net name="V_THR" class="0">
<segment>
<pinref part="THR" gate="G$1" pin="2"/>
<wire x1="187.96" y1="2.54" x2="190.5" y2="2.54" width="0.1524" layer="91"/>
<label x="190.5" y="2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A5"/>
<wire x1="472.44" y1="276.86" x2="474.98" y2="276.86" width="0.1524" layer="91"/>
<label x="474.98" y="276.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="14/A1"/>
<wire x1="337.82" y1="261.62" x2="335.28" y2="261.62" width="0.1524" layer="91"/>
<label x="335.28" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="R36" gate="G$1" pin="2"/>
<junction x="180.34" y="30.48"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="180.34" y1="35.56" x2="180.34" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="190.5" y1="76.2" x2="200.66" y2="76.2" width="0.1524" layer="91"/>
<junction x="190.5" y="76.2"/>
<wire x1="185.42" y1="76.2" x2="190.5" y2="76.2" width="0.1524" layer="91"/>
<pinref part="CV_A" gate="G$1" pin="TIP"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="SYNTH/CV_B" gate="G$1" pin="LEFT"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="195.58" y1="35.56" x2="190.5" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="SYNTH/CV_B" gate="G$1" pin="RIGHT"/>
<wire x1="195.58" y1="33.02" x2="190.5" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="190.5" y1="33.02" x2="190.5" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CV_PWM" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="175.26" y1="76.2" x2="172.72" y2="76.2" width="0.1524" layer="91"/>
<label x="172.72" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="L_OUT_D" gate="G$1" pin="A"/>
<junction x="175.26" y="76.2"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="IO10*"/>
<wire x1="436.88" y1="261.62" x2="434.34" y2="261.62" width="0.1524" layer="91"/>
<label x="434.34" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="3/CAN-TX/PWM"/>
<wire x1="337.82" y1="289.56" x2="335.28" y2="289.56" width="0.1524" layer="91"/>
<label x="335.28" y="289.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AUDIO_PWM" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="170.18" y1="30.48" x2="165.1" y2="30.48" width="0.1524" layer="91"/>
<label x="165.1" y="30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="L_OUT_E" gate="G$1" pin="A"/>
<junction x="170.18" y="30.48"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="IO9*"/>
<wire x1="436.88" y1="264.16" x2="434.34" y2="264.16" width="0.1524" layer="91"/>
<label x="434.34" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="383.54" y1="264.16" x2="386.08" y2="264.16" width="0.1524" layer="91"/>
<label x="386.08" y="264.16" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="MIDI_IN" gate="G$1" pin="RIGHT"/>
<pinref part="R152" gate="G$1" pin="1"/>
<wire x1="172.72" y1="172.72" x2="162.56" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D0/RX"/>
<wire x1="436.88" y1="289.56" x2="434.34" y2="289.56" width="0.1524" layer="91"/>
<label x="434.34" y="289.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="0/RX1/T"/>
<wire x1="337.82" y1="297.18" x2="335.28" y2="297.18" width="0.1524" layer="91"/>
<label x="335.28" y="297.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R150" gate="G$1" pin="1"/>
<wire x1="232.41" y1="168.91" x2="227.33" y2="168.91" width="0.1524" layer="91"/>
<wire x1="232.41" y1="168.91" x2="232.41" y2="163.83" width="0.1524" layer="91"/>
<junction x="232.41" y="168.91"/>
<wire x1="232.41" y1="163.83" x2="234.95" y2="163.83" width="0.1524" layer="91"/>
<label x="234.95" y="163.83" size="1.778" layer="95" xref="yes"/>
<pinref part="IC16" gate="G$1" pin="VO"/>
<wire x1="223.52" y1="168.91" x2="224.79" y2="167.64" width="0.1524" layer="91"/>
<wire x1="224.79" y1="167.64" x2="227.33" y2="168.91" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="MIDI_OUT" gate="G$1" pin="RIGHT"/>
<pinref part="R40" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="MIDI_OUT" gate="G$1" pin="LEFT"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="274.32" y1="177.8" x2="274.32" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="264.16" y1="177.8" x2="261.62" y2="177.8" width="0.1524" layer="91"/>
<label x="261.62" y="177.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D1/TX"/>
<wire x1="436.88" y1="292.1" x2="434.34" y2="292.1" width="0.1524" layer="91"/>
<label x="434.34" y="292.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="1/TX1/T"/>
<wire x1="337.82" y1="294.64" x2="335.28" y2="294.64" width="0.1524" layer="91"/>
<label x="335.28" y="294.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="426.72" y1="27.94" x2="424.18" y2="27.94" width="0.1524" layer="91"/>
<pinref part="B_1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="426.72" y1="20.32" x2="424.18" y2="20.32" width="0.1524" layer="91"/>
<pinref part="B_2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="D14" gate="G$1" pin="C"/>
<pinref part="B_7" gate="G$2" pin="2"/>
<wire x1="426.72" y1="-66.04" x2="424.18" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="B_8" gate="G$2" pin="2"/>
<pinref part="D13" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-58.42" x2="426.72" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="B_MUTE" gate="G$2" pin="2"/>
<pinref part="D12" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-50.8" x2="426.72" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="B_SYNTH_MODE" gate="G$2" pin="2"/>
<pinref part="D11" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-43.18" x2="426.72" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="B_SHIFT" gate="G$2" pin="2"/>
<pinref part="D10" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-35.56" x2="426.72" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="424.18" y1="12.7" x2="426.72" y2="12.7" width="0.1524" layer="91"/>
<pinref part="B_3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="424.18" y1="5.08" x2="426.72" y2="5.08" width="0.1524" layer="91"/>
<pinref part="B_4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-2.54" x2="426.72" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="B_5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-10.16" x2="426.72" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="B_6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="BUTTON_PIN_1" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="A"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-10.16" x2="431.8" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-2.54" x2="431.8" y2="5.08" width="0.1524" layer="91"/>
<junction x="431.8" y="-2.54"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="431.8" y1="5.08" x2="431.8" y2="12.7" width="0.1524" layer="91"/>
<junction x="431.8" y="5.08"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="431.8" y1="12.7" x2="431.8" y2="20.32" width="0.1524" layer="91"/>
<junction x="431.8" y="12.7"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="431.8" y1="20.32" x2="431.8" y2="27.94" width="0.1524" layer="91"/>
<junction x="431.8" y="20.32"/>
<junction x="431.8" y="27.94"/>
<label x="434.34" y="27.94" size="1.778" layer="95" xref="yes"/>
<wire x1="431.8" y1="27.94" x2="434.34" y2="27.94" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-20.32" x2="431.8" y2="-10.16" width="0.1524" layer="91"/>
<junction x="431.8" y="-10.16"/>
<pinref part="D9" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-27.94" x2="431.8" y2="-20.32" width="0.1524" layer="91"/>
<junction x="431.8" y="-20.32"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D7"/>
<wire x1="436.88" y1="269.24" x2="434.34" y2="269.24" width="0.1524" layer="91"/>
<label x="434.34" y="269.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="5/PWM"/>
<wire x1="337.82" y1="284.48" x2="335.28" y2="284.48" width="0.1524" layer="91"/>
<label x="335.28" y="284.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="B_CALIBRATE" gate="G$2" pin="2"/>
<pinref part="D8" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-20.32" x2="426.72" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="B_INVERT" gate="G$2" pin="2"/>
<pinref part="D9" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-27.94" x2="426.72" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QH*"/>
<wire x1="373.38" y1="50.8" x2="373.38" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="SER"/>
<wire x1="373.38" y1="40.64" x2="347.98" y2="40.64" width="0.1524" layer="91"/>
<wire x1="347.98" y1="40.64" x2="347.98" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QH*"/>
<wire x1="373.38" y1="99.06" x2="373.38" y2="86.36" width="0.1524" layer="91"/>
<wire x1="373.38" y1="86.36" x2="347.98" y2="86.36" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="SER"/>
<wire x1="347.98" y1="86.36" x2="347.98" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LATCH" class="0">
<segment>
<pinref part="IC7" gate="A" pin="RCK"/>
<wire x1="347.98" y1="15.24" x2="335.28" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC5" gate="A" pin="RCK"/>
<wire x1="335.28" y1="15.24" x2="335.28" y2="60.96" width="0.1524" layer="91"/>
<wire x1="335.28" y1="60.96" x2="335.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="335.28" y1="109.22" x2="347.98" y2="109.22" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="RCK"/>
<wire x1="347.98" y1="60.96" x2="335.28" y2="60.96" width="0.1524" layer="91"/>
<junction x="335.28" y="60.96"/>
<label x="335.28" y="109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="IC1" gate="A" pin="RCK"/>
<wire x1="345.44" y1="170.18" x2="335.28" y2="170.18" width="0.1524" layer="91"/>
<wire x1="335.28" y1="170.18" x2="335.28" y2="109.22" width="0.1524" layer="91"/>
<junction x="335.28" y="109.22"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D6*"/>
<wire x1="436.88" y1="271.78" x2="434.34" y2="271.78" width="0.1524" layer="91"/>
<label x="434.34" y="271.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="6/PWM"/>
<wire x1="337.82" y1="281.94" x2="335.28" y2="281.94" width="0.1524" layer="91"/>
<label x="335.28" y="281.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DATA" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D4*"/>
<wire x1="436.88" y1="276.86" x2="434.34" y2="276.86" width="0.1524" layer="91"/>
<label x="434.34" y="276.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="8/TX3"/>
<wire x1="337.82" y1="276.86" x2="335.28" y2="276.86" width="0.1524" layer="91"/>
<label x="335.28" y="276.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="SER"/>
<wire x1="345.44" y1="182.88" x2="335.28" y2="182.88" width="0.1524" layer="91"/>
<label x="335.28" y="182.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="IC5" gate="A" pin="SCK"/>
<wire x1="347.98" y1="116.84" x2="337.82" y2="116.84" width="0.1524" layer="91"/>
<wire x1="337.82" y1="116.84" x2="337.82" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="SCK"/>
<wire x1="337.82" y1="68.58" x2="337.82" y2="22.86" width="0.1524" layer="91"/>
<wire x1="337.82" y1="22.86" x2="347.98" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="SCK"/>
<wire x1="347.98" y1="68.58" x2="337.82" y2="68.58" width="0.1524" layer="91"/>
<junction x="337.82" y="68.58"/>
<wire x1="337.82" y1="116.84" x2="332.74" y2="116.84" width="0.1524" layer="91"/>
<junction x="337.82" y="116.84"/>
<label x="332.74" y="116.84" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="IC1" gate="A" pin="SCK"/>
<wire x1="345.44" y1="177.8" x2="337.82" y2="177.8" width="0.1524" layer="91"/>
<wire x1="337.82" y1="116.84" x2="337.82" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D5*"/>
<wire x1="436.88" y1="274.32" x2="434.34" y2="274.32" width="0.1524" layer="91"/>
<label x="434.34" y="274.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="7/RX3"/>
<wire x1="337.82" y1="279.4" x2="335.28" y2="279.4" width="0.1524" layer="91"/>
<label x="335.28" y="279.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<pinref part="PTC" gate="G$1" pin="1"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="241.3" x2="-6.35" y2="241.3" width="0.1524" layer="91"/>
<junction x="-6.35" y="241.3"/>
<pinref part="J2" gate="G$1" pin="PIN"/>
<wire x1="-15.24" y1="261.62" x2="-5.08" y2="261.62" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="261.62" x2="-6.35" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="241.3" x2="-6.35" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="IN"/>
<wire x1="22.86" y1="241.3" x2="17.78" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="17.78" y1="241.3" x2="12.7" y2="243.84" width="0.1524" layer="91"/>
<pinref part="PTC" gate="G$1" pin="2"/>
<pinref part="D15" gate="G$1" pin="C"/>
<wire x1="12.7" y1="243.84" x2="6.35" y2="241.3" width="0.1524" layer="91"/>
<junction x="12.7" y="243.84"/>
<junction x="6.35" y="241.3"/>
<wire x1="7.62" y1="241.3" x2="6.35" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<junction x="22.86" y="241.3"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="9"/>
<pinref part="J1" gate="G$1" pin="10"/>
<wire x1="-20.32" y1="231.14" x2="-5.08" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GT_B" class="0">
<segment>
<wire x1="76.2" y1="152.4" x2="73.66" y2="152.4" width="0.1524" layer="91"/>
<label x="73.66" y="152.4" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R42" gate="G$1" pin="2"/>
<pinref part="L_GATE_B" gate="G$1" pin="A"/>
<junction x="76.2" y="152.4"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="QB"/>
<wire x1="370.84" y1="180.34" x2="373.38" y2="180.34" width="0.1524" layer="91"/>
<label x="373.38" y="180.34" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GT_C" class="0">
<segment>
<wire x1="81.28" y1="83.82" x2="78.74" y2="83.82" width="0.1524" layer="91"/>
<label x="78.74" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R43" gate="G$1" pin="2"/>
<pinref part="L_GATE_C" gate="G$1" pin="A"/>
<junction x="81.28" y="83.82"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="QC"/>
<wire x1="370.84" y1="177.8" x2="373.38" y2="177.8" width="0.1524" layer="91"/>
<label x="373.38" y="177.8" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GT_D" class="0">
<segment>
<wire x1="185.42" y1="58.42" x2="182.88" y2="58.42" width="0.1524" layer="91"/>
<label x="182.88" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R44" gate="G$1" pin="2"/>
<pinref part="L_GATE_D" gate="G$1" pin="A"/>
<junction x="185.42" y="58.42"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="QD"/>
<wire x1="370.84" y1="175.26" x2="373.38" y2="175.26" width="0.1524" layer="91"/>
<label x="373.38" y="175.26" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="L_0" gate="G$1" pin="C"/>
<pinref part="R45" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="IC7" gate="A" pin="QA"/>
<wire x1="373.38" y1="27.94" x2="396.24" y2="27.94" width="0.1524" layer="91"/>
<pinref part="B_SHIFT" gate="G$2" pin="1"/>
<wire x1="396.24" y1="27.94" x2="414.02" y2="27.94" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-35.56" x2="396.24" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="396.24" y1="-35.56" x2="396.24" y2="27.94" width="0.1524" layer="91"/>
<junction x="396.24" y="27.94"/>
<pinref part="B_1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="IC7" gate="A" pin="QC"/>
<wire x1="373.38" y1="22.86" x2="391.16" y2="22.86" width="0.1524" layer="91"/>
<wire x1="391.16" y1="22.86" x2="411.48" y2="22.86" width="0.1524" layer="91"/>
<wire x1="411.48" y1="22.86" x2="411.48" y2="12.7" width="0.1524" layer="91"/>
<wire x1="411.48" y1="12.7" x2="414.02" y2="12.7" width="0.1524" layer="91"/>
<pinref part="B_MUTE" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-50.8" x2="391.16" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="391.16" y1="-50.8" x2="391.16" y2="22.86" width="0.1524" layer="91"/>
<junction x="391.16" y="22.86"/>
<pinref part="B_3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="IC7" gate="A" pin="QD"/>
<wire x1="373.38" y1="20.32" x2="388.62" y2="20.32" width="0.1524" layer="91"/>
<wire x1="388.62" y1="20.32" x2="408.94" y2="20.32" width="0.1524" layer="91"/>
<wire x1="408.94" y1="20.32" x2="408.94" y2="5.08" width="0.1524" layer="91"/>
<wire x1="408.94" y1="5.08" x2="414.02" y2="5.08" width="0.1524" layer="91"/>
<pinref part="B_8" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-58.42" x2="388.62" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-58.42" x2="388.62" y2="20.32" width="0.1524" layer="91"/>
<junction x="388.62" y="20.32"/>
<pinref part="B_4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="IC7" gate="A" pin="QE"/>
<wire x1="373.38" y1="17.78" x2="386.08" y2="17.78" width="0.1524" layer="91"/>
<wire x1="386.08" y1="17.78" x2="406.4" y2="17.78" width="0.1524" layer="91"/>
<wire x1="406.4" y1="17.78" x2="406.4" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-2.54" x2="414.02" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="B_7" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-66.04" x2="386.08" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="386.08" y1="-66.04" x2="386.08" y2="17.78" width="0.1524" layer="91"/>
<junction x="386.08" y="17.78"/>
<pinref part="B_5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="IN_1" gate="G$1" pin="TIP"/>
<wire x1="43.18" y1="-35.56" x2="50.8" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="IN_1" gate="G$1" pin="SWITCH"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="48.26" y1="-38.1" x2="43.18" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="P$2"/>
<wire x1="55.88" y1="-40.64" x2="55.88" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-38.1" x2="48.26" y2="-38.1" width="0.1524" layer="91"/>
<junction x="48.26" y="-38.1"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="78.74" y1="-40.64" x2="78.74" y2="-48.26" width="0.1524" layer="91"/>
<junction x="78.74" y="-48.26"/>
<pinref part="AMP_M" gate="A" pin="+IN"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-50.8" x2="78.74" y2="-48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="R49" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="60.96" y1="-35.56" x2="63.5" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="137.16" y1="-38.1" x2="111.76" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="1_PREAMP" gate="G$1" pin="TIP"/>
</segment>
</net>
<net name="V_D" class="0">
<segment>
<wire x1="27.94" y1="30.48" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<label x="40.64" y="30.48" size="1.778" layer="95" xref="yes"/>
<pinref part="R52" gate="G$1" pin="1"/>
<pinref part="D16" gate="G$1" pin="C"/>
<wire x1="30.48" y1="30.48" x2="40.64" y2="30.48" width="0.1524" layer="91"/>
<junction x="30.48" y="30.48"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A3"/>
<wire x1="472.44" y1="271.78" x2="474.98" y2="271.78" width="0.1524" layer="91"/>
<label x="474.98" y="271.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="20/A6/PWM"/>
<wire x1="337.82" y1="246.38" x2="335.28" y2="246.38" width="0.1524" layer="91"/>
<label x="335.28" y="246.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="V_E" class="0">
<segment>
<wire x1="86.36" y1="30.48" x2="99.06" y2="30.48" width="0.1524" layer="91"/>
<label x="99.06" y="30.48" size="1.778" layer="95" xref="yes"/>
<pinref part="R53" gate="G$1" pin="1"/>
<pinref part="D18" gate="G$1" pin="C"/>
<junction x="86.36" y="30.48"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A4"/>
<wire x1="472.44" y1="274.32" x2="474.98" y2="274.32" width="0.1524" layer="91"/>
<label x="474.98" y="274.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="17/A3/T"/>
<wire x1="337.82" y1="254" x2="335.28" y2="254" width="0.1524" layer="91"/>
<label x="335.28" y="254" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="IN_PIN_5" gate="G$1" pin="2"/>
<pinref part="R30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="IN_PIN_4" gate="G$1" pin="2"/>
<pinref part="R29" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IN_PIN_3" gate="G$1" pin="2"/>
<pinref part="R24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IN_PIN_2" gate="G$1" pin="2"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="L_1" gate="G$1" pin="C"/>
<pinref part="R33" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="L_2" gate="G$1" pin="C"/>
<pinref part="R54" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="L_3" gate="G$1" pin="C"/>
<pinref part="R55" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="L_A" gate="G$1" pin="C"/>
<pinref part="R58" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="L_B" gate="G$1" pin="C"/>
<pinref part="R59" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="L_C" gate="G$1" pin="C"/>
<pinref part="R60" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="L_D" gate="G$1" pin="C"/>
<pinref part="R61" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="L_INV" gate="G$1" pin="C"/>
<pinref part="R67" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QB"/>
<wire x1="373.38" y1="119.38" x2="383.54" y2="119.38" width="0.1524" layer="91"/>
<wire x1="383.54" y1="119.38" x2="383.54" y2="149.86" width="0.1524" layer="91"/>
<pinref part="L_1" gate="G$1" pin="A"/>
<wire x1="383.54" y1="149.86" x2="396.24" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QC"/>
<wire x1="373.38" y1="116.84" x2="386.08" y2="116.84" width="0.1524" layer="91"/>
<wire x1="386.08" y1="116.84" x2="386.08" y2="142.24" width="0.1524" layer="91"/>
<pinref part="L_2" gate="G$1" pin="A"/>
<wire x1="386.08" y1="142.24" x2="396.24" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QD"/>
<wire x1="373.38" y1="114.3" x2="388.62" y2="114.3" width="0.1524" layer="91"/>
<wire x1="388.62" y1="114.3" x2="388.62" y2="134.62" width="0.1524" layer="91"/>
<pinref part="L_3" gate="G$1" pin="A"/>
<wire x1="388.62" y1="134.62" x2="396.24" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QE"/>
<wire x1="373.38" y1="111.76" x2="391.16" y2="111.76" width="0.1524" layer="91"/>
<wire x1="391.16" y1="111.76" x2="391.16" y2="127" width="0.1524" layer="91"/>
<pinref part="L_4" gate="G$1" pin="C"/>
<wire x1="398.78" y1="127" x2="391.16" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QF"/>
<wire x1="373.38" y1="109.22" x2="393.7" y2="109.22" width="0.1524" layer="91"/>
<wire x1="393.7" y1="119.38" x2="393.7" y2="109.22" width="0.1524" layer="91"/>
<pinref part="L_5" gate="G$1" pin="C"/>
<wire x1="393.7" y1="119.38" x2="398.78" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<pinref part="B_CALIBRATE" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-20.32" x2="401.32" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="401.32" y1="-20.32" x2="401.32" y2="12.7" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="QG"/>
<wire x1="401.32" y1="12.7" x2="381" y2="12.7" width="0.1524" layer="91"/>
<pinref part="B_CV_A" gate="G$2" pin="1"/>
<wire x1="381" y1="12.7" x2="373.38" y2="12.7" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-81.28" x2="381" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="381" y1="-81.28" x2="381" y2="12.7" width="0.1524" layer="91"/>
<junction x="381" y="12.7"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="B_INVERT" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-27.94" x2="398.78" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="QH"/>
<wire x1="373.38" y1="10.16" x2="378.46" y2="10.16" width="0.1524" layer="91"/>
<wire x1="378.46" y1="10.16" x2="398.78" y2="10.16" width="0.1524" layer="91"/>
<wire x1="398.78" y1="10.16" x2="398.78" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="378.46" y1="10.16" x2="378.46" y2="-88.9" width="0.1524" layer="91"/>
<junction x="378.46" y="10.16"/>
<wire x1="378.46" y1="-88.9" x2="414.02" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="B_MIDI" gate="G$2" pin="1"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="B_GATE_A" gate="G$2" pin="2"/>
<pinref part="D17" gate="G$1" pin="C"/>
<wire x1="426.72" y1="-73.66" x2="424.18" y2="-73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<pinref part="B_CV_A" gate="G$2" pin="2"/>
<pinref part="D19" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-81.28" x2="426.72" y2="-81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BUTTON_PIN_2" class="0">
<segment>
<pinref part="D19" gate="G$1" pin="A"/>
<pinref part="D17" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-81.28" x2="431.8" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="D14" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-73.66" x2="431.8" y2="-66.04" width="0.1524" layer="91"/>
<junction x="431.8" y="-73.66"/>
<pinref part="D13" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-66.04" x2="431.8" y2="-58.42" width="0.1524" layer="91"/>
<junction x="431.8" y="-66.04"/>
<pinref part="D12" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-58.42" x2="431.8" y2="-50.8" width="0.1524" layer="91"/>
<junction x="431.8" y="-58.42"/>
<pinref part="D11" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-50.8" x2="431.8" y2="-43.18" width="0.1524" layer="91"/>
<junction x="431.8" y="-50.8"/>
<pinref part="D10" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-43.18" x2="431.8" y2="-35.56" width="0.1524" layer="91"/>
<junction x="431.8" y="-43.18"/>
<wire x1="431.8" y1="-35.56" x2="436.88" y2="-35.56" width="0.1524" layer="91"/>
<junction x="431.8" y="-35.56"/>
<pinref part="D20" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-88.9" x2="431.8" y2="-81.28" width="0.1524" layer="91"/>
<junction x="431.8" y="-81.28"/>
<label x="436.88" y="-35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="IO11*"/>
<wire x1="436.88" y1="259.08" x2="434.34" y2="259.08" width="0.1524" layer="91"/>
<label x="434.34" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="4/CAN-RX/PWM"/>
<wire x1="337.82" y1="287.02" x2="335.28" y2="287.02" width="0.1524" layer="91"/>
<label x="335.28" y="287.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<pinref part="B_SYNTH_MODE" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-43.18" x2="393.7" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="QB"/>
<wire x1="373.38" y1="25.4" x2="393.7" y2="25.4" width="0.1524" layer="91"/>
<wire x1="393.7" y1="25.4" x2="414.02" y2="25.4" width="0.1524" layer="91"/>
<wire x1="414.02" y1="25.4" x2="414.02" y2="20.32" width="0.1524" layer="91"/>
<wire x1="393.7" y1="-43.18" x2="393.7" y2="25.4" width="0.1524" layer="91"/>
<junction x="393.7" y="25.4"/>
<pinref part="B_2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<wire x1="414.02" y1="-10.16" x2="403.86" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="403.86" y1="-10.16" x2="403.86" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="QF"/>
<wire x1="373.38" y1="15.24" x2="383.54" y2="15.24" width="0.1524" layer="91"/>
<pinref part="B_GATE_A" gate="G$2" pin="1"/>
<wire x1="383.54" y1="15.24" x2="403.86" y2="15.24" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-73.66" x2="383.54" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="383.54" y1="-73.66" x2="383.54" y2="15.24" width="0.1524" layer="91"/>
<junction x="383.54" y="15.24"/>
<pinref part="B_6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SPI_MOSI" class="0">
<segment>
<wire x1="190.5" y1="215.9" x2="193.04" y2="215.9" width="0.1524" layer="91"/>
<label x="193.04" y="215.9" size="1.778" layer="95" xref="yes"/>
<pinref part="SPI_HEADER" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="MOSI"/>
<wire x1="436.88" y1="297.18" x2="434.34" y2="297.18" width="0.1524" layer="91"/>
<label x="434.34" y="297.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="11/MOSI"/>
<wire x1="337.82" y1="269.24" x2="335.28" y2="269.24" width="0.1524" layer="91"/>
<label x="335.28" y="269.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_MISO" class="0">
<segment>
<wire x1="190.5" y1="218.44" x2="193.04" y2="218.44" width="0.1524" layer="91"/>
<label x="193.04" y="218.44" size="1.778" layer="95" xref="yes"/>
<pinref part="SPI_HEADER" gate="G$1" pin="5"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="MISO"/>
<wire x1="472.44" y1="294.64" x2="474.98" y2="294.64" width="0.1524" layer="91"/>
<label x="474.98" y="294.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="12/MISO"/>
<wire x1="337.82" y1="266.7" x2="335.28" y2="266.7" width="0.1524" layer="91"/>
<label x="335.28" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_CLK" class="0">
<segment>
<wire x1="190.5" y1="220.98" x2="193.04" y2="220.98" width="0.1524" layer="91"/>
<label x="193.04" y="220.98" size="1.778" layer="95" xref="yes"/>
<pinref part="SPI_HEADER" gate="G$1" pin="6"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SCK"/>
<wire x1="472.44" y1="297.18" x2="474.98" y2="297.18" width="0.1524" layer="91"/>
<label x="474.98" y="297.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="13/SCK/LED"/>
<wire x1="337.82" y1="264.16" x2="335.28" y2="264.16" width="0.1524" layer="91"/>
<label x="335.28" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2C_SDA" class="0">
<segment>
<pinref part="I2C_HEADER" gate="G$1" pin="3"/>
<wire x1="190.5" y1="241.3" x2="193.04" y2="241.3" width="0.1524" layer="91"/>
<label x="193.04" y="241.3" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D2/SDA"/>
<wire x1="436.88" y1="281.94" x2="434.34" y2="281.94" width="0.1524" layer="91"/>
<label x="434.34" y="281.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="18/A4/T/SDA0"/>
<wire x1="337.82" y1="251.46" x2="335.28" y2="251.46" width="0.1524" layer="91"/>
<label x="335.28" y="251.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2C_SCL" class="0">
<segment>
<pinref part="I2C_HEADER" gate="G$1" pin="4"/>
<wire x1="190.5" y1="243.84" x2="193.04" y2="243.84" width="0.1524" layer="91"/>
<label x="193.04" y="243.84" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D3/SCL"/>
<wire x1="436.88" y1="279.4" x2="434.34" y2="279.4" width="0.1524" layer="91"/>
<label x="434.34" y="279.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="19/A5/T/SCL0"/>
<wire x1="337.82" y1="248.92" x2="335.28" y2="248.92" width="0.1524" layer="91"/>
<label x="335.28" y="248.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_SS" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="RXLED/SS"/>
<wire x1="436.88" y1="294.64" x2="434.34" y2="294.64" width="0.1524" layer="91"/>
<label x="434.34" y="294.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SPI_HEADER" gate="G$1" pin="3"/>
<wire x1="190.5" y1="213.36" x2="193.04" y2="213.36" width="0.1524" layer="91"/>
<label x="193.04" y="213.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="10/TX2/PWM"/>
<wire x1="337.82" y1="271.78" x2="335.28" y2="271.78" width="0.1524" layer="91"/>
<label x="335.28" y="271.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="V_H" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IO8"/>
<wire x1="436.88" y1="266.7" x2="434.34" y2="266.7" width="0.1524" layer="91"/>
<label x="434.34" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R69" gate="G$1" pin="2"/>
<wire x1="185.42" y1="-71.12" x2="190.5" y2="-71.12" width="0.1524" layer="91"/>
<label x="190.5" y="-71.12" size="1.778" layer="95" xref="yes"/>
<junction x="185.42" y="-71.12"/>
<pinref part="JP4" gate="G$1" pin="3"/>
<wire x1="175.26" y1="-45.72" x2="175.26" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="175.26" y1="-71.12" x2="182.88" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="D25" gate="G$1" pin="C"/>
<wire x1="182.88" y1="-71.12" x2="185.42" y2="-71.12" width="0.1524" layer="91"/>
<junction x="182.88" y="-71.12"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="15/A1/T"/>
<wire x1="337.82" y1="259.08" x2="335.28" y2="259.08" width="0.1524" layer="91"/>
<label x="335.28" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="V_B" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="81.28" y1="170.18" x2="81.28" y2="167.64" width="0.1524" layer="91"/>
<junction x="81.28" y="170.18"/>
<label x="81.28" y="167.64" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="GAIN_2" gate="G$1" pin="3"/>
<pinref part="GAIN_2" gate="G$1" pin="2"/>
<wire x1="68.58" y1="182.88" x2="63.5" y2="182.88" width="0.1524" layer="91"/>
<wire x1="68.58" y1="175.26" x2="68.58" y2="182.88" width="0.1524" layer="91"/>
<wire x1="68.58" y1="162.56" x2="68.58" y2="170.18" width="0.1524" layer="91"/>
<junction x="68.58" y="175.26"/>
<pinref part="AMP_B" gate="A" pin="OUT"/>
<wire x1="68.58" y1="170.18" x2="68.58" y2="175.26" width="0.1524" layer="91"/>
<wire x1="81.28" y1="170.18" x2="73.66" y2="170.18" width="0.1524" layer="91"/>
<junction x="68.58" y="170.18"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="73.66" y1="170.18" x2="68.58" y2="170.18" width="0.1524" layer="91"/>
<wire x1="60.96" y1="187.96" x2="68.58" y2="187.96" width="0.1524" layer="91"/>
<wire x1="68.58" y1="187.96" x2="68.58" y2="182.88" width="0.1524" layer="91"/>
<junction x="68.58" y="182.88"/>
<pinref part="L_OUT_B" gate="G$1" pin="A"/>
<junction x="73.66" y="170.18"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A1"/>
<wire x1="472.44" y1="266.7" x2="474.98" y2="266.7" width="0.1524" layer="91"/>
<label x="474.98" y="266.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="22/A8/T/PWM"/>
<wire x1="337.82" y1="241.3" x2="335.28" y2="241.3" width="0.1524" layer="91"/>
<label x="335.28" y="241.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IO13*"/>
<wire x1="472.44" y1="256.54" x2="472.44" y2="254" width="0.1524" layer="91"/>
<label x="472.44" y="254" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="9/RX2/PWM"/>
<wire x1="337.82" y1="274.32" x2="335.28" y2="274.32" width="0.1524" layer="91"/>
<label x="335.28" y="274.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="4"/>
<wire x1="414.02" y1="170.18" x2="416.56" y2="170.18" width="0.1524" layer="91"/>
<label x="416.56" y="170.18" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="V_G" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IO12*"/>
<wire x1="436.88" y1="256.54" x2="434.34" y2="256.54" width="0.1524" layer="91"/>
<label x="434.34" y="256.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R68" gate="G$1" pin="2"/>
<wire x1="185.42" y1="-40.64" x2="193.04" y2="-40.64" width="0.1524" layer="91"/>
<label x="193.04" y="-40.64" size="1.778" layer="95" xref="yes"/>
<junction x="185.42" y="-40.64"/>
<pinref part="JP4" gate="G$1" pin="1"/>
<wire x1="185.42" y1="-40.64" x2="182.88" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="D23" gate="G$1" pin="C"/>
<wire x1="182.88" y1="-40.64" x2="175.26" y2="-40.64" width="0.1524" layer="91"/>
<junction x="182.88" y="-40.64"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="16/A2/T"/>
<wire x1="337.82" y1="256.54" x2="335.28" y2="256.54" width="0.1524" layer="91"/>
<label x="335.28" y="256.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QA"/>
<wire x1="373.38" y1="121.92" x2="381" y2="121.92" width="0.1524" layer="91"/>
<wire x1="381" y1="121.92" x2="381" y2="157.48" width="0.1524" layer="91"/>
<pinref part="L_0" gate="G$1" pin="A"/>
<wire x1="381" y1="157.48" x2="396.24" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GT_E" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QE"/>
<wire x1="370.84" y1="172.72" x2="373.38" y2="172.72" width="0.1524" layer="91"/>
<label x="373.38" y="172.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="1"/>
<wire x1="398.78" y1="172.72" x2="396.24" y2="172.72" width="0.1524" layer="91"/>
<label x="396.24" y="172.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="210.82" y1="20.32" x2="208.28" y2="20.32" width="0.1524" layer="91"/>
<label x="208.28" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="L_GATE_E" gate="G$1" pin="A"/>
<junction x="210.82" y="20.32"/>
</segment>
</net>
<net name="GT_F" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QF"/>
<wire x1="370.84" y1="170.18" x2="373.38" y2="170.18" width="0.1524" layer="91"/>
<label x="373.38" y="170.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="5"/>
<wire x1="398.78" y1="167.64" x2="396.24" y2="167.64" width="0.1524" layer="91"/>
<label x="396.24" y="167.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GT_G" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QG"/>
<wire x1="370.84" y1="167.64" x2="373.38" y2="167.64" width="0.1524" layer="91"/>
<label x="373.38" y="167.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="2"/>
<wire x1="414.02" y1="172.72" x2="416.56" y2="172.72" width="0.1524" layer="91"/>
<label x="416.56" y="172.72" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GT_H" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QH"/>
<wire x1="370.84" y1="165.1" x2="373.38" y2="165.1" width="0.1524" layer="91"/>
<label x="373.38" y="165.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="6"/>
<wire x1="414.02" y1="167.64" x2="416.56" y2="167.64" width="0.1524" layer="91"/>
<label x="416.56" y="167.64" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="D24" gate="G$1" pin="C"/>
<pinref part="AMP_M" gate="B" pin="+IN"/>
<wire x1="101.6" y1="-66.04" x2="104.14" y2="-66.04" width="0.1524" layer="91"/>
<junction x="101.6" y="-66.04"/>
<pinref part="R74" gate="G$1" pin="2"/>
<wire x1="104.14" y1="-66.04" x2="106.68" y2="-66.04" width="0.1524" layer="91"/>
<junction x="104.14" y="-66.04"/>
<pinref part="C11" gate="G$1" pin="2"/>
<junction x="106.68" y="-66.04"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<wire x1="121.92" y1="-55.88" x2="121.92" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-63.5" x2="127" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="1"/>
<wire x1="127" y1="-63.5" x2="132.08" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-55.88" x2="121.92" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R73" gate="G$1" pin="2"/>
<pinref part="AMP_M" gate="B" pin="OUT"/>
<junction x="121.92" y="-63.5"/>
<wire x1="121.92" y1="-55.88" x2="121.92" y2="-35.56" width="0.1524" layer="91"/>
<junction x="121.92" y="-55.88"/>
<wire x1="121.92" y1="-35.56" x2="132.08" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-35.56" x2="132.08" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="1_READ" gate="G$1" pin="3"/>
<pinref part="L_EF" gate="G$1" pin="A"/>
<junction x="127" y="-63.5"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<wire x1="106.68" y1="-60.96" x2="106.68" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="2"/>
<pinref part="R72" gate="G$1" pin="2"/>
<junction x="106.68" y="-55.88"/>
<pinref part="AMP_M" gate="B" pin="-IN"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="2"/>
<wire x1="337.82" y1="292.1" x2="335.28" y2="292.1" width="0.1524" layer="91"/>
<label x="335.28" y="292.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="12.7" y1="160.02" x2="17.78" y2="160.02" width="0.1524" layer="91"/>
<pinref part="AMP_B" gate="A" pin="+IN"/>
<junction x="53.34" y="160.02"/>
<label x="12.7" y="160.02" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="PULLUP_2" gate="G$1" pin="P"/>
<wire x1="17.78" y1="160.02" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<wire x1="17.78" y1="167.64" x2="17.78" y2="160.02" width="0.1524" layer="91"/>
<junction x="17.78" y="160.02"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="L_INV" gate="G$1" pin="A"/>
<pinref part="IC1" gate="A" pin="QA"/>
<wire x1="386.08" y1="182.88" x2="370.84" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QG"/>
<wire x1="398.78" y1="106.68" x2="373.38" y2="106.68" width="0.1524" layer="91"/>
<pinref part="L_6" gate="G$1" pin="C"/>
<wire x1="398.78" y1="111.76" x2="398.78" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="L_A" gate="G$1" pin="A"/>
<wire x1="396.24" y1="88.9" x2="375.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="375.92" y1="88.9" x2="375.92" y2="73.66" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="QA"/>
<wire x1="375.92" y1="73.66" x2="373.38" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QB"/>
<wire x1="373.38" y1="71.12" x2="378.46" y2="71.12" width="0.1524" layer="91"/>
<wire x1="378.46" y1="71.12" x2="378.46" y2="81.28" width="0.1524" layer="91"/>
<pinref part="L_B" gate="G$1" pin="A"/>
<wire x1="378.46" y1="81.28" x2="396.24" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QC"/>
<wire x1="373.38" y1="68.58" x2="381" y2="68.58" width="0.1524" layer="91"/>
<wire x1="381" y1="68.58" x2="381" y2="73.66" width="0.1524" layer="91"/>
<pinref part="L_C" gate="G$1" pin="A"/>
<wire x1="381" y1="73.66" x2="396.24" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QD"/>
<pinref part="L_D" gate="G$1" pin="A"/>
<wire x1="373.38" y1="66.04" x2="396.24" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<wire x1="375.92" y1="35.56" x2="375.92" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="QH"/>
<wire x1="375.92" y1="55.88" x2="373.38" y2="55.88" width="0.1524" layer="91"/>
<pinref part="L_H" gate="G$1" pin="C"/>
<wire x1="393.7" y1="35.56" x2="375.92" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QG"/>
<wire x1="373.38" y1="58.42" x2="378.46" y2="58.42" width="0.1524" layer="91"/>
<wire x1="378.46" y1="58.42" x2="378.46" y2="43.18" width="0.1524" layer="91"/>
<pinref part="L_G" gate="G$1" pin="C"/>
<wire x1="393.7" y1="43.18" x2="378.46" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QF"/>
<wire x1="373.38" y1="60.96" x2="381" y2="60.96" width="0.1524" layer="91"/>
<wire x1="381" y1="60.96" x2="381" y2="50.8" width="0.1524" layer="91"/>
<pinref part="L_F" gate="G$1" pin="C"/>
<wire x1="393.7" y1="50.8" x2="381" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QE"/>
<wire x1="373.38" y1="63.5" x2="383.54" y2="63.5" width="0.1524" layer="91"/>
<wire x1="383.54" y1="63.5" x2="383.54" y2="58.42" width="0.1524" layer="91"/>
<pinref part="L_E" gate="G$1" pin="C"/>
<wire x1="393.7" y1="58.42" x2="383.54" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="R70" gate="G$1" pin="2"/>
<pinref part="D24" gate="G$1" pin="A"/>
<wire x1="93.98" y1="-66.04" x2="96.52" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-66.04" x2="93.98" y2="-66.04" width="0.1524" layer="91"/>
<junction x="93.98" y="-66.04"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="SUPPLY2" gate="G$1" pin="+5V"/>
<wire x1="15.24" y1="124.46" x2="15.24" y2="116.84" width="0.1524" layer="91"/>
<label x="15.24" y="119.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="G$1" pin="+5V"/>
<wire x1="20.32" y1="124.46" x2="20.32" y2="116.84" width="0.1524" layer="91"/>
<label x="20.32" y="121.92" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="SUPPLY10" gate="G$1" pin="+5V"/>
<wire x1="30.48" y1="124.46" x2="30.48" y2="121.92" width="0.1524" layer="91"/>
<label x="30.48" y="124.46" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="SUPPLY5" gate="G$1" pin="+5V"/>
<wire x1="15.24" y1="73.66" x2="15.24" y2="71.12" width="0.1524" layer="91"/>
<label x="15.24" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="SUPPLY9" gate="G$1" pin="+5V"/>
<wire x1="30.48" y1="193.04" x2="30.48" y2="190.5" width="0.1524" layer="91"/>
<label x="30.48" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="G$1" pin="+5V"/>
<wire x1="20.32" y1="193.04" x2="20.32" y2="187.96" width="0.1524" layer="91"/>
<label x="20.32" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="SUPPLY1" gate="G$1" pin="+5V"/>
<wire x1="15.24" y1="190.5" x2="15.24" y2="187.96" width="0.1524" layer="91"/>
<label x="15.24" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="144.78" y1="243.84" x2="144.78" y2="246.38" width="0.1524" layer="91"/>
<pinref part="AMP_B" gate="P" pin="V+"/>
<wire x1="144.78" y1="246.38" x2="152.4" y2="246.38" width="0.1524" layer="91"/>
<pinref part="AMP_M" gate="P" pin="V+"/>
<wire x1="152.4" y1="246.38" x2="157.48" y2="246.38" width="0.1524" layer="91"/>
<junction x="152.4" y="246.38"/>
<wire x1="157.48" y1="246.38" x2="162.56" y2="246.38" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="157.48" y1="243.84" x2="157.48" y2="246.38" width="0.1524" layer="91"/>
<junction x="157.48" y="246.38"/>
<pinref part="SUPPLY19" gate="G$1" pin="+5V"/>
<junction x="144.78" y="246.38"/>
<label x="162.56" y="248.92" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="VOLTAGERANGE" gate="G$1" pin="2"/>
<wire x1="139.7" y1="246.38" x2="144.78" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<pinref part="SUPPLY13" gate="G$1" pin="+5V"/>
<wire x1="78.74" y1="15.24" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<label x="78.74" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="SUPPLY37" gate="G$1" pin="+5V"/>
<label x="71.12" y="60.96" size="1.778" layer="95" xref="yes"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="71.12" y1="60.96" x2="71.12" y2="50.8" width="0.1524" layer="91"/>
<wire x1="81.28" y1="60.96" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="71.12" y="60.96"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<pinref part="SUPPLY36" gate="G$1" pin="+5V"/>
<wire x1="66.04" y1="53.34" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
<label x="66.04" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<pinref part="SUPPLY38" gate="G$1" pin="+5V"/>
<wire x1="7.62" y1="53.34" x2="7.62" y2="50.8" width="0.1524" layer="91"/>
<label x="7.62" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<pinref part="SUPPLY39" gate="G$1" pin="+5V"/>
<wire x1="12.7" y1="60.96" x2="12.7" y2="50.8" width="0.1524" layer="91"/>
<label x="12.7" y="60.96" size="1.778" layer="95" xref="yes"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="22.86" y1="60.96" x2="12.7" y2="60.96" width="0.1524" layer="91"/>
<junction x="12.7" y="60.96"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="SUPPLY4" gate="G$1" pin="+5V"/>
<wire x1="20.32" y1="15.24" x2="20.32" y2="12.7" width="0.1524" layer="91"/>
<label x="20.32" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R47" gate="G$1" pin="2"/>
<pinref part="SUPPLY12" gate="G$1" pin="+5V"/>
<wire x1="68.58" y1="-48.26" x2="68.58" y2="-50.8" width="0.1524" layer="91"/>
<label x="68.58" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R46" gate="G$1" pin="2"/>
<pinref part="SUPPLY11" gate="G$1" pin="+5V"/>
<wire x1="48.26" y1="-22.86" x2="48.26" y2="-27.94" width="0.1524" layer="91"/>
<label x="48.26" y="-22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R72" gate="G$1" pin="1"/>
<pinref part="SUPPLY33" gate="G$1" pin="+5V"/>
<wire x1="96.52" y1="-50.8" x2="96.52" y2="-55.88" width="0.1524" layer="91"/>
<label x="96.52" y="-50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R69" gate="G$1" pin="1"/>
<pinref part="SUPPLY16" gate="G$1" pin="+5V"/>
<wire x1="185.42" y1="-58.42" x2="185.42" y2="-60.96" width="0.1524" layer="91"/>
<label x="185.42" y="-58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R68" gate="G$1" pin="1"/>
<pinref part="SUPPLY15" gate="G$1" pin="+5V"/>
<wire x1="185.42" y1="-27.94" x2="185.42" y2="-30.48" width="0.1524" layer="91"/>
<label x="185.42" y="-27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="THR" gate="G$1" pin="3"/>
<pinref part="SUPPLY26" gate="G$1" pin="+5V"/>
<wire x1="180.34" y1="10.16" x2="180.34" y2="7.62" width="0.1524" layer="91"/>
<label x="180.34" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="AREF"/>
<wire x1="487.68" y1="261.62" x2="472.44" y2="261.62" width="0.1524" layer="91"/>
<pinref part="SUPPLY27" gate="G$1" pin="+5V"/>
<label x="487.68" y="261.62" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="SUPPLY40" gate="G$1" pin="+5V"/>
<wire x1="12.7" y1="142.24" x2="12.7" y2="139.7" width="0.1524" layer="91"/>
<label x="12.7" y="142.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="OUT"/>
<pinref part="C17" gate="G$1" pin="1"/>
<junction x="121.92" y="243.84"/>
<pinref part="VOLTAGERANGE" gate="G$1" pin="1"/>
<wire x1="139.7" y1="243.84" x2="139.7" y2="241.3" width="0.1524" layer="91"/>
<wire x1="139.7" y1="241.3" x2="121.92" y2="243.84" width="0.1524" layer="91"/>
<label x="124.46" y="246.38" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="PRE_AMP_OUT" class="0">
<segment>
<pinref part="GAIN_1" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-17.78" x2="101.6" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="GAIN_1" gate="G$1" pin="3"/>
<wire x1="101.6" y1="-17.78" x2="101.6" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-38.1" x2="99.06" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-38.1" x2="101.6" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-25.4" x2="101.6" y2="-33.02" width="0.1524" layer="91"/>
<junction x="101.6" y="-25.4"/>
<pinref part="R51" gate="G$1" pin="2"/>
<junction x="101.6" y="-38.1"/>
<label x="101.6" y="-17.78" size="1.778" layer="95" xref="yes"/>
<pinref part="R70" gate="G$1" pin="1"/>
<wire x1="101.6" y1="-33.02" x2="101.6" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-55.88" x2="93.98" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="AMP_M" gate="A" pin="OUT"/>
<junction x="93.98" y="-38.1"/>
<wire x1="127" y1="-33.02" x2="101.6" y2="-33.02" width="0.1524" layer="91"/>
<junction x="101.6" y="-33.02"/>
<pinref part="1_READ" gate="G$1" pin="1"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="93.98" y1="-7.62" x2="101.6" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-7.62" x2="101.6" y2="-17.78" width="0.1524" layer="91"/>
<junction x="101.6" y="-17.78"/>
<pinref part="R75" gate="G$1" pin="1"/>
<wire x1="88.9" y1="-55.88" x2="93.98" y2="-55.88" width="0.1524" layer="91"/>
<junction x="93.98" y="-55.88"/>
<pinref part="L_AMP" gate="G$1" pin="A"/>
<junction x="99.06" y="-38.1"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QH*"/>
<wire x1="370.84" y1="160.02" x2="370.84" y2="139.7" width="0.1524" layer="91"/>
<wire x1="370.84" y1="139.7" x2="347.98" y2="139.7" width="0.1524" layer="91"/>
<pinref part="IC5" gate="A" pin="SER"/>
<wire x1="347.98" y1="139.7" x2="347.98" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="GAIN_3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="83.82" y1="-7.62" x2="78.74" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-7.62" x2="78.74" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-35.56" x2="78.74" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-25.4" x2="78.74" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="GAIN_1" gate="G$1" pin="1"/>
<wire x1="91.44" y1="-25.4" x2="78.74" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="AMP_M" gate="A" pin="-IN"/>
<junction x="78.74" y="-35.56"/>
<junction x="78.74" y="-25.4"/>
</segment>
</net>
<net name="MIDI_2_D" class="0">
<segment>
<pinref part="IC16" gate="G$1" pin="A"/>
<pinref part="R151" gate="G$1" pin="2"/>
<wire x1="198.12" y1="172.72" x2="195.58" y2="170.18" width="0.1524" layer="91"/>
<pinref part="D21" gate="G$1" pin="C"/>
<wire x1="195.58" y1="170.18" x2="193.04" y2="172.72" width="0.1524" layer="91"/>
<junction x="195.58" y="170.18"/>
</segment>
</net>
<net name="MIDI_3_D" class="0">
<segment>
<pinref part="IC16" gate="G$1" pin="C"/>
<wire x1="195.58" y1="165.1" x2="198.12" y2="165.1" width="0.1524" layer="91"/>
<pinref part="D21" gate="G$1" pin="A"/>
<wire x1="195.58" y1="165.1" x2="170.18" y2="165.1" width="0.1524" layer="91"/>
<junction x="195.58" y="165.1"/>
<wire x1="170.18" y1="165.1" x2="170.18" y2="175.26" width="0.1524" layer="91"/>
<pinref part="MIDI_IN" gate="G$1" pin="LEFT"/>
<wire x1="170.18" y1="175.26" x2="162.56" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$168" class="0">
<segment>
<pinref part="R152" gate="G$1" pin="2"/>
<pinref part="R151" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="GATE_A" gate="G$1" pin="TIP"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="200.66" y1="58.42" x2="195.58" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R42" gate="G$1" pin="1"/>
<pinref part="GATE_2" gate="G$1" pin="TIP"/>
<wire x1="86.36" y1="152.4" x2="91.44" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="1"/>
<pinref part="GATE_3" gate="G$1" pin="TIP"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R73" gate="G$1" pin="1"/>
<pinref part="1_EF" gate="G$1" pin="TIP"/>
<wire x1="144.78" y1="-63.5" x2="142.24" y2="-63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="DIR_CV_3" gate="G$1" pin="TIP"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="DIR_CV_2" gate="G$1" pin="TIP"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="GATE_B" gate="G$1" pin="TIP"/>
<pinref part="R10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="L_GATE" gate="G$1" pin="C"/>
<pinref part="IC5" gate="A" pin="QH"/>
<wire x1="398.78" y1="104.14" x2="373.38" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="L_GATE" gate="G$1" pin="A"/>
<pinref part="R66" gate="G$1" pin="2"/>
<wire x1="406.4" y1="104.14" x2="411.48" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="R50" gate="G$1" pin="2"/>
<pinref part="L_6" gate="G$1" pin="A"/>
<wire x1="411.48" y1="111.76" x2="406.4" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="L_5" gate="G$1" pin="A"/>
<pinref part="R57" gate="G$1" pin="2"/>
<wire x1="406.4" y1="119.38" x2="411.48" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="R56" gate="G$1" pin="2"/>
<pinref part="L_4" gate="G$1" pin="A"/>
<wire x1="411.48" y1="127" x2="406.4" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="L_H" gate="G$1" pin="A"/>
<pinref part="R65" gate="G$1" pin="2"/>
<wire x1="401.32" y1="35.56" x2="403.86" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="L_G" gate="G$1" pin="A"/>
<pinref part="R64" gate="G$1" pin="2"/>
<wire x1="401.32" y1="43.18" x2="403.86" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="L_F" gate="G$1" pin="A"/>
<pinref part="R63" gate="G$1" pin="2"/>
<wire x1="401.32" y1="50.8" x2="403.86" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="L_E" gate="G$1" pin="A"/>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="401.32" y1="58.42" x2="403.86" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="L_GATE_B" gate="G$1" pin="C"/>
<pinref part="R11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="L_GATE_C" gate="G$1" pin="C"/>
<pinref part="R12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="L_OUT_D" gate="G$1" pin="C"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="L_GATE_D" gate="G$1" pin="C"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="L_OUT_E" gate="G$1" pin="C"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="L_GATE_E" gate="G$1" pin="C"/>
<pinref part="R38" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="L_OUT_C" gate="G$1" pin="C"/>
<pinref part="R39" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="L_OUT_B" gate="G$1" pin="C"/>
<pinref part="R76" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<pinref part="L_EF" gate="G$1" pin="C"/>
<pinref part="R77" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<pinref part="L_AMP" gate="G$1" pin="C"/>
<pinref part="R78" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<pinref part="B_MIDI" gate="G$2" pin="2"/>
<pinref part="D20" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-88.9" x2="426.72" y2="-88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<pinref part="IN_5" gate="G$1" pin="SWITCH"/>
<wire x1="63.5" y1="27.94" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<pinref part="LDR" gate="G$1" pin="2"/>
<wire x1="63.5" y1="43.18" x2="50.8" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
