<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="5" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Mechanical" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Gehäuse" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="blind_vias" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="card" color="13" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="_Dimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="6" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="_tanames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="_banames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="_taDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="_baDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="252" name="Panel_note" color="13" fill="1" visible="yes" active="yes"/>
<layer number="253" name="panel" color="13" fill="1" visible="yes" active="yes"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="xfer" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+05V" urn="urn:adsk.eagle:symbol:26987/1">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" urn="urn:adsk.eagle:component:27032/1" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_R&amp;D">
<packages>
<package name="DIL08">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="5.08" y1="2.921" x2="-5.08" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-3.937" y="-3.81" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="2" x="-1.27" y="-3.683" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="3" x="1.27" y="-3.683" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="4" x="3.937" y="-3.81" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="5" x="3.937" y="3.81" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="6" x="1.27" y="3.683" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="7" x="-1.27" y="3.683" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<pad name="8" x="-3.937" y="3.81" drill="0.8128" diameter="1.27" shape="octagon" rot="R90"/>
<text x="-5.334" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-5.08" y1="4.318" x2="-5.334" y2="4.064" width="0.127" layer="21"/>
<wire x1="-5.334" y1="4.064" x2="-5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="4.318" x2="5.334" y2="4.064" width="0.127" layer="21"/>
<wire x1="5.334" y1="4.064" x2="5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="1.016" y1="5.08" x2="1.27" y2="4.826" width="0.127" layer="21"/>
<wire x1="1.27" y1="4.826" x2="1.524" y2="5.08" width="0.127" layer="21"/>
<wire x1="-1.524" y1="5.08" x2="-1.27" y2="4.826" width="0.127" layer="21"/>
<wire x1="-1.27" y1="4.826" x2="-1.016" y2="5.08" width="0.127" layer="21"/>
<wire x1="-1.524" y1="-4.826" x2="-1.27" y2="-4.572" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-4.572" x2="-1.016" y2="-4.826" width="0.127" layer="21"/>
<wire x1="1.016" y1="-4.826" x2="1.27" y2="-4.572" width="0.127" layer="21"/>
<wire x1="1.27" y1="-4.572" x2="1.524" y2="-4.826" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.556" x2="-5.334" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-5.334" y1="-3.81" x2="-5.08" y2="-4.064" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.556" x2="5.334" y2="-3.81" width="0.127" layer="21"/>
<wire x1="5.334" y1="-3.81" x2="5.08" y2="-4.064" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
</symbols>
<devicesets>
<deviceset name="DIL8">
<gates>
</gates>
<devices>
<device name="" package="DIL08">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_PINHEADERS">
<packages>
<package name="HEADER_1X3_FEMALE">
<wire x1="3.81" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<pad name="1" x="-2.7178" y="0" drill="0.8128" rot="R90"/>
<pad name="2" x="0" y="0" drill="0.8128" rot="R90"/>
<pad name="3" x="2.7178" y="0" drill="0.8128"/>
<text x="0" y="2.54" size="1.016" layer="51" font="fixed" rot="R180" align="center">&gt;NAME</text>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
</package>
<package name="1X04">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<wire x1="-4.3" y1="6.9" x2="26.8" y2="6.9" width="0.2032" layer="21"/>
<wire x1="26.8" y1="6.9" x2="26.8" y2="-6.6" width="0.2032" layer="21"/>
<wire x1="26.8" y1="-6.6" x2="-4.3" y2="-6.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-6.6" x2="-4.3" y2="6.9" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.4" shape="offset" rot="R90"/>
<pad name="2" x="7.62" y="0" drill="1.4" shape="offset" rot="R90"/>
<pad name="3" x="15.24" y="0" drill="1.4" shape="offset" rot="R90"/>
<pad name="4" x="22.86" y="0" drill="1.4" shape="offset" rot="R90"/>
<text x="-1.27" y="5.04" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="3.77" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="0" y1="6.85" x2="6.85" y2="6.85" width="0.127" layer="21"/>
<wire x1="3" y1="7" x2="3" y2="-6.5" width="0.127" layer="21"/>
<wire x1="10.5" y1="7" x2="10.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="18.5" y1="7" x2="18.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="-3" y1="7" x2="-3" y2="-6.5" width="0.127" layer="21"/>
<wire x1="25.5" y1="7" x2="25.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="4" y1="7" x2="4" y2="-6.5" width="0.127" layer="21"/>
<wire x1="11.5" y1="7" x2="11.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="19.5" y1="7" x2="19.5" y2="-6.5" width="0.127" layer="21"/>
</package>
<package name="1X04-1.27MM">
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="-0.127" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
</package>
<package name="MOLEX-1X4_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
</package>
<package name="1X04-SMD">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="1X04_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-4-PTH">
<wire x1="-4.5" y1="-5" x2="-5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5" x2="-5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-6.3" x2="-6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-6" y1="-6.3" x2="-6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-6" y1="1.1" x2="6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6" y1="1.1" x2="6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="6" y1="-6.3" x2="5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-6.3" x2="5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-5" x2="4.5" y2="-5" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-5" drill="0.7"/>
<pad name="2" x="-1" y="-5" drill="0.7"/>
<pad name="3" x="1" y="-5" drill="0.7"/>
<pad name="4" x="3" y="-5" drill="0.7"/>
<text x="-2.27" y="0.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.27" y="-1" size="0.4064" layer="27">&gt;Value</text>
<text x="-3.4" y="-4.3" size="1.27" layer="51">+</text>
<text x="-1.4" y="-4.3" size="1.27" layer="51">-</text>
<text x="0.7" y="-4.1" size="0.8" layer="51">S</text>
<text x="2.7" y="-4.1" size="0.8" layer="51">S</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1MM-RA">
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X04_SMD_STRAIGHT_COMBO">
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-SMD_LONG">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
</package>
<package name="1X04_SMALLPADS">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0.127" y="0" drill="0.85" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.85" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.85" rot="R90"/>
<pad name="4" x="7.493" y="0" drill="0.85" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X6">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="MOLEX-1X6-RA">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SMD">
<wire x1="7.62" y1="1.25" x2="-7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="1.25" x2="-7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-1.25" x2="-6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="7.62" y1="-1.25" x2="7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="6.35" y2="-7.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-6.35" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-5.08" y="0" drill="1.4"/>
<hole x="5.08" y="0" drill="1.4"/>
</package>
<package name="1X06_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="0.508" x2="-0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.143" x2="0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.143" x2="1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.905" y2="1.143" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.143" x2="3.175" y2="1.143" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.143" x2="3.81" y2="0.508" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="4.445" y2="1.143" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.143" x2="5.715" y2="1.143" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.143" x2="6.35" y2="0.508" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.985" y2="1.143" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.143" x2="8.255" y2="1.143" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.143" x2="8.89" y2="0.508" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="9.525" y2="1.143" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.143" x2="10.795" y2="1.143" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.143" x2="11.43" y2="0.508" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="12.065" y2="1.143" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.143" x2="13.335" y2="1.143" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.143" x2="13.97" y2="0.508" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.508" x2="13.97" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.762" x2="13.335" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.397" x2="12.065" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.397" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.762" x2="10.795" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.397" x2="9.525" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.397" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.762" x2="8.255" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.397" x2="6.985" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.397" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="5.715" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.397" x2="4.445" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.397" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.175" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.397" x2="1.905" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.397" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="octagon"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="octagon"/>
<pad name="5" x="10.16" y="0" drill="1.016" shape="octagon"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" shape="octagon"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="9.144" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="11.684" y1="-0.127" x2="11.176" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.716" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.1176" x2="13.6906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.8636" x2="13.6906" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6-RA_LOCK">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SIP_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-2.921" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_FEMALE_LOCK.010">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="13.97" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="1.27" x2="13.97" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3175" y1="-0.1905" x2="0.3175" y2="0.1905" layer="51"/>
<rectangle x1="2.2225" y1="-0.1905" x2="2.8575" y2="0.1905" layer="51"/>
<rectangle x1="4.7625" y1="-0.1905" x2="5.3975" y2="0.1905" layer="51"/>
<rectangle x1="7.3025" y1="-0.1905" x2="7.9375" y2="0.1905" layer="51"/>
<rectangle x1="9.8425" y1="-0.1905" x2="10.4775" y2="0.1905" layer="51"/>
<rectangle x1="12.3825" y1="-0.1905" x2="13.0175" y2="0.1905" layer="51"/>
</package>
<package name="1X06_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06-SMD_EDGE">
<wire x1="13.97" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-11.176" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-11.176" x2="13.97" y2="-11.176" width="0.127" layer="51"/>
<wire x1="13.97" y1="-11.176" x2="13.97" y2="-2.54" width="0.127" layer="51"/>
<smd name="4" x="7.62" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="10.16" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="12.7" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<text x="0" y="-6.35" size="0.4064" layer="51">thru-hole vertical Female Header</text>
<text x="0" y="-7.62" size="0.4064" layer="51">used as an SMD part </text>
<text x="0" y="-8.89" size="0.4064" layer="51">(placed horizontally, at board's edge)</text>
<rectangle x1="-0.381" y1="-2.4892" x2="0.381" y2="0.2794" layer="51"/>
<rectangle x1="2.159" y1="-2.4892" x2="2.921" y2="0.2794" layer="51"/>
<rectangle x1="4.699" y1="-2.4892" x2="5.461" y2="0.2794" layer="51"/>
<rectangle x1="7.239" y1="-2.4892" x2="8.001" y2="0.2794" layer="51"/>
<rectangle x1="9.779" y1="-2.4892" x2="10.541" y2="0.2794" layer="51"/>
<rectangle x1="12.319" y1="-2.4892" x2="13.081" y2="0.2794" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-6">
<wire x1="-2.3" y1="3.4" x2="19.76" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.76" y1="3.4" x2="19.76" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-2.8" x2="19.76" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="19.76" y1="3.15" x2="20.16" y2="3.15" width="0.2032" layer="51"/>
<wire x1="20.16" y1="3.15" x2="20.16" y2="2.15" width="0.2032" layer="51"/>
<wire x1="20.16" y1="2.15" x2="19.76" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="5" x="14" y="0" drill="1.2" diameter="2.032"/>
<pad name="6" x="17.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X06-SMD-FEMALE">
<wire x1="-7.62" y1="4.05" x2="7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="4.05" x2="7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="-4.05" x2="-7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="-4.05" x2="-7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="-6.85" y1="-3" x2="-6.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-6.85" y1="-2" x2="-5.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-5.85" y1="-2" x2="-5.85" y2="-3" width="0.2032" layer="21"/>
<wire x1="-7.1" y1="4" x2="-7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="4" x2="-7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="-4" x2="-7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.1" y1="4" x2="7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="4" x2="7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="-4" x2="7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="4" x2="0.3" y2="4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="4" x2="-2.2" y2="4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="4" x2="-4.8" y2="4" width="0.2032" layer="21"/>
<wire x1="2.3" y1="4" x2="2.9" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="4" x2="5.4" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-4" x2="5.4" y2="-4" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-4" x2="2.8" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-4" x2="0.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-4" x2="-2.2" y2="-4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="-4" x2="-4.8" y2="-4" width="0.2032" layer="21"/>
<smd name="6" x="6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="5" x="3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="4" x="1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="3" x="-1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="2" x="-3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="1" x="-6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A1" x="-6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A2" x="-3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A3" x="-1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A4" x="1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A5" x="3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A6" x="6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
</package>
<package name="1X06-SMD-FEMALE-V2">
<description>Package for 4UCONN part #19686 *UNPROVEN*</description>
<wire x1="-7.5" y1="0.45" x2="-7.5" y2="-8.05" width="0.127" layer="21"/>
<wire x1="7.5" y1="0.45" x2="-7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="7.5" y1="-8.05" x2="7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-8.05" x2="7.5" y2="-8.05" width="0.127" layer="21"/>
<smd name="4" x="-1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="5" x="-3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="6" x="-6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="3" x="1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="7.6" y="-8.3" size="1" layer="27" rot="R180">&gt;Value</text>
<text x="-7.4" y="-9.3" size="1" layer="25">&gt;Name</text>
</package>
<package name="1X06_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<hole x="7.62" y="0" drill="1.4732"/>
<hole x="10.16" y="0" drill="1.4732"/>
<hole x="12.7" y="0" drill="1.4732"/>
</package>
<package name="1X06_SMD_STRAIGHT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="1.25" x2="-10.883" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="-1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="-1.817" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.377" y1="1.25" x2="-0.703" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.457" y1="1.25" x2="-5.783" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.329" y1="-1.25" x2="-6.831" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.409" y1="-1.25" x2="-11.911" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="-12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_ALT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="1.25" x2="-1.817" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-10.883" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.323" y1="1.25" x2="-11.997" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.243" y1="1.25" x2="-6.917" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.371" y1="-1.25" x2="-5.869" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.291" y1="-1.25" x2="-0.789" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="-12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="-2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_COMBO">
<wire x1="12.7" y1="1.27" x2="12.7" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="1.25" x2="14.07" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="-1.25" x2="13.4" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.4" y1="1.25" x2="14.07" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="1.25" x2="11.911" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="-1.29" x2="11.911" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="8.409" y1="1.25" x2="9.371" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.409" y1="-1.29" x2="9.371" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="5" x="10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5-2" x="10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6-2" x="12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="BTA">
<description>British Telecom connector, used for Vernier sensors (analog)</description>
<hole x="6.45" y="0" drill="3.2"/>
<hole x="-6.45" y="0" drill="3.2"/>
<pad name="1" x="4.75" y="9" drill="1.4" diameter="2.54" shape="square" rot="R90"/>
<pad name="2" x="3.25" y="6" drill="1.4" diameter="2.54"/>
<pad name="3" x="1.75" y="9" drill="1.4" diameter="2.54"/>
<pad name="5" x="-1.25" y="9" drill="1.4" diameter="2.54"/>
<pad name="4" x="0.25" y="6" drill="1.4" diameter="2.54"/>
<pad name="6" x="-2.75" y="6" drill="1.4" diameter="2.54"/>
<wire x1="-8" y1="-7.5" x2="-8" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-3.8" x2="-7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-8" y1="-7.5" x2="7.9" y2="-7.5" width="0.2032" layer="21"/>
<wire x1="7.9" y1="-7.5" x2="7.9" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="9.9" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-8.925" y1="-3.8" x2="-8.925" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="8.925" y1="-3.8" x2="8.925" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-8.925" y1="-5.1" x2="-8" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.9" y1="-5.1" x2="8.925" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-8.925" y1="-3.8" x2="-7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-3.8" x2="7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="8.925" y2="-3.8" width="0.2032" layer="21"/>
<text x="-2.92" y="-7.07" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-8" y1="-5.1" x2="7.9" y2="-5.1" width="0.2032" layer="21"/>
</package>
<package name="BTD">
<description>British Telecom connector, used for Vernier sensors (digital)</description>
<hole x="6.45" y="0" drill="3.2"/>
<hole x="-6.45" y="0" drill="3.2"/>
<wire x1="-7.95" y1="-3.8" x2="-7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-7.5" x2="7.95" y2="-7.5" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="9.9" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-8.975" y1="-3.8" x2="-8.975" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="8.875" y1="-3.8" x2="8.875" y2="-5.1" width="0.2032" layer="21"/>
<pad name="2" x="-3.25" y="9" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="1" x="-4.75" y="6" drill="1.4" diameter="2.54" shape="square" rot="R180"/>
<pad name="4" x="-0.25" y="9" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="6" x="2.75" y="9" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="3" x="-1.75" y="6" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="5" x="1.25" y="6" drill="1.4" diameter="2.54" rot="R180"/>
<wire x1="-8.975" y1="-5.1" x2="-7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-5.1" x2="7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-5.1" x2="8.875" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-8.975" y1="-3.8" x2="-7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-3.8" x2="7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-7.5" x2="7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-7.5" x2="-7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="8.875" y2="-3.8" width="0.2032" layer="21"/>
<text x="-3" y="-7" size="1.27" layer="25">&gt;Name</text>
</package>
<package name="1X06_SMD_MALE">
<text x="-1" y="3.321" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1" y="-4.591" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="13.97" y2="-1.25" width="0.127" layer="51"/>
<wire x1="13.97" y1="-1.25" x2="13.97" y2="1.25" width="0.127" layer="51"/>
<wire x1="13.97" y1="1.25" x2="-1.27" y2="1.25" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="0" x2="0.32" y2="2.75" layer="51"/>
<rectangle x1="4.76" y1="0" x2="5.4" y2="2.75" layer="51"/>
<rectangle x1="9.84" y1="0" x2="10.48" y2="2.75" layer="51"/>
<rectangle x1="2.22" y1="-2.75" x2="2.86" y2="0" layer="51" rot="R180"/>
<rectangle x1="7.3" y1="-2.75" x2="7.94" y2="0" layer="51" rot="R180"/>
<rectangle x1="12.38" y1="-2.75" x2="13.02" y2="0" layer="51" rot="R180"/>
<smd name="1" x="0" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="3" x="5.08" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="4" x="7.62" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="5" x="10.16" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="6" x="12.7" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="-1.25" x2="-0.635" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="1.25" x2="-0.635" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.762" y1="1.25" x2="1.778" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.302" y1="1.25" x2="4.318" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.842" y1="1.25" x2="6.858" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.382" y1="1.25" x2="9.398" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.922" y1="1.25" x2="11.938" y2="1.25" width="0.1778" layer="21"/>
<wire x1="1.778" y1="-1.25" x2="0.762" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="4.318" y1="-1.25" x2="3.302" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="6.858" y1="-1.25" x2="5.842" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="9.398" y1="-1.25" x2="8.382" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="11.938" y1="-1.25" x2="10.922" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="-1.25" x2="13.97" y2="1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="-1.25" x2="13.335" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="1.25" x2="13.335" y2="1.25" width="0.1778" layer="21"/>
</package>
<package name="1X06-1MM">
<description>Works with part #579 (https://www.sparkfun.com/products/579). Commonly used on GPS modules EM408, EM406 and GP-635T. Also compatible with cable 9123 (https://www.sparkfun.com/products/9123) and cable 574 (https://www.sparkfun.com/products/574).</description>
<circle x="-3.6" y="1.2" radius="0.1047" width="0.4064" layer="51"/>
<wire x1="-2.54" y1="-1.651" x2="2.54" y2="-1.651" width="0.254" layer="21"/>
<wire x1="-4.318" y1="0.508" x2="-4.318" y2="1.905" width="0.254" layer="21"/>
<wire x1="3.302" y1="1.905" x2="4.318" y2="1.905" width="0.254" layer="21"/>
<wire x1="4.318" y1="1.905" x2="4.318" y2="0.508" width="0.254" layer="21"/>
<wire x1="-4.318" y1="1.905" x2="-3.302" y2="1.905" width="0.254" layer="21"/>
<smd name="1" x="-2.54" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-1.54" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-0.54" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="0.46" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.46" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="2.46" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="P$1" x="-3.84" y="-0.955" dx="1.2" dy="1.8" layer="1"/>
<smd name="P$2" x="3.76" y="-0.955" dx="1.2" dy="1.8" layer="1"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06_LOCK_SMALLPADS">
<pad name="1" x="0.1524" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="6" x="12.5476" y="0" drill="0.9" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<wire x1="-1.27" y1="0.6270625" x2="-0.635" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.2620625" x2="0.635" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.2620625" x2="1.27" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.6270625" x2="1.905" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.2620625" x2="3.175" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.2620625" x2="3.81" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.6270625" x2="4.445" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.2620625" x2="5.715" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.2620625" x2="6.35" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.6270625" x2="6.985" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.2620625" x2="8.255" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.2620625" x2="8.89" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.6270625" x2="9.525" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.2620625" x2="10.795" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.2620625" x2="11.43" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.6270625" x2="12.065" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.2620625" x2="13.335" y2="1.2620625" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.2620625" x2="13.97" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.6270625" x2="13.97" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.6429375" x2="13.335" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.2779375" x2="12.065" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.2779375" x2="11.43" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.6429375" x2="10.795" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.2779375" x2="9.525" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.2779375" x2="8.89" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.6429375" x2="8.255" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.2779375" x2="6.985" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.2779375" x2="6.35" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.6429375" x2="5.715" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.2779375" x2="4.445" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.2779375" x2="3.81" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.6429375" x2="3.175" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.2779375" x2="1.905" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.2779375" x2="1.27" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.6429375" x2="0.635" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.2779375" x2="-0.635" y2="-1.2779375" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.2779375" x2="-1.27" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.6429375" x2="-1.27" y2="0.6270625" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.6270625" x2="1.27" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.6270625" x2="3.81" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.6270625" x2="6.35" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.6270625" x2="8.89" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.6270625" x2="11.43" y2="-0.6429375" width="0.2032" layer="21"/>
<wire x1="-0.714375" y1="-1.6271875" x2="0.635" y2="-1.6271875" width="0.1524" layer="21"/>
</package>
<package name="1X06_POGGO">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01_LONGPAD">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1" diameter="1.6764" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="LUXEON-PAD">
<smd name="P$1" x="0" y="0" dx="3.9" dy="2.4" layer="1" roundness="25"/>
<text x="-1.5" y="2" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SMTSO-256-ET">
<wire x1="-2.286" y1="1.016" x2="-1.016" y2="2.286" width="1.016" layer="31" curve="-42.075022"/>
<wire x1="1.016" y1="2.286" x2="2.286" y2="1.016" width="1.016" layer="31" curve="-42.075022"/>
<wire x1="2.286" y1="-1.016" x2="1.016" y2="-2.286" width="1.016" layer="31" curve="-42.075022"/>
<wire x1="-1.016" y1="-2.286" x2="-2.286" y2="-1.016" width="1.016" layer="31" curve="-42.075022"/>
<circle x="0" y="0" radius="1.016" width="0.127" layer="51"/>
<pad name="P$1" x="0" y="0" drill="3.81" diameter="6.1976"/>
</package>
<package name="SMTRA-256-8-6">
<pad name="P$1" x="-1.9939" y="0" drill="1.3462"/>
<pad name="P$2" x="1.9939" y="0" drill="1.3462"/>
<smd name="P$3" x="0" y="0" dx="6.6548" dy="4.3434" layer="1" cream="no"/>
<text x="1.27" y="2.54" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.54" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<rectangle x1="-3.302" y1="0.762" x2="3.302" y2="2.032" layer="31"/>
<rectangle x1="-1.016" y1="0.508" x2="1.016" y2="0.762" layer="31"/>
<rectangle x1="-1.016" y1="-1.016" x2="1.016" y2="-0.762" layer="31"/>
<rectangle x1="-3.302" y1="-2.032" x2="3.302" y2="-0.762" layer="31"/>
<rectangle x1="-1.016" y1="-0.762" x2="1.016" y2="-0.508" layer="31"/>
<rectangle x1="2.794" y1="0.508" x2="3.302" y2="0.762" layer="31"/>
<rectangle x1="2.794" y1="-0.762" x2="3.302" y2="-0.508" layer="31"/>
<rectangle x1="-3.302" y1="-0.762" x2="-2.794" y2="-0.508" layer="31"/>
<rectangle x1="-3.302" y1="0.508" x2="-2.794" y2="0.762" layer="31"/>
</package>
<package name="1X01NS">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
</package>
<package name="1X01">
<wire x1="1.27" y1="0.635" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="1.1938" y="1.8288" size="1.27" layer="25" font="fixed">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27" font="fixed">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01_2MM">
<pad name="1" x="0" y="0" drill="2" diameter="3.302" rot="R90"/>
<text x="-1.3462" y="1.8288" size="0.8" layer="25" font="fixed">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="0.8" layer="27" font="fixed">&gt;VALUE</text>
</package>
<package name="1X01_OFFSET">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<text x="1.1938" y="1.8288" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="PAD-1.5X4.5">
<smd name="P$1" x="0" y="0" dx="1.5" dy="2" layer="1"/>
</package>
<package name="1X01_POGOPIN_HOLE_LARGE">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90" thermals="no"/>
<hole x="0" y="0" drill="1.5494"/>
</package>
<package name="1X01_POGOPIN_HOLE_0.58">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90" thermals="no"/>
<hole x="0" y="0" drill="1.4732"/>
</package>
<package name="SNAP-FEMALE">
<pad name="1" x="0" y="0" drill="2.921" diameter="4.572"/>
<polygon width="0.254" layer="1">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="29">
<vertex x="-4.0005" y="0" curve="-90.002865"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="31">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="41">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
</package>
<package name="SNAP-MALE">
<smd name="2" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100"/>
<text x="-1.397" y="0.889" size="0.8128" layer="21" font="vector" ratio="15">&gt;NAME</text>
</package>
<package name="SPRING-CONNECTOR">
<smd name="P$2" x="0" y="0" dx="7.112" dy="7.112" layer="1" roundness="100"/>
<text x="-1.3462" y="5.6388" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X01NS-KIT">
<pad name="1" x="0" y="0" drill="1.016" shape="octagon" rot="R90" stop="no"/>
<circle x="0" y="0" radius="0.508" width="0" layer="29"/>
<circle x="0" y="0" radius="0.9398" width="0" layer="30"/>
</package>
<package name="1X01_POGO_SMALL">
<pad name="1" x="0" y="0" drill="0.6" shape="octagon" rot="R90" stop="no"/>
<circle x="0" y="0" radius="0.3" width="0" layer="29"/>
<circle x="0" y="0" radius="0.7" width="0" layer="30"/>
</package>
<package name="1X03">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X3">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3">
<wire x1="-2.3" y1="3.4" x2="9.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="3.4" x2="9.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="9.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="9.3" y1="3.15" x2="9.7" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="3.15" x2="9.7" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="2.15" x2="9.3" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_LOCK">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0.1666875" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0.03175" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="4.92125" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.1176" x2="6.0706" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.8636" x2="6.0706" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X3_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<wire x1="-2.3" y1="3.4" x2="9.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="3.4" x2="9.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="9.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="9.3" y1="3.15" x2="9.7" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="3.15" x2="9.7" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="2.15" x2="9.3" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7.1778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-3-PTH">
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6256"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6256"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6256"/>
<text x="-1.27" y="5.24" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="3.97" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
</package>
<package name="1X03_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-5MM-3">
<wire x1="-3.1" y1="4.2" x2="13.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="4.2" x2="13.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="13.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="13.1" y1="4" x2="13.7" y2="4" width="0.2032" layer="51"/>
<wire x1="13.7" y1="4" x2="13.7" y2="3" width="0.2032" layer="51"/>
<wire x1="13.7" y1="3" x2="13.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.413" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.413"/>
<pad name="3" x="10" y="0" drill="1.3" diameter="2.413"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_NO_SILK">
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-3-SMD">
<wire x1="-4.99" y1="-2.07" x2="-4.99" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.99" y1="-5.57" x2="-4.19" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-5.57" x2="-4.19" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-3.07" x2="-2.99" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="-3.07" x2="4.21" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-3.07" x2="4.21" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-5.57" x2="5.01" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="5.01" y1="-5.57" x2="5.01" y2="-2.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="1.93" x2="-2.99" y2="1.93" width="0.2032" layer="21"/>
<smd name="1" x="-1.99" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="3" x="2.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-4.39" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="4.41" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="0.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<text x="-2.26" y="0.2" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.26" y="-1.07" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="1X03-1MM-RA">
<wire x1="-1" y1="-4.6" x2="1" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="1.75" y1="-0.35" x2="2.4997" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.4997" y1="-0.35" x2="2.4997" y2="-2" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-0.35" x2="-1.75" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="0" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="1" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_FEMALE">
<wire x1="-3.935" y1="4.25" x2="-3.935" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="4.25" x2="-3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="-4.25" x2="3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-3.935" y1="-4.25" x2="3.935" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-0.32" y1="6.8" x2="0.32" y2="7.65" layer="51"/>
<rectangle x1="2.22" y1="6.8" x2="2.86" y2="7.65" layer="51"/>
<rectangle x1="-2.86" y1="6.8" x2="-2.22" y2="7.65" layer="51"/>
<smd name="3" x="2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="0" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-3.155" y="2.775" size="1" layer="27">&gt;Value</text>
<text x="-2.955" y="-3.395" size="1" layer="25">&gt;Name</text>
</package>
<package name="1X03_SMD_RA_MALE">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
</package>
<package name="1X03_SMD_RA_MALE_POST">
<description>&lt;h3&gt;SMD 3-Pin Male Right-Angle Header w/ Alignment posts&lt;/h3&gt;

Matches 4UCONN part # 11026&lt;br&gt;
&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt;http://www.4uconnector.com/online/object/4udrawing/11026.pdf&lt;/a&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.6"/>
<hole x="1.27" y="0" drill="1.6"/>
</package>
<package name="JST-3-PTH-VERT">
<description>This 3-pin connector mates with the JST cable sold on SparkFun.</description>
<wire x1="-3.95" y1="-2.25" x2="-3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="2.25" x2="3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="2.25" x2="3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="0" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="3" x="2" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.4" y="0.75" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.75" size="1.27" layer="51">-</text>
<text x="1.7" y="0.95" size="0.8" layer="51">S</text>
</package>
<package name="2X3_SMD">
<circle x="0" y="1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-2.54" y="1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-2.54" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="0" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="2.54" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="2.54" y="1.27" radius="0.7" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-2.5" x2="-3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.5" x2="3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.5" x2="3.81" y2="-2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.5" x2="-3.81" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-0.3" y1="2.55" x2="0.3" y2="3.35" layer="51"/>
<rectangle x1="-2.84" y1="2.55" x2="-2.24" y2="3.35" layer="51"/>
<rectangle x1="2.24" y1="2.55" x2="2.84" y2="3.35" layer="51"/>
<rectangle x1="-2.84" y1="-3.35" x2="-2.24" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="-0.3" y1="-3.35" x2="0.3" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="2.24" y1="-3.35" x2="2.84" y2="-2.55" layer="51" rot="R180"/>
<smd name="1" x="-2.54" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="2" x="-2.54" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="3" x="0" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="4" x="0" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="5" x="2.54" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="6" x="2.54" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<text x="-3.502" y="0.1" size="0.4064" layer="25">&gt;Name</text>
<text x="-3.502" y="-0.408" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="2X3">
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.605" x2="-0.635" y2="-1.605" width="0.2032" layer="21"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<text x="-1.27" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2X3-NS">
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.2032" layer="51"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.2032" layer="51"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.2032" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<pad name="1" x="-2.38125" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="2" x="-2.38125" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="5" x="2.38125" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="6" x="2.38125" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2X3_POGO">
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.127" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.127" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.127" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.127" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="1.905" width="0.127" layer="21"/>
<wire x1="6.35" y1="1.905" x2="5.715" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.127" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$2" x="0" y="2.54" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$3" x="2.54" y="0" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$4" x="2.54" y="2.54" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$5" x="5.08" y="0" drill="1.2" diameter="1.8288" shape="octagon"/>
<pad name="P$6" x="5.08" y="2.54" drill="1.2" diameter="1.8288" shape="octagon"/>
<text x="-0.508" y="4.064" size="0.4064" layer="25">&gt;NAME</text>
<text x="1.778" y="4.064" size="0.4064" layer="25">&gt;VALUE</text>
</package>
<package name="2X3-SHROUDED">
<wire x1="-2.775" y1="3.175" x2="-2.775" y2="1.905" width="0.2032" layer="21"/>
<wire x1="4.5" y1="7.56" x2="4.5" y2="-7.56" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-7.56" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="7.56" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="7.56" x2="4.4" y2="7.56" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-7.56" x2="-4.5" y2="-7.56" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="6.46" x2="3.4" y2="6.46" width="0.2032" layer="51"/>
<wire x1="3.4" y1="6.46" x2="3.4" y2="-6.46" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-6.46" x2="3.4" y2="-6.46" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="6.46" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-6.46" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<pad name="1" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<text x="-2.921" y="7.874" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<text x="-2.921" y="-8.382" size="0.4064" layer="104">&gt;NAME</text>
</package>
<package name="2X3_SMT_POSTS">
<description>4UCON 15881&lt;br&gt;
Male .1" spaced SMT&lt;br&gt;
Keying posts into board</description>
<wire x1="-1.778" y1="2.54" x2="-0.762" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.762" y1="2.54" x2="1.778" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-2.54" x2="-0.889" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.762" y1="-2.54" x2="1.778" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.175" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="3.175" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.429" y1="-4.064" x2="-3.429" y2="-2.794" width="0.127" layer="21"/>
<smd name="1" x="-2.54" y="-2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="3" x="0" y="-2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="4" x="0" y="2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="5" x="2.54" y="-2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<smd name="6" x="2.54" y="2.54" dx="3.17" dy="1" layer="1" rot="R90"/>
<text x="0" y="-5.08" size="0.6096" layer="27">&gt;VALUE</text>
<text x="0" y="-6.35" size="0.6096" layer="25">&gt;NAME</text>
<hole x="-1.27" y="0" drill="1.8"/>
<hole x="1.27" y="0" drill="1.8"/>
</package>
<package name="2X3_LOCK">
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.2032" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.2032" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.2032" layer="21"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<pad name="1" x="-2.3415625" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="2" x="-2.3415625" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="5" x="2.3415625" y="-1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<pad name="6" x="2.3415625" y="1.27" drill="1.016" diameter="1.524" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="1X3_FEMALE">
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="-2.54" size="1.778" layer="95" rot="R90">&gt;name</text>
</symbol>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M06">
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M01">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="off" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M03">
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M03X2">
<circle x="-1.778" y="2.54" radius="1.016" width="0.1524" layer="94"/>
<circle x="1.778" y="2.54" radius="1.016" width="0.1524" layer="94"/>
<circle x="1.778" y="0" radius="1.016" width="0.1524" layer="94"/>
<circle x="1.778" y="-2.54" radius="1.016" width="0.1524" layer="94"/>
<circle x="-1.778" y="-2.54" radius="1.016" width="0.1524" layer="94"/>
<circle x="-1.778" y="0" radius="1.016" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-2.794" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-2.794" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-2.794" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="2.794" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="2.794" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="2.794" y2="2.54" width="0.1524" layer="94"/>
<pin name="1" x="-7.62" y="2.54" visible="off" length="short"/>
<pin name="2" x="7.62" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="3" x="-7.62" y="0" visible="off" length="short"/>
<pin name="4" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="5" x="-7.62" y="-2.54" visible="off" length="short"/>
<pin name="6" x="7.62" y="-2.54" visible="off" length="short" rot="R180"/>
<text x="-5.08" y="5.588" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.112" size="1.27" layer="96">&gt;VALUE</text>
<text x="-4.318" y="2.794" size="1.4224" layer="95">1</text>
<text x="3.302" y="2.794" size="1.4224" layer="95">2</text>
<text x="-4.318" y="0.254" size="1.4224" layer="95">3</text>
<text x="3.302" y="0.254" size="1.4224" layer="95">4</text>
<text x="-4.318" y="-2.286" size="1.4224" layer="95">5</text>
<text x="3.302" y="-2.286" size="1.4224" layer="95">6</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1X3_FEMALE" prefix="JP">
<gates>
<gate name="G$1" symbol="1X3_FEMALE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HEADER_1X3_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M04" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 4&lt;/b&gt;
Standard 4-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.27MM" package="1X04-1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X04-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_LONG" package="1X04-SMD_LONG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1X04_SMALLPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="SMALL_PADS"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M06" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 6&lt;/b&gt;&lt;br&gt;
Standard 6-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08094 with associated crimp pins and housings.&lt;p&gt;



NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS...
This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place. You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers. Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<gates>
<gate name="G$1" symbol="M06" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SIP" package="1X06">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA" package="MOLEX-1X6-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X06-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X06_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X06_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X6_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA_LOCK" package="MOLEX-1X6-RA_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIP_LOCK" package="1X06-SIP_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE_LOCK" package="1X06_FEMALE_LOCK.010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X06_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X06-SMD_EDGE_FEMALE" package="1X06-SMD_EDGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-6" package="SCREWTERMINAL-3.5MM-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMDF" package="1X06-SMD-FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-FEMALE-V2" package="1X06-SMD-FEMALE-V2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLES_ONLY" package="1X06_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT" package="1X06_SMD_STRAIGHT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-ALT" package="1X06_SMD_STRAIGHT_ALT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-COMBO" package="1X06_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VERNIER-ANALOG" package="BTA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VERNIER-DIGITAL" package="BTD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_MALE" package="1X06_SMD_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11293"/>
</technology>
</technologies>
</device>
<device name="SMD-1MM" package="1X06-1MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_SILK" package="1X06_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_SMALLPADS" package="1X06_LOCK_SMALLPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGGO" package="1X06_POGGO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M01" prefix="JP">
<description>&lt;b&gt;Header 1&lt;/b&gt;
Standard 1-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115).</description>
<gates>
<gate name="G$1" symbol="M01" x="0" y="0"/>
</gates>
<devices>
<device name="PTH_LONGPAD" package="1X01_LONGPAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="LUXEON-PAD">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMTSO-256-ET" package="SMTSO-256-ET">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMTRA-256-8-6" package="SMTRA-256-8-6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMDNS" package="1X01NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_2MM" package="1X01_2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="OFFSET" package="1X01_OFFSET">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-4.5X1.5" package="PAD-1.5X4.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLE_LARGE" package="1X01_POGOPIN_HOLE_LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLE_0.58" package="1X01_POGOPIN_HOLE_0.58">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SNAP-FEMALE" package="SNAP-FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SNAP-MALE" package="SNAP-MALE">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-CONN" package="SPRING-CONNECTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NOSILK-KIT" package="1X01NS-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1X01_POGO_SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name="POGO_SMALL">
<attribute name="VARIANT" value="POGO_SMALL" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M03" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 3&lt;/b&gt;
Standard 3-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08232 with associated crimp pins and housings.</description>
<gates>
<gate name="G$1" symbol="M03" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X03_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X03_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X3_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X03_NO_SILK" package="1X03_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X03_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-3-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09915" constant="no"/>
</technology>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X03_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SCREW-5MM" package="SCREWTERMINAL-5MM-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X03_LOCK_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-SMD" package="JST-3-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X03-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X03_SMD_RA_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10926"/>
<attribute name="VALUE" value="1x3 RA Female .1&quot;"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE" package="1X03_SMD_RA_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10925"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE_POST" package="1X03_SMD_RA_MALE_POST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-3-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M03X2" prefix="JP" uservalue="yes">
<description>2x3 .1" header.&lt;br&gt;
Shrouded, with keying- CONN-10681&lt;br&gt;
SMT- CONN-11415&lt;br&gt;
Pogo pins- HW-11044</description>
<gates>
<gate name="G$1" symbol="M03X2" x="0" y="0"/>
</gates>
<devices>
<device name="&quot;" package="2X3_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2X3_SILK_SM" package="2X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE_SMD" package="2X3_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11290"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="2X3-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PINS" package="2X3_POGO">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="HW-11044"/>
</technology>
</technologies>
</device>
<device name="SHROUD" package="2X3-SHROUDED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10681"/>
</technology>
</technologies>
</device>
<device name="SMT" package="2X3_SMT_POSTS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11415"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="2X3_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_CONNECTORS">
<packages>
<package name="THONKICONN">
<description>3.5mm socket for Eurorack modular synths</description>
<pad name="P$3_TIP" x="0" y="4.84" drill="1.55" shape="square"/>
<pad name="P$2_SWITCH" x="0" y="-3.38" drill="1.55" shape="square"/>
<pad name="P$1_SLEEVE" x="0" y="-6.48" drill="1.35" shape="square"/>
<wire x1="-4.5" y1="6" x2="4.5" y2="6" width="0.127" layer="21"/>
<wire x1="4.5" y1="6" x2="4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4.5" x2="-4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4.5" x2="-4.5" y2="6" width="0.127" layer="21"/>
<text x="-3.8" y="1.5" size="1" layer="21" font="vector">&gt;NAME</text>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.08" layer="21"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.08" layer="21"/>
</package>
<package name="WQP-PJ301M-12_JACK">
<wire x1="-4.5" y1="6" x2="-1.5" y2="6" width="0.127" layer="51"/>
<wire x1="-1.5" y1="6" x2="1.5" y2="6" width="0.127" layer="51"/>
<wire x1="1.5" y1="6" x2="4.5" y2="6" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6" x2="-4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-4.5" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<wire x1="0.2" y1="-4.5" x2="4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="4.5" y1="-4.5" x2="4.5" y2="6" width="0.127" layer="51"/>
<circle x="0" y="0" radius="3.162275" width="0.127" layer="253"/>
<wire x1="-1.5" y1="6" x2="-1.5" y2="4" width="0.127" layer="51"/>
<wire x1="-1.5" y1="4" x2="1.5" y2="4" width="0.127" layer="51"/>
<wire x1="1.5" y1="4" x2="1.5" y2="6" width="0.127" layer="51"/>
<pad name="P$1_TIP" x="0" y="5" drill="1.1" thermals="no"/>
<pad name="P$2_SWITCH" x="0" y="-3.5" drill="1.1"/>
<pad name="P$3_SLEEVE" x="0" y="-6.5" drill="1.1"/>
<circle x="0" y="0" radius="2.690721875" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-4.6" x2="-0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="-0.2" y1="-6.7" x2="0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="0.2" y1="-6.7" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<rectangle x1="-2.8" y1="-2.8" x2="2.8" y2="2.8" layer="39"/>
<text x="4.5212" y="-4.7244" size="1.016" layer="21" rot="SR180">&gt;NAME</text>
<wire x1="-4.5" y1="5.5" x2="-4.5" y2="6" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6" x2="-4" y2="6" width="0.127" layer="21"/>
<wire x1="4" y1="6" x2="4.5" y2="6" width="0.127" layer="21"/>
<wire x1="4.5" y1="6" x2="4.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4" x2="4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4.5" x2="4" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4" x2="-4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4.5" x2="-4" y2="-4.5" width="0.127" layer="21"/>
</package>
<package name="THONKICONN_TIGHT">
<wire x1="-4.5" y1="6" x2="-1.5" y2="6" width="0.127" layer="51"/>
<wire x1="-1.5" y1="6" x2="1.5" y2="6" width="0.127" layer="51"/>
<wire x1="1.5" y1="6" x2="4.5" y2="6" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6" x2="-4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-4.5" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<wire x1="0.2" y1="-4.5" x2="4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="4.5" y1="-4.5" x2="4.5" y2="6" width="0.127" layer="51"/>
<circle x="0" y="0" radius="3.162275" width="0.127" layer="253"/>
<wire x1="-1.5" y1="6" x2="-1.5" y2="4" width="0.127" layer="51"/>
<wire x1="-1.5" y1="4" x2="1.5" y2="4" width="0.127" layer="51"/>
<wire x1="1.5" y1="4" x2="1.5" y2="6" width="0.127" layer="51"/>
<pad name="P$1_TIP" x="0" y="5" drill="1.1" thermals="no"/>
<pad name="P$2_SWITCH" x="0" y="-3.5" drill="1.1"/>
<pad name="P$3_SLEEVE" x="0" y="-6.1063" drill="1.1"/>
<circle x="0" y="0" radius="2.690721875" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-4.6" x2="-0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="-0.2" y1="-6.7" x2="0.2" y2="-6.7" width="0.127" layer="51"/>
<wire x1="0.2" y1="-6.7" x2="0.2" y2="-4.5" width="0.127" layer="51"/>
<rectangle x1="-2.8" y1="-2.8" x2="2.8" y2="2.8" layer="39"/>
<text x="-2.3622" y="6.2738" size="1.016" layer="21" rot="SR0">&gt;NAME</text>
<wire x1="-4.5" y1="5.5" x2="-4.5" y2="6" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6" x2="-4" y2="6" width="0.127" layer="21"/>
<wire x1="4" y1="6" x2="4.5" y2="6" width="0.127" layer="21"/>
<wire x1="4.5" y1="6" x2="4.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4" x2="4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-4.5" x2="4" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4" x2="-4.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-4.5" x2="-4" y2="-4.5" width="0.127" layer="21"/>
</package>
<package name="WQP-PJ366ST_JACK">
<wire x1="-5" y1="6" x2="-1.5" y2="6" width="0.0508" layer="51"/>
<wire x1="-1.5" y1="6" x2="1.5" y2="6" width="0.0508" layer="51"/>
<wire x1="1.5" y1="6" x2="5" y2="6" width="0.0508" layer="51"/>
<wire x1="-5" y1="6" x2="-5" y2="-4.5" width="0.0508" layer="51"/>
<wire x1="-5" y1="-4.5" x2="0.2" y2="-4.5" width="0.0508" layer="51"/>
<wire x1="0.2" y1="-4.5" x2="5" y2="-4.5" width="0.0508" layer="51"/>
<wire x1="5" y1="-4.5" x2="5" y2="6" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="3.162275" width="0.127" layer="253"/>
<wire x1="-1.5" y1="6" x2="-1.5" y2="4" width="0.0508" layer="51"/>
<wire x1="-1.5" y1="4" x2="1.5" y2="4" width="0.0508" layer="51"/>
<wire x1="1.5" y1="4" x2="1.5" y2="6" width="0.0508" layer="51"/>
<pad name="P$1_TIP" x="0" y="5" drill="1.1" thermals="no"/>
<pad name="P$2_SWITCH" x="0" y="-3.5" drill="1.1"/>
<pad name="P$3_SLEEVE" x="0" y="-6.5" drill="1.1"/>
<circle x="0" y="0" radius="2.690721875" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-4.6" x2="-0.2" y2="-6.7" width="0.0508" layer="51"/>
<wire x1="-0.2" y1="-6.7" x2="0.2" y2="-6.7" width="0.0508" layer="51"/>
<wire x1="0.2" y1="-6.7" x2="0.2" y2="-4.5" width="0.0508" layer="51"/>
<rectangle x1="-2.8" y1="-2.8" x2="2.8" y2="2.8" layer="39"/>
<text x="4.5212" y="-4.7244" size="1.016" layer="21" rot="SR180">&gt;NAME</text>
<wire x1="-5" y1="-4.5" x2="-5" y2="-4" width="0.127" layer="21"/>
<wire x1="-5" y1="-4" x2="-5" y2="-2" width="0.127" layer="21"/>
<wire x1="-5" y1="-2" x2="-5" y2="0" width="0.127" layer="21"/>
<wire x1="-5" y1="0" x2="-5" y2="2" width="0.127" layer="21"/>
<wire x1="-5" y1="2" x2="-5" y2="4" width="0.127" layer="21"/>
<wire x1="-5" y1="4" x2="-5" y2="6" width="0.127" layer="21"/>
<wire x1="3" y1="6" x2="5" y2="6" width="0.127" layer="21"/>
<wire x1="5" y1="6" x2="5" y2="4" width="0.127" layer="21"/>
<wire x1="5" y1="4" x2="5" y2="2" width="0.127" layer="21"/>
<wire x1="5" y1="2" x2="5" y2="0" width="0.127" layer="21"/>
<wire x1="5" y1="0" x2="5" y2="-2" width="0.127" layer="21"/>
<wire x1="5" y1="-2" x2="5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="5" y1="-4.5" x2="-5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-5" y1="6" x2="-3" y2="6" width="0.127" layer="21"/>
<wire x1="-3" y1="6" x2="-1" y2="6" width="0.127" layer="21"/>
<wire x1="-1" y1="6" x2="3" y2="6" width="0.127" layer="21"/>
<wire x1="3" y1="6" x2="1" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="4" x2="5" y2="6" width="0.127" layer="21"/>
<wire x1="-5" y1="4" x2="-3" y2="6" width="0.127" layer="21"/>
<wire x1="3" y1="2" x2="5" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="0" x2="5" y2="2" width="0.127" layer="21"/>
<wire x1="3" y1="-2" x2="5" y2="0" width="0.127" layer="21"/>
<wire x1="3" y1="-4" x2="5" y2="-2" width="0.127" layer="21"/>
<wire x1="-1" y1="6" x2="-5" y2="2" width="0.127" layer="21"/>
<wire x1="-3" y1="2" x2="-5" y2="0" width="0.127" layer="21"/>
<wire x1="-3" y1="0" x2="-5" y2="-2" width="0.127" layer="21"/>
<wire x1="-3" y1="-2" x2="-5" y2="-4" width="0.127" layer="21"/>
</package>
<package name="MICROPHONE">
<circle x="0" y="0" radius="4.8" width="0.1524" layer="21"/>
<pad name="P$1" x="-1.27" y="2" drill="0.7"/>
<pad name="P$2" x="1.27" y="2" drill="0.7"/>
<circle x="0" y="0" radius="4.57905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="21"/>
<circle x="0" y="-1.27" radius="1.27" width="0.1524" layer="21"/>
</package>
<package name="PWR_CON_WITHOUT_LABLES">
<wire x1="-1.905" y1="6.35" x2="-2.54" y2="5.715" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="4.445" x2="-1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="3.81" x2="-2.54" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="6.35" x2="1.905" y2="6.35" width="0.1524" layer="21"/>
<wire x1="1.905" y1="6.35" x2="2.54" y2="5.715" width="0.1524" layer="21"/>
<wire x1="2.54" y1="5.715" x2="2.54" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.54" y1="4.445" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="2.54" y2="3.175" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="-1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="3.175" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-3.81" x2="-2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-5.715" x2="-1.905" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-4.445" x2="2.54" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-5.715" x2="1.905" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="-1.905" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-4.445" x2="-2.54" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-6.985" x2="2.54" y2="-6.985" width="0.6096" layer="21"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<pad name="1" x="-1.27" y="4.9037875" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="4.9037875" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="9" x="-1.27" y="-4.9037875" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
<pad name="10" x="1.27" y="-4.9037875" drill="1.016" diameter="1.6764" shape="octagon" rot="R270"/>
</package>
<package name="POWER_JACK">
<wire x1="-4.445" y1="-5.207" x2="-4.445" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-6.35" x2="-2.032" y2="-6.35" width="0.127" layer="21"/>
<wire x1="2.032" y1="-6.35" x2="4.445" y2="-6.35" width="0.127" layer="21"/>
<wire x1="4.445" y1="-6.35" x2="4.445" y2="7.366" width="0.127" layer="21"/>
<wire x1="4.445" y1="7.366" x2="-4.445" y2="7.366" width="0.127" layer="21"/>
<wire x1="-4.445" y1="7.366" x2="-4.445" y2="-1.143" width="0.127" layer="21"/>
<pad name="PIN" x="0" y="-6.0452" drill="0.5" diameter="1.6764" shape="long"/>
<pad name="RING" x="0" y="-0.3048" drill="0.5" diameter="1.7" shape="long" rot="R180"/>
<pad name="SW" x="-4.445" y="-3.175" drill="0.5" diameter="1.6764" shape="long" rot="R270"/>
<polygon width="0.0508" layer="46">
<vertex x="-1.166" y="-0.0508"/>
<vertex x="1.166" y="-0.0508" curve="-90"/>
<vertex x="1.42" y="-0.3048" curve="-90"/>
<vertex x="1.166" y="-0.5588"/>
<vertex x="-1.166" y="-0.5588" curve="-90"/>
<vertex x="-1.42" y="-0.3048" curve="-90"/>
</polygon>
<polygon width="0.0508" layer="46">
<vertex x="-1.016" y="-5.7912"/>
<vertex x="1.016" y="-5.7912" curve="-90"/>
<vertex x="1.27" y="-6.0452" curve="-90"/>
<vertex x="1.016" y="-6.2992"/>
<vertex x="-1.016" y="-6.2992" curve="-90"/>
<vertex x="-1.27" y="-6.0452" curve="-90"/>
</polygon>
<polygon width="0.0508" layer="46">
<vertex x="-4.699" y="-4.191"/>
<vertex x="-4.699" y="-2.159" curve="-90"/>
<vertex x="-4.445" y="-1.905" curve="-90"/>
<vertex x="-4.191" y="-2.159"/>
<vertex x="-4.191" y="-4.191" curve="-90"/>
<vertex x="-4.445" y="-4.445" curve="-90"/>
</polygon>
<text x="0" y="-2.54" size="0.8" layer="21" font="fixed" ratio="15" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-3.81" size="0.8" layer="21" font="fixed" ratio="15" rot="R180" align="center">&gt;VALUE</text>
<polygon width="0.1524" layer="39">
<vertex x="-4.445" y="7.366"/>
<vertex x="4.445" y="7.366"/>
<vertex x="4.445" y="-6.35"/>
<vertex x="-4.445" y="-6.35"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="THONKICONN">
<description>pj301-b vertical 3.5mm jack socket</description>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="1.524" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.524" y1="1.016" x2="2.286" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="2.286" x2="-1.016" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.524" x2="-0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="-0.762" y2="2.286" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="4.572" y2="-2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="4.064" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="2.286" y1="-0.508" x2="7.874" y2="0.508" layer="94" rot="R90"/>
<pin name="SLEEVE" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="TIP" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="SWITCH" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="STEREO-AUDIO-JACK">
<wire x1="-1.27" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="3.048" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.286" y="-2.286" size="1.016" layer="94" ratio="15">L</text>
<text x="2.286" y="0.254" size="1.016" layer="94" ratio="15">R</text>
<rectangle x1="-7.62" y1="-0.762" x2="-2.54" y2="0.762" layer="94" rot="R90"/>
<pin name="RIGHT" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="LEFT" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="SLEEVE" x="5.08" y="2.54" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="MICROHONE">
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="5.08" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" length="middle"/>
<pin name="P$2" x="5.08" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="PWR_CON">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="8.382" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="PWR_JACK">
<wire x1="-5.715" y1="1.27" x2="-5.715" y2="3.81" width="0.1524" layer="94" curve="-180"/>
<wire x1="-5.715" y1="3.81" x2="-1.27" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="4.445" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="-1.27" y2="4.445" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.715" y1="-2.54" x2="-6.35" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="-6.985" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<pin name="PIN" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="RING" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="SW" x="5.08" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<text x="-7.62" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="0" y="-2.54"/>
<vertex x="-0.508" y="-1.27"/>
<vertex x="0.508" y="-1.27"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="THONKICONN" prefix="U">
<description>3.5mm socket for Eurorack modular synths</description>
<gates>
<gate name="G$1" symbol="THONKICONN" x="0" y="0"/>
</gates>
<devices>
<device name="OLD" package="THONKICONN">
<connects>
<connect gate="G$1" pin="SLEEVE" pad="P$1_SLEEVE"/>
<connect gate="G$1" pin="SWITCH" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="TIP" pad="P$3_TIP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NEW" package="WQP-PJ301M-12_JACK">
<connects>
<connect gate="G$1" pin="SLEEVE" pad="P$3_SLEEVE"/>
<connect gate="G$1" pin="SWITCH" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="TIP" pad="P$1_TIP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="THONKICONN_TIGHT">
<connects>
<connect gate="G$1" pin="SLEEVE" pad="P$3_SLEEVE"/>
<connect gate="G$1" pin="SWITCH" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="TIP" pad="P$1_TIP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="THONKI-STEREO" prefix="U">
<description>3.5mm socket for Eurorack modular synths</description>
<gates>
<gate name="G$1" symbol="STEREO-AUDIO-JACK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WQP-PJ366ST_JACK">
<connects>
<connect gate="G$1" pin="LEFT" pad="P$1_TIP"/>
<connect gate="G$1" pin="RIGHT" pad="P$2_SWITCH"/>
<connect gate="G$1" pin="SLEEVE" pad="P$3_SLEEVE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICROPHONE" prefix="M">
<gates>
<gate name="G$1" symbol="MICROHONE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MICROPHONE">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value=""/>
<attribute name="ODOO" value=""/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWR_CON_WITHOUT_LABLES" prefix="J">
<gates>
<gate name="G$1" symbol="PWR_CON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PWR_CON_WITHOUT_LABLES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value="M2X5"/>
<attribute name="ODOO" value=""/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWR_JACK" prefix="J">
<gates>
<gate name="G$1" symbol="PWR_JACK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="POWER_JACK">
<connects>
<connect gate="G$1" pin="PIN" pad="PIN"/>
<connect gate="G$1" pin="RING" pad="RING"/>
<connect gate="G$1" pin="SW" pad="SW"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value=""/>
<attribute name="ODOO" value="PC-GK2.1"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_SWITCHES">
<packages>
<package name="TACTILE-PTH">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.778"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.778"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.778"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.778"/>
<circle x="0" y="0" radius="3.181" width="0.127" layer="253"/>
<circle x="0" y="0" radius="1.55" width="0.127" layer="253"/>
<text x="-5.08" y="0" size="0.8" layer="25" font="fixed" ratio="15" rot="R270" align="center">&gt;NAME</text>
<text x="5.08" y="0" size="0.8" layer="27" font="fixed" ratio="15" rot="R90" align="center">&gt;VALUE</text>
<rectangle x1="-3.1242" y1="-3.1242" x2="3.1242" y2="3.1242" layer="39"/>
<circle x="0" y="0" radius="2.55" width="0.1524" layer="253"/>
</package>
<package name="TACTILE-PTH-12MM">
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
<wire x1="-4.703125" y1="5.375" x2="4.703125" y2="5.375" width="0.127" layer="253"/>
<wire x1="5.375" y1="4.703125" x2="5.375" y2="-4.703125" width="0.127" layer="253"/>
<wire x1="4.703125" y1="-5.375" x2="-4.703125" y2="-5.375" width="0.127" layer="253"/>
<wire x1="-5.375" y1="-4.703125" x2="-5.375" y2="4.703125" width="0.127" layer="253"/>
<wire x1="-5.375" y1="4.703125" x2="-4.703125" y2="5.375" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="5.375" x2="5.375" y2="4.703125" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="-5.375" x2="5.375" y2="-4.703125" width="0.127" layer="253" curve="90"/>
<wire x1="-4.703125" y1="-5.375" x2="-5.375" y2="-4.703125" width="0.127" layer="253" curve="-90"/>
<text x="0" y="7.62" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-7.62" size="0.8" layer="27" font="fixed" ratio="15" rot="R180" align="center">&gt;VALUE</text>
<circle x="0" y="0" radius="4.75" width="0.1524" layer="253"/>
<wire x1="-6.1" y1="6.1" x2="6.1" y2="6.1" width="0.0762" layer="253"/>
<wire x1="6.1" y1="6.1" x2="6.1" y2="-6.1" width="0.0762" layer="253"/>
<wire x1="6.1" y1="-6.1" x2="-6.1" y2="-6.1" width="0.0762" layer="253"/>
<wire x1="-6.1" y1="-6.1" x2="-6.1" y2="6.1" width="0.0762" layer="253"/>
<wire x1="0" y1="6.1" x2="4.7" y2="8" width="0.0762" layer="252"/>
<wire x1="5.2" y1="5.1" x2="8.8" y2="5.8" width="0.0762" layer="252"/>
<wire x1="4.8" y1="0.5" x2="8.1" y2="0.6" width="0.0762" layer="252"/>
<text x="5" y="7.8" size="0.8" layer="252">thyme_rect</text>
<text x="8.4" y="0.3" size="0.8" layer="252">thyme_round</text>
<text x="9.1" y="5.8" size="0.8" layer="252">cv_trin</text>
<rectangle x1="-6.096" y1="-6.096" x2="6.096" y2="6.096" layer="39"/>
</package>
<package name="TACTILE-PTH-SIDEEZ">
<circle x="2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<circle x="2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="-2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<wire x1="1.5" y1="-3.8" x2="-1.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-3.65" y2="3.5" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="3.5" x2="-3" y2="3.5" width="0.2032" layer="51"/>
<wire x1="3" y1="3.5" x2="3.65" y2="3.5" width="0.2032" layer="51"/>
<wire x1="3.65" y1="3.5" x2="3.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2" x2="3" y2="2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2" x2="-3" y2="3.5" width="0.2032" layer="51"/>
<wire x1="3" y1="2" x2="3" y2="3.5" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-1.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2" x2="1.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-2" x2="3.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="1" x2="-3.65" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="-2" x2="3.65" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.65" y1="-2" x2="3.65" y2="1" width="0.2032" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="ANCHOR1" x="-3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="ANCHOR2" x="3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<text x="-5.08" y="0" size="0.8" layer="25" font="fixed" ratio="15" rot="R270" align="center">&gt;NAME</text>
<text x="5.08" y="0" size="0.8" layer="27" font="fixed" ratio="15" rot="R90" align="center">&gt;VALUE</text>
</package>
<package name="TACTILE_SWITCH_TALL">
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.254" layer="21"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.254" layer="21"/>
<smd name="A1" x="-4.0258" y="-2.25" dx="1.2" dy="1.6" layer="1" rot="R90"/>
<smd name="A2" x="4.0258" y="-2.25" dx="1.2" dy="1.56" layer="1" rot="R90"/>
<smd name="B1" x="-4.0258" y="2.25" dx="1.2" dy="1.6" layer="1" rot="R90"/>
<smd name="B2" x="4.0258" y="2.25" dx="1.2" dy="1.56" layer="1" rot="R90"/>
<circle x="0" y="0" radius="3.181" width="0.127" layer="253"/>
<circle x="0" y="0" radius="1.55" width="0.127" layer="253"/>
<text x="0" y="3.81" size="0.8" layer="25" font="fixed" ratio="15" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-3.81" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="TACTILE-SMD-12MM">
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="-6" y2="5" width="0.2032" layer="21"/>
<smd name="1" x="6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="2" x="-6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="3" x="6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="4" x="-6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<wire x1="-4.703125" y1="5.375" x2="4.703125" y2="5.375" width="0.127" layer="253"/>
<wire x1="5.375" y1="4.703125" x2="5.375" y2="-4.703125" width="0.127" layer="253"/>
<wire x1="4.703125" y1="-5.375" x2="-4.703125" y2="-5.375" width="0.127" layer="253"/>
<wire x1="-5.375" y1="-4.703125" x2="-5.375" y2="4.703125" width="0.127" layer="253"/>
<wire x1="-5.375" y1="4.703125" x2="-4.703125" y2="5.375" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="5.375" x2="5.375" y2="4.703125" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="-5.375" x2="5.375" y2="-4.703125" width="0.127" layer="253" curve="90"/>
<wire x1="-4.703125" y1="-5.375" x2="-5.375" y2="-4.703125" width="0.127" layer="253" curve="-90"/>
<text x="0" y="7.62" size="0.8" layer="25" font="fixed" ratio="15" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-7.62" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="TACTICLE-PTH-12MM-WITHHOLES">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<text x="0" y="6.985" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-7.112" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
<text x="4.064" y="-4.699" size="1.27" layer="21" ratio="10">4</text>
<hole x="0" y="-4.4958" drill="1.8034"/>
<hole x="0" y="4.4958" drill="1.8034"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
<wire x1="-4.703125" y1="5.375" x2="4.703125" y2="5.375" width="0.127" layer="253"/>
<wire x1="5.375" y1="4.703125" x2="5.375" y2="-4.703125" width="0.127" layer="253"/>
<wire x1="4.703125" y1="-5.375" x2="-4.703125" y2="-5.375" width="0.127" layer="253"/>
<wire x1="-5.375" y1="-4.703125" x2="-5.375" y2="4.703125" width="0.127" layer="253"/>
<wire x1="-5.375" y1="4.703125" x2="-4.703125" y2="5.375" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="5.375" x2="5.375" y2="4.703125" width="0.127" layer="253" curve="-90"/>
<wire x1="4.703125" y1="-5.375" x2="5.375" y2="-4.703125" width="0.127" layer="253" curve="90"/>
<wire x1="-4.703125" y1="-5.375" x2="-5.375" y2="-4.703125" width="0.127" layer="253" curve="-90"/>
<wire x1="-6.096" y1="6.096" x2="-6.096" y2="-6.096" width="0.127" layer="253"/>
<wire x1="-6.096" y1="-6.096" x2="6.096" y2="-6.096" width="0.127" layer="253"/>
<wire x1="6.096" y1="-6.096" x2="6.096" y2="6.096" width="0.127" layer="253"/>
<wire x1="6.096" y1="6.096" x2="-6.096" y2="6.096" width="0.127" layer="253"/>
<circle x="0" y="0" radius="4.75" width="0.127" layer="253"/>
</package>
<package name="SLIDE_MID_SPDT_HORIZONTAL">
<pad name="C" x="0" y="0" drill="0.9" rot="R90"/>
<pad name="D" x="0" y="-3.016" drill="0.9" rot="R90"/>
<pad name="U" x="0" y="3.016" drill="0.9" rot="R90"/>
<wire x1="-2.794" y1="-6.35" x2="-2.794" y2="6.35" width="0.127" layer="21"/>
<wire x1="2.794" y1="-6.35" x2="2.794" y2="6.35" width="0.127" layer="21"/>
<wire x1="-2.794" y1="-6.35" x2="-1.651" y2="-6.35" width="0.127" layer="21"/>
<wire x1="1.651" y1="-6.35" x2="2.794" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.794" y1="6.35" x2="-1.651" y2="6.35" width="0.127" layer="21"/>
<rectangle x1="2.794" y1="0.508" x2="9.652" y2="3.302" layer="49"/>
<circle x="-1.6002" y="-3.048" radius="0.071840625" width="0.127" layer="49"/>
<pad name="H3" x="-0.000578125" y="6.5786" drill="0.6" diameter="1.4224" shape="long" rot="R180"/>
<pad name="H4" x="0" y="-6.5786" drill="0.6" diameter="1.4224" shape="long" rot="R180"/>
<polygon width="0.002540625" layer="46">
<vertex x="-0.7112" y="6.5278" curve="-90"/>
<vertex x="-0.3556" y="6.8834"/>
<vertex x="0.381" y="6.8834" curve="-90"/>
<vertex x="0.7112" y="6.5278" curve="-90"/>
<vertex x="0.381" y="6.2738"/>
<vertex x="-0.4064" y="6.2738" curve="-90"/>
</polygon>
<polygon width="0.002540625" layer="46">
<vertex x="-0.7112" y="-6.5786" curve="-90"/>
<vertex x="-0.3556" y="-6.2738"/>
<vertex x="0.381" y="-6.2738" curve="-90"/>
<vertex x="0.7112" y="-6.5786" curve="-90"/>
<vertex x="0.381" y="-6.8834"/>
<vertex x="-0.4064" y="-6.8834" curve="-90"/>
</polygon>
<wire x1="1.651" y1="6.35" x2="2.794" y2="6.35" width="0.127" layer="21"/>
<text x="-0.127" y="8.382" size="0.8" layer="25" font="fixed" ratio="15" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-8.382" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="-2.921" y1="-6.477" x2="2.921" y2="6.477" layer="39"/>
</package>
<package name="SLIDE_MID_SPDT_VERTICAL">
<pad name="C" x="0" y="0" drill="0.9" rot="R90"/>
<pad name="D" x="0" y="-3.016" drill="0.9" rot="R90"/>
<pad name="U" x="0" y="3.016" drill="0.9" rot="R90"/>
<wire x1="-2.794" y1="-6.35" x2="-2.794" y2="6.35" width="0.127" layer="21"/>
<wire x1="2.794" y1="-6.35" x2="2.794" y2="6.35" width="0.127" layer="21"/>
<wire x1="-2.794" y1="-6.35" x2="-1.905" y2="-6.35" width="0.127" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="2.794" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.794" y1="6.35" x2="-1.778" y2="6.35" width="0.127" layer="21"/>
<rectangle x1="-1.27" y1="-3.302" x2="1.27" y2="3.302" layer="49"/>
<wire x1="1.778" y1="6.35" x2="2.794" y2="6.35" width="0.127" layer="21"/>
<circle x="-1.6002" y="-3.0988" radius="0.071840625" width="0.127" layer="49"/>
<pad name="H5" x="0" y="6.174378125" drill="0.6" diameter="1.6764" shape="long" rot="R180"/>
<pad name="H6" x="0" y="-6.1484" drill="0.6" diameter="1.6764" shape="long" rot="R180"/>
<polygon width="0.002540625" layer="46">
<vertex x="-0.7874" y="6.4786"/>
<vertex x="0.7366" y="6.4786" curve="-90"/>
<vertex x="1.1176" y="6.1738" curve="-90"/>
<vertex x="0.7366" y="5.869"/>
<vertex x="-0.7874" y="5.869" curve="-90"/>
<vertex x="-1.1684" y="6.1738" curve="-90"/>
</polygon>
<polygon width="0.002540625" layer="46">
<vertex x="-0.762" y="-5.8404"/>
<vertex x="0.762" y="-5.8404" curve="-90"/>
<vertex x="1.143" y="-6.1452" curve="-90"/>
<vertex x="0.762" y="-6.45"/>
<vertex x="-0.762" y="-6.45" curve="-90"/>
<vertex x="-1.143" y="-6.1452" curve="-90"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="-3.048" y="-5.842"/>
<vertex x="3.048" y="-5.842"/>
<vertex x="3.048" y="-6.858"/>
<vertex x="-3.048" y="-6.858"/>
</polygon>
<polygon width="0" layer="41">
<vertex x="-3.048" y="6.858"/>
<vertex x="3.048" y="6.858"/>
<vertex x="3.048" y="5.842"/>
<vertex x="-3.048" y="5.842"/>
</polygon>
<wire x1="-1.6" y1="3.302" x2="1.6" y2="3.302" width="0.1524" layer="253"/>
<wire x1="1.6" y1="3.302" x2="1.6" y2="-3.302" width="0.1524" layer="253"/>
<wire x1="1.6" y1="-3.302" x2="-1.6" y2="-3.302" width="0.1524" layer="253"/>
<wire x1="-1.6" y1="-3.302" x2="-1.6" y2="3.302" width="0.1524" layer="253"/>
<text x="0" y="7.874" size="0.8" layer="25" font="fixed" ratio="15" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-8.128" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<polygon width="0.1524" layer="39">
<vertex x="-2.794" y="6.35"/>
<vertex x="2.794" y="6.35"/>
<vertex x="2.794" y="-6.35"/>
<vertex x="-2.794" y="-6.35"/>
</polygon>
</package>
<package name="PUSH_320.02E11.08BLK">
<description>&lt;b&gt;320.02E11.08BLK-1&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="-3.81" y="3.81" drill="1.22" diameter="1.83"/>
<pad name="3" x="1.27" y="-1.27" drill="1.22" diameter="1.83"/>
<text x="5.9" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="5.9" y="-7.62" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-6.2" y1="6.2" x2="6.2" y2="6.2" width="0.2" layer="51"/>
<wire x1="6.2" y1="6.2" x2="6.2" y2="-6.2" width="0.2" layer="51"/>
<wire x1="6.2" y1="-6.2" x2="-6.2" y2="-6.2" width="0.2" layer="51"/>
<wire x1="-6.2" y1="-6.2" x2="-6.2" y2="6.2" width="0.2" layer="51"/>
<wire x1="-6.2" y1="6.2" x2="6.2" y2="6.2" width="0.1" layer="21"/>
<wire x1="6.2" y1="6.2" x2="6.2" y2="-6.2" width="0.1" layer="21"/>
<wire x1="6.2" y1="-6.2" x2="-6.2" y2="-6.2" width="0.1" layer="21"/>
<wire x1="-6.2" y1="-6.2" x2="-6.2" y2="6.2" width="0.1" layer="21"/>
<hole x="3.81" y="1.27" drill="1"/>
<hole x="-3.81" y="-3.81" drill="1"/>
<circle x="0" y="0" radius="6.1" width="0.1524" layer="253"/>
</package>
</packages>
<symbols>
<symbol name="OFF_ON_MOM">
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<wire x1="-2.54" y1="1.016" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.016" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="1.016" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="2.032" x2="0.508" y2="2.032" width="0.1524" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ON_ON">
<wire x1="0" y1="-3.175" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="3.175" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.175" width="0.254" layer="94"/>
<text x="5.08" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<pin name="C" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="U" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<circle x="2.032" y="3.556" radius="0.254" width="0" layer="94"/>
<text x="7.62" y="-1.905" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PUSH_MOMENTARY" prefix="S">
<description>Various NO switches- pushbuttons, reed, etc</description>
<gates>
<gate name="G$2" symbol="OFF_ON_MOM" x="0" y="0"/>
</gates>
<devices>
<device name="12MM" package="TACTILE-PTH-12MM">
<connects>
<connect gate="G$2" pin="1" pad="1 2" route="any"/>
<connect gate="G$2" pin="2" pad="3 4" route="any"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_THT_6X6MM" package="TACTILE-PTH">
<connects>
<connect gate="G$2" pin="1" pad="1 2" route="any"/>
<connect gate="G$2" pin="2" pad="3 4" route="any"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
<technology name="_17MM">
<attribute name="MAN" value="WEALTHMETAL"/>
<attribute name="MPN" value="TC-0111-T"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value="https://www.gme.cz/tc-0111-t"/>
</technology>
</technologies>
</device>
<device name="SIDE_EZ" package="TACTILE-PTH-SIDEEZ">
<connects>
<connect gate="G$2" pin="1" pad="1"/>
<connect gate="G$2" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_SMT_6X6MM" package="TACTILE_SWITCH_TALL">
<connects>
<connect gate="G$2" pin="1" pad="A1 A2" route="any"/>
<connect gate="G$2" pin="2" pad="B1 B2" route="any"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD-12MM" package="TACTILE-SMD-12MM">
<connects>
<connect gate="G$2" pin="1" pad="1 2" route="any"/>
<connect gate="G$2" pin="2" pad="3 4" route="any"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="yes" constant="no"/>
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="SMT"/>
<attribute name="VENDOR" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_12MM_HOLES" package="TACTICLE-PTH-12MM-WITHHOLES">
<connects>
<connect gate="G$2" pin="1" pad="1 2" route="any"/>
<connect gate="G$2" pin="2" pad="3 4" route="any"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SLIDE" prefix="S">
<gates>
<gate name="G$1" symbol="ON_ON" x="0" y="0"/>
</gates>
<devices>
<device name="HORIZONTAL" package="SLIDE_MID_SPDT_HORIZONTAL">
<connects>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="U" pad="U"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="PIC" constant="no"/>
<attribute name="MPN" value="SK12F14G7" constant="no"/>
<attribute name="ODOO" value="SW_SK12F14G7_SPDT_SL_RA"/>
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
<device name="VERTICAL" package="SLIDE_MID_SPDT_VERTICAL">
<connects>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="U" pad="U"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="WEALTH METAL" constant="no"/>
<attribute name="MPN" value="SS12F45G8" constant="no"/>
<attribute name="ODOO" value="SW_SS12F45G8_SPDT_SL_V"/>
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PUSHBUTTON_MOM_320.02">
<gates>
<gate name="G$1" symbol="OFF_ON_MOM" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="PUSH_320.02E11.08BLK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_LEDS">
<packages>
<package name="LEDSMALLHOLES">
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.1016" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.1016" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.1016" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.1016" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.1016" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.1016" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.1016" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="1.0414" width="0.1016" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-1.0922" width="0.1016" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.7" diameter="1.143"/>
<pad name="K" x="1.27" y="0" drill="0.7" diameter="1.143"/>
<text x="0" y="2.794" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="-2.8575" y="0.9525" size="1.6764" layer="21" font="fixed">+</text>
<circle x="0" y="0" radius="1.501" width="0.1016" layer="253"/>
</package>
<package name="LED_5MM">
<wire x1="2.0748" y1="-1.27" x2="2.0748" y2="1.27" width="0.1016" layer="51"/>
<wire x1="0.5" y1="2.532" x2="2.061" y2="1.3009" width="0.1016" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.9929" y1="1.4562" x2="0.5" y2="2.532" width="0.1016" layer="21" curve="-61.926949"/>
<wire x1="0.5" y1="-2.532" x2="2.0512" y2="-1.3126" width="0.1016" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-2.2643" y1="-1.0082" x2="0.5" y2="-2.532" width="0.1016" layer="21" curve="60.255215"/>
<wire x1="-2.532" y1="0" x2="-1.9891" y2="1.4634" width="0.1016" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.532" y1="0" x2="-2.2306" y2="-1.065" width="0.1016" layer="51" curve="31.60822" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.7" diameter="1.143"/>
<pad name="K" x="1.27" y="0" drill="0.7" diameter="1.143"/>
<text x="0" y="3.294" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="-3.3575" y="0.9525" size="1.6764" layer="21" font="fixed">+</text>
<circle x="0" y="0" radius="2.501" width="0.1016" layer="253"/>
</package>
</packages>
<symbols>
<symbol name="LED3MM">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_THT" prefix="LD" uservalue="yes">
<description>[color]_[diameter]_[difused/clear]


R_3mm_D</description>
<gates>
<gate name="G$1" symbol="LED3MM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LEDSMALLHOLES">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
</technology>
<technology name="_GREEN">
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
</technology>
<technology name="_RED">
<attribute name="ODOO" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5MM" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
<technology name="_GREEN">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_POTS">
<packages>
<package name="POT_ALPHA">
<wire x1="3.73" y1="0" x2="5.08" y2="0" width="0.127" layer="21"/>
<wire x1="5.08" y1="0" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="8.89" x2="5.08" y2="12.5" width="0.127" layer="21"/>
<wire x1="5.08" y1="12.5" x2="5" y2="12.5" width="0.127" layer="21"/>
<wire x1="5" y1="12.5" x2="-5" y2="12.5" width="0.127" layer="21"/>
<wire x1="-5" y1="12.5" x2="-5.08" y2="12.5" width="0.127" layer="21"/>
<wire x1="-5.08" y1="12.5" x2="-5.08" y2="8.89" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.127" layer="21"/>
<wire x1="-5.08" y1="0" x2="-3.73" y2="0" width="0.127" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="2" x="0" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="3" x="2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="P$1" x="-5.5" y="7" drill="0.6" diameter="1.6" shape="long" rot="R90"/>
<pad name="P$2" x="5.5" y="7" drill="0.6" diameter="1.6" shape="long" rot="R90"/>
<text x="0.072" y="11.7065" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0.072" y="2.523" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<polygon width="0.01" layer="46">
<vertex x="-5" y="6.2"/>
<vertex x="-5" y="7.7" curve="90"/>
<vertex x="-5.5" y="8.2" curve="90"/>
<vertex x="-6" y="7.7"/>
<vertex x="-6" y="6.2" curve="90"/>
<vertex x="-5.5" y="5.7" curve="90"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="6" y="6.2"/>
<vertex x="6" y="7.7" curve="90"/>
<vertex x="5.5" y="8.2" curve="90"/>
<vertex x="5" y="7.7"/>
<vertex x="5" y="6.2" curve="90"/>
<vertex x="5.5" y="5.7" curve="90"/>
</polygon>
<circle x="0" y="7" radius="3.5" width="0.1524" layer="21"/>
<circle x="0" y="7" radius="3.190834375" width="0.1524" layer="21"/>
<wire x1="-2.2225" y1="9.2" x2="2.2225" y2="9.2" width="0.1524" layer="21"/>
<circle x="0" y="7" radius="3.4" width="0.127" layer="253"/>
<rectangle x1="-5.1308" y1="-0.0254" x2="5.1054" y2="12.5476" layer="39"/>
<rectangle x1="-3.81" y1="-0.9144" x2="3.81" y2="-0.0254" layer="39"/>
<rectangle x1="-5.3594" y1="9.017" x2="-5.1308" y2="10.2362" layer="39"/>
<rectangle x1="5.1054" y1="9.017" x2="5.334" y2="10.2362" layer="39"/>
<rectangle x1="5.1054" y1="3.7592" x2="5.334" y2="4.9784" layer="39"/>
<rectangle x1="-5.3594" y1="3.7592" x2="-5.1308" y2="4.9784" layer="39"/>
</package>
<package name="POT_APHA_NO_SILK">
<wire x1="3.73" y1="0" x2="5.08" y2="0" width="0.127" layer="51"/>
<wire x1="5.08" y1="0" x2="5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="5.08" y1="8.89" x2="5.08" y2="12.5" width="0.127" layer="51"/>
<wire x1="5.08" y1="12.5" x2="5" y2="12.5" width="0.127" layer="51"/>
<wire x1="5" y1="12.5" x2="-5" y2="12.5" width="0.127" layer="51"/>
<wire x1="-5" y1="12.5" x2="-5.08" y2="12.5" width="0.127" layer="51"/>
<wire x1="-5.08" y1="12.5" x2="-5.08" y2="8.89" width="0.127" layer="51"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.127" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.73" y2="0" width="0.127" layer="51"/>
<pad name="1" x="-2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="2" x="0" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="3" x="2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="P$1" x="-5.5" y="7" drill="0.6" diameter="1.6" shape="long" rot="R90"/>
<pad name="P$2" x="5.5" y="7" drill="0.6" diameter="1.6" shape="long" rot="R90"/>
<text x="0.072" y="11.7065" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0.072" y="2.523" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<polygon width="0.01" layer="46">
<vertex x="-5" y="6.2"/>
<vertex x="-5" y="7.7" curve="90"/>
<vertex x="-5.5" y="8.2" curve="90"/>
<vertex x="-6" y="7.7"/>
<vertex x="-6" y="6.2" curve="90"/>
<vertex x="-5.5" y="5.7" curve="90"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="6" y="6.2"/>
<vertex x="6" y="7.7" curve="90"/>
<vertex x="5.5" y="8.2" curve="90"/>
<vertex x="5" y="7.7"/>
<vertex x="5" y="6.2" curve="90"/>
<vertex x="5.5" y="5.7" curve="90"/>
</polygon>
<circle x="0" y="7" radius="3.5" width="0.1524" layer="51"/>
<circle x="0" y="7" radius="3.190834375" width="0.1524" layer="51"/>
<wire x1="-2.2225" y1="9.2" x2="2.2225" y2="9.2" width="0.1524" layer="51"/>
<circle x="0" y="7" radius="3.4" width="0.127" layer="253"/>
<polygon width="0.127" layer="41">
<vertex x="-5.333" y="9.061"/>
<vertex x="-5.087" y="9.061"/>
<vertex x="-5.087" y="10.188"/>
<vertex x="-5.333" y="10.188"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="5.067" y="9.061"/>
<vertex x="5.313" y="9.061"/>
<vertex x="5.313" y="10.188"/>
<vertex x="5.067" y="10.188"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-5.333" y="3.811"/>
<vertex x="-5.087" y="3.811"/>
<vertex x="-5.087" y="4.938"/>
<vertex x="-5.333" y="4.938"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="5.067" y="3.811"/>
<vertex x="5.313" y="3.811"/>
<vertex x="5.313" y="4.938"/>
<vertex x="5.067" y="4.938"/>
</polygon>
<polygon width="0.127" layer="39">
<vertex x="-5.08" y="12.4968"/>
<vertex x="-5.08" y="0"/>
<vertex x="-3.81" y="0"/>
<vertex x="-3.81" y="-1.27"/>
<vertex x="3.81" y="-1.27"/>
<vertex x="3.81" y="0"/>
<vertex x="5.08" y="0"/>
<vertex x="5.08" y="12.4968"/>
</polygon>
</package>
<package name="POT_ALPHA_METAL">
<wire x1="3.73" y1="0.5" x2="5.08" y2="0.5" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.5" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="8.89" x2="5.08" y2="12" width="0.127" layer="21"/>
<wire x1="5.08" y1="12" x2="-5.08" y2="12" width="0.127" layer="21"/>
<wire x1="-5.08" y1="12" x2="-5.08" y2="8.89" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0.5" width="0.127" layer="21"/>
<wire x1="-5.08" y1="0.5" x2="-3.73" y2="0.5" width="0.127" layer="21"/>
<pad name="1" x="-2.5" y="-0.5" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="2" x="0" y="-0.5" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="3" x="2.5" y="-0.5" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="P$1" x="-5.5" y="7" drill="0.6" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$2" x="5.5" y="7" drill="0.6" diameter="1.27" shape="long" rot="R90"/>
<text x="0.072" y="11.2065" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0.072" y="2.023" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<circle x="0" y="7" radius="3.5" width="0.1524" layer="21"/>
<circle x="0" y="7" radius="3.190834375" width="0.1524" layer="21"/>
<wire x1="-2.2225" y1="9.2" x2="2.2225" y2="9.2" width="0.1524" layer="21"/>
<circle x="0" y="7" radius="3.5" width="0.127" layer="253"/>
<polygon width="0.127" layer="39">
<vertex x="-5.08" y="11.9968"/>
<vertex x="-5.08" y="0.5"/>
<vertex x="-3.81" y="0.5"/>
<vertex x="-3.81" y="-1.77"/>
<vertex x="3.81" y="-1.77"/>
<vertex x="3.81" y="0.5"/>
<vertex x="5.08" y="0.5"/>
<vertex x="5.08" y="11.9968"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="-5" y="6.7"/>
<vertex x="-5" y="7.2" curve="90"/>
<vertex x="-5.5" y="7.7" curve="90"/>
<vertex x="-6" y="7.2"/>
<vertex x="-6" y="6.7" curve="90"/>
<vertex x="-5.5" y="6.2" curve="90"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="6" y="6.7"/>
<vertex x="6" y="7.2" curve="90"/>
<vertex x="5.5" y="7.7" curve="90"/>
<vertex x="5" y="7.2"/>
<vertex x="5" y="6.7" curve="90"/>
<vertex x="5.5" y="6.2" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="12"/>
<vertex x="1" y="12"/>
<vertex x="1" y="11.5"/>
<vertex x="-1" y="11.5"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="2.5"/>
<vertex x="1" y="2.5"/>
<vertex x="1" y="2"/>
<vertex x="-1" y="2"/>
</polygon>
</package>
<package name="POT_ALPHA_METAL_NO_SILK">
<pad name="1" x="-2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="2" x="0" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="3" x="2.5" y="0" drill="0.9" diameter="1.4224" rot="R180"/>
<pad name="P$1" x="-5.5" y="7" drill="0.6" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$2" x="5.5" y="7" drill="0.6" diameter="1.27" shape="long" rot="R90"/>
<text x="0.072" y="11.2065" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="0.072" y="2.523" size="0.8" layer="21" font="fixed" ratio="15" align="center">&gt;VALUE</text>
<circle x="0" y="7" radius="3.5" width="0.127" layer="253"/>
<polygon width="0.127" layer="39">
<vertex x="-5.08" y="11.9968"/>
<vertex x="-5.08" y="0"/>
<vertex x="-3.81" y="0"/>
<vertex x="-3.81" y="-1.27"/>
<vertex x="3.81" y="-1.27"/>
<vertex x="3.81" y="0"/>
<vertex x="5.08" y="0"/>
<vertex x="5.08" y="11.9968"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="-5" y="6.7"/>
<vertex x="-5" y="7.2" curve="90"/>
<vertex x="-5.5" y="7.7" curve="90"/>
<vertex x="-6" y="7.2"/>
<vertex x="-6" y="6.7" curve="90"/>
<vertex x="-5.5" y="6.2" curve="90"/>
</polygon>
<polygon width="0.01" layer="46">
<vertex x="6" y="6.7"/>
<vertex x="6" y="7.2" curve="90"/>
<vertex x="5.5" y="7.7" curve="90"/>
<vertex x="5" y="7.2"/>
<vertex x="5" y="6.7" curve="90"/>
<vertex x="5.5" y="6.2" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="12"/>
<vertex x="1" y="12"/>
<vertex x="1" y="11.5"/>
<vertex x="-1" y="11.5"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="2.5"/>
<vertex x="1" y="2.5"/>
<vertex x="1" y="2"/>
<vertex x="-1" y="2"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="POT_ALPHA">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="-0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-3.048" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-2.032" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="4.826" y1="0" x2="2.54" y2="0" width="0.127" layer="94"/>
<wire x1="2.54" y1="0" x2="1.778" y2="0" width="0.127" layer="94"/>
<wire x1="1.778" y1="0" x2="1.778" y2="1.524" width="0.127" layer="94"/>
<wire x1="1.778" y1="1.524" x2="1.016" y2="1.524" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.524" x2="1.27" y2="1.016" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.524" x2="1.27" y2="2.032" width="0.127" layer="94"/>
<pin name="1" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<text x="-5.969" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P$1" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="2.54" y1="-1.778" x2="2.54" y2="-1.524" width="0.127" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="-0.508" width="0.127" layer="94"/>
<wire x1="2.54" y1="0.254" x2="2.54" y2="0.508" width="0.127" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="1.524" width="0.127" layer="94"/>
<wire x1="2.54" y1="2.286" x2="2.54" y2="2.54" width="0.127" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="POT_ALPHA" prefix="P">
<description>"A/B""value"_"height"_"D(centrer detent)"_"W(white mark)"</description>
<gates>
<gate name="G$1" symbol="POT_ALPHA" x="0" y="0"/>
</gates>
<devices>
<device name="POT_ALPHA" package="POT_ALPHA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="P$1" pad="P$1 P$2" route="any"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="POT_APHA_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="P$1" pad="P$1 P$2" route="any"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
</technology>
</technologies>
</device>
<device name="METAL" package="POT_ALPHA_METAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="P$1" pad="P$1 P$2" route="any"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT" constant="no"/>
</technology>
</technologies>
</device>
<device name="METAL_NO_SILK" package="POT_ALPHA_METAL_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="P$1" pad="P$1 P$2" route="any"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_ICS">
<packages>
<package name="DIL08" urn="urn:adsk.eagle:footprint:16129/1">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="5.08" y1="2.921" x2="-5.08" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<text x="-5.334" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="51" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="51" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="51" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="51" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="-1.4478" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.4478" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="-1.4478" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.4478" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.4478" x2="5.08" y2="-1.4478" width="0.0508" layer="21"/>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
<smd name="1" x="-4.445" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="2" x="-3.175" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="3" x="-1.905" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="4" x="-0.635" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="5" x="0.635" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="6" x="1.905" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="7" x="3.175" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="8" x="4.445" y="-2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="9" x="4.445" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="10" x="3.175" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="11" x="1.905" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="12" x="0.635" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="13" x="-0.635" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="14" x="-1.905" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="15" x="-3.175" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<smd name="16" x="-4.445" y="2.63525" dx="0.6" dy="1.6" layer="1"/>
<text x="-5.715" y="0" size="0.8" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="15" rot="SR0" align="center">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-3.81" x2="5.334" y2="3.81" layer="39"/>
</package>
<package name="TSSOP-16">
<description>8-Bit Shift Registers With 3-State Output Registers</description>
<text x="0.0076" y="3.4572" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<smd name="1" x="-2.8" y="2.275" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="2" x="-2.8" y="1.625" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="3" x="-2.8" y="0.975" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="4" x="-2.8" y="0.325" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="5" x="-2.8" y="-0.325" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="6" x="-2.8" y="-0.975" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="7" x="-2.8" y="-1.625" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="8" x="-2.8" y="-2.275" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="9" x="2.8" y="-2.275" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="10" x="2.8" y="-1.625" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="11" x="2.8" y="-0.975" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="12" x="2.8" y="-0.325" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="13" x="2.8" y="0.325" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="14" x="2.8" y="0.975" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="15" x="2.8" y="1.625" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<smd name="16" x="2.8" y="2.275" dx="0.35" dy="1.6" layer="1" rot="R270"/>
<circle x="-1.5" y="2.1208" radius="0.325" width="0" layer="21"/>
<wire x1="2.1" y1="-0.0792" x2="2.1" y2="2.7208" width="0.2032" layer="21"/>
<wire x1="2.1" y1="2.7208" x2="-2.1" y2="2.7208" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="2.7208" x2="-2.1" y2="-0.0792" width="0.2032" layer="21"/>
<rectangle x1="-2.725" y1="1.7958" x2="-2.475" y2="2.7958" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="1.1458" x2="-2.475" y2="2.1458" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="0.4958" x2="-2.475" y2="1.4958" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="-0.1542" x2="-2.475" y2="0.8458" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-0.1542" x2="2.725" y2="0.8458" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="0.4958" x2="2.725" y2="1.4958" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="1.1458" x2="2.725" y2="2.1458" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="1.7958" x2="2.725" y2="2.7958" layer="51" rot="R270"/>
<wire x1="-2.413" y1="2.7178" x2="-3.175" y2="2.7178" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-2.6954" x2="2.1" y2="-2.6954" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-2.6954" x2="2.1" y2="0.1046" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="0.1046" x2="-2.1" y2="-2.6954" width="0.2032" layer="21"/>
<rectangle x1="-2.725" y1="-0.8204" x2="-2.475" y2="0.1796" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="-1.4704" x2="-2.475" y2="-0.4704" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="-2.1204" x2="-2.475" y2="-1.1204" layer="51" rot="R270"/>
<rectangle x1="-2.725" y1="-2.7704" x2="-2.475" y2="-1.7704" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-2.7704" x2="2.725" y2="-1.7704" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-2.1204" x2="2.725" y2="-1.1204" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-1.4704" x2="2.725" y2="-0.4704" layer="51" rot="R270"/>
<rectangle x1="2.475" y1="-0.8204" x2="2.725" y2="0.1796" layer="51" rot="R270"/>
<text x="0" y="0" size="0.8" layer="25" ratio="15" rot="R90" align="center">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.048" x2="3.81" y2="3.048" layer="39"/>
</package>
<package name="DIL16">
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="octagon" rot="R90"/>
<text x="-10.922" y="0" size="0.8" layer="25" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="SO8">
<rectangle x1="-2.79515" y1="1.35495" x2="-2.30495" y2="2.45505" layer="51" rot="R270"/>
<rectangle x1="-2.79515" y1="0.08495" x2="-2.30495" y2="1.18505" layer="51" rot="R270"/>
<rectangle x1="-2.79515" y1="-1.18505" x2="-2.30495" y2="-0.08495" layer="51" rot="R270"/>
<rectangle x1="-2.79515" y1="-2.45505" x2="-2.30495" y2="-1.35495" layer="51" rot="R270"/>
<rectangle x1="2.30495" y1="-2.45505" x2="2.79515" y2="-1.35495" layer="51" rot="R270"/>
<rectangle x1="2.30495" y1="-1.18505" x2="2.79515" y2="-0.08495" layer="51" rot="R270"/>
<rectangle x1="2.30495" y1="0.08495" x2="2.79515" y2="1.18505" layer="51" rot="R270"/>
<rectangle x1="2.30495" y1="1.35495" x2="2.79515" y2="2.45505" layer="51" rot="R270"/>
<smd name="1" x="-2.6" y="1.905" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="2" x="-2.6" y="0.635" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="3" x="-2.6" y="-0.635" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="4" x="-2.6" y="-1.905" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="5" x="2.6" y="-1.905" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="6" x="2.6" y="-0.635" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="7" x="2.6" y="0.635" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<smd name="8" x="2.6" y="1.905" dx="0.6" dy="1.6" layer="1" rot="R270"/>
<text x="0" y="3.14325" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="16" rot="SR270" align="center">&gt;VALUE</text>
<wire x1="1.9558" y1="-2.159" x2="1.9558" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-1.9558" y1="-2.159" x2="-1.5748" y2="-2.54" width="0.1524" layer="51" curve="90"/>
<wire x1="1.5748" y1="2.54" x2="1.9558" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="1.9558" y1="-2.159" x2="1.5748" y2="-2.54" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.5748" y1="2.54" x2="-1.9558" y2="2.159" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.9558" y1="2.159" x2="-1.9558" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-1.5748" y1="-2.54" x2="-1.4478" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.4478" y1="-2.54" x2="1.5748" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.5748" y1="2.54" x2="0.508" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.54" x2="-0.508" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="2.54" x2="-1.4478" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.4478" y1="2.54" x2="-1.5748" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.54" x2="-0.508" y2="2.54" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.4478" y1="2.54" x2="-1.4478" y2="-2.54" width="0.127" layer="21"/>
<rectangle x1="-3.683" y1="-2.667" x2="3.683" y2="2.667" layer="39"/>
</package>
<package name="TSSOP8">
<circle x="-0.8" y="-1.5" radius="0.325" width="0" layer="21"/>
<wire x1="1.4" y1="-2.1" x2="1.4" y2="2.1" width="0.2032" layer="21"/>
<wire x1="1.4" y1="2.1" x2="-1.4" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="2.1" x2="-1.4" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-2.1" x2="1.4" y2="-2.1" width="0.2032" layer="21"/>
<rectangle x1="-1.1" y1="-3.1" x2="-0.85" y2="-2.1" layer="51"/>
<rectangle x1="-0.45" y1="-3.1" x2="-0.2" y2="-2.1" layer="51"/>
<rectangle x1="0.2" y1="-3.1" x2="0.45" y2="-2.1" layer="51"/>
<rectangle x1="0.85" y1="-3.1" x2="1.1" y2="-2.1" layer="51"/>
<rectangle x1="0.85" y1="2.1" x2="1.1" y2="3.1" layer="51"/>
<rectangle x1="0.2" y1="2.1" x2="0.45" y2="3.1" layer="51"/>
<rectangle x1="-0.45" y1="2.1" x2="-0.2" y2="3.1" layer="51"/>
<rectangle x1="-1.1" y1="2.1" x2="-0.85" y2="3.1" layer="51"/>
<smd name="1" x="-0.975" y="-2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="2" x="-0.325" y="-2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="3" x="0.325" y="-2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="4" x="0.975" y="-2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="5" x="0.975" y="2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="6" x="0.325" y="2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="7" x="-0.325" y="2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="8" x="-0.975" y="2.925" dx="0.35" dy="1.2" layer="1"/>
<text x="-1.9685" y="-0.0318" size="0.8" layer="25" font="fixed" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="0.0318" y="-0.0635" size="0.8" layer="25" font="fixed" ratio="15" rot="SR90" align="center">&gt;VALUE</text>
<wire x1="-1.397" y1="-2.413" x2="-1.397" y2="-3.175" width="0.127" layer="21"/>
<rectangle x1="-1.651" y1="-3.81" x2="1.651" y2="3.81" layer="39"/>
</package>
</packages>
<packages3d>
<package3d name="DIL08" urn="urn:adsk.eagle:package:16409/2" type="model">
<description>Dual In Line Package</description>
<packageinstances>
<packageinstance name="DIL08"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="OPAMP">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="7.62" y2="0" width="0.4064" layer="94"/>
<wire x1="7.62" y1="0" x2="-2.54" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="94"/>
<pin name="+IN" x="-5.08" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="-IN" x="-5.08" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="10.16" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<text x="7.62" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="7.62" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="OPAMP_PWR">
<pin name="V+" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="V-" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<text x="-1.905" y="-0.762" size="1.778" layer="95">&gt;NAME</text>
<text x="0.635" y="6.985" size="1.016" layer="95" rot="R270">V+</text>
<text x="1.5875" y="-6.985" size="1.016" layer="95" rot="R90">V-</text>
</symbol>
<symbol name="74595">
<wire x1="-7.62" y1="-15.24" x2="7.62" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="7.62" y2="12.7" width="0.4064" layer="94"/>
<wire x1="7.62" y1="12.7" x2="-7.62" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-15.24" width="0.4064" layer="94"/>
<pin name="G" x="-12.7" y="-12.7" length="middle" direction="in" function="dot"/>
<pin name="QA" x="12.7" y="10.16" length="middle" direction="hiz" rot="R180"/>
<pin name="QB" x="12.7" y="7.62" length="middle" direction="hiz" rot="R180"/>
<pin name="QC" x="12.7" y="5.08" length="middle" direction="hiz" rot="R180"/>
<pin name="QD" x="12.7" y="2.54" length="middle" direction="hiz" rot="R180"/>
<pin name="QE" x="12.7" y="0" length="middle" direction="hiz" rot="R180"/>
<pin name="QF" x="12.7" y="-2.54" length="middle" direction="hiz" rot="R180"/>
<pin name="QG" x="12.7" y="-5.08" length="middle" direction="hiz" rot="R180"/>
<pin name="QH" x="12.7" y="-7.62" length="middle" direction="hiz" rot="R180"/>
<pin name="QH*" x="12.7" y="-12.7" length="middle" direction="hiz" rot="R180"/>
<pin name="RCK" x="-12.7" y="-2.54" length="middle" direction="in" function="clk"/>
<pin name="SCK" x="-12.7" y="5.08" length="middle" direction="in" function="clk"/>
<pin name="SCL" x="-12.7" y="2.54" length="middle" direction="in" function="dot"/>
<pin name="SER" x="-12.7" y="10.16" length="middle" direction="in"/>
<text x="-7.62" y="13.335" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-7.62" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PWRN">
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
</symbol>
<symbol name="137">
<circle x="3.175" y="1.27" radius="0.635" width="0.1524" layer="94"/>
<wire x1="-5.715" y1="2.54" x2="-6.985" y2="0" width="0.254" layer="94"/>
<wire x1="-6.985" y1="0" x2="-8.255" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.715" y1="0" x2="-6.985" y2="0" width="0.254" layer="94"/>
<wire x1="-6.985" y1="0" x2="-8.255" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="1.905" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-4.191" y2="2.159" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-4.191" y2="1.651" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0.635" x2="-4.191" y2="0.889" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0.635" x2="-4.191" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-5.715" y1="2.54" x2="-6.985" y2="2.54" width="0.254" layer="94"/>
<wire x1="-6.985" y1="2.54" x2="-6.985" y2="0" width="0.254" layer="94"/>
<wire x1="-6.985" y1="2.54" x2="-8.255" y2="2.54" width="0.254" layer="94"/>
<wire x1="-6.985" y1="2.54" x2="-6.985" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="-6.985" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.207" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-6.985" y1="-2.54" x2="-6.985" y2="0" width="0.1524" layer="94"/>
<wire x1="-9.525" y1="7.62" x2="-9.525" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="6.985" y1="7.62" x2="6.985" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="6.985" y1="7.62" x2="-9.525" y2="7.62" width="0.4064" layer="94"/>
<wire x1="6.985" y1="-5.08" x2="-9.525" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.842" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-2.54" width="0.4064" layer="94" curve="-180"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="3.81" width="0.4064" layer="94"/>
<wire x1="3.81" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="94"/>
<wire x1="4.445" y1="1.27" x2="4.445" y2="0" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-2.54" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="5.715" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.715" x2="3.81" y2="5.715" width="0.1524" layer="94"/>
<wire x1="3.81" y1="5.715" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="94"/>
<pin name="A" x="-12.7" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="C" x="-12.7" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="GND" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="VCC" x="10.16" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="VE" x="10.16" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="VO" x="10.16" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<text x="-9.525" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-9.525" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="4.572" y="5.334" size="0.8128" layer="93">Vcc</text>
<text x="4.572" y="-2.286" size="0.8128" layer="93">GND</text>
<text x="-8.255" y="4.064" size="0.8128" layer="93">A</text>
<text x="-8.255" y="-2.286" size="0.8128" layer="93">C</text>
<text x="5.207" y="0.254" size="0.8128" layer="93">Vo</text>
<text x="5.207" y="2.794" size="0.8128" layer="93">Ve</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TL072" prefix="IC">
<gates>
<gate name="A" symbol="OPAMP" x="7.62" y="15.24"/>
<gate name="B" symbol="OPAMP" x="7.62" y="-15.24"/>
<gate name="P" symbol="OPAMP_PWR" x="-12.7" y="0"/>
</gates>
<devices>
<device name="CD" package="SO8">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="TEXAS INSTRUMENTS"/>
<attribute name="MPN" value="TL072CDR"/>
<attribute name="ODOO" value="IC_TL072_SO8" constant="no"/>
<attribute name="PRICE" value="5.04" constant="no"/>
<attribute name="SIZEX" value="6.2"/>
<attribute name="SIZEY" value="7.8"/>
<attribute name="SIZEZ" value="1.5"/>
<attribute name="TYPE" value="SMT"/>
</technology>
</technologies>
</device>
<device name="CP" package="TSSOP8">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="TEXAS INSTRUMENT"/>
<attribute name="MPN" value="TL072CPWR"/>
<attribute name="ODOO" value="IC_TL072_TSSOP8" constant="no"/>
<attribute name="PRICE" value="7.25" constant="no"/>
<attribute name="SIZEX" value="6.4"/>
<attribute name="SIZEY" value="3"/>
<attribute name="SIZEZ" value="1.2"/>
<attribute name="TYPE" value="SMT"/>
</technology>
</technologies>
</device>
<device name="" package="DIL08">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16409/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="74*595" prefix="IC">
<gates>
<gate name="A" symbol="74595" x="22.86" y="0"/>
<gate name="P" symbol="PWRN" x="-5.08" y="0" addlevel="request"/>
</gates>
<devices>
<device name="D" package="SO16">
<connects>
<connect gate="A" pin="G" pad="13"/>
<connect gate="A" pin="QA" pad="15"/>
<connect gate="A" pin="QB" pad="1"/>
<connect gate="A" pin="QC" pad="2"/>
<connect gate="A" pin="QD" pad="3"/>
<connect gate="A" pin="QE" pad="4"/>
<connect gate="A" pin="QF" pad="5"/>
<connect gate="A" pin="QG" pad="6"/>
<connect gate="A" pin="QH" pad="7"/>
<connect gate="A" pin="QH*" pad="9"/>
<connect gate="A" pin="RCK" pad="12"/>
<connect gate="A" pin="SCK" pad="11"/>
<connect gate="A" pin="SCL" pad="10"/>
<connect gate="A" pin="SER" pad="14"/>
<connect gate="P" pin="GND" pad="8"/>
<connect gate="P" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="Texas Instruments"/>
<attribute name="MPN" value="SN74HC595DR"/>
<attribute name="ODOO" value="IC_74HC595_SO16" constant="no"/>
<attribute name="SIZEX" value="10.8"/>
<attribute name="SIZEY" value="6"/>
<attribute name="SIZEZ" value="1.5"/>
<attribute name="TYPE" value="SMT"/>
</technology>
</technologies>
</device>
<device name="P" package="TSSOP-16">
<connects>
<connect gate="A" pin="G" pad="13"/>
<connect gate="A" pin="QA" pad="15"/>
<connect gate="A" pin="QB" pad="1"/>
<connect gate="A" pin="QC" pad="2"/>
<connect gate="A" pin="QD" pad="3"/>
<connect gate="A" pin="QE" pad="4"/>
<connect gate="A" pin="QF" pad="5"/>
<connect gate="A" pin="QG" pad="6"/>
<connect gate="A" pin="QH" pad="7"/>
<connect gate="A" pin="QH*" pad="9"/>
<connect gate="A" pin="RCK" pad="12"/>
<connect gate="A" pin="SCK" pad="11"/>
<connect gate="A" pin="SCL" pad="10"/>
<connect gate="A" pin="SER" pad="14"/>
<connect gate="P" pin="GND" pad="8"/>
<connect gate="P" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name="">
<attribute name="ASSEMBLED" value="YES" constant="no"/>
<attribute name="MAN" value="Texas Instruments"/>
<attribute name="MPN" value="SN74HC595DBR"/>
<attribute name="ODOO" value="IC_74HC595_TSSOP16"/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="SMT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
<device name="" package="DIL16">
<connects>
<connect gate="A" pin="G" pad="13"/>
<connect gate="A" pin="QA" pad="15"/>
<connect gate="A" pin="QB" pad="1"/>
<connect gate="A" pin="QC" pad="2"/>
<connect gate="A" pin="QD" pad="3"/>
<connect gate="A" pin="QE" pad="4"/>
<connect gate="A" pin="QF" pad="5"/>
<connect gate="A" pin="QG" pad="6"/>
<connect gate="A" pin="QH" pad="7"/>
<connect gate="A" pin="QH*" pad="9"/>
<connect gate="A" pin="RCK" pad="12"/>
<connect gate="A" pin="SCK" pad="11"/>
<connect gate="A" pin="SCL" pad="10"/>
<connect gate="A" pin="SER" pad="14"/>
<connect gate="P" pin="GND" pad="8"/>
<connect gate="P" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="6N137" prefix="OK">
<description>&lt;b&gt;MOTOROLA OPTO COUPLER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="137" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIL08">
<connects>
<connect gate="A" pin="A" pad="2"/>
<connect gate="A" pin="C" pad="3"/>
<connect gate="A" pin="GND" pad="5"/>
<connect gate="A" pin="VCC" pad="8"/>
<connect gate="A" pin="VE" pad="7"/>
<connect gate="A" pin="VO" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16409/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MAN" value="" constant="no"/>
<attribute name="MPN" value="6N137" constant="no"/>
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
<device name="VO0611T" package="SO8">
<connects>
<connect gate="A" pin="A" pad="2"/>
<connect gate="A" pin="C" pad="3"/>
<connect gate="A" pin="GND" pad="5"/>
<connect gate="A" pin="VCC" pad="8"/>
<connect gate="A" pin="VE" pad="7"/>
<connect gate="A" pin="VO" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="SIZEX" value="6.2"/>
<attribute name="SIZEY" value="7.8"/>
<attribute name="SIZEZ" value="1.5"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_RESISTORS">
<packages>
<package name="RCL_0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.7" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.7" shape="octagon"/>
<text x="-0.1016" y="1.5494" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RCL_R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR_0204_THT" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="RCL_R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0204/5" package="RCL_0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="v-reg" urn="urn:adsk.eagle:library:409">
<description>&lt;b&gt;Voltage Regulators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="78XXL" urn="urn:adsk.eagle:footprint:30303/1" library_version="5">
<description>&lt;b&gt;VOLTAGE REGULATOR&lt;/b&gt;</description>
<wire x1="-5.207" y1="-1.27" x2="5.207" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.207" y1="14.605" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-1.27" x2="5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="5.207" y1="11.176" x2="4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="4.318" y1="11.176" x2="4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="4.318" y1="12.7" x2="5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="5.207" y1="12.7" x2="5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-1.27" x2="-5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="11.176" x2="-4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="11.176" x2="-4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="12.7" x2="-5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="12.7" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<circle x="0" y="11.176" radius="1.8034" width="0.1524" layer="21"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="42"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="43"/>
<pad name="IN" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="GND" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="OUT" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.937" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.445" y="7.874" size="0.9906" layer="21" ratio="10">A15,2mm</text>
<text x="-0.508" y="0" size="1.27" layer="51" ratio="10">-</text>
<text x="-3.048" y="0" size="1.27" layer="51" ratio="10">I</text>
<text x="2.032" y="0" size="1.27" layer="51" ratio="10">O</text>
<rectangle x1="1.905" y1="-2.159" x2="3.175" y2="-1.27" layer="21"/>
<rectangle x1="1.905" y1="-3.81" x2="3.175" y2="-2.159" layer="51"/>
<rectangle x1="-0.635" y1="-2.159" x2="0.635" y2="-1.27" layer="21"/>
<rectangle x1="-3.175" y1="-2.159" x2="-1.905" y2="-1.27" layer="21"/>
<rectangle x1="-0.635" y1="-3.81" x2="0.635" y2="-2.159" layer="51"/>
<rectangle x1="-3.175" y1="-3.81" x2="-1.905" y2="-2.159" layer="51"/>
<hole x="0" y="11.176" drill="3.302"/>
</package>
<package name="78LXX" urn="urn:adsk.eagle:footprint:30283/1" library_version="5">
<description>&lt;b&gt;VOLTAGE REGULATOR&lt;/b&gt;</description>
<wire x1="-2.0946" y1="-1.651" x2="-0.7863" y2="2.5485" width="0.1524" layer="21" curve="-111.098957"/>
<wire x1="0.7868" y1="2.5484" x2="2.095" y2="-1.651" width="0.1524" layer="21" curve="-111.09954"/>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.655" y1="-0.254" x2="-2.254" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="2.254" y1="-0.254" x2="2.655" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-0.7863" y1="2.5485" x2="0.7863" y2="2.5485" width="0.1524" layer="51" curve="-34.293591"/>
<pad name="OUT" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="GND" x="0" y="1.905" drill="0.8128" shape="octagon"/>
<pad name="IN" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.635" y="0.889" size="1.27" layer="51" ratio="10">-</text>
<text x="3.175" y="0.635" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.778" y="-0.635" size="1.27" layer="51" ratio="10">O</text>
<text x="0.635" y="-0.635" size="1.27" layer="51" ratio="10">I</text>
</package>
</packages>
<packages3d>
<package3d name="78XXL" urn="urn:adsk.eagle:package:30361/1" type="box" library_version="5">
<description>VOLTAGE REGULATOR</description>
<packageinstances>
<packageinstance name="78XXL"/>
</packageinstances>
</package3d>
<package3d name="78LXX" urn="urn:adsk.eagle:package:30346/1" type="box" library_version="5">
<description>VOLTAGE REGULATOR</description>
<packageinstances>
<packageinstance name="78LXX"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="78XX" urn="urn:adsk.eagle:symbol:30277/1" library_version="5">
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="2.54" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">GND</text>
<text x="-4.445" y="-0.635" size="1.524" layer="95">IN</text>
<text x="0.635" y="-0.635" size="1.524" layer="95">OUT</text>
<pin name="IN" x="-7.62" y="0" visible="off" length="short" direction="in"/>
<pin name="GND" x="0" y="-7.62" visible="off" length="short" direction="in" rot="R90"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="78XXL" urn="urn:adsk.eagle:component:30408/2" prefix="IC" uservalue="yes" library_version="5">
<description>&lt;b&gt;VOLTAGE REGULATOR&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="78XX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="78XXL">
<connects>
<connect gate="A" pin="GND" pad="GND"/>
<connect gate="A" pin="IN" pad="IN"/>
<connect gate="A" pin="OUT" pad="OUT"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30361/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="10" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="78LXX" urn="urn:adsk.eagle:component:30384/2" prefix="IC" uservalue="yes" library_version="5">
<description>&lt;b&gt;VOLTAGE REGULATOR&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="78XX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="78LXX">
<connects>
<connect gate="1" pin="GND" pad="GND"/>
<connect gate="1" pin="IN" pad="IN"/>
<connect gate="1" pin="OUT" pad="OUT"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30346/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="6" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_CAPACITORS">
<packages>
<package name="C_C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.7" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.7" shape="octagon"/>
<text x="0" y="2.032" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="E2,5-6E" urn="urn:adsk.eagle:footprint:22932/1" locally_modified="yes">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, diameter 6 mm</description>
<wire x1="-2.159" y1="0" x2="-2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.254" x2="-2.413" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="2.794" width="0.1524" layer="21"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.5748" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.5748"/>
<text x="0" y="3.429" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-3.556" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
<wire x1="2.38125" y1="1.508125" x2="2.38125" y2="-1.42875" width="0.127" layer="21"/>
<wire x1="2.460625" y1="1.190625" x2="2.460625" y2="-1.190625" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.031875" x2="2.54" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="2.619375" y1="0.714375" x2="2.619375" y2="-0.79375" width="0.127" layer="21"/>
<wire x1="2.69875" y1="0.47625" x2="2.69875" y2="-0.396875" width="0.127" layer="21"/>
</package>
<package name="E1,5-4" urn="urn:adsk.eagle:footprint:22840/1" locally_modified="yes">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 1.5 mm, diameter 4 mm</description>
<wire x1="-1.524" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="1.397" x2="-0.508" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.143" x2="-0.762" y2="1.651" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.032" width="0.1524" layer="21"/>
<pad name="-" x="0.8" y="0" drill="0.7112" diameter="1.1684" shape="octagon"/>
<pad name="+" x="-0.8" y="0" drill="0.7112" diameter="1.1684"/>
<text x="0" y="3.048" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-3.048" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="51"/>
<wire x1="1.5875" y1="1.27" x2="1.5875" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.666875" y1="1.031875" x2="1.666875" y2="-1.11125" width="0.127" layer="21"/>
<wire x1="1.74625" y1="0.9525" x2="1.74625" y2="-0.873125" width="0.127" layer="21"/>
<wire x1="1.825625" y1="0.714375" x2="1.825625" y2="-0.714375" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.47625" x2="1.905" y2="-0.47625" width="0.127" layer="21"/>
</package>
<package name="E2-5" urn="urn:adsk.eagle:footprint:22850/1" locally_modified="yes">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2 mm, diameter 5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.778" x2="-0.762" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="1.524" x2="-1.016" y2="2.032" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="-" x="1.016" y="0" drill="0.8128" diameter="1.27" shape="octagon"/>
<pad name="+" x="-1.016" y="0" drill="0.8128" diameter="1.27"/>
<text x="0" y="3.302" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0.127" y="-3.302" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="51"/>
<wire x1="2.06375" y1="1.42875" x2="2.06375" y2="-1.42875" width="0.127" layer="21"/>
<wire x1="2.143125" y1="1.190625" x2="2.143125" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.2225" y1="1.11125" x2="2.2225" y2="-1.11125" width="0.127" layer="21"/>
<wire x1="2.301875" y1="0.873125" x2="2.301875" y2="-0.873125" width="0.127" layer="21"/>
<wire x1="2.38125" y1="0.635" x2="2.38125" y2="-0.635" width="0.127" layer="21"/>
<wire x1="2.460625" y1="0.15875" x2="2.460625" y2="-0.238125" width="0.127" layer="21"/>
</package>
<package name="E5-8" urn="urn:adsk.eagle:footprint:22875/1" locally_modified="yes">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, diameter 8 mm</description>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.587625" y1="2.032" x2="-2.587625" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.968625" y1="1.651" x2="-2.206625" y2="1.651" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="4" width="0.1524" layer="21"/>
<pad name="+" x="-2.5" y="0" drill="1.016" diameter="1.9304"/>
<pad name="-" x="2.5" y="0" drill="1.016" diameter="1.9304" shape="octagon"/>
<text x="0" y="5.207" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-5.207" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
<wire x1="3.730625" y1="1.27" x2="3.730625" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.031875" x2="3.81" y2="-1.190625" width="0.127" layer="21"/>
<wire x1="3.889375" y1="0.635" x2="3.889375" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.65125" y1="1.508125" x2="3.65125" y2="-1.508125" width="0.127" layer="21"/>
<wire x1="3.571875" y1="1.666875" x2="3.571875" y2="-1.74625" width="0.127" layer="21"/>
<wire x1="3.4925" y1="1.905" x2="3.4925" y2="-1.905" width="0.127" layer="21"/>
<wire x1="3.413125" y1="2.06375" x2="3.413125" y2="-2.06375" width="0.127" layer="21"/>
</package>
<package name="E2,5-6,3">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, diameter 6.3 mm</description>
<wire x1="-2.159" y1="0" x2="-2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.254" x2="-2.413" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="3.13" width="0.1524" layer="21"/>
<pad name="-" x="1.25" y="0" drill="0.8128" diameter="1.5748" shape="octagon"/>
<pad name="+" x="-1.25" y="0" drill="0.8128" diameter="1.5748"/>
<text x="-0.127" y="3.937" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-3.937" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
<wire x1="2.460625" y1="1.825625" x2="2.460625" y2="-1.905" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.74625" x2="2.54" y2="-1.74625" width="0.127" layer="21"/>
<wire x1="2.619375" y1="1.5875" x2="2.619375" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="2.69875" y1="1.508125" x2="2.69875" y2="-1.42875" width="0.127" layer="21"/>
<wire x1="2.778125" y1="1.349375" x2="2.778125" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.8575" y1="1.11125" x2="2.8575" y2="-1.190625" width="0.127" layer="21"/>
<wire x1="2.936875" y1="0.9525" x2="2.936875" y2="-0.873125" width="0.127" layer="21"/>
<wire x1="3.01625" y1="0.555625" x2="3.01625" y2="-0.635" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RCL_C-EU">
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="CPOL">
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.381" layer="94"/>
<wire x1="-1.524" y1="0.381" x2="1.524" y2="0.381" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.381" x2="1.524" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.524" y1="1.27" x2="-1.524" y2="0.381" width="0.254" layer="94"/>
<wire x1="-1.524" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="-" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<text x="1.143" y="1.7526" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="1.6764" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-3.3274" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="0" y1="1.27" x2="1.524" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR_CERAMIC_THT" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="RCL_C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="050-024X044" package="C_C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value=""/>
<attribute name="ODOO" value=""/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CPOL_THT" prefix="C">
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="0"/>
</gates>
<devices>
<device name="-6X2.5" package="E2,5-6E">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MAN" value=""/>
<attribute name="MPN" value=""/>
<attribute name="ODOO" value=""/>
<attribute name="PRICE" value="" constant="no"/>
<attribute name="SIZEX" value=""/>
<attribute name="SIZEY" value=""/>
<attribute name="SIZEZ" value=""/>
<attribute name="TYPE" value="THT"/>
<attribute name="WEB" value=""/>
</technology>
</technologies>
</device>
<device name="-4X1.5" package="E1,5-4">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5X2" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
<device name="-8X5" package="E5-8">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
<device name="-6.3X2.5" package="E2,5-6,3">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_DIODES">
<packages>
<package name="DIODE_1N4148">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<wire x1="1.397" y1="0.889" x2="-1.397" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.889" x2="1.397" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="0.889" x2="-1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-0.889" x2="1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="0" x2="-1.397" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.397" y1="0" x2="1.397" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="-1.016" y1="-0.889" x2="-0.508" y2="0.889" layer="21"/>
<pad name="A" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="C" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="0" y="1.905" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.905" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="DIODE_1N4001">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<wire x1="1.905" y1="0.889" x2="-1.905" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-0.889" x2="1.905" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="-2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0.889" x2="-1.905" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="1.905" y2="0" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-0.889" x2="1.905" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.905" y1="0" x2="1.905" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="-1.27" y1="-0.889" x2="-0.762" y2="0.889" layer="21"/>
<pad name="A" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="C" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="0" y="1.905" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.905" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="DIODE_ZDIO-5">
<description>&lt;b&gt;Z DIODE&lt;/b&gt;</description>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="21"/>
<wire x1="1.27" y1="0" x2="1.651" y2="0" width="0.254" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-1.651" y1="0" x2="-1.27" y2="0" width="0.254" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<pad name="A" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="C" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="0.0508" y="2.4638" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.413" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="DIODE_ZDIO-7.5">
<description>&lt;b&gt;Z DIODE&lt;/b&gt;</description>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="21"/>
<wire x1="1.27" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.794" y1="0" x2="-1.27" y2="0" width="0.254" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<pad name="A" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="C" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="0" y="2.159" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.159" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="DIODE_D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DIODE_ZD">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-1.778" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.778" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIODE_1N4148_THT" prefix="D" uservalue="yes">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE_D" x="0" y="0"/>
</gates>
<devices>
<device name="D-5" package="DIODE_1N4148">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_1N4001_THT" prefix="D" uservalue="yes">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE_D" x="0" y="0"/>
</gates>
<devices>
<device name="D-7.5" package="DIODE_1N4001">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER_DIODE_THT" prefix="D" uservalue="yes">
<description>Z-Diode</description>
<gates>
<gate name="G$1" symbol="DIODE_ZD" x="0" y="0"/>
</gates>
<devices>
<device name="ZD-5" package="DIODE_ZDIO-5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
<device name="ZD-7.5" package="DIODE_ZDIO-7.5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_FUSES">
<packages>
<package name="PTC_THT">
<wire x1="-3.81" y1="1.524" x2="3.81" y2="1.524" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.524" x2="3.81" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.524" x2="-3.81" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-1.524" x2="-3.81" y2="1.524" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="0.8" diameter="1.8796"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" diameter="1.8796"/>
<text x="0" y="2.086" size="0.8" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.14" size="0.8" layer="27" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SPARKFUN-POWERIC_PTC">
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="3.048" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.302" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PTC_THT" prefix="F">
<description>&lt;b&gt;Resettable Fuse PTC&lt;/b&gt;
Resettable Fuse. Spark Fun Electronics SKU : COM-08357</description>
<gates>
<gate name="G$1" symbol="SPARKFUN-POWERIC_PTC" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="PTC_THT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="arduino-micro">
<packages>
<package name="ARDUINO-MICRO">
<pad name="D12" x="1.27" y="1.27" drill="0.9"/>
<pad name="D11" x="1.27" y="3.81" drill="0.9"/>
<pad name="D10" x="1.27" y="6.35" drill="0.9"/>
<pad name="D9" x="1.27" y="8.89" drill="0.9"/>
<pad name="D8" x="1.27" y="11.43" drill="0.9"/>
<pad name="D7" x="1.27" y="13.97" drill="0.9"/>
<pad name="D6" x="1.27" y="16.51" drill="0.9"/>
<pad name="D5" x="1.27" y="19.05" drill="0.9"/>
<pad name="D4" x="1.27" y="21.59" drill="0.9"/>
<pad name="D3" x="1.27" y="24.13" drill="0.9"/>
<pad name="D2" x="1.27" y="26.67" drill="0.9"/>
<pad name="GND" x="1.27" y="29.21" drill="0.9"/>
<pad name="RST" x="1.27" y="31.75" drill="0.9"/>
<pad name="RX" x="1.27" y="34.29" drill="0.9"/>
<pad name="TX" x="1.27" y="36.83" drill="0.9"/>
<pad name="SS" x="1.27" y="39.37" drill="0.9"/>
<pad name="MOSI" x="1.27" y="41.91" drill="0.9"/>
<pad name="D13" x="16.51" y="1.27" drill="0.9"/>
<pad name="3V3" x="16.51" y="3.81" drill="0.9"/>
<pad name="REF" x="16.51" y="6.35" drill="0.9"/>
<pad name="A0" x="16.51" y="8.89" drill="0.9"/>
<pad name="A1" x="16.51" y="11.43" drill="0.9"/>
<pad name="A2" x="16.51" y="13.97" drill="0.9"/>
<pad name="A3" x="16.51" y="16.51" drill="0.9"/>
<pad name="A4" x="16.51" y="19.05" drill="0.9"/>
<pad name="A5" x="16.51" y="21.59" drill="0.9"/>
<pad name="NC8" x="16.51" y="24.13" drill="0.9"/>
<pad name="NC7" x="16.51" y="26.67" drill="0.9"/>
<pad name="5V" x="16.51" y="29.21" drill="0.9"/>
<pad name="RST1" x="16.51" y="31.75" drill="0.9"/>
<pad name="GND1" x="16.51" y="34.29" drill="0.9"/>
<pad name="VIN" x="16.51" y="36.83" drill="0.9"/>
<pad name="MISO" x="16.51" y="39.37" drill="0.9"/>
<pad name="SCK" x="16.51" y="41.91" drill="0.9"/>
<wire x1="0" y1="-2.54" x2="17.78" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="17.78" y1="-2.54" x2="17.78" y2="45.72" width="0.4064" layer="21"/>
<wire x1="17.78" y1="45.72" x2="0" y2="45.72" width="0.4064" layer="21"/>
<wire x1="0" y1="45.72" x2="0" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="5.5245" y1="-2.032" x2="5.5245" y2="-3.302" width="0.4064" layer="51"/>
<wire x1="5.5245" y1="-3.302" x2="12.6365" y2="-3.302" width="0.4064" layer="51"/>
<wire x1="12.6365" y1="-3.302" x2="12.6365" y2="-2.032" width="0.4064" layer="51"/>
<wire x1="12.6365" y1="-2.032" x2="13.3985" y2="-2.032" width="0.4064" layer="51"/>
<wire x1="13.3985" y1="-2.032" x2="13.3985" y2="0" width="0.4064" layer="51"/>
<wire x1="13.3985" y1="0" x2="12.6365" y2="0" width="0.4064" layer="51"/>
<wire x1="12.6365" y1="0" x2="12.6365" y2="2.032" width="0.4064" layer="51"/>
<wire x1="12.6365" y1="2.032" x2="5.5245" y2="2.032" width="0.4064" layer="51"/>
<wire x1="5.5245" y1="2.032" x2="5.5245" y2="0" width="0.4064" layer="51"/>
<wire x1="5.5245" y1="0" x2="4.7625" y2="0" width="0.4064" layer="51"/>
<wire x1="4.7625" y1="0" x2="4.7625" y2="-2.032" width="0.4064" layer="51"/>
<wire x1="4.7625" y1="-2.032" x2="5.5245" y2="-2.032" width="0.4064" layer="51"/>
<text x="0.9525" y="46.4185" size="1.27" layer="25">&gt;NAME</text>
<text x="0.1905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ARDUINO-MICRO">
<text x="-12.065" y="25.273" size="1.27" layer="95">&gt;NAME</text>
<text x="-12.5095" y="-25.0825" size="1.27" layer="95">&gt;VALUE</text>
<pin name="IO12*" x="-17.78" y="-20.32" length="middle"/>
<pin name="IO11*" x="-17.78" y="-17.78" length="middle"/>
<pin name="IO10*" x="-17.78" y="-15.24" length="middle"/>
<pin name="IO9*" x="-17.78" y="-12.7" length="middle"/>
<pin name="IO8" x="-17.78" y="-10.16" length="middle"/>
<pin name="D7" x="-17.78" y="-7.62" length="middle"/>
<pin name="D6*" x="-17.78" y="-5.08" length="middle"/>
<pin name="D5*" x="-17.78" y="-2.54" length="middle"/>
<pin name="D4*" x="-17.78" y="0" length="middle"/>
<pin name="D3/SCL" x="-17.78" y="2.54" length="middle"/>
<pin name="D2/SDA" x="-17.78" y="5.08" length="middle"/>
<pin name="GND" x="-17.78" y="7.62" length="middle"/>
<pin name="RESET" x="-17.78" y="10.16" length="middle"/>
<pin name="D0/RX" x="-17.78" y="12.7" length="middle"/>
<pin name="D1/TX" x="-17.78" y="15.24" length="middle"/>
<pin name="RXLED/SS" x="-17.78" y="17.78" length="middle"/>
<pin name="MOSI" x="-17.78" y="20.32" length="middle"/>
<wire x1="12.7" y1="-22.86" x2="12.7" y2="22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="-12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-22.86" x2="-12.7" y2="22.86" width="0.254" layer="94"/>
<wire x1="-12.7" y1="22.86" x2="12.7" y2="22.86" width="0.254" layer="94"/>
<pin name="IO13*" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="3V" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="AREF" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="A0" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="A1" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="A2" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="A3" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="A4" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="A5" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="NC8" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="NC7" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="5V" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="RESET1" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="GND1" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="VIN" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="MISO" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="SCK" x="17.78" y="20.32" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARDUINO_MICRO">
<gates>
<gate name="G$1" symbol="ARDUINO-MICRO" x="0" y="0"/>
</gates>
<devices>
<device name="FOOTPRINT" package="ARDUINO-MICRO">
<connects>
<connect gate="G$1" pin="3V" pad="3V3"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="A0" pad="A0"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="AREF" pad="REF"/>
<connect gate="G$1" pin="D0/RX" pad="RX"/>
<connect gate="G$1" pin="D1/TX" pad="TX"/>
<connect gate="G$1" pin="D2/SDA" pad="D2"/>
<connect gate="G$1" pin="D3/SCL" pad="D3"/>
<connect gate="G$1" pin="D4*" pad="D4"/>
<connect gate="G$1" pin="D5*" pad="D5"/>
<connect gate="G$1" pin="D6*" pad="D6"/>
<connect gate="G$1" pin="D7" pad="D7"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="IO10*" pad="D10"/>
<connect gate="G$1" pin="IO11*" pad="D11"/>
<connect gate="G$1" pin="IO12*" pad="D12"/>
<connect gate="G$1" pin="IO13*" pad="D13"/>
<connect gate="G$1" pin="IO8" pad="D8"/>
<connect gate="G$1" pin="IO9*" pad="D9"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="NC7" pad="NC7"/>
<connect gate="G$1" pin="NC8" pad="NC8"/>
<connect gate="G$1" pin="RESET" pad="RST"/>
<connect gate="G$1" pin="RESET1" pad="RST1"/>
<connect gate="G$1" pin="RXLED/SS" pad="SS"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_SYMBOLS">
<packages>
</packages>
<symbols>
<symbol name="+3V3">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BASTL_JUMPERS">
<packages>
<package name="SOLDERJUMPER">
<wire x1="0.8" y1="-1" x2="-0.8" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="1" x2="1" y2="0.7" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1" y1="0.7" x2="-0.8" y2="1" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1" y1="-0.7" x2="-0.8" y2="-1" width="0.2032" layer="21" curve="90"/>
<wire x1="0.8" y1="-1" x2="1" y2="-0.7" width="0.2032" layer="21" curve="90"/>
<wire x1="-0.8" y1="1" x2="0.8" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.45" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0.45" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-0.019" y="1.651" size="0.8" layer="25" font="fixed" ratio="15" align="center">&gt;NAME</text>
<text x="-0.019" y="-1.651" size="0.8" layer="27" font="fixed" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SOLDERJUMPER">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SOLDERJUMPER" prefix="SJ">
<gates>
<gate name="G$1" symbol="SOLDERJUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOLDERJUMPER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="SOLDER"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Teensy_3_and_LC_Series_Boards_v1.4">
<packages>
<package name="TEENSY_3.0-3.2&amp;LC_DIL">
<pad name="GND" x="-7.62" y="16.51" drill="0.9652"/>
<pad name="0" x="-7.62" y="13.97" drill="0.9652"/>
<pad name="1" x="-7.62" y="11.43" drill="0.9652"/>
<pad name="2" x="-7.62" y="8.89" drill="0.9652"/>
<pad name="3" x="-7.62" y="6.35" drill="0.9652"/>
<pad name="4" x="-7.62" y="3.81" drill="0.9652"/>
<pad name="5" x="-7.62" y="1.27" drill="0.9652"/>
<pad name="6" x="-7.62" y="-1.27" drill="0.9652"/>
<pad name="7" x="-7.62" y="-3.81" drill="0.9652"/>
<pad name="8" x="-7.62" y="-6.35" drill="0.9652"/>
<pad name="9" x="-7.62" y="-8.89" drill="0.9652"/>
<pad name="10" x="-7.62" y="-11.43" drill="0.9652"/>
<pad name="11" x="-7.62" y="-13.97" drill="0.9652"/>
<pad name="12" x="-7.62" y="-16.51" drill="0.9652"/>
<pad name="13" x="7.62" y="-16.51" drill="0.9652"/>
<pad name="14/A0" x="7.62" y="-13.97" drill="0.9652"/>
<pad name="15/A1" x="7.62" y="-11.43" drill="0.9652"/>
<pad name="16/A2" x="7.62" y="-8.89" drill="0.9652"/>
<pad name="17/A3" x="7.62" y="-6.35" drill="0.9652"/>
<pad name="18/A4" x="7.62" y="-3.81" drill="0.9652"/>
<pad name="19/A5" x="7.62" y="-1.27" drill="0.9652"/>
<pad name="20/A6" x="7.62" y="1.27" drill="0.9652"/>
<pad name="21/A7" x="7.62" y="3.81" drill="0.9652"/>
<pad name="22/A8" x="7.62" y="6.35" drill="0.9652"/>
<pad name="23/A9" x="7.62" y="8.89" drill="0.9652"/>
<pad name="3.3V" x="7.62" y="11.43" drill="0.9652"/>
<pad name="AGND" x="7.62" y="13.97" drill="0.9652"/>
<pad name="VIN" x="7.62" y="16.51" drill="0.9652"/>
<wire x1="-8.89" y1="17.78" x2="8.89" y2="17.78" width="0.127" layer="51"/>
<wire x1="8.89" y1="17.78" x2="8.89" y2="-17.78" width="0.127" layer="51"/>
<wire x1="8.89" y1="-17.78" x2="-8.89" y2="-17.78" width="0.127" layer="51"/>
<wire x1="-8.89" y1="-17.78" x2="-8.89" y2="17.78" width="0.127" layer="51"/>
<wire x1="-1.27" y1="16.51" x2="1.27" y2="16.51" width="0.2032" layer="21"/>
<wire x1="1.27" y1="16.51" x2="1.27" y2="17.78" width="0.2032" layer="21"/>
<wire x1="1.27" y1="17.78" x2="8.89" y2="17.78" width="0.2032" layer="21"/>
<wire x1="8.89" y1="17.78" x2="8.89" y2="-17.78" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-17.78" x2="-8.89" y2="-17.78" width="0.2032" layer="21"/>
<wire x1="-8.89" y1="-17.78" x2="-8.89" y2="17.78" width="0.2032" layer="21"/>
<wire x1="-8.89" y1="17.78" x2="-1.27" y2="17.78" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="17.78" x2="-1.27" y2="16.51" width="0.2032" layer="21"/>
<text x="-3.81" y="13.97" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.81" y="-13.97" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TEENSY_3.1-3.2_DIL">
<wire x1="-17.78" y1="-38.1" x2="17.78" y2="-38.1" width="0.254" layer="94"/>
<wire x1="17.78" y1="-38.1" x2="17.78" y2="30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="30.48" x2="-17.78" y2="30.48" width="0.254" layer="94"/>
<wire x1="-17.78" y1="30.48" x2="-17.78" y2="-38.1" width="0.254" layer="94"/>
<pin name="12/MISO" x="-22.86" y="-5.08" visible="pin" length="middle"/>
<pin name="11/MOSI" x="-22.86" y="-2.54" visible="pin" length="middle"/>
<pin name="10/TX2/PWM" x="-22.86" y="0" visible="pin" length="middle"/>
<pin name="9/RX2/PWM" x="-22.86" y="2.54" visible="pin" length="middle"/>
<pin name="8/TX3" x="-22.86" y="5.08" visible="pin" length="middle"/>
<pin name="7/RX3" x="-22.86" y="7.62" visible="pin" length="middle"/>
<pin name="6/PWM" x="-22.86" y="10.16" visible="pin" length="middle"/>
<pin name="5/PWM" x="-22.86" y="12.7" visible="pin" length="middle"/>
<pin name="4/CAN-RX/PWM" x="-22.86" y="15.24" visible="pin" length="middle"/>
<pin name="3/CAN-TX/PWM" x="-22.86" y="17.78" visible="pin" length="middle"/>
<pin name="2" x="-22.86" y="20.32" visible="pin" length="middle"/>
<pin name="1/TX1/T" x="-22.86" y="22.86" visible="pin" length="middle"/>
<pin name="0/RX1/T" x="-22.86" y="25.4" visible="pin" length="middle"/>
<pin name="GND" x="22.86" y="17.78" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="AGND" x="22.86" y="5.08" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="3.3V" x="22.86" y="22.86" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="23/A9/T/PWM" x="-22.86" y="-33.02" visible="pin" length="middle"/>
<pin name="22/A8/T/PWM" x="-22.86" y="-30.48" visible="pin" length="middle"/>
<pin name="21/A7/PWM" x="-22.86" y="-27.94" visible="pin" length="middle"/>
<pin name="20/A6/PWM" x="-22.86" y="-25.4" visible="pin" length="middle"/>
<pin name="19/A5/T/SCL0" x="-22.86" y="-22.86" visible="pin" length="middle"/>
<pin name="18/A4/T/SDA0" x="-22.86" y="-20.32" visible="pin" length="middle"/>
<pin name="17/A3/T" x="-22.86" y="-17.78" visible="pin" length="middle"/>
<pin name="16/A2/T" x="-22.86" y="-15.24" visible="pin" length="middle"/>
<pin name="15/A1/T" x="-22.86" y="-12.7" visible="pin" length="middle"/>
<pin name="14/A1" x="-22.86" y="-10.16" visible="pin" length="middle"/>
<pin name="13/SCK/LED" x="-22.86" y="-7.62" visible="pin" length="middle"/>
<text x="-5.588" y="31.75" size="1.27" layer="95" font="vector" ratio="15">&gt;NAME</text>
<pin name="VIN" x="22.86" y="25.4" visible="pin" length="middle" direction="pwr" rot="R180"/>
<text x="-7.112" y="-40.894" size="1.27" layer="96" font="vector" ratio="15">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEENSY_3.1-3.2_DIL">
<description>Teensy 3.1 or 3.2 in a DIL Layout.</description>
<gates>
<gate name="G$1" symbol="TEENSY_3.1-3.2_DIL" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="TEENSY_3.0-3.2&amp;LC_DIL">
<connects>
<connect gate="G$1" pin="0/RX1/T" pad="0"/>
<connect gate="G$1" pin="1/TX1/T" pad="1"/>
<connect gate="G$1" pin="10/TX2/PWM" pad="10"/>
<connect gate="G$1" pin="11/MOSI" pad="11"/>
<connect gate="G$1" pin="12/MISO" pad="12"/>
<connect gate="G$1" pin="13/SCK/LED" pad="13"/>
<connect gate="G$1" pin="14/A1" pad="14/A0"/>
<connect gate="G$1" pin="15/A1/T" pad="15/A1"/>
<connect gate="G$1" pin="16/A2/T" pad="16/A2"/>
<connect gate="G$1" pin="17/A3/T" pad="17/A3"/>
<connect gate="G$1" pin="18/A4/T/SDA0" pad="18/A4"/>
<connect gate="G$1" pin="19/A5/T/SCL0" pad="19/A5"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20/A6/PWM" pad="20/A6"/>
<connect gate="G$1" pin="21/A7/PWM" pad="21/A7"/>
<connect gate="G$1" pin="22/A8/T/PWM" pad="22/A8"/>
<connect gate="G$1" pin="23/A9/T/PWM" pad="23/A9"/>
<connect gate="G$1" pin="3.3V" pad="3.3V"/>
<connect gate="G$1" pin="3/CAN-TX/PWM" pad="3"/>
<connect gate="G$1" pin="4/CAN-RX/PWM" pad="4"/>
<connect gate="G$1" pin="5/PWM" pad="5"/>
<connect gate="G$1" pin="6/PWM" pad="6"/>
<connect gate="G$1" pin="7/RX3" pad="7"/>
<connect gate="G$1" pin="8/TX3" pad="8"/>
<connect gate="G$1" pin="9/RX2/PWM" pad="9"/>
<connect gate="G$1" pin="AGND" pad="AGND"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="R8" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R9" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="R11" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R12" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="R13" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R14" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="AMP_B" library="BASTL_ICS" deviceset="TL072" device="" package3d_urn="urn:adsk.eagle:package:16409/2" value="MCP6002"/>
<part name="R15" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="R17" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="R18" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1M"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="R19" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R7" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="100"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="IN_E" library="BASTL_CONNECTORS" deviceset="THONKICONN" device="NEW"/>
<part name="IN_D" library="BASTL_CONNECTORS" deviceset="THONKICONN" device="NEW"/>
<part name="IN_C" library="BASTL_CONNECTORS" deviceset="THONKICONN" device="NEW"/>
<part name="IN_B" library="BASTL_CONNECTORS" deviceset="THONKICONN" device="NEW"/>
<part name="IN_A" library="BASTL_CONNECTORS" deviceset="THONKICONN" device="NEW"/>
<part name="MIDI_OUT" library="BASTL_CONNECTORS" deviceset="THONKI-STEREO" device=""/>
<part name="MIDI_IN" library="BASTL_CONNECTORS" deviceset="THONKI-STEREO" device=""/>
<part name="OUT_A" library="BASTL_CONNECTORS" deviceset="THONKI-STEREO" device=""/>
<part name="OUT_AUDIO" library="BASTL_CONNECTORS" deviceset="THONKI-STEREO" device=""/>
<part name="OUT_D" library="BASTL_CONNECTORS" deviceset="THONKI-STEREO" device=""/>
<part name="OUT_C" library="BASTL_CONNECTORS" deviceset="THONKI-STEREO" device=""/>
<part name="OUT_B" library="BASTL_CONNECTORS" deviceset="THONKI-STEREO" device=""/>
<part name="B_CAL" library="BASTL_SWITCHES" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM"/>
<part name="B_INV" library="BASTL_SWITCHES" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM"/>
<part name="B_DATA" library="BASTL_SWITCHES" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM"/>
<part name="B_SYNTH" library="BASTL_SWITCHES" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM"/>
<part name="B_ASSIGN" library="BASTL_SWITCHES" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM"/>
<part name="B_MUTE" library="BASTL_SWITCHES" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM"/>
<part name="B_TOUCH" library="BASTL_SWITCHES" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM"/>
<part name="IC4" library="v-reg" library_urn="urn:adsk.eagle:library:409" deviceset="78XXL" device="" package3d_urn="urn:adsk.eagle:package:30361/1"/>
<part name="IC5" library="BASTL_ICS" deviceset="74*595" device=""/>
<part name="IC6" library="BASTL_ICS" deviceset="74*595" device=""/>
<part name="IC7" library="BASTL_ICS" deviceset="74*595" device=""/>
<part name="OK1" library="BASTL_ICS" deviceset="6N137" device="" package3d_urn="urn:adsk.eagle:package:16409/2"/>
<part name="L_0" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_1" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_2" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_3" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_4" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_5" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_F" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_E" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_D" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_C" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_B" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_A" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="M1" library="BASTL_CONNECTORS" deviceset="MICROPHONE" device=""/>
<part name="PULLUP_A" library="BASTL_SWITCHES" deviceset="SLIDE" device="VERTICAL" value="3_WAY"/>
<part name="INVERT_A" library="BASTL_SWITCHES" deviceset="SLIDE" device="VERTICAL"/>
<part name="PULLUP_B" library="BASTL_SWITCHES" deviceset="SLIDE" device="VERTICAL"/>
<part name="INVERT_B" library="BASTL_SWITCHES" deviceset="SLIDE" device="VERTICAL"/>
<part name="PULLUP_C" library="BASTL_SWITCHES" deviceset="SLIDE" device="VERTICAL"/>
<part name="PULLUP_D" library="BASTL_SWITCHES" deviceset="SLIDE" device="VERTICAL"/>
<part name="GAIN_MIC" library="BASTL_POTS" deviceset="POT_ALPHA" device="POT_ALPHA" value="B100k"/>
<part name="THR" library="BASTL_POTS" deviceset="POT_ALPHA" device="POT_ALPHA" value="B100k"/>
<part name="OFFSET_C" library="BASTL_POTS" deviceset="POT_ALPHA" device="POT_ALPHA" value="B100k"/>
<part name="OFFSET_B" library="BASTL_POTS" deviceset="POT_ALPHA" device="POT_ALPHA" value="B100k"/>
<part name="GAIN_C" library="BASTL_POTS" deviceset="POT_ALPHA" device="POT_ALPHA" value="B100k"/>
<part name="GAIN_B" library="BASTL_POTS" deviceset="POT_ALPHA" device="POT_ALPHA" value="B100k"/>
<part name="IN_PIN_B" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="IN_PIN_C" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="IN_PIN_D" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="IN_PIN_E" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="AMP_M" library="BASTL_ICS" deviceset="TL072" device="" package3d_urn="urn:adsk.eagle:package:16409/2" value="MCP6002"/>
<part name="R1" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R2" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1M"/>
<part name="R3" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R4" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="R5" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R6" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="R10" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R16" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="AMP_C" library="BASTL_ICS" deviceset="TL072" device="" package3d_urn="urn:adsk.eagle:package:16409/2" value="MCP6002"/>
<part name="R20" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="R21" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="R22" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1M"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="R23" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R24" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="100"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="R25" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R26" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1M"/>
<part name="R27" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R28" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1M"/>
<part name="R29" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="100"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="R30" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="100"/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="R31" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R32" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1M"/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="R34" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="C1" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="R35" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="C2" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="R36" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R37" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="C3" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="C4" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="C5" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="R38" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="100"/>
<part name="D1" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="R39" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="SUPPLY20" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY21" library="supply2" deviceset="+5V" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="R40" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="100"/>
<part name="R41" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="100"/>
<part name="SUPPLY22" library="supply2" deviceset="+5V" device=""/>
<part name="D2" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D3" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D4" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D5" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D6" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D7" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D8" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D9" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D10" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D11" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D12" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D13" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D14" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="SUPPLY23" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY24" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY25" library="supply2" deviceset="+5V" device=""/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="J1" library="BASTL_CONNECTORS" deviceset="PWR_CON_WITHOUT_LABLES" device=""/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="PTC" library="BASTL_FUSES" deviceset="PTC_THT" device="PTH"/>
<part name="D15" library="BASTL_DIODES" deviceset="DIODE_1N4001_THT" device="D-7.5"/>
<part name="C6" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="C7" library="BASTL_CAPACITORS" deviceset="CPOL_THT" device="-5X2" value="10uF"/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="C8" library="BASTL_CAPACITORS" deviceset="CPOL_THT" device="-5X2" value="10uF"/>
<part name="C9" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="U$1" library="arduino-micro" deviceset="ARDUINO_MICRO" device="FOOTPRINT"/>
<part name="R42" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R43" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R44" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="J2" library="BASTL_CONNECTORS" deviceset="PWR_JACK" device=""/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="R45" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="B_SHIFT" library="BASTL_SWITCHES" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM"/>
<part name="B_MIDI_" library="BASTL_SWITCHES" deviceset="PUSH_MOMENTARY" device="_THT_6X6MM"/>
<part name="L_G" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_H" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_INV" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="L_GATE" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="R46" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="R47" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R48" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="R49" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R51" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="R52" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="D16" library="BASTL_DIODES" deviceset="ZENER_DIODE_THT" device="ZD-5" value="5V1"/>
<part name="GND38" library="supply1" deviceset="GND" device=""/>
<part name="R53" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="D18" library="BASTL_DIODES" deviceset="ZENER_DIODE_THT" device="ZD-5" value="5V1"/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="C11" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="C12" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="C13" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="C14" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="C15" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="R33" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R54" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R55" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R56" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R57" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R58" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R59" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R60" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R61" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R62" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R63" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R64" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R65" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R66" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R67" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="GND40" library="supply1" deviceset="GND" device=""/>
<part name="GND41" library="supply1" deviceset="GND" device=""/>
<part name="GND42" library="supply1" deviceset="GND" device=""/>
<part name="GND43" library="supply1" deviceset="GND" device=""/>
<part name="GND44" library="supply1" deviceset="GND" device=""/>
<part name="GND45" library="supply1" deviceset="GND" device=""/>
<part name="GND46" library="supply1" deviceset="GND" device=""/>
<part name="GND47" library="supply1" deviceset="GND" device=""/>
<part name="GND48" library="supply1" deviceset="GND" device=""/>
<part name="GND49" library="supply1" deviceset="GND" device=""/>
<part name="GND50" library="supply1" deviceset="GND" device=""/>
<part name="GND51" library="supply1" deviceset="GND" device=""/>
<part name="GND52" library="supply1" deviceset="GND" device=""/>
<part name="GND53" library="supply1" deviceset="GND" device=""/>
<part name="D17" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D19" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="D20" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="I2C_HEADER" library="BASTL_PINHEADERS" deviceset="M04" device="LOCK"/>
<part name="GND54" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY28" library="supply2" deviceset="+5V" device=""/>
<part name="GND55" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY29" library="supply2" deviceset="+5V" device=""/>
<part name="GND56" library="supply1" deviceset="GND" device=""/>
<part name="GND57" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY30" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY31" library="supply2" deviceset="+5V" device=""/>
<part name="R68" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1M"/>
<part name="R69" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1M"/>
<part name="GND58" library="supply1" deviceset="GND" device=""/>
<part name="GND59" library="supply1" deviceset="GND" device=""/>
<part name="GND60" library="supply1" deviceset="GND" device=""/>
<part name="IC1" library="BASTL_ICS" deviceset="74*595" device=""/>
<part name="GND61" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY35" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY36" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY37" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY38" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY39" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY2" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY6" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY10" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY1" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY7" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY9" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY8" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY3" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="D21" library="BASTL_DIODES" deviceset="ZENER_DIODE_THT" device="ZD-5" value="3V3"/>
<part name="D3VPRT" library="BASTL_JUMPERS" deviceset="SOLDERJUMPER" device=""/>
<part name="GND62" library="supply1" deviceset="GND" device=""/>
<part name="D22" library="BASTL_DIODES" deviceset="ZENER_DIODE_THT" device="ZD-5" value="3V3"/>
<part name="E3VPRT" library="BASTL_JUMPERS" deviceset="SOLDERJUMPER" device=""/>
<part name="GND63" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY11" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY12" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY15" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY16" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY26" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY18" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY19" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="GND64" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY27" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="IC2" library="v-reg" library_urn="urn:adsk.eagle:library:409" deviceset="78LXX" device="" package3d_urn="urn:adsk.eagle:package:30346/1" value="78L33"/>
<part name="C16" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="C17" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="GND65" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY32" library="supply2" deviceset="+5V" device=""/>
<part name="GND68" library="supply1" deviceset="GND" device=""/>
<part name="D24" library="BASTL_DIODES" deviceset="DIODE_1N4148_THT" device="D-5"/>
<part name="R71" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="R72" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="10k"/>
<part name="SUPPLY33" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="R73" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="R70" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="SPI_HEADER" library="BASTL_PINHEADERS" deviceset="M06" device="LOCK"/>
<part name="U$5" library="Teensy_3_and_LC_Series_Boards_v1.4" deviceset="TEENSY_3.1-3.2_DIL" device=""/>
<part name="GND69" library="supply1" deviceset="GND" device=""/>
<part name="GND70" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY17" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY34" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="C19" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="C20" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="GND66" library="supply1" deviceset="GND" device=""/>
<part name="GND67" library="supply1" deviceset="GND" device=""/>
<part name="JP2" library="BASTL_PINHEADERS" deviceset="M01" device="PTH"/>
<part name="C21" library="BASTL_CAPACITORS" deviceset="CAPACITOR_CERAMIC_THT" device="050-024X044" value="100n"/>
<part name="B_A" library="BASTL_SWITCHES" deviceset="PUSHBUTTON_MOM_320.02" device=""/>
<part name="B_B" library="BASTL_SWITCHES" deviceset="PUSHBUTTON_MOM_320.02" device=""/>
<part name="B_C" library="BASTL_SWITCHES" deviceset="PUSHBUTTON_MOM_320.02" device=""/>
<part name="B_D" library="BASTL_SWITCHES" deviceset="PUSHBUTTON_MOM_320.02" device=""/>
<part name="B_E" library="BASTL_SWITCHES" deviceset="PUSHBUTTON_MOM_320.02" device=""/>
<part name="B_F" library="BASTL_SWITCHES" deviceset="PUSHBUTTON_MOM_320.02" device=""/>
<part name="L_6" library="BASTL_LEDS" deviceset="LED_THT" device=""/>
<part name="R50" library="BASTL_RESISTORS" deviceset="RESISTOR_0204_THT" device="0204/5" value="1k"/>
<part name="GND71" library="supply1" deviceset="GND" device=""/>
<part name="JP4" library="BASTL_PINHEADERS" deviceset="1X3_FEMALE" device=""/>
<part name="GND72" library="supply1" deviceset="GND" device=""/>
<part name="C22" library="BASTL_CAPACITORS" deviceset="CPOL_THT" device="-5X2" value="10uF"/>
<part name="SUPPLY5" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY40" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY4" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="SUPPLY13" library="BASTL_SYMBOLS" deviceset="+3V3" device=""/>
<part name="VOLTAGERANGE" library="BASTL_PINHEADERS" deviceset="M03" device="LOCK"/>
<part name="C10" library="BASTL_CAPACITORS" deviceset="CPOL_THT" device="-5X2" value="10uF"/>
<part name="EXP" library="BASTL_PINHEADERS" deviceset="M03X2" device="LOCK"/>
<part name="GND73" library="supply1" deviceset="GND" device=""/>
<part name="DIG_READ" library="BASTL_PINHEADERS" deviceset="M03" device="LOCK"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="175.26" y="177.8" size="5.08" layer="91">verify polarity</text>
<text x="370.84" y="309.88" size="5.08" layer="91">connect everything to the microcontroller</text>
<text x="22.86" y="259.08" size="5.08" layer="91">is it possible to runn at 3.3V?</text>
<text x="370.84" y="375.92" size="5.08" layer="91">8-analog pins</text>
<text x="370.84" y="368.3" size="5.08" layer="91">3+2 pins = UI</text>
<text x="370.84" y="360.68" size="5.08" layer="91">2 CV/audio PWM pins</text>
<text x="370.84" y="317.5" size="5.08" layer="91">3 gate outs</text>
<text x="370.84" y="345.44" size="5.08" layer="91">2 pins for MIDI</text>
<text x="370.84" y="337.82" size="5.08" layer="91">3 pins SPI</text>
<text x="370.84" y="330.2" size="5.08" layer="91">2 pins I2C</text>
<text x="370.84" y="322.58" size="5.08" layer="91">22-25 pins</text>
</plain>
<instances>
<instance part="R8" gate="G$1" x="53.34" y="175.26" smashed="yes">
<attribute name="NAME" x="49.53" y="176.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="171.958" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="30.48" y="185.42" smashed="yes" rot="R270">
<attribute name="NAME" x="31.9786" y="189.23" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="27.178" y="189.23" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND10" gate="1" x="30.48" y="165.1" smashed="yes">
<attribute name="VALUE" x="27.94" y="162.56" size="1.778" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="71.12" y="149.86" smashed="yes">
<attribute name="NAME" x="67.31" y="151.3586" size="1.778" layer="95"/>
<attribute name="VALUE" x="67.31" y="146.558" size="1.778" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="81.28" y="149.86" smashed="yes">
<attribute name="NAME" x="77.47" y="151.3586" size="1.778" layer="95"/>
<attribute name="VALUE" x="77.47" y="146.558" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="86.36" y="147.32" smashed="yes">
<attribute name="VALUE" x="83.82" y="144.78" size="1.778" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="76.2" y="162.56" smashed="yes">
<attribute name="NAME" x="72.39" y="164.0586" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.39" y="159.258" size="1.778" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="86.36" y="172.72" smashed="yes">
<attribute name="NAME" x="82.55" y="174.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="82.55" y="169.418" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="45.72" y="160.02" smashed="yes">
<attribute name="NAME" x="41.91" y="161.5186" size="1.778" layer="95"/>
<attribute name="VALUE" x="41.91" y="156.718" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="12.7" y="152.4" smashed="yes">
<attribute name="VALUE" x="10.16" y="149.86" size="1.778" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="129.54" y="170.18" smashed="yes" rot="R180">
<attribute name="NAME" x="133.35" y="168.6814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="133.35" y="173.482" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND14" gate="1" x="134.62" y="162.56" smashed="yes">
<attribute name="VALUE" x="132.08" y="160.02" size="1.778" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="53.34" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="51.8414" y="151.13" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="56.642" y="151.13" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND15" gate="1" x="53.34" y="147.32" smashed="yes">
<attribute name="VALUE" x="50.8" y="144.78" size="1.778" layer="96"/>
</instance>
<instance part="R19" gate="G$1" x="43.18" y="175.26" smashed="yes" rot="R180">
<attribute name="NAME" x="46.99" y="173.7614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="46.99" y="178.562" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R7" gate="G$1" x="22.86" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="26.67" y="138.2014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="26.67" y="143.002" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND7" gate="1" x="17.78" y="134.62" smashed="yes">
<attribute name="VALUE" x="15.24" y="132.08" size="1.778" layer="96"/>
</instance>
<instance part="IN_E" gate="G$1" x="58.42" y="27.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="60.96" y="32.004" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="IN_D" gate="G$1" x="0" y="27.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="2.54" y="32.004" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="IN_C" gate="G$1" x="7.62" y="88.9" smashed="yes" rot="MR0">
<attribute name="NAME" x="10.16" y="92.964" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="IN_B" gate="G$1" x="7.62" y="157.48" smashed="yes" rot="MR0">
<attribute name="NAME" x="10.16" y="161.544" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="IN_A" gate="G$1" x="38.1" y="-38.1" smashed="yes" rot="MR0">
<attribute name="NAME" x="40.64" y="-34.036" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="MIDI_OUT" gate="G$1" x="279.4" y="165.1" smashed="yes" rot="R180">
<attribute name="NAME" x="284.48" y="162.052" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="MIDI_IN" gate="G$1" x="180.34" y="170.18" smashed="yes" rot="MR180">
<attribute name="NAME" x="175.26" y="167.132" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="OUT_A" gate="G$1" x="142.24" y="-40.64" smashed="yes" rot="R180">
<attribute name="NAME" x="147.32" y="-43.688" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="OUT_AUDIO" gate="G$1" x="200.66" y="33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="205.74" y="29.972" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="OUT_D" gate="G$1" x="205.74" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="210.82" y="68.072" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="OUT_C" gate="G$1" x="139.7" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="144.78" y="96.012" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="OUT_B" gate="G$1" x="139.7" y="167.64" smashed="yes" rot="R180">
<attribute name="NAME" x="144.78" y="164.592" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="B_CAL" gate="G$2" x="419.1" y="-20.32" smashed="yes">
<attribute name="NAME" x="416.56" y="-17.78" size="1.778" layer="95"/>
</instance>
<instance part="B_INV" gate="G$2" x="419.1" y="-27.94" smashed="yes">
<attribute name="NAME" x="416.56" y="-25.4" size="1.778" layer="95"/>
</instance>
<instance part="B_DATA" gate="G$2" x="419.1" y="-35.56" smashed="yes">
<attribute name="NAME" x="416.56" y="-33.02" size="1.778" layer="95"/>
</instance>
<instance part="B_SYNTH" gate="G$2" x="419.1" y="-43.18" smashed="yes">
<attribute name="NAME" x="416.56" y="-40.64" size="1.778" layer="95"/>
</instance>
<instance part="B_ASSIGN" gate="G$2" x="419.1" y="-50.8" smashed="yes">
<attribute name="NAME" x="416.56" y="-48.26" size="1.778" layer="95"/>
</instance>
<instance part="B_MUTE" gate="G$2" x="419.1" y="-58.42" smashed="yes">
<attribute name="NAME" x="416.56" y="-55.88" size="1.778" layer="95"/>
</instance>
<instance part="B_TOUCH" gate="G$2" x="419.1" y="-66.04" smashed="yes">
<attribute name="NAME" x="416.56" y="-63.5" size="1.778" layer="95"/>
</instance>
<instance part="IC4" gate="A" x="30.48" y="241.3" smashed="yes">
<attribute name="NAME" x="33.02" y="233.68" size="1.778" layer="95"/>
<attribute name="VALUE" x="33.02" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="IC5" gate="A" x="360.68" y="111.76" smashed="yes">
<attribute name="NAME" x="353.06" y="125.095" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="353.06" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="IC6" gate="A" x="360.68" y="63.5" smashed="yes">
<attribute name="NAME" x="353.06" y="76.835" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="353.06" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="IC7" gate="A" x="360.68" y="17.78" smashed="yes">
<attribute name="NAME" x="353.06" y="31.115" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="358.14" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="OK1" gate="A" x="220.98" y="165.1" smashed="yes">
<attribute name="NAME" x="211.455" y="173.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="211.455" y="157.48" size="1.778" layer="96"/>
</instance>
<instance part="L_0" gate="G$1" x="398.78" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="163.576" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="163.195" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_1" gate="G$1" x="398.78" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="153.416" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="155.575" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_2" gate="G$1" x="398.78" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="145.796" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="147.955" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_3" gate="G$1" x="398.78" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="138.176" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="140.335" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_4" gate="G$1" x="398.78" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="130.556" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="132.715" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_5" gate="G$1" x="398.78" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="122.936" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="125.095" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_F" gate="G$1" x="398.78" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="54.356" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="56.515" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_E" gate="G$1" x="398.78" y="58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="61.976" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="64.135" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_D" gate="G$1" x="398.78" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="69.596" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="71.755" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_C" gate="G$1" x="398.78" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="77.216" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="79.375" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_B" gate="G$1" x="398.78" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="84.836" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="86.995" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_A" gate="G$1" x="398.78" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="92.456" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="94.615" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="M1" gate="G$1" x="55.88" y="-43.18" smashed="yes" rot="R270"/>
<instance part="PULLUP_A" gate="G$1" x="17.78" y="165.1" smashed="yes">
<attribute name="NAME" x="10.16" y="163.195" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="INVERT_A" gate="G$1" x="111.76" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="113.665" y="175.26" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="PULLUP_B" gate="G$1" x="17.78" y="96.52" smashed="yes">
<attribute name="NAME" x="22.86" y="94.615" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="INVERT_B" gate="G$1" x="111.76" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="113.665" y="106.68" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="PULLUP_C" gate="G$1" x="10.16" y="35.56" smashed="yes">
<attribute name="NAME" x="15.24" y="33.655" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="PULLUP_D" gate="G$1" x="68.58" y="35.56" smashed="yes">
<attribute name="NAME" x="73.66" y="33.655" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="GAIN_MIC" gate="G$1" x="96.52" y="-25.4" smashed="yes" rot="MR90">
<attribute name="NAME" x="92.71" y="-31.369" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="92.71" y="-29.21" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="THR" gate="G$1" x="180.34" y="2.54" smashed="yes">
<attribute name="NAME" x="174.371" y="-1.27" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="176.53" y="-1.27" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="OFFSET_C" gate="G$1" x="30.48" y="106.68" smashed="yes">
<attribute name="NAME" x="24.511" y="102.87" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.67" y="102.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="OFFSET_B" gate="G$1" x="30.48" y="175.26" smashed="yes">
<attribute name="NAME" x="27.051" y="179.07" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.67" y="171.45" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GAIN_C" gate="G$1" x="63.5" y="106.68" smashed="yes" rot="MR90">
<attribute name="NAME" x="59.69" y="100.711" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="59.69" y="102.87" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GAIN_B" gate="G$1" x="63.5" y="175.26" smashed="yes" rot="MR90">
<attribute name="NAME" x="59.69" y="169.291" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="59.69" y="171.45" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="IN_PIN_B" gate="G$1" x="10.16" y="139.7" smashed="yes">
<attribute name="NAME" x="7.62" y="137.16" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="IN_PIN_C" gate="G$1" x="10.16" y="71.12" smashed="yes">
<attribute name="NAME" x="7.62" y="68.58" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="IN_PIN_D" gate="G$1" x="2.54" y="12.7" smashed="yes">
<attribute name="NAME" x="0" y="10.16" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="IN_PIN_E" gate="G$1" x="60.96" y="12.7" smashed="yes">
<attribute name="NAME" x="58.42" y="10.16" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="AMP_M" gate="P" x="162.56" y="238.76" smashed="yes">
<attribute name="NAME" x="160.655" y="237.998" size="1.778" layer="95"/>
</instance>
<instance part="R1" gate="G$1" x="15.24" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="13.7414" y="171.45" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="13.462" y="176.53" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="20.32" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="18.8214" y="171.45" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="18.542" y="176.53" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="53.34" y="106.68" smashed="yes">
<attribute name="NAME" x="49.53" y="108.1786" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="103.378" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="30.48" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="31.9786" y="120.65" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="27.178" y="120.65" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND1" gate="1" x="30.48" y="96.52" smashed="yes">
<attribute name="VALUE" x="27.94" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="71.12" y="81.28" smashed="yes">
<attribute name="NAME" x="67.31" y="82.7786" size="1.778" layer="95"/>
<attribute name="VALUE" x="67.31" y="77.978" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="81.28" y="81.28" smashed="yes">
<attribute name="NAME" x="77.47" y="82.7786" size="1.778" layer="95"/>
<attribute name="VALUE" x="77.47" y="77.978" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="86.36" y="78.74" smashed="yes">
<attribute name="VALUE" x="83.82" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="73.66" y="93.98" smashed="yes">
<attribute name="NAME" x="69.85" y="95.4786" size="1.778" layer="95"/>
<attribute name="VALUE" x="69.85" y="90.678" size="1.778" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="86.36" y="104.14" smashed="yes">
<attribute name="NAME" x="82.55" y="105.6386" size="1.778" layer="95"/>
<attribute name="VALUE" x="82.55" y="100.838" size="1.778" layer="96"/>
</instance>
<instance part="AMP_C" gate="B" x="86.36" y="93.98" smashed="yes">
<attribute name="NAME" x="93.98" y="99.06" size="1.778" layer="95"/>
<attribute name="VALUE" x="93.98" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="AMP_C" gate="A" x="58.42" y="93.98" smashed="yes">
<attribute name="NAME" x="66.04" y="99.06" size="1.778" layer="95"/>
<attribute name="VALUE" x="66.04" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="R20" gate="G$1" x="30.48" y="91.44" smashed="yes">
<attribute name="NAME" x="26.67" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="26.67" y="88.138" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="12.7" y="83.82" smashed="yes">
<attribute name="VALUE" x="10.16" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="R21" gate="G$1" x="129.54" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="133.35" y="100.1014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="133.35" y="104.902" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND4" gate="1" x="134.62" y="93.98" smashed="yes">
<attribute name="VALUE" x="132.08" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="53.34" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="51.8414" y="82.55" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="56.642" y="82.55" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="53.34" y="78.74" smashed="yes">
<attribute name="VALUE" x="50.8" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="43.18" y="106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="46.99" y="105.1814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="46.99" y="109.982" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R24" gate="G$1" x="22.86" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="26.67" y="69.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="26.67" y="74.422" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND6" gate="1" x="17.78" y="66.04" smashed="yes">
<attribute name="VALUE" x="15.24" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="15.24" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="13.7414" y="102.87" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="18.542" y="102.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R26" gate="G$1" x="20.32" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="18.8214" y="102.87" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="23.622" y="102.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R27" gate="G$1" x="7.62" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="6.1214" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.922" y="41.91" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R28" gate="G$1" x="12.7" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="11.2014" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="16.002" y="41.91" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R29" gate="G$1" x="15.24" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="19.05" y="11.2014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="19.05" y="16.002" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND8" gate="1" x="10.16" y="7.62" smashed="yes">
<attribute name="VALUE" x="7.62" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="5.08" y="22.86" smashed="yes">
<attribute name="VALUE" x="2.54" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="63.5" y="22.86" smashed="yes">
<attribute name="VALUE" x="60.96" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="73.66" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="77.47" y="11.2014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="77.47" y="16.002" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND16" gate="1" x="68.58" y="7.62" smashed="yes">
<attribute name="VALUE" x="66.04" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="R31" gate="G$1" x="66.04" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="64.5414" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="69.342" y="41.91" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R32" gate="G$1" x="71.12" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="69.6214" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="74.422" y="41.91" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND19" gate="1" x="180.34" y="-5.08" smashed="yes">
<attribute name="VALUE" x="177.8" y="-7.62" size="1.778" layer="96"/>
</instance>
<instance part="R34" gate="G$1" x="175.26" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="179.07" y="28.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="179.07" y="33.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="180.34" y="27.94" smashed="yes">
<attribute name="NAME" x="181.864" y="28.321" size="1.778" layer="95"/>
<attribute name="VALUE" x="181.864" y="23.241" size="1.778" layer="96"/>
</instance>
<instance part="R35" gate="G$1" x="185.42" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="189.23" y="72.1614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="189.23" y="76.962" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C2" gate="G$1" x="190.5" y="71.12" smashed="yes">
<attribute name="NAME" x="192.024" y="71.501" size="1.778" layer="95"/>
<attribute name="VALUE" x="192.024" y="66.421" size="1.778" layer="96"/>
</instance>
<instance part="GND20" gate="1" x="190.5" y="63.5" smashed="yes">
<attribute name="VALUE" x="187.96" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND21" gate="1" x="180.34" y="20.32" smashed="yes">
<attribute name="VALUE" x="177.8" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="R36" gate="G$1" x="185.42" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="189.23" y="28.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="189.23" y="33.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R37" gate="G$1" x="185.42" y="35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="189.23" y="34.0614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="189.23" y="38.862" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND22" gate="1" x="200.66" y="66.04" smashed="yes">
<attribute name="VALUE" x="198.12" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="GND23" gate="1" x="195.58" y="27.94" smashed="yes">
<attribute name="VALUE" x="193.04" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="AMP_B" gate="P" x="152.4" y="238.76" smashed="yes">
<attribute name="NAME" x="150.495" y="237.998" size="1.778" layer="95"/>
</instance>
<instance part="AMP_C" gate="P" x="175.26" y="238.76" smashed="yes">
<attribute name="NAME" x="173.355" y="237.998" size="1.778" layer="95"/>
</instance>
<instance part="C3" gate="G$1" x="144.78" y="238.76" smashed="yes">
<attribute name="NAME" x="146.304" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="146.304" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="157.48" y="238.76" smashed="yes">
<attribute name="NAME" x="159.004" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="159.004" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="170.18" y="238.76" smashed="yes">
<attribute name="NAME" x="171.704" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="171.704" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="GND26" gate="1" x="231.14" y="160.02" smashed="yes">
<attribute name="VALUE" x="228.6" y="157.48" size="1.778" layer="96"/>
</instance>
<instance part="R38" gate="G$1" x="195.58" y="162.56" smashed="yes" rot="R180">
<attribute name="NAME" x="199.39" y="161.0614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="199.39" y="165.862" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D1" gate="G$1" x="203.2" y="167.64" smashed="yes" rot="R90">
<attribute name="NAME" x="202.7174" y="170.18" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="205.5114" y="170.18" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R39" gate="G$1" x="241.3" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="242.7986" y="173.99" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="237.998" y="173.99" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY20" gate="+5V" x="231.14" y="172.72" smashed="yes">
<attribute name="VALUE" x="229.235" y="175.895" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY21" gate="+5V" x="241.3" y="177.8" smashed="yes">
<attribute name="VALUE" x="239.395" y="180.975" size="1.778" layer="96"/>
</instance>
<instance part="GND27" gate="1" x="274.32" y="160.02" smashed="yes">
<attribute name="VALUE" x="271.78" y="157.48" size="1.778" layer="96"/>
</instance>
<instance part="R40" gate="G$1" x="269.24" y="165.1" smashed="yes" rot="R180">
<attribute name="NAME" x="273.05" y="163.6014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="273.05" y="168.402" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R41" gate="G$1" x="269.24" y="177.8" smashed="yes" rot="R180">
<attribute name="NAME" x="273.05" y="176.3014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="273.05" y="181.102" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY22" gate="+5V" x="264.16" y="167.64" smashed="yes">
<attribute name="VALUE" x="262.255" y="170.815" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="429.26" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="27.4574" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="30.2514" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D3" gate="G$1" x="429.26" y="20.32" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="19.8374" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="22.6314" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D4" gate="G$1" x="429.26" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="12.2174" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="15.0114" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D5" gate="G$1" x="429.26" y="5.08" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="4.5974" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="7.3914" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D6" gate="G$1" x="429.26" y="-2.54" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-3.0226" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-0.2286" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D7" gate="G$1" x="429.26" y="-10.16" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-10.6426" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-7.8486" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D8" gate="G$1" x="429.26" y="-20.32" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-20.8026" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-18.0086" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D9" gate="G$1" x="429.26" y="-27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-28.4226" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-25.6286" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D10" gate="G$1" x="429.26" y="-35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-36.0426" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-33.2486" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D11" gate="G$1" x="429.26" y="-43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-43.6626" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-40.8686" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D12" gate="G$1" x="429.26" y="-50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-51.2826" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-48.4886" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D13" gate="G$1" x="429.26" y="-58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-58.9026" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-56.1086" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D14" gate="G$1" x="429.26" y="-66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-66.5226" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-63.7286" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY23" gate="+5V" x="345.44" y="66.04" smashed="yes" rot="R90">
<attribute name="VALUE" x="342.265" y="64.135" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY24" gate="+5V" x="345.44" y="20.32" smashed="yes" rot="R90">
<attribute name="VALUE" x="342.265" y="18.415" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY25" gate="+5V" x="345.44" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="342.265" y="112.395" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND25" gate="1" x="347.98" y="96.52" smashed="yes">
<attribute name="VALUE" x="345.44" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="GND28" gate="1" x="347.98" y="48.26" smashed="yes">
<attribute name="VALUE" x="345.44" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="GND29" gate="1" x="347.98" y="2.54" smashed="yes">
<attribute name="VALUE" x="345.44" y="0" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="-12.7" y="236.22" smashed="yes">
<attribute name="VALUE" x="-30.48" y="226.06" size="1.778" layer="96"/>
<attribute name="NAME" x="-15.24" y="244.602" size="1.778" layer="95"/>
</instance>
<instance part="GND30" gate="1" x="0" y="233.68" smashed="yes">
<attribute name="VALUE" x="-2.54" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="PTC" gate="G$1" x="0" y="241.3" smashed="yes">
<attribute name="NAME" x="-2.54" y="244.348" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.302" y="236.22" size="1.778" layer="96"/>
</instance>
<instance part="D15" gate="G$1" x="7.62" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="7.1374" y="241.3" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="9.9314" y="241.3" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="12.7" y="238.76" smashed="yes">
<attribute name="NAME" x="14.224" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="14.224" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="17.78" y="238.76" smashed="yes">
<attribute name="NAME" x="18.923" y="240.5126" size="1.778" layer="95"/>
<attribute name="VALUE" x="18.923" y="235.4326" size="1.778" layer="96"/>
</instance>
<instance part="GND31" gate="1" x="30.48" y="231.14" smashed="yes">
<attribute name="VALUE" x="27.94" y="228.6" size="1.778" layer="96"/>
</instance>
<instance part="GND32" gate="1" x="7.62" y="233.68" smashed="yes">
<attribute name="VALUE" x="5.08" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="38.1" y="238.76" smashed="yes">
<attribute name="NAME" x="39.243" y="240.5126" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.243" y="235.4326" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="43.18" y="238.76" smashed="yes">
<attribute name="NAME" x="44.704" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="44.704" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="U$1" gate="G$1" x="454.66" y="276.86" smashed="yes">
<attribute name="NAME" x="442.595" y="302.133" size="1.27" layer="95"/>
<attribute name="VALUE" x="442.1505" y="251.7775" size="1.27" layer="95"/>
</instance>
<instance part="R42" gate="G$1" x="129.54" y="157.48" smashed="yes" rot="R270">
<attribute name="NAME" x="131.0386" y="161.29" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="126.238" y="161.29" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R43" gate="G$1" x="129.54" y="88.9" smashed="yes" rot="R270">
<attribute name="NAME" x="131.0386" y="92.71" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="126.238" y="92.71" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R44" gate="G$1" x="195.58" y="55.88" smashed="yes" rot="R270">
<attribute name="NAME" x="197.0786" y="59.69" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="192.278" y="59.69" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J2" gate="G$1" x="-20.32" y="259.08" smashed="yes">
<attribute name="NAME" x="-27.94" y="264.16" size="1.778" layer="95"/>
<attribute name="VALUE" x="-27.94" y="254" size="1.778" layer="96"/>
</instance>
<instance part="GND35" gate="1" x="-15.24" y="254" smashed="yes">
<attribute name="VALUE" x="-17.78" y="251.46" size="1.778" layer="96"/>
</instance>
<instance part="R45" gate="G$1" x="408.94" y="157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="155.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="410.21" y="163.322" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="B_SHIFT" gate="G$2" x="419.1" y="-73.66" smashed="yes">
<attribute name="NAME" x="416.56" y="-71.12" size="1.778" layer="95"/>
</instance>
<instance part="B_MIDI_" gate="G$2" x="419.1" y="-81.28" smashed="yes">
<attribute name="NAME" x="416.56" y="-78.74" size="1.778" layer="95"/>
</instance>
<instance part="L_G" gate="G$1" x="398.78" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="46.736" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="48.895" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_H" gate="G$1" x="398.78" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="39.116" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="41.275" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_INV" gate="G$1" x="388.62" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="393.192" y="186.436" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="393.192" y="188.595" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L_GATE" gate="G$1" x="398.78" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="107.696" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="109.855" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R46" gate="G$1" x="48.26" y="-33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="46.7614" y="-36.83" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="51.562" y="-36.83" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND33" gate="1" x="43.18" y="-43.18" smashed="yes">
<attribute name="VALUE" x="40.64" y="-45.72" size="1.778" layer="96"/>
</instance>
<instance part="GND34" gate="1" x="55.88" y="-55.88" smashed="yes">
<attribute name="VALUE" x="53.34" y="-58.42" size="1.778" layer="96"/>
</instance>
<instance part="R47" gate="G$1" x="73.66" y="-50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="77.47" y="-52.2986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="77.47" y="-47.498" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R48" gate="G$1" x="83.82" y="-50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="87.63" y="-52.2986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="87.63" y="-47.498" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND36" gate="1" x="88.9" y="-53.34" smashed="yes">
<attribute name="VALUE" x="86.36" y="-55.88" size="1.778" layer="96"/>
</instance>
<instance part="R49" gate="G$1" x="68.58" y="-35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="72.39" y="-37.0586" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="72.39" y="-32.258" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R51" gate="G$1" x="106.68" y="-38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="105.41" y="-39.5986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="110.49" y="-34.798" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND37" gate="1" x="137.16" y="-45.72" smashed="yes">
<attribute name="VALUE" x="134.62" y="-48.26" size="1.778" layer="96"/>
</instance>
<instance part="R52" gate="G$1" x="22.86" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="26.67" y="28.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="26.67" y="33.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D16" gate="G$1" x="30.48" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="28.575" y="26.162" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="33.909" y="26.162" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND38" gate="1" x="30.48" y="22.86" smashed="yes">
<attribute name="VALUE" x="27.94" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="R53" gate="G$1" x="81.28" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="85.09" y="28.9814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="85.09" y="33.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D18" gate="G$1" x="86.36" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="84.455" y="26.162" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="89.789" y="26.162" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND39" gate="1" x="86.36" y="22.86" smashed="yes">
<attribute name="VALUE" x="83.82" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="C11" gate="G$1" x="236.22" y="167.64" smashed="yes">
<attribute name="NAME" x="237.744" y="168.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="237.744" y="162.941" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="497.84" y="269.24" smashed="yes">
<attribute name="NAME" x="499.364" y="269.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="499.364" y="264.541" size="1.778" layer="96"/>
</instance>
<instance part="IC5" gate="P" x="66.04" y="233.68" smashed="yes">
<attribute name="NAME" x="65.405" y="233.045" size="1.778" layer="95"/>
</instance>
<instance part="IC6" gate="P" x="76.2" y="233.68" smashed="yes">
<attribute name="NAME" x="75.565" y="233.045" size="1.778" layer="95"/>
</instance>
<instance part="IC7" gate="P" x="86.36" y="233.68" smashed="yes">
<attribute name="NAME" x="85.725" y="233.045" size="1.778" layer="95"/>
</instance>
<instance part="C13" gate="G$1" x="60.96" y="236.22" smashed="yes">
<attribute name="NAME" x="62.484" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="62.484" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="71.12" y="236.22" smashed="yes">
<attribute name="NAME" x="72.644" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.644" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="C15" gate="G$1" x="81.28" y="236.22" smashed="yes">
<attribute name="NAME" x="82.804" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="82.804" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="R33" gate="G$1" x="408.94" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="148.3614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="153.162" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R54" gate="G$1" x="408.94" y="142.24" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="140.7414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="145.542" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R55" gate="G$1" x="408.94" y="134.62" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="133.1214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="137.922" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R56" gate="G$1" x="408.94" y="127" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="125.5014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="130.302" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R57" gate="G$1" x="408.94" y="119.38" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="117.8814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="122.682" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R58" gate="G$1" x="408.94" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="87.4014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="410.21" y="92.202" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R59" gate="G$1" x="408.94" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="79.7814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="84.582" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R60" gate="G$1" x="408.94" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="72.1614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="76.962" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R61" gate="G$1" x="408.94" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="64.5414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="69.342" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R62" gate="G$1" x="408.94" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="56.9214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="61.722" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R63" gate="G$1" x="408.94" y="50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="49.3014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="54.102" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R64" gate="G$1" x="408.94" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="41.6814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="46.482" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R65" gate="G$1" x="408.94" y="35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="34.0614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="38.862" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R66" gate="G$1" x="408.94" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="102.6414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="407.67" y="109.982" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R67" gate="G$1" x="398.78" y="182.88" smashed="yes" rot="R180">
<attribute name="NAME" x="402.59" y="181.3814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="402.59" y="186.182" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND17" gate="1" x="403.86" y="180.34" smashed="yes">
<attribute name="VALUE" x="401.32" y="177.8" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="414.02" y="101.6" smashed="yes">
<attribute name="VALUE" x="411.48" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND40" gate="1" x="414.02" y="33.02" smashed="yes">
<attribute name="VALUE" x="414.02" y="30.48" size="1.778" layer="96"/>
</instance>
<instance part="GND41" gate="1" x="414.02" y="40.64" smashed="yes">
<attribute name="VALUE" x="411.48" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="GND42" gate="1" x="414.02" y="48.26" smashed="yes">
<attribute name="VALUE" x="411.48" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="GND43" gate="1" x="414.02" y="55.88" smashed="yes">
<attribute name="VALUE" x="411.48" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="GND44" gate="1" x="414.02" y="63.5" smashed="yes">
<attribute name="VALUE" x="411.48" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND45" gate="1" x="414.02" y="71.12" smashed="yes">
<attribute name="VALUE" x="411.48" y="68.58" size="1.778" layer="96"/>
</instance>
<instance part="GND46" gate="1" x="414.02" y="78.74" smashed="yes">
<attribute name="VALUE" x="411.48" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="GND47" gate="1" x="414.02" y="86.36" smashed="yes">
<attribute name="VALUE" x="411.48" y="83.82" size="1.778" layer="96"/>
</instance>
<instance part="GND48" gate="1" x="414.02" y="116.84" smashed="yes">
<attribute name="VALUE" x="411.48" y="114.3" size="1.778" layer="96"/>
</instance>
<instance part="GND49" gate="1" x="414.02" y="124.46" smashed="yes">
<attribute name="VALUE" x="411.48" y="121.92" size="1.778" layer="96"/>
</instance>
<instance part="GND50" gate="1" x="414.02" y="132.08" smashed="yes">
<attribute name="VALUE" x="411.48" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND51" gate="1" x="414.02" y="139.7" smashed="yes">
<attribute name="VALUE" x="411.48" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="GND52" gate="1" x="414.02" y="147.32" smashed="yes">
<attribute name="VALUE" x="411.48" y="144.78" size="1.778" layer="96"/>
</instance>
<instance part="GND53" gate="1" x="414.02" y="154.94" smashed="yes">
<attribute name="VALUE" x="411.48" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="D17" gate="G$1" x="429.26" y="-73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-74.1426" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-71.3486" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D19" gate="G$1" x="429.26" y="-81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-81.7626" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-78.9686" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D20" gate="G$1" x="429.26" y="-88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="426.72" y="-89.3826" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="426.72" y="-86.5886" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="I2C_HEADER" gate="G$1" x="185.42" y="238.76" smashed="yes">
<attribute name="VALUE" x="180.34" y="231.14" size="1.778" layer="96"/>
<attribute name="NAME" x="180.34" y="247.142" size="1.778" layer="95"/>
</instance>
<instance part="GND54" gate="1" x="190.5" y="205.74" smashed="yes">
<attribute name="VALUE" x="187.96" y="203.2" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY28" gate="+5V" x="198.12" y="210.82" smashed="yes" rot="R270">
<attribute name="VALUE" x="201.295" y="212.725" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND55" gate="1" x="190.5" y="233.68" smashed="yes">
<attribute name="VALUE" x="187.96" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY29" gate="+5V" x="198.12" y="238.76" smashed="yes" rot="R270">
<attribute name="VALUE" x="201.295" y="240.665" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND56" gate="1" x="434.34" y="284.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="431.8" y="287.02" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND57" gate="1" x="474.98" y="289.56" smashed="yes" rot="R90">
<attribute name="VALUE" x="477.52" y="287.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY30" gate="+5V" x="490.22" y="284.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="493.395" y="286.385" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY31" gate="+5V" x="502.92" y="271.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="506.095" y="273.685" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R68" gate="G$1" x="185.42" y="-35.56" smashed="yes" rot="R270">
<attribute name="NAME" x="186.9186" y="-31.75" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="182.118" y="-31.75" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R69" gate="G$1" x="185.42" y="-66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="186.9186" y="-62.23" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="182.118" y="-62.23" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND58" gate="1" x="185.42" y="-45.72" smashed="yes">
<attribute name="VALUE" x="182.88" y="-48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND59" gate="1" x="185.42" y="-76.2" smashed="yes">
<attribute name="VALUE" x="182.88" y="-78.74" size="1.778" layer="96"/>
</instance>
<instance part="GND60" gate="1" x="497.84" y="261.62" smashed="yes">
<attribute name="VALUE" x="495.3" y="259.08" size="1.778" layer="96"/>
</instance>
<instance part="IC1" gate="A" x="358.14" y="172.72" smashed="yes">
<attribute name="NAME" x="350.52" y="186.055" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="355.6" y="175.26" size="1.778" layer="96"/>
</instance>
<instance part="GND61" gate="1" x="345.44" y="157.48" smashed="yes">
<attribute name="VALUE" x="342.9" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY35" gate="+5V" x="340.36" y="175.26" smashed="yes" rot="R90">
<attribute name="VALUE" x="337.185" y="173.355" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY36" gate="G$1" x="66.04" y="55.88" smashed="yes">
<attribute name="VALUE" x="64.135" y="59.055" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY37" gate="G$1" x="71.12" y="58.42" smashed="yes">
<attribute name="VALUE" x="69.215" y="61.595" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY38" gate="G$1" x="7.62" y="55.88" smashed="yes">
<attribute name="VALUE" x="5.715" y="59.055" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY39" gate="G$1" x="12.7" y="58.42" smashed="yes">
<attribute name="VALUE" x="10.795" y="61.595" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="15.24" y="121.92" smashed="yes">
<attribute name="VALUE" x="13.335" y="125.095" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="20.32" y="121.92" smashed="yes">
<attribute name="VALUE" x="18.415" y="125.095" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY10" gate="G$1" x="30.48" y="127" smashed="yes">
<attribute name="VALUE" x="28.575" y="130.175" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="15.24" y="185.42" smashed="yes">
<attribute name="VALUE" x="13.335" y="188.595" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="G$1" x="20.32" y="187.96" smashed="yes">
<attribute name="VALUE" x="18.415" y="191.135" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="G$1" x="30.48" y="195.58" smashed="yes">
<attribute name="VALUE" x="28.575" y="198.755" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY8" gate="G$1" x="66.04" y="154.94" smashed="yes">
<attribute name="VALUE" x="64.135" y="158.115" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="66.04" y="86.36" smashed="yes">
<attribute name="VALUE" x="64.135" y="89.535" size="1.778" layer="96"/>
</instance>
<instance part="D21" gate="G$1" x="38.1" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="36.195" y="26.162" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="41.529" y="26.162" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D3VPRT" gate="G$1" x="38.1" y="20.32" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="17.78" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="43.18" y="2.54" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND62" gate="1" x="38.1" y="12.7" smashed="yes">
<attribute name="VALUE" x="35.56" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="D22" gate="G$1" x="93.98" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="92.075" y="26.162" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="97.409" y="26.162" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="E3VPRT" gate="G$1" x="93.98" y="20.32" smashed="yes" rot="R90">
<attribute name="NAME" x="91.44" y="17.78" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="99.06" y="17.78" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND63" gate="1" x="93.98" y="12.7" smashed="yes">
<attribute name="VALUE" x="91.44" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY11" gate="G$1" x="48.26" y="-20.32" smashed="yes">
<attribute name="VALUE" x="46.355" y="-17.145" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY12" gate="G$1" x="68.58" y="-45.72" smashed="yes">
<attribute name="VALUE" x="66.675" y="-42.545" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY15" gate="G$1" x="185.42" y="-25.4" smashed="yes">
<attribute name="VALUE" x="183.515" y="-22.225" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY16" gate="G$1" x="185.42" y="-55.88" smashed="yes">
<attribute name="VALUE" x="183.515" y="-52.705" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY26" gate="G$1" x="180.34" y="12.7" smashed="yes">
<attribute name="VALUE" x="178.435" y="15.875" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="81.28" y="223.52" smashed="yes">
<attribute name="VALUE" x="78.74" y="220.98" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY18" gate="+5V" x="81.28" y="243.84" smashed="yes">
<attribute name="VALUE" x="79.375" y="247.015" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY19" gate="G$1" x="144.78" y="248.92" smashed="yes">
<attribute name="VALUE" x="147.955" y="252.095" size="1.778" layer="96"/>
</instance>
<instance part="GND64" gate="1" x="144.78" y="228.6" smashed="yes">
<attribute name="VALUE" x="142.24" y="226.06" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY27" gate="G$1" x="490.22" y="261.62" smashed="yes" rot="R270">
<attribute name="VALUE" x="493.395" y="263.525" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="IC2" gate="1" x="114.3" y="241.3" smashed="yes">
<attribute name="NAME" x="116.84" y="233.68" size="1.778" layer="95"/>
<attribute name="VALUE" x="116.84" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="106.68" y="238.76" smashed="yes">
<attribute name="NAME" x="108.204" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="108.204" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="121.92" y="238.76" smashed="yes">
<attribute name="NAME" x="123.444" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="123.444" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="GND65" gate="1" x="114.3" y="231.14" smashed="yes">
<attribute name="VALUE" x="111.76" y="228.6" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY32" gate="+5V" x="139.7" y="259.08" smashed="yes">
<attribute name="VALUE" x="137.795" y="262.255" size="1.778" layer="96"/>
</instance>
<instance part="GND68" gate="1" x="106.68" y="-73.66" smashed="yes">
<attribute name="VALUE" x="104.14" y="-76.2" size="1.778" layer="96"/>
</instance>
<instance part="D24" gate="G$1" x="104.14" y="-66.04" smashed="yes">
<attribute name="NAME" x="106.68" y="-65.5574" size="1.778" layer="95"/>
<attribute name="VALUE" x="106.68" y="-68.3514" size="1.778" layer="96"/>
</instance>
<instance part="R71" gate="G$1" x="111.76" y="-55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="115.57" y="-57.3786" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="115.57" y="-52.578" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R72" gate="G$1" x="101.6" y="-55.88" smashed="yes">
<attribute name="NAME" x="97.79" y="-54.3814" size="1.778" layer="95"/>
<attribute name="VALUE" x="97.79" y="-59.182" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY33" gate="G$1" x="96.52" y="-48.26" smashed="yes">
<attribute name="VALUE" x="94.615" y="-45.085" size="1.778" layer="96"/>
</instance>
<instance part="R73" gate="G$1" x="129.54" y="-63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="133.35" y="-64.9986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="133.35" y="-60.198" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R70" gate="G$1" x="93.98" y="-60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="95.4786" y="-57.15" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="90.678" y="-57.15" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SPI_HEADER" gate="G$1" x="185.42" y="213.36" smashed="yes">
<attribute name="VALUE" x="180.34" y="203.2" size="1.778" layer="96"/>
<attribute name="NAME" x="180.34" y="224.282" size="1.778" layer="95"/>
</instance>
<instance part="U$5" gate="G$1" x="360.68" y="271.78" smashed="yes">
<attribute name="NAME" x="355.092" y="303.53" size="1.27" layer="95" font="vector" ratio="15"/>
<attribute name="VALUE" x="353.568" y="230.886" size="1.27" layer="96" font="vector" ratio="15"/>
</instance>
<instance part="GND69" gate="1" x="383.54" y="274.32" smashed="yes">
<attribute name="VALUE" x="381" y="271.78" size="1.778" layer="96"/>
</instance>
<instance part="GND70" gate="1" x="383.54" y="287.02" smashed="yes">
<attribute name="VALUE" x="381" y="284.48" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY17" gate="+5V" x="396.24" y="297.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="399.415" y="299.085" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY34" gate="G$1" x="401.32" y="294.64" smashed="yes" rot="R270">
<attribute name="VALUE" x="404.495" y="296.545" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C19" gate="G$1" x="388.62" y="294.64" smashed="yes">
<attribute name="NAME" x="390.144" y="295.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="390.144" y="289.941" size="1.778" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="393.7" y="292.1" smashed="yes">
<attribute name="NAME" x="395.224" y="292.481" size="1.778" layer="95"/>
<attribute name="VALUE" x="395.224" y="287.401" size="1.778" layer="96"/>
</instance>
<instance part="GND66" gate="1" x="388.62" y="287.02" smashed="yes">
<attribute name="VALUE" x="386.08" y="284.48" size="1.778" layer="96"/>
</instance>
<instance part="GND67" gate="1" x="393.7" y="284.48" smashed="yes">
<attribute name="VALUE" x="391.16" y="281.94" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="G$1" x="375.92" y="264.16" smashed="yes">
<attribute name="VALUE" x="373.38" y="259.08" size="1.778" layer="96"/>
<attribute name="NAME" x="373.38" y="267.462" size="1.778" layer="95"/>
</instance>
<instance part="IC1" gate="P" x="96.52" y="233.68" smashed="yes">
<attribute name="NAME" x="95.885" y="233.045" size="1.778" layer="95"/>
</instance>
<instance part="C21" gate="G$1" x="91.44" y="236.22" smashed="yes">
<attribute name="NAME" x="92.964" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.964" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="B_A" gate="G$1" x="419.1" y="27.94" smashed="yes">
<attribute name="NAME" x="416.56" y="30.48" size="1.778" layer="95"/>
</instance>
<instance part="B_B" gate="G$1" x="419.1" y="20.32" smashed="yes">
<attribute name="NAME" x="416.56" y="22.86" size="1.778" layer="95"/>
</instance>
<instance part="B_C" gate="G$1" x="419.1" y="12.7" smashed="yes">
<attribute name="NAME" x="416.56" y="15.24" size="1.778" layer="95"/>
</instance>
<instance part="B_D" gate="G$1" x="419.1" y="5.08" smashed="yes">
<attribute name="NAME" x="416.56" y="7.62" size="1.778" layer="95"/>
</instance>
<instance part="B_E" gate="G$1" x="419.1" y="-2.54" smashed="yes">
<attribute name="NAME" x="416.56" y="0" size="1.778" layer="95"/>
</instance>
<instance part="B_F" gate="G$1" x="419.1" y="-10.16" smashed="yes">
<attribute name="NAME" x="416.56" y="-7.62" size="1.778" layer="95"/>
</instance>
<instance part="L_6" gate="G$1" x="398.78" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="403.352" y="115.316" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="403.352" y="117.475" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R50" gate="G$1" x="408.94" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="412.75" y="110.2614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="412.75" y="115.062" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND71" gate="1" x="414.02" y="109.22" smashed="yes">
<attribute name="VALUE" x="411.48" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="JP4" gate="G$1" x="167.64" y="-43.18" smashed="yes" rot="MR180">
<attribute name="NAME" x="165.1" y="-40.64" size="1.778" layer="95" rot="MR270"/>
</instance>
<instance part="GND72" gate="1" x="177.8" y="-43.18" smashed="yes" rot="R90">
<attribute name="VALUE" x="180.34" y="-45.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="AMP_M" gate="B" x="111.76" y="-63.5" smashed="yes">
<attribute name="NAME" x="119.38" y="-58.42" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="-60.96" size="1.778" layer="96"/>
</instance>
<instance part="AMP_M" gate="A" x="83.82" y="-38.1" smashed="yes">
<attribute name="NAME" x="91.44" y="-33.02" size="1.778" layer="95"/>
<attribute name="VALUE" x="91.44" y="-35.56" size="1.778" layer="96"/>
</instance>
<instance part="AMP_B" gate="B" x="86.36" y="162.56" smashed="yes">
<attribute name="NAME" x="93.98" y="167.64" size="1.778" layer="95"/>
<attribute name="VALUE" x="93.98" y="165.1" size="1.778" layer="96"/>
</instance>
<instance part="AMP_B" gate="A" x="58.42" y="162.56" smashed="yes">
<attribute name="NAME" x="66.04" y="167.64" size="1.778" layer="95"/>
<attribute name="VALUE" x="66.04" y="165.1" size="1.778" layer="96"/>
</instance>
<instance part="C22" gate="G$1" x="55.88" y="-35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="54.1274" y="-34.417" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="59.2074" y="-34.417" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="27.94" y="76.2" smashed="yes">
<attribute name="VALUE" x="26.035" y="79.375" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY40" gate="G$1" x="27.94" y="144.78" smashed="yes">
<attribute name="VALUE" x="26.035" y="147.955" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="20.32" y="17.78" smashed="yes">
<attribute name="VALUE" x="18.415" y="20.955" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY13" gate="G$1" x="78.74" y="17.78" smashed="yes">
<attribute name="VALUE" x="76.835" y="20.955" size="1.778" layer="96"/>
</instance>
<instance part="VOLTAGERANGE" gate="G$1" x="132.08" y="246.38" smashed="yes">
<attribute name="VALUE" x="129.54" y="238.76" size="1.778" layer="96"/>
<attribute name="NAME" x="129.54" y="252.222" size="1.778" layer="95"/>
</instance>
<instance part="C10" gate="G$1" x="106.68" y="-68.58" smashed="yes">
<attribute name="NAME" x="107.823" y="-66.8274" size="1.778" layer="95"/>
<attribute name="VALUE" x="107.823" y="-71.9074" size="1.778" layer="96"/>
</instance>
<instance part="EXP" gate="G$1" x="406.4" y="170.18" smashed="yes">
<attribute name="NAME" x="401.32" y="175.768" size="1.27" layer="95"/>
<attribute name="VALUE" x="401.32" y="163.068" size="1.27" layer="96"/>
</instance>
<instance part="GND73" gate="1" x="396.24" y="170.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="393.7" y="172.72" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="DIG_READ" gate="G$1" x="129.54" y="-25.4" smashed="yes" rot="R270">
<attribute name="VALUE" x="121.92" y="-22.86" size="1.778" layer="96" rot="R270"/>
<attribute name="NAME" x="135.382" y="-22.86" size="1.778" layer="95" rot="R270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="30.48" y1="167.64" x2="30.48" y2="170.18" width="0.1524" layer="91"/>
<pinref part="OFFSET_B" gate="G$1" pin="1"/>
<pinref part="OFFSET_B" gate="G$1" pin="P$1"/>
<wire x1="33.02" y1="170.18" x2="30.48" y2="170.18" width="0.1524" layer="91"/>
<junction x="30.48" y="170.18"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="IN_PIN_B" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IN_B" gate="G$1" pin="SLEEVE"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="OUT_B" gate="G$1" pin="SLEEVE"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="30.48" y1="99.06" x2="30.48" y2="101.6" width="0.1524" layer="91"/>
<pinref part="OFFSET_C" gate="G$1" pin="1"/>
<pinref part="OFFSET_C" gate="G$1" pin="P$1"/>
<wire x1="33.02" y1="101.6" x2="30.48" y2="101.6" width="0.1524" layer="91"/>
<junction x="30.48" y="101.6"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="IN_PIN_C" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="OUT_C" gate="G$1" pin="SLEEVE"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="IN_C" gate="G$1" pin="SLEEVE"/>
</segment>
<segment>
<pinref part="IN_PIN_D" gate="G$1" pin="1"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IN_D" gate="G$1" pin="SLEEVE"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IN_E" gate="G$1" pin="SLEEVE"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IN_PIN_E" gate="G$1" pin="1"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="THR" gate="G$1" pin="1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="THR" gate="G$1" pin="P$1"/>
<wire x1="182.88" y1="-2.54" x2="180.34" y2="-2.54" width="0.1524" layer="91"/>
<junction x="180.34" y="-2.54"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="OUT_D" gate="G$1" pin="SLEEVE"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="OUT_AUDIO" gate="G$1" pin="SLEEVE"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="OK1" gate="A" pin="GND"/>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="231.14" y1="162.56" x2="236.22" y2="162.56" width="0.1524" layer="91"/>
<junction x="231.14" y="162.56"/>
</segment>
<segment>
<pinref part="MIDI_OUT" gate="G$1" pin="SLEEVE"/>
<pinref part="GND27" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC5" gate="A" pin="G"/>
<pinref part="GND25" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC6" gate="A" pin="G"/>
<pinref part="GND28" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="G"/>
<pinref part="GND29" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="J1" gate="G$1" pin="6"/>
<wire x1="0" y1="236.22" x2="-5.08" y2="236.22" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="8"/>
<wire x1="-5.08" y1="233.68" x2="-5.08" y2="236.22" width="0.1524" layer="91"/>
<junction x="-5.08" y="236.22"/>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="-5.08" y1="236.22" x2="-5.08" y2="238.76" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="-5.08" y1="238.76" x2="-20.32" y2="238.76" width="0.1524" layer="91"/>
<junction x="-5.08" y="238.76"/>
<pinref part="J1" gate="G$1" pin="5"/>
<wire x1="-20.32" y1="238.76" x2="-20.32" y2="236.22" width="0.1524" layer="91"/>
<junction x="-20.32" y="238.76"/>
<pinref part="J1" gate="G$1" pin="7"/>
<wire x1="-20.32" y1="236.22" x2="-20.32" y2="233.68" width="0.1524" layer="91"/>
<junction x="-20.32" y="236.22"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="GND"/>
<pinref part="GND31" gate="1" pin="GND"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="30.48" y1="233.68" x2="17.78" y2="233.68" width="0.1524" layer="91"/>
<junction x="30.48" y="233.68"/>
<pinref part="C7" gate="G$1" pin="-"/>
<wire x1="17.78" y1="233.68" x2="12.7" y2="233.68" width="0.1524" layer="91"/>
<wire x1="17.78" y1="236.22" x2="17.78" y2="233.68" width="0.1524" layer="91"/>
<junction x="17.78" y="233.68"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="30.48" y1="233.68" x2="38.1" y2="233.68" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="-"/>
<wire x1="38.1" y1="233.68" x2="43.18" y2="233.68" width="0.1524" layer="91"/>
<wire x1="38.1" y1="236.22" x2="38.1" y2="233.68" width="0.1524" layer="91"/>
<junction x="38.1" y="233.68"/>
</segment>
<segment>
<pinref part="D15" gate="G$1" pin="A"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="RING"/>
<pinref part="GND35" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IN_A" gate="G$1" pin="SLEEVE"/>
<pinref part="GND33" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P$2"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="55.88" y1="-53.34" x2="55.88" y2="-48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R48" gate="G$1" pin="1"/>
<pinref part="GND36" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="OUT_A" gate="G$1" pin="SLEEVE"/>
<pinref part="GND37" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D16" gate="G$1" pin="A"/>
<pinref part="GND38" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D18" gate="G$1" pin="A"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R67" gate="G$1" pin="1"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R66" gate="G$1" pin="1"/>
<pinref part="GND18" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R65" gate="G$1" pin="1"/>
<pinref part="GND40" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R64" gate="G$1" pin="1"/>
<pinref part="GND41" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R63" gate="G$1" pin="1"/>
<pinref part="GND42" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R62" gate="G$1" pin="1"/>
<pinref part="GND43" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<pinref part="GND44" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<pinref part="GND45" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<pinref part="GND46" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R58" gate="G$1" pin="1"/>
<pinref part="GND47" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R57" gate="G$1" pin="1"/>
<pinref part="GND48" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R56" gate="G$1" pin="1"/>
<pinref part="GND49" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R55" gate="G$1" pin="1"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R54" gate="G$1" pin="1"/>
<pinref part="GND51" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="GND52" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<pinref part="GND53" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="I2C_HEADER" gate="G$1" pin="1"/>
<pinref part="GND55" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<pinref part="GND56" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND1"/>
<pinref part="GND57" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND58" gate="1" pin="GND"/>
<wire x1="185.42" y1="-43.18" x2="193.04" y2="-43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="185.42" y1="-73.66" x2="190.5" y2="-73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND60" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="G"/>
<pinref part="GND61" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D3VPRT" gate="G$1" pin="1"/>
<pinref part="GND62" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="E3VPRT" gate="G$1" pin="1"/>
<pinref part="GND63" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC7" gate="P" pin="GND"/>
<wire x1="81.28" y1="226.06" x2="86.36" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="81.28" y1="231.14" x2="81.28" y2="226.06" width="0.1524" layer="91"/>
<junction x="81.28" y="226.06"/>
<pinref part="IC6" gate="P" pin="GND"/>
<wire x1="76.2" y1="226.06" x2="81.28" y2="226.06" width="0.1524" layer="91"/>
<junction x="76.2" y="226.06"/>
<wire x1="71.12" y1="226.06" x2="76.2" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="71.12" y1="231.14" x2="71.12" y2="226.06" width="0.1524" layer="91"/>
<junction x="71.12" y="226.06"/>
<pinref part="IC5" gate="P" pin="GND"/>
<wire x1="66.04" y1="226.06" x2="71.12" y2="226.06" width="0.1524" layer="91"/>
<junction x="66.04" y="226.06"/>
<wire x1="66.04" y1="226.06" x2="60.96" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="60.96" y1="231.14" x2="60.96" y2="226.06" width="0.1524" layer="91"/>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="IC1" gate="P" pin="GND"/>
<wire x1="86.36" y1="226.06" x2="91.44" y2="226.06" width="0.1524" layer="91"/>
<junction x="86.36" y="226.06"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="91.44" y1="226.06" x2="96.52" y2="226.06" width="0.1524" layer="91"/>
<wire x1="91.44" y1="231.14" x2="91.44" y2="226.06" width="0.1524" layer="91"/>
<junction x="91.44" y="226.06"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="144.78" y1="233.68" x2="144.78" y2="231.14" width="0.1524" layer="91"/>
<pinref part="AMP_B" gate="P" pin="V-"/>
<wire x1="144.78" y1="231.14" x2="152.4" y2="231.14" width="0.1524" layer="91"/>
<pinref part="AMP_M" gate="P" pin="V-"/>
<wire x1="152.4" y1="231.14" x2="157.48" y2="231.14" width="0.1524" layer="91"/>
<junction x="152.4" y="231.14"/>
<pinref part="AMP_C" gate="P" pin="V-"/>
<wire x1="157.48" y1="231.14" x2="162.56" y2="231.14" width="0.1524" layer="91"/>
<wire x1="162.56" y1="231.14" x2="170.18" y2="231.14" width="0.1524" layer="91"/>
<junction x="162.56" y="231.14"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="170.18" y1="231.14" x2="175.26" y2="231.14" width="0.1524" layer="91"/>
<wire x1="170.18" y1="233.68" x2="170.18" y2="231.14" width="0.1524" layer="91"/>
<junction x="170.18" y="231.14"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="157.48" y1="233.68" x2="157.48" y2="231.14" width="0.1524" layer="91"/>
<junction x="157.48" y="231.14"/>
<pinref part="GND64" gate="1" pin="GND"/>
<junction x="144.78" y="231.14"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="IC2" gate="1" pin="GND"/>
<wire x1="121.92" y1="233.68" x2="114.3" y2="233.68" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="114.3" y1="233.68" x2="106.68" y2="233.68" width="0.1524" layer="91"/>
<junction x="114.3" y="233.68"/>
<pinref part="GND65" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND54" gate="1" pin="GND"/>
<pinref part="SPI_HEADER" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="AGND"/>
<pinref part="GND69" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="GND"/>
<pinref part="GND70" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND66" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND67" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R50" gate="G$1" pin="1"/>
<pinref part="GND71" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="2"/>
<pinref part="GND72" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="-"/>
<pinref part="GND68" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="3"/>
<pinref part="GND73" gate="1" pin="GND"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="OK1" gate="A" pin="VCC"/>
<pinref part="SUPPLY20" gate="+5V" pin="+5V"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="236.22" y1="170.18" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
<junction x="231.14" y="170.18"/>
</segment>
<segment>
<pinref part="R39" gate="G$1" pin="1"/>
<pinref part="SUPPLY21" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="SUPPLY22" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="IC6" gate="A" pin="SCL"/>
<pinref part="SUPPLY23" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="SCL"/>
<pinref part="SUPPLY24" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="IC5" gate="A" pin="SCL"/>
<pinref part="SUPPLY25" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="I2C_HEADER" gate="G$1" pin="2"/>
<pinref part="SUPPLY29" gate="+5V" pin="+5V"/>
<wire x1="195.58" y1="238.76" x2="190.5" y2="238.76" width="0.1524" layer="91"/>
<label x="195.58" y="238.76" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="5V"/>
<pinref part="SUPPLY30" gate="+5V" pin="+5V"/>
<wire x1="487.68" y1="284.48" x2="472.44" y2="284.48" width="0.1524" layer="91"/>
<label x="485.14" y="284.48" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY31" gate="+5V" pin="+5V"/>
<wire x1="500.38" y1="271.78" x2="497.84" y2="271.78" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="497.84" y1="271.78" x2="472.44" y2="292.1" width="0.1524" layer="91"/>
<junction x="497.84" y="271.78"/>
<label x="497.84" y="271.78" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="SCL"/>
<pinref part="SUPPLY35" gate="+5V" pin="+5V"/>
<wire x1="342.9" y1="175.26" x2="345.44" y2="175.26" width="0.1524" layer="91"/>
<label x="345.44" y="175.26" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="IC4" gate="A" pin="OUT"/>
<pinref part="C8" gate="G$1" pin="+"/>
<wire x1="38.1" y1="241.3" x2="43.18" y2="241.3" width="0.1524" layer="91"/>
<junction x="38.1" y="241.3"/>
<pinref part="IC5" gate="P" pin="VCC"/>
<pinref part="IC6" gate="P" pin="VCC"/>
<wire x1="60.96" y1="241.3" x2="66.04" y2="241.3" width="0.1524" layer="91"/>
<wire x1="66.04" y1="241.3" x2="71.12" y2="241.3" width="0.1524" layer="91"/>
<junction x="66.04" y="241.3"/>
<pinref part="IC7" gate="P" pin="VCC"/>
<wire x1="71.12" y1="241.3" x2="76.2" y2="241.3" width="0.1524" layer="91"/>
<wire x1="76.2" y1="241.3" x2="81.28" y2="241.3" width="0.1524" layer="91"/>
<junction x="76.2" y="241.3"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="81.28" y1="241.3" x2="86.36" y2="241.3" width="0.1524" layer="91"/>
<wire x1="81.28" y1="238.76" x2="81.28" y2="241.3" width="0.1524" layer="91"/>
<junction x="81.28" y="241.3"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="71.12" y1="238.76" x2="71.12" y2="241.3" width="0.1524" layer="91"/>
<junction x="71.12" y="241.3"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="60.96" y1="238.76" x2="60.96" y2="241.3" width="0.1524" layer="91"/>
<wire x1="43.18" y1="241.3" x2="60.96" y2="241.3" width="0.1524" layer="91"/>
<junction x="43.18" y="241.3"/>
<junction x="60.96" y="241.3"/>
<pinref part="SUPPLY18" gate="+5V" pin="+5V"/>
<pinref part="IC2" gate="1" pin="IN"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="86.36" y1="241.3" x2="91.44" y2="241.3" width="0.1524" layer="91"/>
<junction x="86.36" y="241.3"/>
<junction x="106.68" y="241.3"/>
<pinref part="IC1" gate="P" pin="VCC"/>
<wire x1="91.44" y1="241.3" x2="96.52" y2="241.3" width="0.1524" layer="91"/>
<wire x1="96.52" y1="241.3" x2="106.68" y2="241.3" width="0.1524" layer="91"/>
<junction x="96.52" y="241.3"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="91.44" y1="238.76" x2="91.44" y2="241.3" width="0.1524" layer="91"/>
<junction x="91.44" y="241.3"/>
<label x="101.6" y="241.3" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="SUPPLY28" gate="+5V" pin="+5V"/>
<pinref part="SPI_HEADER" gate="G$1" pin="2"/>
<wire x1="195.58" y1="210.82" x2="190.5" y2="210.82" width="0.1524" layer="91"/>
<label x="195.58" y="210.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY17" gate="+5V" pin="+5V"/>
<wire x1="393.7" y1="297.18" x2="388.62" y2="297.18" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="388.62" y1="297.18" x2="383.54" y2="297.18" width="0.1524" layer="91"/>
<junction x="388.62" y="297.18"/>
<label x="391.16" y="297.18" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="VOLTAGERANGE" gate="G$1" pin="3"/>
<pinref part="SUPPLY32" gate="+5V" pin="+5V"/>
<wire x1="139.7" y1="256.54" x2="139.7" y2="248.92" width="0.1524" layer="91"/>
<label x="139.7" y="256.54" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="81.28" y1="160.02" x2="76.2" y2="160.02" width="0.1524" layer="91"/>
<wire x1="76.2" y1="160.02" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<junction x="76.2" y="149.86"/>
<pinref part="AMP_B" gate="B" pin="+IN"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="81.28" y1="172.72" x2="81.28" y2="165.1" width="0.1524" layer="91"/>
<pinref part="AMP_B" gate="B" pin="-IN"/>
<wire x1="81.28" y1="165.1" x2="81.28" y2="162.56" width="0.1524" layer="91"/>
<junction x="81.28" y="165.1"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="91.44" y1="172.72" x2="96.52" y2="172.72" width="0.1524" layer="91"/>
<wire x1="96.52" y1="172.72" x2="96.52" y2="162.56" width="0.1524" layer="91"/>
<wire x1="106.68" y1="167.64" x2="106.68" y2="162.56" width="0.1524" layer="91"/>
<wire x1="106.68" y1="162.56" x2="96.52" y2="162.56" width="0.1524" layer="91"/>
<pinref part="INVERT_A" gate="G$1" pin="U"/>
<pinref part="AMP_B" gate="B" pin="OUT"/>
<junction x="96.52" y="162.56"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="53.34" y1="165.1" x2="48.26" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="48.26" y1="175.26" x2="48.26" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<junction x="48.26" y="175.26"/>
<pinref part="AMP_B" gate="A" pin="-IN"/>
</segment>
</net>
<net name="V_C" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="124.46" y1="101.6" x2="116.84" y2="101.6" width="0.1524" layer="91"/>
<wire x1="124.46" y1="101.6" x2="124.46" y2="99.06" width="0.1524" layer="91"/>
<junction x="124.46" y="101.6"/>
<label x="124.46" y="99.06" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="INVERT_B" gate="G$1" pin="C"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A2"/>
<wire x1="472.44" y1="269.24" x2="474.98" y2="269.24" width="0.1524" layer="91"/>
<label x="474.98" y="269.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="21/A7/PWM"/>
<wire x1="337.82" y1="243.84" x2="335.28" y2="243.84" width="0.1524" layer="91"/>
<label x="335.28" y="243.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="22.86" y1="142.24" x2="22.86" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="22.86" y1="160.02" x2="40.64" y2="160.02" width="0.1524" layer="91"/>
<wire x1="12.7" y1="160.02" x2="17.78" y2="160.02" width="0.1524" layer="91"/>
<junction x="22.86" y="160.02"/>
<pinref part="PULLUP_A" gate="G$1" pin="C"/>
<wire x1="17.78" y1="160.02" x2="22.86" y2="160.02" width="0.1524" layer="91"/>
<junction x="17.78" y="160.02"/>
<pinref part="IN_B" gate="G$1" pin="TIP"/>
<pinref part="IN_PIN_B" gate="G$1" pin="3"/>
<wire x1="17.78" y1="142.24" x2="22.86" y2="142.24" width="0.1524" layer="91"/>
<label x="40.64" y="160.02" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="PULLUP_A" gate="G$1" pin="U"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="PULLUP_A" gate="G$1" pin="D"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="OFFSET_B" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="OFFSET_B" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="GAIN_B" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="OUT_B" gate="G$1" pin="LEFT"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="AMP_C" gate="A" pin="OUT"/>
<junction x="68.58" y="93.98"/>
<wire x1="68.58" y1="111.76" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<wire x1="68.58" y1="106.68" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<wire x1="106.68" y1="104.14" x2="106.68" y2="111.76" width="0.1524" layer="91"/>
<wire x1="106.68" y1="111.76" x2="68.58" y2="111.76" width="0.1524" layer="91"/>
<wire x1="68.58" y1="111.76" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<junction x="68.58" y="111.76"/>
<pinref part="GAIN_C" gate="G$1" pin="3"/>
<junction x="68.58" y="106.68"/>
<pinref part="GAIN_C" gate="G$1" pin="2"/>
<wire x1="63.5" y1="114.3" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<pinref part="INVERT_B" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="81.28" y1="91.44" x2="76.2" y2="91.44" width="0.1524" layer="91"/>
<wire x1="76.2" y1="91.44" x2="76.2" y2="81.28" width="0.1524" layer="91"/>
<junction x="76.2" y="81.28"/>
<pinref part="AMP_C" gate="B" pin="+IN"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="78.74" y1="93.98" x2="81.28" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="81.28" y1="104.14" x2="81.28" y2="96.52" width="0.1524" layer="91"/>
<pinref part="AMP_C" gate="B" pin="-IN"/>
<wire x1="81.28" y1="96.52" x2="81.28" y2="93.98" width="0.1524" layer="91"/>
<junction x="81.28" y="96.52"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="91.44" y1="104.14" x2="96.52" y2="104.14" width="0.1524" layer="91"/>
<wire x1="96.52" y1="104.14" x2="96.52" y2="93.98" width="0.1524" layer="91"/>
<pinref part="AMP_C" gate="B" pin="OUT"/>
<junction x="96.52" y="93.98"/>
<wire x1="106.68" y1="99.06" x2="106.68" y2="93.98" width="0.1524" layer="91"/>
<wire x1="106.68" y1="93.98" x2="96.52" y2="93.98" width="0.1524" layer="91"/>
<pinref part="INVERT_B" gate="G$1" pin="U"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="AMP_C" gate="A" pin="+IN"/>
<pinref part="R22" gate="G$1" pin="2"/>
<junction x="53.34" y="91.44"/>
<wire x1="35.56" y1="91.44" x2="53.34" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="AMP_C" gate="A" pin="-IN"/>
<wire x1="53.34" y1="96.52" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="48.26" y1="106.68" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<junction x="48.26" y="106.68"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="22.86" y1="73.66" x2="22.86" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="22.86" y1="91.44" x2="25.4" y2="91.44" width="0.1524" layer="91"/>
<junction x="22.86" y="91.44"/>
<wire x1="12.7" y1="91.44" x2="17.78" y2="91.44" width="0.1524" layer="91"/>
<pinref part="IN_C" gate="G$1" pin="TIP"/>
<pinref part="PULLUP_B" gate="G$1" pin="C"/>
<wire x1="17.78" y1="91.44" x2="22.86" y2="91.44" width="0.1524" layer="91"/>
<junction x="17.78" y="91.44"/>
<pinref part="IN_PIN_C" gate="G$1" pin="3"/>
<wire x1="17.78" y1="73.66" x2="22.86" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="OUT_C" gate="G$1" pin="LEFT"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="OFFSET_C" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<pinref part="OFFSET_C" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="GAIN_C" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="PULLUP_B" gate="G$1" pin="U"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="PULLUP_B" gate="G$1" pin="D"/>
</segment>
</net>
<net name="V_A" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A0"/>
<wire x1="472.44" y1="264.16" x2="474.98" y2="264.16" width="0.1524" layer="91"/>
<label x="474.98" y="264.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="23/A9/T/PWM"/>
<wire x1="337.82" y1="238.76" x2="335.28" y2="233.68" width="0.1524" layer="91"/>
<label x="335.28" y="233.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="129.54" y1="-33.02" x2="129.54" y2="-17.78" width="0.1524" layer="91"/>
<label x="129.54" y="-17.78" size="1.778" layer="95" xref="yes"/>
<pinref part="DIG_READ" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GATE_A" class="0">
<segment>
<pinref part="OUT_B" gate="G$1" pin="RIGHT"/>
<wire x1="134.62" y1="167.64" x2="129.54" y2="167.64" width="0.1524" layer="91"/>
<wire x1="129.54" y1="167.64" x2="129.54" y2="162.56" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GATE_B" class="0">
<segment>
<pinref part="OUT_C" gate="G$1" pin="RIGHT"/>
<wire x1="134.62" y1="99.06" x2="129.54" y2="99.06" width="0.1524" layer="91"/>
<wire x1="129.54" y1="99.06" x2="129.54" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="1"/>
</segment>
</net>
<net name="P_V_D" class="0">
<segment>
<pinref part="IN_D" gate="G$1" pin="TIP"/>
<pinref part="PULLUP_C" gate="G$1" pin="C"/>
<wire x1="5.08" y1="30.48" x2="10.16" y2="30.48" width="0.1524" layer="91"/>
<wire x1="10.16" y1="30.48" x2="15.24" y2="30.48" width="0.1524" layer="91"/>
<junction x="10.16" y="30.48"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="30.48" width="0.1524" layer="91"/>
<junction x="15.24" y="30.48"/>
<wire x1="15.24" y1="30.48" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
<pinref part="IN_PIN_D" gate="G$1" pin="3"/>
<wire x1="10.16" y1="15.24" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="PULLUP_C" gate="G$1" pin="U"/>
<pinref part="R27" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="PULLUP_C" gate="G$1" pin="D"/>
<pinref part="R28" gate="G$1" pin="1"/>
</segment>
</net>
<net name="P_V_E" class="0">
<segment>
<pinref part="IN_E" gate="G$1" pin="TIP"/>
<pinref part="PULLUP_D" gate="G$1" pin="C"/>
<wire x1="63.5" y1="30.48" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="68.58" y1="30.48" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<junction x="68.58" y="30.48"/>
<wire x1="73.66" y1="15.24" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<junction x="73.66" y="30.48"/>
<wire x1="73.66" y1="30.48" x2="76.2" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="2"/>
<pinref part="IN_PIN_E" gate="G$1" pin="3"/>
<wire x1="68.58" y1="15.24" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<label x="76.2" y="35.56" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="PULLUP_D" gate="G$1" pin="U"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<pinref part="PULLUP_D" gate="G$1" pin="D"/>
</segment>
</net>
<net name="V_THR" class="0">
<segment>
<pinref part="THR" gate="G$1" pin="2"/>
<wire x1="187.96" y1="2.54" x2="190.5" y2="2.54" width="0.1524" layer="91"/>
<label x="190.5" y="2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A5"/>
<wire x1="472.44" y1="276.86" x2="474.98" y2="276.86" width="0.1524" layer="91"/>
<label x="474.98" y="276.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="14/A1"/>
<wire x1="337.82" y1="261.62" x2="335.28" y2="261.62" width="0.1524" layer="91"/>
<label x="335.28" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="R36" gate="G$1" pin="2"/>
<junction x="180.34" y="30.48"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="180.34" y1="35.56" x2="180.34" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="OUT_D" gate="G$1" pin="LEFT"/>
<wire x1="190.5" y1="73.66" x2="200.66" y2="73.66" width="0.1524" layer="91"/>
<junction x="190.5" y="73.66"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="OUT_AUDIO" gate="G$1" pin="LEFT"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="195.58" y1="35.56" x2="190.5" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="OUT_AUDIO" gate="G$1" pin="RIGHT"/>
<wire x1="195.58" y1="33.02" x2="190.5" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="190.5" y1="33.02" x2="190.5" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GATE_C" class="0">
<segment>
<pinref part="OUT_D" gate="G$1" pin="RIGHT"/>
<wire x1="200.66" y1="71.12" x2="195.58" y2="71.12" width="0.1524" layer="91"/>
<wire x1="195.58" y1="71.12" x2="195.58" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CV_PWM" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="180.34" y1="73.66" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
<label x="175.26" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="IO10*"/>
<wire x1="436.88" y1="261.62" x2="434.34" y2="261.62" width="0.1524" layer="91"/>
<label x="434.34" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="3/CAN-TX/PWM"/>
<wire x1="337.82" y1="289.56" x2="335.28" y2="289.56" width="0.1524" layer="91"/>
<label x="335.28" y="289.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AUDIO_PWM" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="170.18" y1="30.48" x2="165.1" y2="30.48" width="0.1524" layer="91"/>
<label x="165.1" y="30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="IO9*"/>
<wire x1="436.88" y1="264.16" x2="434.34" y2="264.16" width="0.1524" layer="91"/>
<label x="434.34" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="383.54" y1="264.16" x2="386.08" y2="264.16" width="0.1524" layer="91"/>
<label x="386.08" y="264.16" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="OK1" gate="A" pin="A"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="203.2" y1="170.18" x2="208.28" y2="170.18" width="0.1524" layer="91"/>
<pinref part="MIDI_IN" gate="G$1" pin="RIGHT"/>
<wire x1="185.42" y1="170.18" x2="203.2" y2="170.18" width="0.1524" layer="91"/>
<junction x="203.2" y="170.18"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="OK1" gate="A" pin="C"/>
<wire x1="208.28" y1="162.56" x2="203.2" y2="162.56" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="203.2" y1="162.56" x2="203.2" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="200.66" y1="162.56" x2="203.2" y2="162.56" width="0.1524" layer="91"/>
<junction x="203.2" y="162.56"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="OK1" gate="A" pin="VO"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="231.14" y1="165.1" x2="241.3" y2="165.1" width="0.1524" layer="91"/>
<wire x1="241.3" y1="165.1" x2="246.38" y2="165.1" width="0.1524" layer="91"/>
<junction x="241.3" y="165.1"/>
<label x="246.38" y="165.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D0/RX"/>
<wire x1="436.88" y1="289.56" x2="434.34" y2="289.56" width="0.1524" layer="91"/>
<label x="434.34" y="289.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="0/RX1/T"/>
<wire x1="337.82" y1="297.18" x2="335.28" y2="297.18" width="0.1524" layer="91"/>
<label x="335.28" y="297.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="MIDI_IN" gate="G$1" pin="LEFT"/>
<wire x1="185.42" y1="172.72" x2="190.5" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="190.5" y1="172.72" x2="190.5" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="MIDI_OUT" gate="G$1" pin="RIGHT"/>
<pinref part="R40" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="MIDI_OUT" gate="G$1" pin="LEFT"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="274.32" y1="177.8" x2="274.32" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="264.16" y1="177.8" x2="261.62" y2="177.8" width="0.1524" layer="91"/>
<label x="261.62" y="177.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D1/TX"/>
<wire x1="436.88" y1="292.1" x2="434.34" y2="292.1" width="0.1524" layer="91"/>
<label x="434.34" y="292.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="1/TX1/T"/>
<wire x1="337.82" y1="294.64" x2="335.28" y2="294.64" width="0.1524" layer="91"/>
<label x="335.28" y="294.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="426.72" y1="27.94" x2="424.18" y2="27.94" width="0.1524" layer="91"/>
<pinref part="B_A" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="426.72" y1="20.32" x2="424.18" y2="20.32" width="0.1524" layer="91"/>
<pinref part="B_B" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="D14" gate="G$1" pin="C"/>
<pinref part="B_TOUCH" gate="G$2" pin="2"/>
<wire x1="426.72" y1="-66.04" x2="424.18" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="B_MUTE" gate="G$2" pin="2"/>
<pinref part="D13" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-58.42" x2="426.72" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="B_ASSIGN" gate="G$2" pin="2"/>
<pinref part="D12" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-50.8" x2="426.72" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="B_SYNTH" gate="G$2" pin="2"/>
<pinref part="D11" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-43.18" x2="426.72" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="B_DATA" gate="G$2" pin="2"/>
<pinref part="D10" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-35.56" x2="426.72" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="424.18" y1="12.7" x2="426.72" y2="12.7" width="0.1524" layer="91"/>
<pinref part="B_C" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="424.18" y1="5.08" x2="426.72" y2="5.08" width="0.1524" layer="91"/>
<pinref part="B_D" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-2.54" x2="426.72" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="B_E" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-10.16" x2="426.72" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="B_F" gate="G$1" pin="2"/>
</segment>
</net>
<net name="BUTTON_PIN_1" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="A"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-10.16" x2="431.8" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-2.54" x2="431.8" y2="5.08" width="0.1524" layer="91"/>
<junction x="431.8" y="-2.54"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="431.8" y1="5.08" x2="431.8" y2="12.7" width="0.1524" layer="91"/>
<junction x="431.8" y="5.08"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="431.8" y1="12.7" x2="431.8" y2="20.32" width="0.1524" layer="91"/>
<junction x="431.8" y="12.7"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="431.8" y1="20.32" x2="431.8" y2="27.94" width="0.1524" layer="91"/>
<junction x="431.8" y="20.32"/>
<junction x="431.8" y="27.94"/>
<label x="434.34" y="27.94" size="1.778" layer="95" xref="yes"/>
<wire x1="431.8" y1="27.94" x2="434.34" y2="27.94" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-20.32" x2="431.8" y2="-10.16" width="0.1524" layer="91"/>
<junction x="431.8" y="-10.16"/>
<pinref part="D9" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-27.94" x2="431.8" y2="-20.32" width="0.1524" layer="91"/>
<junction x="431.8" y="-20.32"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D7"/>
<wire x1="436.88" y1="269.24" x2="434.34" y2="269.24" width="0.1524" layer="91"/>
<label x="434.34" y="269.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="5/PWM"/>
<wire x1="337.82" y1="284.48" x2="335.28" y2="284.48" width="0.1524" layer="91"/>
<label x="335.28" y="284.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="B_CAL" gate="G$2" pin="2"/>
<pinref part="D8" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-20.32" x2="426.72" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="B_INV" gate="G$2" pin="2"/>
<pinref part="D9" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-27.94" x2="426.72" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QH*"/>
<wire x1="373.38" y1="50.8" x2="373.38" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="SER"/>
<wire x1="373.38" y1="40.64" x2="347.98" y2="40.64" width="0.1524" layer="91"/>
<wire x1="347.98" y1="40.64" x2="347.98" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QH*"/>
<wire x1="373.38" y1="99.06" x2="373.38" y2="86.36" width="0.1524" layer="91"/>
<wire x1="373.38" y1="86.36" x2="347.98" y2="86.36" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="SER"/>
<wire x1="347.98" y1="86.36" x2="347.98" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LATCH" class="0">
<segment>
<pinref part="IC7" gate="A" pin="RCK"/>
<wire x1="347.98" y1="15.24" x2="335.28" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC5" gate="A" pin="RCK"/>
<wire x1="335.28" y1="15.24" x2="335.28" y2="60.96" width="0.1524" layer="91"/>
<wire x1="335.28" y1="60.96" x2="335.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="335.28" y1="109.22" x2="347.98" y2="109.22" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="RCK"/>
<wire x1="347.98" y1="60.96" x2="335.28" y2="60.96" width="0.1524" layer="91"/>
<junction x="335.28" y="60.96"/>
<label x="335.28" y="109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="IC1" gate="A" pin="RCK"/>
<wire x1="345.44" y1="170.18" x2="335.28" y2="170.18" width="0.1524" layer="91"/>
<wire x1="335.28" y1="170.18" x2="335.28" y2="109.22" width="0.1524" layer="91"/>
<junction x="335.28" y="109.22"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D6*"/>
<wire x1="436.88" y1="271.78" x2="434.34" y2="271.78" width="0.1524" layer="91"/>
<label x="434.34" y="271.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="6/PWM"/>
<wire x1="337.82" y1="281.94" x2="335.28" y2="281.94" width="0.1524" layer="91"/>
<label x="335.28" y="281.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DATA" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D4*"/>
<wire x1="436.88" y1="276.86" x2="434.34" y2="276.86" width="0.1524" layer="91"/>
<label x="434.34" y="276.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="8/TX3"/>
<wire x1="337.82" y1="276.86" x2="335.28" y2="276.86" width="0.1524" layer="91"/>
<label x="335.28" y="276.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="SER"/>
<wire x1="345.44" y1="182.88" x2="335.28" y2="182.88" width="0.1524" layer="91"/>
<label x="335.28" y="182.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="IC5" gate="A" pin="SCK"/>
<wire x1="347.98" y1="116.84" x2="337.82" y2="116.84" width="0.1524" layer="91"/>
<wire x1="337.82" y1="116.84" x2="337.82" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="SCK"/>
<wire x1="337.82" y1="68.58" x2="337.82" y2="22.86" width="0.1524" layer="91"/>
<wire x1="337.82" y1="22.86" x2="347.98" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="SCK"/>
<wire x1="347.98" y1="68.58" x2="337.82" y2="68.58" width="0.1524" layer="91"/>
<junction x="337.82" y="68.58"/>
<wire x1="337.82" y1="116.84" x2="332.74" y2="116.84" width="0.1524" layer="91"/>
<junction x="337.82" y="116.84"/>
<label x="332.74" y="116.84" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="IC1" gate="A" pin="SCK"/>
<wire x1="345.44" y1="177.8" x2="337.82" y2="177.8" width="0.1524" layer="91"/>
<wire x1="337.82" y1="116.84" x2="337.82" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D5*"/>
<wire x1="436.88" y1="274.32" x2="434.34" y2="274.32" width="0.1524" layer="91"/>
<label x="434.34" y="274.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="7/RX3"/>
<wire x1="337.82" y1="279.4" x2="335.28" y2="279.4" width="0.1524" layer="91"/>
<label x="335.28" y="279.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<pinref part="PTC" gate="G$1" pin="1"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="241.3" x2="-5.08" y2="241.3" width="0.1524" layer="91"/>
<junction x="-5.08" y="241.3"/>
<pinref part="J2" gate="G$1" pin="PIN"/>
<wire x1="-15.24" y1="261.62" x2="-5.08" y2="261.62" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="261.62" x2="-5.08" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="IC4" gate="A" pin="IN"/>
<pinref part="C7" gate="G$1" pin="+"/>
<wire x1="22.86" y1="241.3" x2="17.78" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="17.78" y1="241.3" x2="12.7" y2="241.3" width="0.1524" layer="91"/>
<junction x="17.78" y="241.3"/>
<pinref part="PTC" gate="G$1" pin="2"/>
<pinref part="D15" gate="G$1" pin="C"/>
<wire x1="12.7" y1="241.3" x2="7.62" y2="241.3" width="0.1524" layer="91"/>
<junction x="12.7" y="241.3"/>
<junction x="7.62" y="241.3"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="9"/>
<pinref part="J1" gate="G$1" pin="10"/>
<wire x1="-20.32" y1="231.14" x2="-5.08" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GT_B" class="0">
<segment>
<wire x1="129.54" y1="152.4" x2="129.54" y2="147.32" width="0.1524" layer="91"/>
<label x="129.54" y="147.32" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R42" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="QB"/>
<wire x1="370.84" y1="180.34" x2="373.38" y2="180.34" width="0.1524" layer="91"/>
<label x="373.38" y="180.34" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GT_C" class="0">
<segment>
<wire x1="129.54" y1="83.82" x2="129.54" y2="78.74" width="0.1524" layer="91"/>
<label x="129.54" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R43" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="QC"/>
<wire x1="370.84" y1="177.8" x2="373.38" y2="177.8" width="0.1524" layer="91"/>
<label x="373.38" y="177.8" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GT_D" class="0">
<segment>
<wire x1="195.58" y1="50.8" x2="195.58" y2="48.26" width="0.1524" layer="91"/>
<label x="195.58" y="48.26" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R44" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="QD"/>
<wire x1="370.84" y1="175.26" x2="373.38" y2="175.26" width="0.1524" layer="91"/>
<label x="373.38" y="175.26" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="L_0" gate="G$1" pin="C"/>
<pinref part="R45" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="IC7" gate="A" pin="QA"/>
<wire x1="373.38" y1="27.94" x2="396.24" y2="27.94" width="0.1524" layer="91"/>
<pinref part="B_DATA" gate="G$2" pin="1"/>
<wire x1="396.24" y1="27.94" x2="414.02" y2="27.94" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-35.56" x2="396.24" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="396.24" y1="-35.56" x2="396.24" y2="27.94" width="0.1524" layer="91"/>
<junction x="396.24" y="27.94"/>
<pinref part="B_A" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="IC7" gate="A" pin="QC"/>
<wire x1="373.38" y1="22.86" x2="391.16" y2="22.86" width="0.1524" layer="91"/>
<wire x1="391.16" y1="22.86" x2="411.48" y2="22.86" width="0.1524" layer="91"/>
<wire x1="411.48" y1="22.86" x2="411.48" y2="12.7" width="0.1524" layer="91"/>
<wire x1="411.48" y1="12.7" x2="414.02" y2="12.7" width="0.1524" layer="91"/>
<pinref part="B_ASSIGN" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-50.8" x2="391.16" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="391.16" y1="-50.8" x2="391.16" y2="22.86" width="0.1524" layer="91"/>
<junction x="391.16" y="22.86"/>
<pinref part="B_C" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="IC7" gate="A" pin="QD"/>
<wire x1="373.38" y1="20.32" x2="388.62" y2="20.32" width="0.1524" layer="91"/>
<wire x1="388.62" y1="20.32" x2="408.94" y2="20.32" width="0.1524" layer="91"/>
<wire x1="408.94" y1="20.32" x2="408.94" y2="5.08" width="0.1524" layer="91"/>
<wire x1="408.94" y1="5.08" x2="414.02" y2="5.08" width="0.1524" layer="91"/>
<pinref part="B_MUTE" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-58.42" x2="388.62" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-58.42" x2="388.62" y2="20.32" width="0.1524" layer="91"/>
<junction x="388.62" y="20.32"/>
<pinref part="B_D" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="IC7" gate="A" pin="QE"/>
<wire x1="373.38" y1="17.78" x2="386.08" y2="17.78" width="0.1524" layer="91"/>
<wire x1="386.08" y1="17.78" x2="406.4" y2="17.78" width="0.1524" layer="91"/>
<wire x1="406.4" y1="17.78" x2="406.4" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-2.54" x2="414.02" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="B_TOUCH" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-66.04" x2="386.08" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="386.08" y1="-66.04" x2="386.08" y2="17.78" width="0.1524" layer="91"/>
<junction x="386.08" y="17.78"/>
<pinref part="B_E" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="IN_A" gate="G$1" pin="TIP"/>
<wire x1="43.18" y1="-35.56" x2="53.34" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="M1" gate="G$1" pin="P$1"/>
<wire x1="55.88" y1="-38.1" x2="48.26" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="IN_A" gate="G$1" pin="SWITCH"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="48.26" y1="-38.1" x2="43.18" y2="-38.1" width="0.1524" layer="91"/>
<junction x="48.26" y="-38.1"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<wire x1="73.66" y1="-35.56" x2="78.74" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-25.4" x2="78.74" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="GAIN_MIC" gate="G$1" pin="1"/>
<wire x1="91.44" y1="-25.4" x2="78.74" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="AMP_M" gate="A" pin="-IN"/>
<junction x="78.74" y="-35.56"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="78.74" y1="-40.64" x2="78.74" y2="-50.8" width="0.1524" layer="91"/>
<junction x="78.74" y="-50.8"/>
<pinref part="AMP_M" gate="A" pin="+IN"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="58.42" y1="-35.56" x2="63.5" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="-"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="1"/>
<pinref part="OUT_A" gate="G$1" pin="LEFT"/>
<wire x1="137.16" y1="-38.1" x2="111.76" y2="-38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_D" class="0">
<segment>
<wire x1="27.94" y1="30.48" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<label x="40.64" y="30.48" size="1.778" layer="95" xref="yes"/>
<pinref part="R52" gate="G$1" pin="1"/>
<pinref part="D16" gate="G$1" pin="C"/>
<wire x1="30.48" y1="30.48" x2="38.1" y2="30.48" width="0.1524" layer="91"/>
<junction x="30.48" y="30.48"/>
<pinref part="D21" gate="G$1" pin="C"/>
<wire x1="38.1" y1="30.48" x2="40.64" y2="30.48" width="0.1524" layer="91"/>
<junction x="38.1" y="30.48"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A3"/>
<wire x1="472.44" y1="271.78" x2="474.98" y2="271.78" width="0.1524" layer="91"/>
<label x="474.98" y="271.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="20/A6/PWM"/>
<wire x1="337.82" y1="246.38" x2="335.28" y2="246.38" width="0.1524" layer="91"/>
<label x="335.28" y="246.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="V_E" class="0">
<segment>
<wire x1="86.36" y1="30.48" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<label x="99.06" y="30.48" size="1.778" layer="95" xref="yes"/>
<pinref part="R53" gate="G$1" pin="1"/>
<pinref part="D18" gate="G$1" pin="C"/>
<junction x="86.36" y="30.48"/>
<pinref part="D22" gate="G$1" pin="C"/>
<wire x1="93.98" y1="30.48" x2="99.06" y2="30.48" width="0.1524" layer="91"/>
<junction x="93.98" y="30.48"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A4"/>
<wire x1="472.44" y1="274.32" x2="474.98" y2="274.32" width="0.1524" layer="91"/>
<label x="474.98" y="274.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="17/A3/T"/>
<wire x1="337.82" y1="254" x2="335.28" y2="254" width="0.1524" layer="91"/>
<label x="335.28" y="254" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="IN_PIN_E" gate="G$1" pin="2"/>
<pinref part="R30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="IN_PIN_D" gate="G$1" pin="2"/>
<pinref part="R29" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IN_PIN_C" gate="G$1" pin="2"/>
<pinref part="R24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IN_PIN_B" gate="G$1" pin="2"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="L_1" gate="G$1" pin="C"/>
<pinref part="R33" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="L_2" gate="G$1" pin="C"/>
<pinref part="R54" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="L_3" gate="G$1" pin="C"/>
<pinref part="R55" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="L_4" gate="G$1" pin="C"/>
<pinref part="R56" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="L_5" gate="G$1" pin="C"/>
<pinref part="R57" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="L_A" gate="G$1" pin="C"/>
<pinref part="R58" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="L_B" gate="G$1" pin="C"/>
<pinref part="R59" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="L_C" gate="G$1" pin="C"/>
<pinref part="R60" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="L_D" gate="G$1" pin="C"/>
<pinref part="R61" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="L_E" gate="G$1" pin="C"/>
<pinref part="R62" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="L_F" gate="G$1" pin="C"/>
<pinref part="R63" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="L_G" gate="G$1" pin="C"/>
<pinref part="R64" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="L_H" gate="G$1" pin="C"/>
<pinref part="R65" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="L_GATE" gate="G$1" pin="C"/>
<pinref part="R66" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="L_INV" gate="G$1" pin="C"/>
<pinref part="R67" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QB"/>
<wire x1="373.38" y1="119.38" x2="383.54" y2="119.38" width="0.1524" layer="91"/>
<wire x1="383.54" y1="119.38" x2="383.54" y2="149.86" width="0.1524" layer="91"/>
<pinref part="L_1" gate="G$1" pin="A"/>
<wire x1="383.54" y1="149.86" x2="396.24" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QC"/>
<wire x1="373.38" y1="116.84" x2="386.08" y2="116.84" width="0.1524" layer="91"/>
<wire x1="386.08" y1="116.84" x2="386.08" y2="142.24" width="0.1524" layer="91"/>
<pinref part="L_2" gate="G$1" pin="A"/>
<wire x1="386.08" y1="142.24" x2="396.24" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QD"/>
<wire x1="373.38" y1="114.3" x2="388.62" y2="114.3" width="0.1524" layer="91"/>
<wire x1="388.62" y1="114.3" x2="388.62" y2="134.62" width="0.1524" layer="91"/>
<pinref part="L_3" gate="G$1" pin="A"/>
<wire x1="388.62" y1="134.62" x2="396.24" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QE"/>
<wire x1="373.38" y1="111.76" x2="391.16" y2="111.76" width="0.1524" layer="91"/>
<wire x1="391.16" y1="111.76" x2="391.16" y2="127" width="0.1524" layer="91"/>
<pinref part="L_4" gate="G$1" pin="A"/>
<wire x1="391.16" y1="127" x2="396.24" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QF"/>
<wire x1="373.38" y1="109.22" x2="393.7" y2="109.22" width="0.1524" layer="91"/>
<pinref part="L_5" gate="G$1" pin="A"/>
<wire x1="396.24" y1="119.38" x2="393.7" y2="119.38" width="0.1524" layer="91"/>
<wire x1="393.7" y1="119.38" x2="393.7" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<pinref part="B_CAL" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-20.32" x2="401.32" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="401.32" y1="-20.32" x2="401.32" y2="12.7" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="QG"/>
<wire x1="401.32" y1="12.7" x2="381" y2="12.7" width="0.1524" layer="91"/>
<pinref part="B_MIDI_" gate="G$2" pin="1"/>
<wire x1="381" y1="12.7" x2="373.38" y2="12.7" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-81.28" x2="381" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="381" y1="-81.28" x2="381" y2="12.7" width="0.1524" layer="91"/>
<junction x="381" y="12.7"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="B_INV" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-27.94" x2="398.78" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="QH"/>
<wire x1="373.38" y1="10.16" x2="378.46" y2="10.16" width="0.1524" layer="91"/>
<wire x1="378.46" y1="10.16" x2="398.78" y2="10.16" width="0.1524" layer="91"/>
<wire x1="398.78" y1="10.16" x2="398.78" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="378.46" y1="10.16" x2="378.46" y2="-88.9" width="0.1524" layer="91"/>
<junction x="378.46" y="10.16"/>
<wire x1="378.46" y1="-88.9" x2="414.02" y2="-88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="B_SHIFT" gate="G$2" pin="2"/>
<pinref part="D17" gate="G$1" pin="C"/>
<wire x1="426.72" y1="-73.66" x2="424.18" y2="-73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<pinref part="B_MIDI_" gate="G$2" pin="2"/>
<pinref part="D19" gate="G$1" pin="C"/>
<wire x1="424.18" y1="-81.28" x2="426.72" y2="-81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BUTTON_PIN_2" class="0">
<segment>
<pinref part="D19" gate="G$1" pin="A"/>
<pinref part="D17" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-81.28" x2="431.8" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="D14" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-73.66" x2="431.8" y2="-66.04" width="0.1524" layer="91"/>
<junction x="431.8" y="-73.66"/>
<pinref part="D13" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-66.04" x2="431.8" y2="-58.42" width="0.1524" layer="91"/>
<junction x="431.8" y="-66.04"/>
<pinref part="D12" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-58.42" x2="431.8" y2="-50.8" width="0.1524" layer="91"/>
<junction x="431.8" y="-58.42"/>
<pinref part="D11" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-50.8" x2="431.8" y2="-43.18" width="0.1524" layer="91"/>
<junction x="431.8" y="-50.8"/>
<pinref part="D10" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-43.18" x2="431.8" y2="-35.56" width="0.1524" layer="91"/>
<junction x="431.8" y="-43.18"/>
<wire x1="431.8" y1="-35.56" x2="436.88" y2="-35.56" width="0.1524" layer="91"/>
<junction x="431.8" y="-35.56"/>
<pinref part="D20" gate="G$1" pin="A"/>
<wire x1="431.8" y1="-88.9" x2="431.8" y2="-81.28" width="0.1524" layer="91"/>
<junction x="431.8" y="-81.28"/>
<label x="436.88" y="-35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="IO11*"/>
<wire x1="436.88" y1="259.08" x2="434.34" y2="259.08" width="0.1524" layer="91"/>
<label x="434.34" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="4/CAN-RX/PWM"/>
<wire x1="337.82" y1="287.02" x2="335.28" y2="287.02" width="0.1524" layer="91"/>
<label x="335.28" y="287.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<pinref part="B_SYNTH" gate="G$2" pin="1"/>
<wire x1="414.02" y1="-43.18" x2="393.7" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="QB"/>
<wire x1="373.38" y1="25.4" x2="393.7" y2="25.4" width="0.1524" layer="91"/>
<wire x1="393.7" y1="25.4" x2="414.02" y2="25.4" width="0.1524" layer="91"/>
<wire x1="414.02" y1="25.4" x2="414.02" y2="20.32" width="0.1524" layer="91"/>
<wire x1="393.7" y1="-43.18" x2="393.7" y2="25.4" width="0.1524" layer="91"/>
<junction x="393.7" y="25.4"/>
<pinref part="B_B" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<wire x1="414.02" y1="-10.16" x2="403.86" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="403.86" y1="-10.16" x2="403.86" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="QF"/>
<wire x1="373.38" y1="15.24" x2="383.54" y2="15.24" width="0.1524" layer="91"/>
<pinref part="B_SHIFT" gate="G$2" pin="1"/>
<wire x1="383.54" y1="15.24" x2="403.86" y2="15.24" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-73.66" x2="383.54" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="383.54" y1="-73.66" x2="383.54" y2="15.24" width="0.1524" layer="91"/>
<junction x="383.54" y="15.24"/>
<pinref part="B_F" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SPI_MOSI" class="0">
<segment>
<wire x1="190.5" y1="215.9" x2="193.04" y2="215.9" width="0.1524" layer="91"/>
<label x="193.04" y="215.9" size="1.778" layer="95" xref="yes"/>
<pinref part="SPI_HEADER" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="MOSI"/>
<wire x1="436.88" y1="297.18" x2="434.34" y2="297.18" width="0.1524" layer="91"/>
<label x="434.34" y="297.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="11/MOSI"/>
<wire x1="337.82" y1="269.24" x2="335.28" y2="269.24" width="0.1524" layer="91"/>
<label x="335.28" y="269.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_MISO" class="0">
<segment>
<wire x1="190.5" y1="218.44" x2="193.04" y2="218.44" width="0.1524" layer="91"/>
<label x="193.04" y="218.44" size="1.778" layer="95" xref="yes"/>
<pinref part="SPI_HEADER" gate="G$1" pin="5"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="MISO"/>
<wire x1="472.44" y1="294.64" x2="474.98" y2="294.64" width="0.1524" layer="91"/>
<label x="474.98" y="294.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="12/MISO"/>
<wire x1="337.82" y1="266.7" x2="335.28" y2="266.7" width="0.1524" layer="91"/>
<label x="335.28" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_CLK" class="0">
<segment>
<wire x1="190.5" y1="220.98" x2="193.04" y2="220.98" width="0.1524" layer="91"/>
<label x="193.04" y="220.98" size="1.778" layer="95" xref="yes"/>
<pinref part="SPI_HEADER" gate="G$1" pin="6"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SCK"/>
<wire x1="472.44" y1="297.18" x2="474.98" y2="297.18" width="0.1524" layer="91"/>
<label x="474.98" y="297.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="13/SCK/LED"/>
<wire x1="337.82" y1="264.16" x2="335.28" y2="264.16" width="0.1524" layer="91"/>
<label x="335.28" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2C_SDA" class="0">
<segment>
<pinref part="I2C_HEADER" gate="G$1" pin="3"/>
<wire x1="190.5" y1="241.3" x2="193.04" y2="241.3" width="0.1524" layer="91"/>
<label x="193.04" y="241.3" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D2/SDA"/>
<wire x1="436.88" y1="281.94" x2="434.34" y2="281.94" width="0.1524" layer="91"/>
<label x="434.34" y="281.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="18/A4/T/SDA0"/>
<wire x1="337.82" y1="251.46" x2="335.28" y2="251.46" width="0.1524" layer="91"/>
<label x="335.28" y="251.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2C_SCL" class="0">
<segment>
<pinref part="I2C_HEADER" gate="G$1" pin="4"/>
<wire x1="190.5" y1="243.84" x2="193.04" y2="243.84" width="0.1524" layer="91"/>
<label x="193.04" y="243.84" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="D3/SCL"/>
<wire x1="436.88" y1="279.4" x2="434.34" y2="279.4" width="0.1524" layer="91"/>
<label x="434.34" y="279.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="19/A5/T/SCL0"/>
<wire x1="337.82" y1="248.92" x2="335.28" y2="248.92" width="0.1524" layer="91"/>
<label x="335.28" y="248.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_SS" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="RXLED/SS"/>
<wire x1="436.88" y1="294.64" x2="434.34" y2="294.64" width="0.1524" layer="91"/>
<label x="434.34" y="294.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SPI_HEADER" gate="G$1" pin="3"/>
<wire x1="190.5" y1="213.36" x2="193.04" y2="213.36" width="0.1524" layer="91"/>
<label x="193.04" y="213.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="10/TX2/PWM"/>
<wire x1="337.82" y1="271.78" x2="335.28" y2="271.78" width="0.1524" layer="91"/>
<label x="335.28" y="271.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="V_H" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IO8"/>
<wire x1="436.88" y1="266.7" x2="434.34" y2="266.7" width="0.1524" layer="91"/>
<label x="434.34" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R69" gate="G$1" pin="2"/>
<wire x1="185.42" y1="-71.12" x2="190.5" y2="-71.12" width="0.1524" layer="91"/>
<label x="190.5" y="-71.12" size="1.778" layer="95" xref="yes"/>
<junction x="185.42" y="-71.12"/>
<pinref part="JP4" gate="G$1" pin="3"/>
<wire x1="175.26" y1="-45.72" x2="175.26" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="175.26" y1="-71.12" x2="185.42" y2="-71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="15/A1/T"/>
<wire x1="337.82" y1="259.08" x2="335.28" y2="259.08" width="0.1524" layer="91"/>
<label x="335.28" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="V_B" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="124.46" y1="170.18" x2="116.84" y2="170.18" width="0.1524" layer="91"/>
<wire x1="124.46" y1="170.18" x2="124.46" y2="167.64" width="0.1524" layer="91"/>
<junction x="124.46" y="170.18"/>
<label x="124.46" y="167.64" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="INVERT_A" gate="G$1" pin="C"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A1"/>
<wire x1="472.44" y1="266.7" x2="474.98" y2="266.7" width="0.1524" layer="91"/>
<label x="474.98" y="266.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="22/A8/T/PWM"/>
<wire x1="337.82" y1="241.3" x2="335.28" y2="241.3" width="0.1524" layer="91"/>
<label x="335.28" y="241.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IO13*"/>
<wire x1="472.44" y1="256.54" x2="472.44" y2="254" width="0.1524" layer="91"/>
<label x="472.44" y="254" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="9/RX2/PWM"/>
<wire x1="337.82" y1="274.32" x2="335.28" y2="274.32" width="0.1524" layer="91"/>
<label x="335.28" y="274.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="4"/>
<wire x1="414.02" y1="170.18" x2="416.56" y2="170.18" width="0.1524" layer="91"/>
<label x="416.56" y="170.18" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="V_G" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="IO12*"/>
<wire x1="436.88" y1="256.54" x2="434.34" y2="256.54" width="0.1524" layer="91"/>
<label x="434.34" y="256.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R68" gate="G$1" pin="2"/>
<wire x1="185.42" y1="-40.64" x2="193.04" y2="-40.64" width="0.1524" layer="91"/>
<label x="193.04" y="-40.64" size="1.778" layer="95" xref="yes"/>
<junction x="185.42" y="-40.64"/>
<pinref part="JP4" gate="G$1" pin="1"/>
<wire x1="185.42" y1="-40.64" x2="175.26" y2="-40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="16/A2/T"/>
<wire x1="337.82" y1="256.54" x2="335.28" y2="256.54" width="0.1524" layer="91"/>
<label x="335.28" y="256.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QA"/>
<wire x1="373.38" y1="121.92" x2="381" y2="121.92" width="0.1524" layer="91"/>
<wire x1="381" y1="121.92" x2="381" y2="157.48" width="0.1524" layer="91"/>
<pinref part="L_0" gate="G$1" pin="A"/>
<wire x1="381" y1="157.48" x2="396.24" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GT_E" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QE"/>
<wire x1="370.84" y1="172.72" x2="373.38" y2="172.72" width="0.1524" layer="91"/>
<label x="373.38" y="172.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="1"/>
<wire x1="398.78" y1="172.72" x2="396.24" y2="172.72" width="0.1524" layer="91"/>
<label x="396.24" y="172.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GT_F" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QF"/>
<wire x1="370.84" y1="170.18" x2="373.38" y2="170.18" width="0.1524" layer="91"/>
<label x="373.38" y="170.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="5"/>
<wire x1="398.78" y1="167.64" x2="396.24" y2="167.64" width="0.1524" layer="91"/>
<label x="396.24" y="167.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GT_G" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QG"/>
<wire x1="370.84" y1="167.64" x2="373.38" y2="167.64" width="0.1524" layer="91"/>
<label x="373.38" y="167.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="2"/>
<wire x1="414.02" y1="172.72" x2="416.56" y2="172.72" width="0.1524" layer="91"/>
<label x="416.56" y="172.72" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GT_H" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QH"/>
<wire x1="370.84" y1="165.1" x2="373.38" y2="165.1" width="0.1524" layer="91"/>
<label x="373.38" y="165.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="EXP" gate="G$1" pin="6"/>
<wire x1="414.02" y1="167.64" x2="416.56" y2="167.64" width="0.1524" layer="91"/>
<label x="416.56" y="167.64" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="D24" gate="G$1" pin="C"/>
<pinref part="AMP_M" gate="B" pin="+IN"/>
<pinref part="C10" gate="G$1" pin="+"/>
<junction x="106.68" y="-66.04"/>
<pinref part="C10" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<wire x1="121.92" y1="-55.88" x2="121.92" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-63.5" x2="124.46" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="1"/>
<wire x1="116.84" y1="-55.88" x2="121.92" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R73" gate="G$1" pin="2"/>
<pinref part="AMP_M" gate="B" pin="OUT"/>
<junction x="121.92" y="-63.5"/>
<wire x1="121.92" y1="-55.88" x2="121.92" y2="-35.56" width="0.1524" layer="91"/>
<junction x="121.92" y="-55.88"/>
<wire x1="121.92" y1="-35.56" x2="132.08" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-35.56" x2="132.08" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="DIG_READ" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<wire x1="106.68" y1="-60.96" x2="106.68" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="2"/>
<pinref part="R72" gate="G$1" pin="2"/>
<junction x="106.68" y="-55.88"/>
<pinref part="AMP_M" gate="B" pin="-IN"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="OUT_A" gate="G$1" pin="RIGHT"/>
<wire x1="137.16" y1="-40.64" x2="134.62" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="134.62" y1="-40.64" x2="134.62" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-40.64" x2="134.62" y2="-40.64" width="0.1524" layer="91"/>
<junction x="134.62" y="-40.64"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="2"/>
<wire x1="337.82" y1="292.1" x2="335.28" y2="292.1" width="0.1524" layer="91"/>
<label x="335.28" y="292.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="50.8" y1="160.02" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<pinref part="AMP_B" gate="A" pin="+IN"/>
<junction x="53.34" y="160.02"/>
<label x="50.8" y="160.02" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="71.12" y1="162.56" x2="68.58" y2="162.56" width="0.1524" layer="91"/>
<pinref part="GAIN_B" gate="G$1" pin="3"/>
<wire x1="106.68" y1="172.72" x2="106.68" y2="180.34" width="0.1524" layer="91"/>
<wire x1="106.68" y1="180.34" x2="68.58" y2="180.34" width="0.1524" layer="91"/>
<pinref part="INVERT_A" gate="G$1" pin="D"/>
<wire x1="68.58" y1="180.34" x2="68.58" y2="182.88" width="0.1524" layer="91"/>
<pinref part="GAIN_B" gate="G$1" pin="2"/>
<wire x1="68.58" y1="182.88" x2="63.5" y2="182.88" width="0.1524" layer="91"/>
<wire x1="68.58" y1="175.26" x2="68.58" y2="180.34" width="0.1524" layer="91"/>
<junction x="68.58" y="180.34"/>
<wire x1="68.58" y1="162.56" x2="68.58" y2="175.26" width="0.1524" layer="91"/>
<junction x="68.58" y="175.26"/>
<pinref part="AMP_B" gate="A" pin="OUT"/>
<junction x="68.58" y="162.56"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="L_INV" gate="G$1" pin="A"/>
<pinref part="IC1" gate="A" pin="QA"/>
<wire x1="386.08" y1="182.88" x2="370.84" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="L_6" gate="G$1" pin="C"/>
<pinref part="R50" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="L_6" gate="G$1" pin="A"/>
<wire x1="396.24" y1="111.76" x2="396.24" y2="106.68" width="0.1524" layer="91"/>
<pinref part="IC5" gate="A" pin="QG"/>
<wire x1="396.24" y1="106.68" x2="373.38" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="IC5" gate="A" pin="QH"/>
<pinref part="L_GATE" gate="G$1" pin="A"/>
<wire x1="373.38" y1="104.14" x2="396.24" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="L_A" gate="G$1" pin="A"/>
<wire x1="396.24" y1="88.9" x2="375.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="375.92" y1="88.9" x2="375.92" y2="73.66" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="QA"/>
<wire x1="375.92" y1="73.66" x2="373.38" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QB"/>
<wire x1="373.38" y1="71.12" x2="378.46" y2="71.12" width="0.1524" layer="91"/>
<wire x1="378.46" y1="71.12" x2="378.46" y2="81.28" width="0.1524" layer="91"/>
<pinref part="L_B" gate="G$1" pin="A"/>
<wire x1="378.46" y1="81.28" x2="396.24" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QC"/>
<wire x1="373.38" y1="68.58" x2="381" y2="68.58" width="0.1524" layer="91"/>
<wire x1="381" y1="68.58" x2="381" y2="73.66" width="0.1524" layer="91"/>
<pinref part="L_C" gate="G$1" pin="A"/>
<wire x1="381" y1="73.66" x2="396.24" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QD"/>
<pinref part="L_D" gate="G$1" pin="A"/>
<wire x1="373.38" y1="66.04" x2="396.24" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="L_H" gate="G$1" pin="A"/>
<wire x1="396.24" y1="35.56" x2="375.92" y2="35.56" width="0.1524" layer="91"/>
<wire x1="375.92" y1="35.56" x2="375.92" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC6" gate="A" pin="QH"/>
<wire x1="375.92" y1="55.88" x2="373.38" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QG"/>
<wire x1="373.38" y1="58.42" x2="378.46" y2="58.42" width="0.1524" layer="91"/>
<wire x1="378.46" y1="58.42" x2="378.46" y2="43.18" width="0.1524" layer="91"/>
<pinref part="L_G" gate="G$1" pin="A"/>
<wire x1="378.46" y1="43.18" x2="396.24" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QF"/>
<wire x1="373.38" y1="60.96" x2="381" y2="60.96" width="0.1524" layer="91"/>
<wire x1="381" y1="60.96" x2="381" y2="50.8" width="0.1524" layer="91"/>
<pinref part="L_F" gate="G$1" pin="A"/>
<wire x1="381" y1="50.8" x2="396.24" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="IC6" gate="A" pin="QE"/>
<wire x1="373.38" y1="63.5" x2="383.54" y2="63.5" width="0.1524" layer="91"/>
<wire x1="383.54" y1="63.5" x2="383.54" y2="58.42" width="0.1524" layer="91"/>
<pinref part="L_E" gate="G$1" pin="A"/>
<wire x1="383.54" y1="58.42" x2="396.24" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="R70" gate="G$1" pin="2"/>
<pinref part="D24" gate="G$1" pin="A"/>
<wire x1="93.98" y1="-66.04" x2="101.6" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="SUPPLY2" gate="G$1" pin="+5V"/>
<wire x1="15.24" y1="119.38" x2="15.24" y2="111.76" width="0.1524" layer="91"/>
<label x="15.24" y="114.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="G$1" pin="+5V"/>
<wire x1="20.32" y1="119.38" x2="20.32" y2="111.76" width="0.1524" layer="91"/>
<label x="20.32" y="116.84" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="SUPPLY10" gate="G$1" pin="+5V"/>
<wire x1="30.48" y1="124.46" x2="30.48" y2="121.92" width="0.1524" layer="91"/>
<label x="30.48" y="124.46" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="SUPPLY5" gate="G$1" pin="+5V"/>
<wire x1="27.94" y1="73.66" x2="27.94" y2="71.12" width="0.1524" layer="91"/>
<label x="27.94" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="SUPPLY3" gate="G$1" pin="+5V"/>
<wire x1="66.04" y1="83.82" x2="66.04" y2="81.28" width="0.1524" layer="91"/>
<label x="66.04" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="SUPPLY9" gate="G$1" pin="+5V"/>
<wire x1="30.48" y1="193.04" x2="30.48" y2="190.5" width="0.1524" layer="91"/>
<label x="30.48" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="G$1" pin="+5V"/>
<wire x1="20.32" y1="185.42" x2="20.32" y2="180.34" width="0.1524" layer="91"/>
<label x="20.32" y="185.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="SUPPLY1" gate="G$1" pin="+5V"/>
<wire x1="15.24" y1="182.88" x2="15.24" y2="180.34" width="0.1524" layer="91"/>
<label x="15.24" y="182.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="SUPPLY8" gate="G$1" pin="+5V"/>
<wire x1="66.04" y1="152.4" x2="66.04" y2="149.86" width="0.1524" layer="91"/>
<label x="66.04" y="149.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="144.78" y1="241.3" x2="144.78" y2="246.38" width="0.1524" layer="91"/>
<pinref part="AMP_B" gate="P" pin="V+"/>
<wire x1="144.78" y1="246.38" x2="152.4" y2="246.38" width="0.1524" layer="91"/>
<pinref part="AMP_M" gate="P" pin="V+"/>
<wire x1="152.4" y1="246.38" x2="157.48" y2="246.38" width="0.1524" layer="91"/>
<junction x="152.4" y="246.38"/>
<pinref part="AMP_C" gate="P" pin="V+"/>
<wire x1="157.48" y1="246.38" x2="162.56" y2="246.38" width="0.1524" layer="91"/>
<wire x1="162.56" y1="246.38" x2="170.18" y2="246.38" width="0.1524" layer="91"/>
<junction x="162.56" y="246.38"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="170.18" y1="246.38" x2="175.26" y2="246.38" width="0.1524" layer="91"/>
<wire x1="170.18" y1="241.3" x2="170.18" y2="246.38" width="0.1524" layer="91"/>
<junction x="170.18" y="246.38"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="157.48" y1="241.3" x2="157.48" y2="246.38" width="0.1524" layer="91"/>
<junction x="157.48" y="246.38"/>
<pinref part="SUPPLY19" gate="G$1" pin="+5V"/>
<junction x="144.78" y="246.38"/>
<label x="147.32" y="246.38" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="VOLTAGERANGE" gate="G$1" pin="2"/>
<wire x1="139.7" y1="246.38" x2="144.78" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<pinref part="SUPPLY13" gate="G$1" pin="+5V"/>
<wire x1="78.74" y1="15.24" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<label x="78.74" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="SUPPLY37" gate="G$1" pin="+5V"/>
<wire x1="71.12" y1="55.88" x2="71.12" y2="50.8" width="0.1524" layer="91"/>
<label x="71.12" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<pinref part="SUPPLY36" gate="G$1" pin="+5V"/>
<wire x1="66.04" y1="53.34" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
<label x="66.04" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<pinref part="SUPPLY38" gate="G$1" pin="+5V"/>
<wire x1="7.62" y1="53.34" x2="7.62" y2="50.8" width="0.1524" layer="91"/>
<label x="7.62" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<pinref part="SUPPLY39" gate="G$1" pin="+5V"/>
<wire x1="12.7" y1="55.88" x2="12.7" y2="50.8" width="0.1524" layer="91"/>
<label x="12.7" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="SUPPLY4" gate="G$1" pin="+5V"/>
<wire x1="20.32" y1="15.24" x2="20.32" y2="12.7" width="0.1524" layer="91"/>
<label x="20.32" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R47" gate="G$1" pin="2"/>
<pinref part="SUPPLY12" gate="G$1" pin="+5V"/>
<wire x1="68.58" y1="-48.26" x2="68.58" y2="-50.8" width="0.1524" layer="91"/>
<label x="68.58" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R46" gate="G$1" pin="2"/>
<pinref part="SUPPLY11" gate="G$1" pin="+5V"/>
<wire x1="48.26" y1="-22.86" x2="48.26" y2="-27.94" width="0.1524" layer="91"/>
<label x="48.26" y="-22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R72" gate="G$1" pin="1"/>
<pinref part="SUPPLY33" gate="G$1" pin="+5V"/>
<wire x1="96.52" y1="-50.8" x2="96.52" y2="-55.88" width="0.1524" layer="91"/>
<label x="96.52" y="-50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R69" gate="G$1" pin="1"/>
<pinref part="SUPPLY16" gate="G$1" pin="+5V"/>
<wire x1="185.42" y1="-58.42" x2="185.42" y2="-60.96" width="0.1524" layer="91"/>
<label x="185.42" y="-58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R68" gate="G$1" pin="1"/>
<pinref part="SUPPLY15" gate="G$1" pin="+5V"/>
<wire x1="185.42" y1="-27.94" x2="185.42" y2="-30.48" width="0.1524" layer="91"/>
<label x="185.42" y="-27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="THR" gate="G$1" pin="3"/>
<pinref part="SUPPLY26" gate="G$1" pin="+5V"/>
<wire x1="180.34" y1="10.16" x2="180.34" y2="7.62" width="0.1524" layer="91"/>
<label x="180.34" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="3.3V"/>
<pinref part="SUPPLY34" gate="G$1" pin="+5V"/>
<wire x1="398.78" y1="294.64" x2="393.7" y2="294.64" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="393.7" y1="294.64" x2="383.54" y2="294.64" width="0.1524" layer="91"/>
<junction x="393.7" y="294.64"/>
<label x="398.78" y="294.64" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="AREF"/>
<wire x1="487.68" y1="261.62" x2="472.44" y2="261.62" width="0.1524" layer="91"/>
<pinref part="SUPPLY27" gate="G$1" pin="+5V"/>
<label x="487.68" y="261.62" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="SUPPLY40" gate="G$1" pin="+5V"/>
<wire x1="27.94" y1="142.24" x2="27.94" y2="139.7" width="0.1524" layer="91"/>
<label x="27.94" y="142.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="IC2" gate="1" pin="OUT"/>
<pinref part="C17" gate="G$1" pin="1"/>
<junction x="121.92" y="241.3"/>
<pinref part="VOLTAGERANGE" gate="G$1" pin="1"/>
<wire x1="139.7" y1="243.84" x2="139.7" y2="241.3" width="0.1524" layer="91"/>
<wire x1="139.7" y1="241.3" x2="121.92" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="D21" gate="G$1" pin="A"/>
<pinref part="D3VPRT" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="D22" gate="G$1" pin="A"/>
<pinref part="E3VPRT" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PRE_AMP_OUT" class="0">
<segment>
<pinref part="GAIN_MIC" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-17.78" x2="101.6" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="GAIN_MIC" gate="G$1" pin="3"/>
<wire x1="101.6" y1="-17.78" x2="101.6" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-38.1" x2="101.6" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-25.4" x2="101.6" y2="-33.02" width="0.1524" layer="91"/>
<junction x="101.6" y="-25.4"/>
<pinref part="R51" gate="G$1" pin="2"/>
<junction x="101.6" y="-38.1"/>
<label x="101.6" y="-17.78" size="1.778" layer="95" xref="yes"/>
<pinref part="R70" gate="G$1" pin="1"/>
<wire x1="101.6" y1="-33.02" x2="101.6" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-55.88" x2="93.98" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="AMP_M" gate="A" pin="OUT"/>
<junction x="93.98" y="-38.1"/>
<wire x1="127" y1="-33.02" x2="101.6" y2="-33.02" width="0.1524" layer="91"/>
<junction x="101.6" y="-33.02"/>
<pinref part="DIG_READ" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<pinref part="IC1" gate="A" pin="QH*"/>
<wire x1="370.84" y1="160.02" x2="370.84" y2="139.7" width="0.1524" layer="91"/>
<wire x1="370.84" y1="139.7" x2="347.98" y2="139.7" width="0.1524" layer="91"/>
<pinref part="IC5" gate="A" pin="SER"/>
<wire x1="347.98" y1="139.7" x2="347.98" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
