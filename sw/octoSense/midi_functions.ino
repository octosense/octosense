/*

   each channel contribute in different ways:


   NOTE RELATED (noteOn, noteOff)
   -pich of a note
   -on/off of a note
   -velocity of a note
   -midi channel

    CONTROL RELATED
    -CCs = Control Change = knob controller (knob number and value)
      -number
      -channel
    -PitchBend
      -channel

    CLOCK RELATED
    -tap tempo - clock multiplication, internal clock source that is being set by the TAP mechanism

    while holding MIDI_B learning is activated. we learn: channel, CC number, Note index

    MIDI_B + 6 button = type of message PITCH, CC, VELOCITY, CLOCK, NOTE ON/OFF, PITCH_BEND
    MIDI_B + UP/DOWN (GATE&TOUCH) = set midi channel (use 8 leds to display midi channel 1-8=normal, 9-16=inverse

    MIDI_B + secondary UP/DOWN (CV/SYNTH) = set number of CC up and down - voltageMeter displays 7 bit binary number of the CC


*/

#define MIDI_PITCH_1 0
#define MIDI_PITCH_2 1
#define MIDI_PITCH_3 2
#define MIDI_CC 5
#define MIDI_NOTE_ON 4
#define MIDI_VELOCITY 3



#define MIDI_CLOCK 3
#define MIDI_PITCHBEND 5
uint8_t sentNote[8];
//uint8_t noteOnStates[256];


void dingoOn(uint8_t _note, uint8_t _channel) { //watchdog for hanging notes
  _channel--;
  // bitWrite(noteOnStates[_channel * 16 + (_note / 8)], _note % 8, 1);
}

void dingoOff(uint8_t _note, uint8_t _channel) { //watchdog for hanging notes
  _channel--;
  //bitWrite(noteOnStates[_channel * 16 + (_note / 8)], _note % 8, 0);
}

bool isNoteHanging(uint8_t _note, uint8_t _channel) {
  _channel--;
  return false;//bitRead(noteOnStates[_channel * 16 + (_note / 8)], _note % 8);
}
//2096 RAM

void renderMidiOutput() { //look at assignments of the channels and send corresponding midi messages
  uint8_t i =  hw.getReadIndex(); //we dont need to calculate everything all the time because only one channel is updated in each run of the loop function

  if (midiMessageAssign[i] == MIDI_NOTE_ON) { //note ON/OFF assignment

    uint8_t _note = midiNumberAssign[i];
    bool _quantize = false;
    uint8_t _velocity = 127;
    for (uint8_t j = 0; j < 8; j++) { //look if other channels are defining MIDI_PITCH or VELOCITY on the same midi channel
      if (midiMessageAssign[j] == MIDI_PITCH_1) {
        if (!muteState[j] && midiChannelAssign[j] == midiChannelAssign[i]) _note += map(finalValue[j], 0, 1024, 0, 13) ; //update the defaulet _note value only if the channels are matching and they are not muted
        _quantize = true;
      }
      else if (midiMessageAssign[j] == MIDI_PITCH_2) {
        if (!muteState[j] && midiChannelAssign[j] == midiChannelAssign[i]) _note += map(finalValue[j], 0, 1024, 0, 25); //update the defaulet _note value only if the channels are matching and they are not muted
        _quantize = true;
      }
      else if (midiMessageAssign[j] == MIDI_PITCH_3) {
        if (!muteState[j] && midiChannelAssign[j] == midiChannelAssign[i]) _note += map(finalValue[j], 0, 1024, 0, 37); //update the defaulet _note value only if the channels are matching and they are not muted
        _quantize = true;
      }
      if (midiMessageAssign[j] == MIDI_VELOCITY) {
        if (!muteState[j] && midiChannelAssign[j] == midiChannelAssign[i]) _velocity = finalValue[j] >> 3; //update the defaulet _velocity value only if the channels are matching and they are not muted
      }

    }

    if (gateState[i] && !lastGateState[i]) { //gate just became high threfore send noteOn
      if (!muteState[i]) {
        if (_quantize) { //if other channel is used to control the midi pitch quantize that pitch
          sentNote[i] = quantizeNote(_note); // store sent note to be turned off on GATE OFF
          MIDI.sendNoteOn(quantizeNote(_note), _velocity , midiChannelAssign[i]), dingoOn(quantizeNote(_note), midiChannelAssign[i]);
        }
        else { //if not use the original note number without quantization
          sentNote[i] = _note; // store sent note to be turned off on GATE OFF
          MIDI.sendNoteOn(_note, _velocity , midiChannelAssign[i]), dingoOn(quantizeNote(_note), midiChannelAssign[i]);
        }
      }
    }
    if (!gateState[i] && lastGateState[i]) { //gate just became low threfore send noteOff
      /*
        for (uint8_t x = 0; x < 128; x++) {
        if (!muteState[i]) {
         //if(isNoteHanging(x,midiChannelAssign[i])) MIDI.sendNoteOff(x, 0 , midiChannelAssign[i]), dingoOff(x, midiChannelAssign[i]);
        }
        }
      */
      if (!muteState[i]) MIDI.sendNoteOff(sentNote[i], 0 , midiChannelAssign[i]), dingoOff(sentNote[i], midiChannelAssign[i]); //send noteOff with the stored sentNote
    }
  }

  if (midiMessageAssign[i] == MIDI_CC) { //send midi ccs
    if (!muteState[i]) {
      if (finalValue[i] >> 3 != lastFinalValue[i] >> 3) MIDI.sendControlChange (midiNumberAssign[i], finalValue[i] >> 3, midiChannelAssign[i]); //send the CCs only if the reduced 7bit value changed
    }
  }

}
void initMidi() {
  //initiate the callbacks
  MIDI.setHandleNoteOn(handleNoteOn);
  MIDI.setHandleControlChange(handleCC);
  MIDI.setHandlePitchBend(handlePitchBend);
  MIDI.setHandleClock(handleClock);
  MIDI.setHandleStart(handleStart);
  MIDI.setHandleStop(handleStop);
  MIDI.setHandleContinue(handleContinue);
  MIDI.setHandleNoteOff(handleNoteOff);
  MIDI.turnThruOn(); //midi trafic from MIDI IN will be forwarded to MIDI OUT
  MIDI.begin(MIDI_CHANNEL_OMNI); //recieve on all midi channels
}

/*

   sendNoteOn ( DataByte  inNoteNumber,DataByte  inVelocity,Channel   inChannel)
   sendNoteOff ( DataByte  inNoteNumber,DataByte  inVelocity,Channel   inChannel )
   sendControlChange ( DataByte  inControlNumber,DataByte  inControlValue,Channel   inChannel )
   sendAfterTouch ( DataByte  inNoteNumber,DataByte  inPressure,Channel   inChannel )
   sendPitchBend ( int   inPitchValue,Channel   inChannel)

   sendRealTime ( MIDI::Clock) , sendRealTime ( MIDI.Start), sendRealTime ( MIDI.Stop), sendRealTime ( MIDI.Continue)

*/

void handleNoteOn(byte channel, byte pitch, byte velocity) { //recieving midi noteOn
  if (hw.buttonState(MIDI_B)) { //lear function mapping
    midiChannelAssign[selectedChannel] = channel;
    midiNumberAssign[selectedChannel] = pitch;
    midiMessageAssign[selectedChannel] = MIDI_NOTE_ON;
    midiDisplay = 0;
  }

  if (hw.buttonState(MIDI_B)) { //visual test
    if (velocity == 0) hw.setLed(DATA_L, 0);
    else hw.setLed(DATA_L, 1);
  }
}

void handleNoteOff(byte channel, byte pitch, byte velocity) { //recieving midi noteOff

  if (hw.buttonState(MIDI_B)) { //visual test
    hw.setLed(DATA_L, 0);
  }
}


void handleCC(byte channel, byte number, byte value) { //recieving midi CCs
  if (hw.buttonState(MIDI_B)) { //lear function mapping
    midiChannelAssign[selectedChannel] = channel;
    midiNumberAssign[selectedChannel] = number;
    midiMessageAssign[selectedChannel] = MIDI_CC;
    midiDisplay = 0;
  }
  if (hw.buttonState(MIDI_B)) hw.setLed(INVERT_L, value >> 6);
}

void handlePitchBend(byte channel, int value) { //recieving midi pitchbend

}

void handleClock() { //recieving midi clock

}

void handleStart() { //recieving midi start

}

void handleStop() { //recieving midi stop

}

void handleContinue() { //recieving midi continue

}
