/*
   Developed by DIY community around Osmoza/PIFcamp/Zavod Projekt Atol, Ljubljana, Slovenia
   project group: Jakob Grcman, Lan, Simon Turnšek, Simon Macuh, Igor Vuk, Julian, Martin,
   project coordinators Tina Dolinšek, Rea Vogrincic and Uroš Veber (Zavod Projekt Atol)
   supervised by Václav Peloušek aka Toyota Vangelis
   supported by Kons/EU

   LIBRARY dependencies to be installed:
   EduIntro: https://www.arduino.cc/reference/en/libraries/eduintro/
   MidiLibrary: https://www.arduino.cc/reference/en/libraries/midi-library/
   TimerOne: https://www.arduino.cc/reference/en/libraries/timerone/
   EEPROM - included in the arduino package
*/

#include <EEPROM.h>
#include <MIDI.h>
#include <EduIntro.h>
#include <TimerOne.h>

#define CURRENT_PRESET_OFFSET 1023

MIDI_CREATE_DEFAULT_INSTANCE();

//octoHardware hw;
//dateExtraction extr;
#include "hardware.h"
//#include <DHT.h>
hw hw;


#define NUMBER_OF_CHANNELS 8
#define FAULTY_CALIBRATION_THRESHOLD 50
#define UNASSIGNED 255
#define THRESHOLD_INVERSION 5
#define FREQ_COUNTER 3
#define LOW_THRESHOLD 4
#define AVERAGING 0
#define ENVELOPE_FOLLOWER 1
#define FAST_EF 2
#define INVERT_L 16
#define DATA_L 15

#define ASSIGNED_TO_SYNTH 255
int synthWave = 0;
uint8_t selectedChannel = 0;
uint8_t currentPreset = 0;
bool ch7capacitive = false;
bool ch8capacitive = false;
//variables to be stored in the memory
bool invertState[8];
uint16_t channelMin[8] = {0, 0, 0, 0, 0, 0, 0, 0};
uint16_t channelMax[8] = {1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023};
int channelThreshold[8];
int channelThreshold2[8];
uint8_t extractionSettings[8];
uint8_t synthAssign[8] = {5, 5, 5, 5, 5, 5, 5, 5};
bool muteState[8] = {false, false, false, false, false, false, false, false};

uint8_t midiChannelAssign[8];
uint8_t midiMessageAssign[8];
uint8_t midiNumberAssign[8]; //for CC number or Note number (if not defined by other channel)

int synthParameter[8];
//variables calculated realtime
int finalValue[8], lastFinalValue[8];
bool gateState[8];
bool lastGateState[8];
uint8_t digitalSensorType;
bool knobFrozen = true;
int autoMin, autoMax;
uint8_t assignedGate, assignedGate2, assignedCV, assignedCV2, assignedMute;
bool synthMode = false;
uint8_t selectedParameter = 0;
bool combo = false;
int finalSynthValue[8];
uint8_t rootNote=0;
void setup() {
  //hw.init(teensy);

  Serial.begin(9600);
  initMidi();
  // while (!Serial); //this needs to be figured out

  currentPreset = EEPROM.read(CURRENT_PRESET_OFFSET);
  load(currentPreset);
  // Serial.println("start");
  hw.init();

  for (uint8_t i = 0; i < 16; i++) hw.update();
  if (hw.buttonState(SHIFT_B) && hw.buttonState(SYNTH_B) && hw.buttonState(MUTE_B) && hw.buttonState(CAL_B)) formatMemory();
  for (uint8_t i = 0; i < 6; i++) {
    if (hw.buttonState(i)) {
      digitalSensorType = i;
      save(currentPreset);
    }
  }
  if (hw.buttonState(SELECT_7)) {
    digitalSensorType = 6; //custom
    save(currentPreset);
  }
  if (hw.buttonState(SELECT_8)) {
    digitalSensorType = 7; //custom
    save(currentPreset);
  }

  hw.blinkNumber(digitalSensorType);

  initAudio();
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);

  Timer1.initialize(15);         //66 kHz pwm -
  Timer1.pwm(9, 512);            // setup pwm on pin 9, 50% duty cycle

  Timer1.attachInterrupt(updateAudioEngine);  // attaches callback() as a timer overflow interrupt - every second interrupt renders the audio.
  initDigitalSensor();

  //delay(2000);
  /*
    while (!Serial);
    Serial.println();
    for(uint8_t i=0;i<128;i++){
    if(i%2) dingoOn(i,6);
    else dingoOff(i,6);
    }
    for(uint8_t i=0;i<128;i++) Serial.print(isNoteHanging(i,6));
  */

  hw.setLed(DATA_L, 0);
}

uint16_t incr = 0;

bool inBetween(int _value, int border1, int border2) {
  if (border1 < border2) {
    if (_value >= border1 && _value <= border2) {
      return true;
    }
    else return false;
  }
  else {
    if (_value >= border2 && _value <= border1) {
      return true;
    }
    else return false;
  }
}

void loop() {
  //READ INPUTS
  //hw.setLed(DATA_L,hw.buttonState(0));
  // if(hw.justPressed(0)) MIDI.sendNoteOn(64, 100 , 1);
  // if(hw.justReleased(0)) MIDI.sendNoteOff(64, 0 , 1);
  hw.update(); //update inputs + shift registers

  readDigitalSensor();
  while (MIDI.read()); //read midi

  //PROCESS INPUTS
  renderFinalValues();//data extraction
  renderButtons(); //button interactions UI
  renderKnob(); //threshold knob
  renderDisplay(); //display current mode UI
  renderEnvelope();

  // UPDATE OUTPUTS
  renderMidiOutput();  // send midi
  updateSynth(); //audio engine
  updateAnalogOuts();   //update gates and CVs

  //debugPrint(); //print all values for debugging purposes
  // delay(5);
  for (uint8_t i = 0; i < 16; i++) {
    //if (hw.justPressed(i)) Serial.println(i);
  }

}

void debugPrint() {
  for (uint8_t i = 0; i < 8; i++) {
    Serial.print(finalValue[i]);
    Serial.print(", ");
  }
  Serial.println();
}

void renderKnob() { //softTakeover - you need to hit the originally set value
  if (synthMode) {
    if (knobFrozen) {
      if (inBetween(synthParameter[selectedParameter], hw.getThresholdKnobValue(), hw.getLastThresholdKnobValue())) knobFrozen = false;
    }
    else {
      synthParameter[selectedParameter] = hw.getThresholdKnobValue();
    }
  }
  else {
    if (knobFrozen) {
      //figure out how to unfreeze it
      // if(hw.getThresholdKnobValue()==channelThreshold[selectedChannel]) knobFrozen=false;
      if (bitRead(extractionSettings[selectedChannel], LOW_THRESHOLD)) {
        if (inBetween(channelThreshold2[selectedChannel], hw.getThresholdKnobValue(), hw.getLastThresholdKnobValue())) knobFrozen = false;
      }
      else {
        if (inBetween(channelThreshold[selectedChannel], hw.getThresholdKnobValue(), hw.getLastThresholdKnobValue())) knobFrozen = false;
      }
      // Serial.println("frozen");
    }
    else {
      // Serial.println("unfrozen");
      if (bitRead(extractionSettings[selectedChannel], LOW_THRESHOLD)) {
        channelThreshold2[selectedChannel] = hw.getThresholdKnobValue();
      }
      else {
        channelThreshold[selectedChannel] = hw.getThresholdKnobValue();
      }
    }
  }

}

uint8_t midiDisplay = 0;

void renderButtons() {



  //AUTOMATIC_CALIBRATION
  if (hw.justPressed(CAL_B)) { //initiate automatic calibration by setting the recording variable to oposite extremes
    autoMin = 1023;
    autoMax = 0;
  }
  if (hw.justReleased(CAL_B)) { // finish automatic calibration by writing the channel minimum and maximum
    if (abs(autoMin - autoMax) > FAULTY_CALIBRATION_THRESHOLD) {
      channelMin[selectedChannel] = autoMin;
      channelMax[selectedChannel] = autoMax;
    }

  }
  if (hw.buttonState(CAL_B)) { // automatic calibration - remember the lowest and highes value
    if (hw.getAnalogValue(selectedChannel) < autoMin) autoMin = hw.getAnalogValue(selectedChannel);
    if (hw.getAnalogValue(selectedChannel) > autoMax) autoMax = hw.getAnalogValue(selectedChannel);
    if (hw.buttonState(SHIFT_B)) {
      autoMin = 0, autoMax = 1023; //pressing INVERT when AUTO CALIBRATION resets the calibration
    }
  }

  else if (synthMode) {

    if (hw.justReleased(SYNTH_B) && !combo) {
      synthMode = false;
      knobFrozen = true;
    }
    if (hw.buttonState(SHIFT_B)) {
      if (hw.buttonState(SYNTH_B)) {
        combo = true;

        for (uint8_t i = 0; i < 6; i++) {
          if (hw.justPressed(i)) {
            if (synthAssign[selectedParameter] == i) synthAssign[selectedParameter] = UNASSIGNED;
            else {
              for (uint8_t j = 0; j < 8; j++) { //scann all other synth parameters and if they use the same sensor channel - deassign them
                if (synthAssign[j] == i) synthAssign[j] = UNASSIGNED;
              }
              synthAssign[selectedParameter] = i;
            }

            combo = true;

          }
        }
        if (hw.justPressed(SELECT_7)) {
          if (synthAssign[selectedParameter] == 6) synthAssign[selectedParameter] = UNASSIGNED;
          else {
            for (uint8_t j = 0; j < 8; j++) { //scann all other synth parameters and if they use the same sensor channel - deassign them
              if (synthAssign[j] == 6) synthAssign[j] = UNASSIGNED;
            }

            synthAssign[selectedParameter] = 6;
          }
          combo = true;

        }
        if (hw.justPressed(SELECT_8)) {
          if (synthAssign[selectedParameter] == 7) synthAssign[selectedParameter] = UNASSIGNED;
          else {
            for (uint8_t j = 0; j < 8; j++) { //scann all other synth parameters and if they use the same sensor channel - deassign them
              if (synthAssign[j] == 7) synthAssign[j] = UNASSIGNED;
            }

            synthAssign[selectedParameter] = 7;
          }
          combo = true;

        }

      }
    }
    else {
      for (uint8_t i = 0; i < 6; i++) {
        if (hw.justPressed(i)) {
          selectedParameter = i; //if on of channel buttons is pressed select that channel
          knobFrozen = true; //freeze the threshold knob if channel is changed
        }
      }
      if (hw.justPressed(SELECT_8)) {
        selectedParameter = 7;
        knobFrozen = true; //freeze the threshold knob if channel is changed
      }
      if (hw.justPressed(SELECT_7)) {
        selectedParameter = 6;
        knobFrozen = true; //freeze the threshold knob if channel is changed
      }
    }
  }

  else if (hw.buttonState(MIDI_B)) {
    for (uint8_t i = 0; i < 6; i++) {
      if (hw.justPressed(i)) {
        if(midiMessageAssign[selectedChannel]==i)  midiMessageAssign[selectedChannel]=16, midiDisplay = 0;
        else midiMessageAssign[selectedChannel] = i, midiDisplay = 0;
      }
    }
    if (hw.justPressed(MUTE_B)) {//midichannelUp
      midiDisplay = 1;
      midiChannelAssign[selectedChannel]++;
      if (midiChannelAssign[selectedChannel] > 16) midiChannelAssign[selectedChannel] = 1;
    }
    if (hw.justPressed(SYNTH_B)) {//midichanneldown
      midiDisplay = 1;
      midiChannelAssign[selectedChannel]--;
      if (midiChannelAssign[selectedChannel] < 1) midiChannelAssign[selectedChannel] = 16;

    }
    if (hw.justPressed(GATE_B)) { //midi number up
      midiNumberAssign[selectedChannel]++;
      if (midiNumberAssign[selectedChannel] > 127 ) midiNumberAssign[selectedChannel] = 0;
    }
    if (hw.justPressed(CV_B)) { //midi number down
      if (midiNumberAssign[selectedChannel] > 0 )  midiNumberAssign[selectedChannel]--;
      else midiNumberAssign[selectedChannel] = 127;
    }

    if (hw.justPressed(SELECT_8)) { //midi number up
      rootNote++;
      if (rootNote > 11 ) rootNote = 0;
    }
    if (hw.justPressed(SELECT_7)) { //midi number down
      if (rootNote > 0 )  rootNote--;
      else rootNote = 11;
    }
    
  }
  
  else if (hw.buttonState(SHIFT_B)) {

    if (hw.buttonState(CV_B)) { //CV2

      for (uint8_t i = 0; i < 6; i++) {
        if (hw.justPressed(i)) assignedCV2 = i;
      }
      if (hw.justPressed(SELECT_7)) assignedCV2 = 6;
      if (hw.justPressed(SELECT_8)) assignedCV2 = 7;


      if (hw.justPressed(SYNTH_B)) assignedCV2 = ASSIGNED_TO_SYNTH;
    }

    if (hw.buttonState(GATE_B)) {
      for (uint8_t i = 0; i < 6; i++) {
        if (hw.justPressed(i)) assignedGate2 = i;
      }
      if (hw.justPressed(SELECT_7)) assignedGate2 = 6;
      if (hw.justPressed(SELECT_8)) assignedGate2 = 7;

    }

    else if (hw.buttonState(SELECT_7)) {
      for (uint8_t i = 0; i < 6; i++) { //visual indication
        hw.setLed(i, i != currentPreset);
      }
      hw.updateShiftRegisters();

      for (uint8_t i = 0; i < 6; i++) {
        if ( hw.justPressed(i)) {
          save(i);
          hw.blinkNumber(i);
          currentPreset = i;
          knobFrozen = true;
        }
      }

      //save(currentPreset);
      //hw.blinkNumber(currentPreset);

      // ch7capacitive = !ch7capacitive;
      //hw.setTouchpad1Mode(ch7capacitive);
    }

    else if (hw.buttonState(SELECT_8)) {
      for (uint8_t i = 0; i < 6; i++) { //visual indication
        hw.setLed(i, i == currentPreset);
      }
      //hw.update();
      hw.updateShiftRegisters();
      for (uint8_t i = 0; i < 6; i++) {
        if (hw.justPressed(i)) {
          //Serial.println(i);
          load(i);
          currentPreset = i;
          hw.blinkNumber(currentPreset);
          knobFrozen = true;
        }
      }
    }
    /*
      else if (hw.justPressed(SYNTH_B)) {

      }
      else if (hw.buttonState(MUTE_B)) {

      }
    */
    else if (hw.justPressed(INVERT_B)) {
      resetChannel(selectedChannel);
    }
    else {
      // SET DATA EXTRACTION FILTERS and METHODS
      for (uint8_t i = 0; i < 6; i++) {
        if ( hw.justPressed(i)) { //DATA + ABCDEF = flip the state of the data extraction settings bits and save
          if (i == LOW_THRESHOLD) knobFrozen = true;
          bitWrite(extractionSettings[selectedChannel], i, !bitRead(extractionSettings[selectedChannel], i));
        }
      }


      //  ch8capacitive = !ch8capacitive;
      // hw.setTouchpad2Mode(ch8capacitive);

    }


  }

  //MIDI MAPPING


  //GATE MAPPING
  else if (hw.buttonState(GATE_B)) { // assign the assignable GATE
    for (uint8_t i = 0; i < 6; i++) {
      if (hw.justPressed(i)) assignedGate = i;
    }
    if (hw.justPressed(SELECT_7)) assignedGate = 6;
    if (hw.justPressed(SELECT_8)) assignedGate = 7;
  }

  //CV MAPPING

  else if (hw.buttonState(CV_B)) { // assign the assignable CV

    for (uint8_t i = 0; i < 6; i++) {
      if (hw.justPressed(i)) assignedCV = i;
    }
    if (hw.justPressed(SELECT_7)) assignedCV = 6;
    if (hw.justPressed(SELECT_8)) assignedCV = 7;

  }





  //SETTING MUTES
  else if (hw.buttonState(MUTE_B)) {
    for (uint8_t i = 0; i < 6; i++) {
      if (hw.justPressed(i))  muteState[i] = !muteState[i]; //set to the oposite mute state

    }
    if (hw.justPressed(SELECT_7)) muteState[6] = !muteState[6]; //set to the oposite mute state
    if (hw.justPressed(SELECT_8)) muteState[7] = !muteState[7]; //set to the oposite mute state
  }


  else if ( hw.justPressed(SYNTH_B) ) { // this was here for some reason? && !(hw.buttonState(MUTE_B)
    synthMode = true;//true;
    knobFrozen = true;
    combo = true;
  }

  //l
  else {

    //SETTING INVERT
    if (hw.justPressed(INVERT_B)) invertState[selectedChannel] = !invertState[selectedChannel]; //invert state filp

    //SWITCHING_CHANNELS only when no other buttons are pressed
    for (uint8_t i = 0; i < 6; i++) {
      if (hw.justPressed(i)) {
        selectedChannel = i; //if on of channel buttons is pressed select that channel
        knobFrozen = true; //freeze the threshold knob if channel is changed
      }
    }
    if (hw.justPressed(SELECT_8)) {

      selectedChannel = 7;
      knobFrozen = true; //freeze the threshold knob if channel is changed


    }
    if (hw.justPressed(SELECT_7)) {

      selectedChannel = 6;
      knobFrozen = true; //freeze the threshold knob if channel is changed


    }

  }
  if(hw.justReleased(MIDI_B)) midiDisplay = 0;

  bool _wasAnythingPressed = false; //this resets the combo
  for (uint8_t i = 0; i < 15; i++) { //scan if all buttons are released
    if (hw.buttonState(i)) _wasAnythingPressed = true;
  }
  if (!_wasAnythingPressed) combo = false; // if all buttons are released reset the combo

}



void renderDisplay() {

  if (hw.buttonState(MIDI_B)) {
    if (midiDisplay == 0) {
      hw.displayNumber(midiMessageAssign[selectedChannel]);
    }
    else if (midiDisplay == 1) {
      hw.displayNumber(midiChannelAssign[selectedChannel] - 1);
    }
  }
  else if (hw.buttonState(SHIFT_B) && hw.buttonState(CV_B)) { //seconary CV
    hw.displayNumber(assignedCV2);
  }
  else if (hw.buttonState(SHIFT_B) && hw.buttonState(GATE_B)) { //seconary CV
    hw.displayNumber(assignedGate2);
  }
  else if (synthMode) {
    if (hw.buttonState(SYNTH_B) && hw.buttonState(SHIFT_B)) {
      for (uint8_t i = 0; i < 8; i++) {
        hw.setLed(i, synthAssign[selectedParameter] == i);
      }
      //hw.setLed(6,synthAssign[selectedParameter]==6);
    }
    else {
      hw.displayNumber(selectedParameter);
    }
    hw.displaySynthParameter(finalSynthValue[selectedParameter]);
  }
  else if (hw.buttonState(SHIFT_B)) { //display
    if (hw.buttonState(SELECT_8)) { //show current preset
      for (uint8_t i = 0; i < 6; i++) {
        hw.setLed(i, i == currentPreset); //if the current preset is equal to the button index than light it up
      }
    }
    else {
      for (uint8_t i = 0; i < 6; i++) { //otherwise show the extraction settings
        hw.setLed(i, bitRead(extractionSettings[selectedChannel], i));
      }
      // hw.setLed(6, ch7capacitive);
      // hw.setLed(7, ch8capacitive);
    }
  }
  else if (hw.buttonState(GATE_B)) {
    hw.displayNumber(assignedGate);
  }
  else if (hw.buttonState(CV_B)) {
    hw.displayNumber(assignedCV);
  }
  else if (hw.buttonState(MUTE_B)) {
    for (uint8_t i = 0; i < 6; i++) {
      hw.setLed(i, !muteState[i]);
      //hw.displayNumber(muteState[i]);
    }
    hw.setLed(6, !muteState[6]);
    hw.setLed(7, !muteState[7]);
  }//Here i added the mute visual functionality, it displays all the muted channels.


  else {


    hw.displayVoltage(finalValue[selectedChannel]); //display voltage at the voltmeter from selected chanel
    //hw.displayVoltage(extr.getData(selectedChannel)); //display voltage at the voltmeter from selected chanel
    hw.displayNumber(selectedChannel); // display which channel is selected only when other important buttons are not used
  }

  hw.setLed(INVERT_L, invertState[selectedChannel]); //show the state of the invert
  if (!hw.buttonState(MIDI_B)) hw.setLed(DATA_L, gateState[selectedChannel]); //show the state of the gate

}



//synth assign map
#define SYNTH_FREQUENCY 0
#define SYNTH_OCTAVE 1
#define SYNTH_DETUNE 2
#define SYNTH_DETUNE_LEVEL 3
#define SYNTH_SUB_LEVEL 4
#define SYNTH_LEVEL 5
#define SYNTH_WAVEFORM 6

#define SYNTH_SCALE 7



void updateSynth() {

  finalSynthValue[SYNTH_FREQUENCY] = synthParameter[SYNTH_FREQUENCY]; //set the synth parameter to the value set by threshold knob
  uint8_t sensorChannel = synthAssign[SYNTH_FREQUENCY]; //look which sensor channel controls that parameter
  if (sensorChannel < 8 && !muteState[sensorChannel]) finalSynthValue[SYNTH_FREQUENCY] += finalValue[sensorChannel]; //if the sensor channel is one of the 8 sensor add that to the synth parameter

  //do it for all synth parameters
  finalSynthValue[SYNTH_LEVEL] = synthParameter[SYNTH_LEVEL] ;
  sensorChannel = synthAssign[SYNTH_LEVEL];
  if (sensorChannel < 8 && !muteState[sensorChannel]) finalSynthValue[SYNTH_LEVEL] += finalValue[sensorChannel];
  finalSynthValue[SYNTH_LEVEL]=constrain(finalSynthValue[SYNTH_LEVEL],0,1023);

  finalSynthValue[SYNTH_OCTAVE] = synthParameter[SYNTH_OCTAVE];
  sensorChannel = synthAssign[SYNTH_OCTAVE];
  if (sensorChannel < 8 && !muteState[sensorChannel]) finalSynthValue[SYNTH_OCTAVE] += finalValue[sensorChannel];

  finalSynthValue[SYNTH_SUB_LEVEL] = synthParameter[SYNTH_SUB_LEVEL] ;
  sensorChannel = synthAssign[SYNTH_SUB_LEVEL];
  if (sensorChannel < 8 && !muteState[sensorChannel])  finalSynthValue[SYNTH_SUB_LEVEL] += finalValue[sensorChannel];
  finalSynthValue[SYNTH_SUB_LEVEL]=constrain(finalSynthValue[SYNTH_SUB_LEVEL],0,1023);
  
  finalSynthValue[SYNTH_DETUNE] = synthParameter[SYNTH_DETUNE];
  sensorChannel = synthAssign[SYNTH_DETUNE];
  if (sensorChannel < 8 && !muteState[sensorChannel]) finalSynthValue[SYNTH_DETUNE] += finalValue[sensorChannel];
  finalSynthValue[SYNTH_DETUNE]=constrain(finalSynthValue[SYNTH_DETUNE],0,1023);
  
  finalSynthValue[SYNTH_DETUNE_LEVEL] = synthParameter[SYNTH_DETUNE_LEVEL] ;
  sensorChannel = synthAssign[SYNTH_DETUNE_LEVEL];
  if (sensorChannel < 8 && !muteState[sensorChannel]) finalSynthValue[SYNTH_DETUNE_LEVEL] += finalValue[sensorChannel];
  finalSynthValue[SYNTH_DETUNE_LEVEL]=constrain(finalSynthValue[SYNTH_DETUNE_LEVEL],0,1023);

  finalSynthValue[SYNTH_WAVEFORM] = synthParameter[SYNTH_WAVEFORM];
  sensorChannel = synthAssign[SYNTH_WAVEFORM];
  if (sensorChannel < 8 && !muteState[sensorChannel]) finalSynthValue[SYNTH_WAVEFORM] += finalValue[sensorChannel];

  synthWave = map(constrain(finalSynthValue[SYNTH_WAVEFORM], 0, 1023), 0, 1023, 0, 12);


  finalSynthValue[SYNTH_SCALE] = synthParameter[SYNTH_SCALE];
  sensorChannel = synthAssign[SYNTH_SCALE];
  if (sensorChannel < 8 && !muteState[sensorChannel]) finalSynthValue[SYNTH_SCALE] += finalValue[sensorChannel];
   
  //uint16_t timbreSum = 0;

  //uint16_t decaySum = 0;
  //uint16_t modSum = 0;


  /*
    for (uint8_t i = 0; i < 8; i++) {
      if (muteState[i] == false) {
        if (synthAssign[i] == 0)
        {
          frequencySum += finalValue[i];
        }

        if (synthAssign[i] == 1) {
          //timbreSum += finalValue[i];
          frequencySum1 += finalValue[i];

        }
        if (synthAssign[i] == 2) {

          frequencySum2 += finalValue[i];
          //amplitudeSum += finalValue[i];

        }
        if (synthAssign[i] == 3) {
          amplitudeSum2 += finalValue[i];
          // decaySum += finalValue[i];


        }
        if (synthAssign[i] == 4) {
          amplitudeSum1 += finalValue[i];

          //setEnvelopeGate(gateState[i]);
        }
        if (synthAssign[i] == 5) {

          amplitudeSum += finalValue[i];
          //modSum += finalValue[i];

        }
      }
    }
  */

  setMainFrequency(constrain(finalSynthValue[SYNTH_FREQUENCY], 0, 1023));
  // setSemitone(constrain(frequencySum>>5, 0, 1023));
  setOctave(constrain(finalSynthValue[SYNTH_OCTAVE], 0, 1023));

  setMainAmplitude(constrain(finalSynthValue[SYNTH_LEVEL], 0, 800));//clip so we are not too loud

  // setFrequency(constrain(frequencySum1, 0, 1023), 1);
  setAmplitude(1023, 0);
  setAmplitude(constrain(finalSynthValue[SYNTH_SUB_LEVEL], 0, 800), 1);//clip so we are not too loud
  setAmplitude(constrain(finalSynthValue[SYNTH_DETUNE_LEVEL], 0, 800), 2); //clip so we are not too loud

  setDetune(constrain(finalSynthValue[SYNTH_DETUNE], 0, 1023) - 500);
  setWave(synthWave);

  setScale(map(finalSynthValue[SYNTH_SCALE], 0, 1023, 0, 12));

  //setTimbre(constrain(timbreSum, 0, 1023));
  //setDecay(constrain(decaySum, 0, 1023));
  // setMod(constrain(modSum, 0, 1023));

}
void updateAnalogOuts() {
  //update all individual gates
  for (uint8_t i = 0; i < 8; i++) {
    if (!muteState[i]) hw.setGate(i, gateState[i]);
    else hw.setGate(i, false);
  }
  //update assignable gate
  if (!muteState[assignedGate]) hw.setAssignableGate(gateState[assignedGate]);
  else hw.setAssignableGate(false);

   if (!muteState[assignedGate2]) hw.setAssignableGate2(gateState[assignedGate2]);
  else hw.setAssignableGate2(false);
  // pinMode(2,OUTPUT);
  // digitalWrite(2,gateState[assignedGate]);

  //update assignable cv
  if (!muteState[assignedCV]) hw.setCV(finalValue[assignedCV]);
  else hw.setCV(0);

  //hw.setCV(finalValue[6]);//assignedCV]);
  //Serial.println(finalValue[6]);//assignedCV]);

  if (assignedCV2 != ASSIGNED_TO_SYNTH) {
    if (!muteState[assignedCV2]) Timer1.pwm(9, finalValue[assignedCV2]);
    else Timer1.pwm(9, 0);

  }

}
