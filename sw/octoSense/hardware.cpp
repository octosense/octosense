#include "hardware.h"



void hw::init() {
  //set all pinmodes
  pinMode(PIN_LATCH, OUTPUT);
  pinMode(PIN_CLOCK, OUTPUT);
  pinMode(PIN_SERIALDATA, OUTPUT);
  pinMode(PIN_BUTTON_1, INPUT_PULLUP);
  pinMode(PIN_BUTTON_2, INPUT_PULLUP);
  analogReference(EXTERNAL);

  // let's use SS-Pin / PB0 as the pin to measure the time:
  DDRB = DDRB | 0b00000001;
}
void hw::setDigitalSensorValue(uint16_t _value) {
  digiSensorValue = _value;
}
void hw::updateShiftRegisters(){
    bitClear(PORTD, 7);
  //pump out the bits
  shiftOutFast(PIN_SERIALDATA, PIN_CLOCK, MSBFIRST, shiftOutBytes[0]);  //buttons       // 20us
  shiftOutFast(PIN_SERIALDATA, PIN_CLOCK, MSBFIRST, shiftOutBytes[1]); //leds           // 20us
  shiftOutFast(PIN_SERIALDATA, PIN_CLOCK, MSBFIRST, shiftOutBytes[2]); //leds 2         // 20us
  shiftOutFast(PIN_SERIALDATA, PIN_CLOCK, MSBFIRST, shiftOutBytes[3]);  //gates + 1 led // 20us
  //_delay_ms(1);
 // _delay_ms(3);
  bitSet(PORTD, 7);   // Set PIN_LATCH (D6) HIGH
}
void hw::update() {
  //still needs to be optimized with direct port access

  PORTB = PORTB | 0b00000001;

  //update preivous lastStates to prevent justPressed and justRelease to trigger 8 times in a row
  bitWrite(lastButtonHash, readIndex, bitRead(buttonHash, readIndex));              // 2.2us
  bitWrite(lastButtonHash, readIndex + 8, bitRead(buttonHash, readIndex + 8));      // 8.5us

  unOrderedIndex++; // changing order of readout of analog channels to avoid crosstalk between touchpads
  if (unOrderedIndex > 7) unOrderedIndex = 0;
  readIndex = readOrder[unOrderedIndex];
  //readIndex++; //increment which button row we are reading now                       // 0.6us
  //if (readIndex > 7) readIndex = 0;                                                 // 10-30us

  //remember the last State of the button
  //bitWrite(where, index, 0/1);
  //bitRead(what, index);
  bitWrite(lastButtonHash, readIndex, bitRead(buttonHash, readIndex));                  // 1.8us
  bitWrite(lastButtonHash, readIndex + 8, bitRead(buttonHash, readIndex + 8));          // 1.4us
  shiftOutBytes[0] = B11111111; //turn all bits HIGH B11111111                            // 0.1us
  bitWrite(shiftOutBytes[0], readIndex, 0); //turn one bit low to read only that button  // 0.2us

  bitClear(PORTD, 7);
  //pump out the bits
  shiftOutFast(PIN_SERIALDATA, PIN_CLOCK, MSBFIRST, shiftOutBytes[0]);  //buttons       // 20us
  shiftOutFast(PIN_SERIALDATA, PIN_CLOCK, MSBFIRST, shiftOutBytes[1]); //leds           // 20us
  shiftOutFast(PIN_SERIALDATA, PIN_CLOCK, MSBFIRST, shiftOutBytes[2]); //leds 2         // 20us
  shiftOutFast(PIN_SERIALDATA, PIN_CLOCK, MSBFIRST, shiftOutBytes[3]);  //gates + 1 led // 20us
  //_delay_ms(1);
 // _delay_ms(3);
  bitSet(PORTD, 7);   // Set PIN_LATCH (D6) HIGH
  
  digitalWrite(PIN_SERIALDATA, LOW); // if the invert LED (last in chain for the data pumping) was ON this pin stayed high and that affected analog readout for touchpad G
  
  
 // _delay_ms(3);
  
  
  // we need to wait a little bit for the shiftregisters to update so lets read analog voltage here:
  lastAnalogValues[readIndex] = analogValues[readIndex];
  /*
    analogValues[readIndex] = analogRead(2);
    analogValues[readIndex] =  analogRead(3);
    analogValues[readIndex] =  analogRead(4);
     analogValues[readIndex] = analogRead(0);
    analogValues[readIndex] =  analogRead(5);
    analogValues[readIndex] =  analogRead(1);
  */
  analogValues[readIndex] = analogRead(analogMap[readIndex]);// 110us
 
   //_delay_ms(1);
  //  delay(1);// 4.8us (for bitSet + bitClear)


  // we need to pull down so the logic is inverted
  // bitWrite(buttonHash, readIndex, !digitalRead(PIN_BUTTON_1)); // that's faster but not that readable
  if (bitRead(PINE, 6) /* <-- PIN_BUTTON_1 */) bitWrite(buttonHash, readIndex, 0);     // 1.8us (following 4 lines)
  else bitWrite(buttonHash, readIndex, 1);
  if (bitRead(PINB, 7) /* <-- PIN_BUTTON_2 */) bitWrite(buttonHash, readIndex + 8, 0);
  else bitWrite(buttonHash, readIndex + 8, 1);

  PORTB = PORTB & 0b11111110;                                                            // 0.125us
}

const uint8_t ledMap[17] = {0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0};
const uint8_t buttonMap[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 12, 11, 9, 10, 14, 13,15};
//const uint8_t gateMap[7] = {0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0};

void hw::setLed(uint8_t _led, bool _state) {

  if (_led < 8) {
    if(ledMap[_led]>3) bitWrite(shiftOutBytes[1], ledMap[_led], !_state);
    else bitWrite(shiftOutBytes[1], ledMap[_led], _state);
  }
  else if (_led < 16) {
    if(ledMap[_led]>3) bitWrite(shiftOutBytes[2], ledMap[_led], !_state);
    else bitWrite(shiftOutBytes[2], ledMap[_led], _state);
  }
  else  bitWrite(shiftOutBytes[3], ledMap[_led], _state);

  //bitRead()
}

#define ASSIGNABLE_GATE_INDEX 3
#define ASSIGNABLE_GATE2_INDEX 4

void hw::setGate(uint8_t _gate, bool _state) {

  if (_gate != ASSIGNABLE_GATE2_INDEX && _gate != ASSIGNABLE_GATE_INDEX && _gate != 0 && _gate < 8) bitWrite(shiftOutBytes[3], _gate, _state);

  //bitRead()
}


void hw::setAssignableGate(bool _state) {
  bitWrite(shiftOutBytes[3], ASSIGNABLE_GATE_INDEX, _state);
}
void hw::setAssignableGate2(bool _state) {
  bitWrite(shiftOutBytes[3], ASSIGNABLE_GATE2_INDEX, _state);
}

bool hw::buttonState(uint8_t _button) {
  if (_button < NUMBER_OF_BUTTONS) {
    return bitRead(buttonHash, buttonMap[_button]);
  }
  else return 0;
}
bool hw::justPressed(uint8_t _button) {
  if (_button < NUMBER_OF_BUTTONS) {
    if ( bitRead(buttonHash, buttonMap[_button]) && !bitRead(lastButtonHash, buttonMap[_button])) return 1;
    else return 0;
  }
  else return 0;
}
bool hw::justReleased(uint8_t _button) {
  if (_button < NUMBER_OF_BUTTONS) {
    if (!bitRead(buttonHash, buttonMap[_button]) && bitRead(lastButtonHash, buttonMap[_button])) return 1;
    else return 0;
  }
  else return 0;
}
void hw::setTouchpad1Mode(bool _mode) {
  pad1capacitive = _mode;
}
void hw::setTouchpad2Mode(bool _mode) {
  pad2capacitive = _mode;
}

void hw::blinkNumber(uint8_t _number) {
  for (uint8_t i = 0; i < 6; i++) { //visual indication - blink current preset 6 times
    setLed(_number, 1);
    update();
    delay(100);
    setLed(_number, 0);
    update();
    delay(100);
  }
}

uint16_t hw::getAnalogValue(uint8_t _channel) {
  if (_channel == 6) {
    if (pad1capacitive) {
      return 1023 - analogValues[_channel]; // change this to capacitive readout
    }
    else {
      return 1023 - analogValues[_channel]; //resistive touchpads are inverted by default, lowering resistance to ground by touching
    }
  }
  else if (_channel == 7) {
    if (pad2capacitive) {
      return 1023 - analogValues[_channel]; // change this to capacitive readout
    }
    else {
      return 1023 - analogValues[_channel]; //resistive touchpads are inverted by default, lowering resistance to ground by touching
    }
  }
  else {
    return analogValues[_channel]; // do mapping here
  }
}

uint16_t hw::getThresholdKnobValue() {
  return analogValues[5];
}
uint16_t hw::getLastThresholdKnobValue() {
  return lastAnalogValues[5];
}
#define VOLTMETER_INDEX_OFFSET 8
const uint8_t voltageDisplay[13] = { B00000001, B00000011, B00000010, B00000110, B00000100, B00001100, B00001000, B00011000, B00010000, B00110000, B00100000,  B01100000,  B01000000};


void hw::displayVoltage(uint16_t _voltage) {
  uint8_t _steppedValue = map(constrain(_voltage, 0, 1023), 0, 1023, 0, 12); //divide into 13 steps
  for (uint8_t i = 0; i < 7; i++) { //update all 7 LEDs
    bool _ledBit = bitRead(voltageDisplay[_steppedValue], i); //get bit for each LED
    setLed(i + VOLTMETER_INDEX_OFFSET, _ledBit); //set the LED
  }

}

void hw::displaySynthParameter(uint16_t _voltage) {
  uint8_t _steppedValue = map(constrain(_voltage, 0, 1023), 0, 1023, 0, 12); //divide into 13 steps
  for (uint8_t i = 0; i < 7; i++) { //update all 7 LEDs
    bool _ledBit = bitRead(voltageDisplay[_steppedValue], i); //get bit for each LED
    setLed(i + VOLTMETER_INDEX_OFFSET, !_ledBit); //set the LED
  }

}

void hw::displayNumber(uint8_t _number) {
  if (_number > 7 && _number < 16) {
    for (uint8_t i = 0; i < 8; i++) {
      setLed(i, 1);
    }
    setLed(_number - 8, 0);
  }
  else if (_number <= 7) {
    for (uint8_t i = 0; i < 8; i++) {
      setLed(i, 0);
    }
    setLed(_number, 1);
  }
  else if (_number >= 16) {
    for (uint8_t i = 0; i < 8; i++) {
      setLed(i, 0);
    }
  }
}

void hw::setCV(uint16_t value) {
  Timer1.pwm(10, value);
}

//The fast shiftOutFast is taken from here: https://forum.arduino.cc/t/fast-shiftout/66946
// maybe also try this one https://www.arduino.cc/reference/en/libraries/fastshiftout/

void hw::shiftOutFast(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint8_t val)
{
  uint8_t cnt;
  uint8_t bitData, bitNotData;
  uint8_t bitClock, bitNotClock;
  volatile uint8_t *outData;
  volatile uint8_t *outClock;

  outData = portOutputRegister(digitalPinToPort(dataPin));
  outClock = portOutputRegister(digitalPinToPort(clockPin));
  bitData = digitalPinToBitMask(dataPin);
  bitClock = digitalPinToBitMask(clockPin);

  bitNotClock = bitClock;
  bitNotClock ^= 0x0ff;

  bitNotData = bitData;
  bitNotData ^= 0x0ff;

  cnt = 8;
  if (bitOrder == LSBFIRST)
  {
    do
    {
      if ( val & 1 )
        *outData |= bitData;
      else
        *outData &= bitNotData;

      *outClock |= bitClock;
      *outClock &= bitNotClock;
      val >>= 1;
      cnt--;
    } while ( cnt != 0 );
  }
  else
  {
    do
    {
      if ( val & 128 )
        *outData |= bitData;
      else
        *outData &= bitNotData;

      *outClock |= bitClock;
      *outClock &= bitNotClock;
      val <<= 1;
      cnt--;
    } while ( cnt != 0 );
  }
}
