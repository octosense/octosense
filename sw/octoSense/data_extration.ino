



#define AVG_DEPTH 16
int circularBuffer[8][AVG_DEPTH]; //256 bytes in the memory (total memory is 2056 bytes)
uint8_t circularIndex = 0;
int envelopeFollower[8];
uint8_t decayRate=4; // 4 ~ 1s , 80 ~ 20hz cutoff
uint32_t channelTime[8];
uint32_t channelDuration[8];
void renderFinalValues() {

  uint8_t i =  hw.getReadIndex(); //we dont need to calculate everything all the time because only one channel is updated in each run of the loop function

  lastFinalValue[i] = finalValue[i];
  if (i == 5) finalValue[i] = hw.getDigitalSensorValue(); //digitalSensor readout is not analog value
  else finalValue[i] = hw.getAnalogValue(i); //get value from the hw

  finalValue[i] = map(finalValue[i], channelMin[i], channelMax[i], 0, 1023); //map it based on automatic calibration
  finalValue[i] = constrain(finalValue[i], 0, 1023); //make sure it clips and does not overflow
  if (invertState[i])  finalValue[i] = 1023 - finalValue[i]; //invert the value if inversion is ON

  //average
  if (i == 0) circularIndex++;
  if (circularIndex >= AVG_DEPTH) circularIndex = 0;
  circularBuffer[i][circularIndex] = finalValue[i];
  uint16_t average = 0;
  for (uint8_t j = 0; j < AVG_DEPTH; j++) {
    average = average + circularBuffer[i][j];
  }

  average = average >> 4; //optimized version for divided by 16 - simple bitshift ... is is the same as ~ average = average / AVG_DEPTH;
  //00001011 >>1 = 00000101 = /2
  //00000101 >>1 = 00000010 = /2
  if (bitRead(extractionSettings[i], AVERAGING)) {
    finalValue[i] = average;
  }

  //envelope follower rendering
  if (bitRead(extractionSettings[i], FAST_EF) && bitRead(extractionSettings[i], ENVELOPE_FOLLOWER)) {
    decayRate=8;
  }
  else if (bitRead(extractionSettings[i], FAST_EF) ){
    decayRate=50;
  }
  else{
    decayRate=16;
  }
  if (envelopeFollower[i] > decayRate) envelopeFollower[i] = envelopeFollower[i] - decayRate;
  if (finalValue[i] > envelopeFollower[i]) envelopeFollower[i] = finalValue[i];
  if (bitRead(extractionSettings[i], ENVELOPE_FOLLOWER) || bitRead(extractionSettings[i], FAST_EF)) {
    finalValue[i] = envelopeFollower[i];
  }
  

  //SET GATE ACCORDING TO THRESHOLD
  lastGateState[i] = gateState[i];
  if (bitRead(extractionSettings[i], THRESHOLD_INVERSION)) {
    if (inBetween(finalValue[i], channelThreshold[i], channelThreshold2[i])) gateState[i] = false;
    else gateState[i] = true;
  }
  else {
    if (inBetween(finalValue[i], channelThreshold[i], channelThreshold2[i])) gateState[i] = true;
    else gateState[i] = false;
  }

  // FREQ counter
  if(!lastGateState[i] && gateState[i]) {
    uint32_t duration=measurementTime-channelTime[i];
    channelTime[i]=measurementTime;
    channelDuration[i]=duration;
  }
  if (bitRead(extractionSettings[i], FREQ_COUNTER)) finalValue[i]=constrain(map(channelDuration[i]>>7,2000,1,0,1023),0,1023);

}
