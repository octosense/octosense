#define PRESET_OFFSET 160


#define INVERT_OFFSET 0
#define MIN_OFFSET 8
#define MAX_OFFSET 24
#define THR_OFFSET 40
#define THR_2_OFFSET 56
#define EXTR_OFFSET 72
#define SYNTHASSIGN_OFFSET 80
#define MUTE_OFFSET 88
#define CHANNEL_OFFSET 96
#define MESSAGE_OFFSET 104
#define NUMBER_OFFSET 112
#define CVASSIGN_OFFSET 121
#define GATEASSIGN_OFFSET 122
#define CV2ASSIGN_OFFSET 123
#define PAD1MODE_OFFSET 124
#define PAD2MODE_OFFSET 125
#define SYNTH_PARAMETER_OFFSET 126

#define GATEASSIGN2_OFFSET 143

//40
#define DIGITAL_SENSOR_TYPE_OFFSET 1019

#define CHECK_BYTE_1 37
#define CHECK_BYTE_2 203
#define CHECK_BYTE_3 65

#define CHECK_ADDRESS_1 1020
#define CHECK_ADDRESS_2 1021
#define CHECK_ADDRESS_3 1022

void formatPreset6(uint8_t _channel) {
  if (_channel == 0) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000000; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch7capacitive = false;

  }
  if (_channel == 1 ) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000000; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch7capacitive = false;

  }
  if (_channel == 2 ) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000000; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch7capacitive = false;

  }
  if (_channel == 3 ) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000000; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch7capacitive = false;

  }
  if (_channel == 4 ) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000000; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch7capacitive = false;

  }
  if (_channel == 5 ) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000000; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch7capacitive = false;

  }
  if (_channel == 6 ) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000001; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch7capacitive = false;

  }
  else if (_channel == 7) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000001; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch8capacitive = false;
  }

  switch (_channel) {
    case 0:
      synthAssign[0] = UNASSIGNED;
      midiMessageAssign[0] = UNASSIGNED;
      break;
    case 1:
      synthAssign[1] = UNASSIGNED;
      midiMessageAssign[1] = UNASSIGNED;
      break;
    case 2:
      synthAssign[2] = UNASSIGNED;
      midiMessageAssign[2] = UNASSIGNED;
      break;
    case 3:
      synthAssign[3] = UNASSIGNED;
      midiMessageAssign[3] = UNASSIGNED;
      break;
    case 4:
      synthAssign[4] = UNASSIGNED;
      midiMessageAssign[4] = UNASSIGNED;
      break;
    case 5:
      synthAssign[5] = UNASSIGNED;
      midiMessageAssign[5] = UNASSIGNED;
      break;
    case 6:
      synthAssign[6] = UNASSIGNED;
      midiMessageAssign[6] = UNASSIGNED;
      break;
    case 7:
      synthAssign[7] = UNASSIGNED;
      midiMessageAssign[7] = UNASSIGNED;
      break;
  }


}
void resetChannel(uint8_t _channel) {

  if (_channel == 6 ) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000001; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch7capacitive = false;

  }
  else if (_channel == 7) {
    invertState[_channel] = false;
    channelMin[_channel] = 200;
    channelMax[_channel] = 800;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = B00000001; //1; //first bit is high because we want averaging ON B00000001
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
    ch8capacitive = false;
  }
  else {
    invertState[_channel] = false;
    channelMin[_channel] = 0;
    channelMax[_channel] = 1023;
    channelThreshold[_channel] = 512;
    channelThreshold2[_channel] = 1023;
    extractionSettings[_channel] = 0;
    muteState[_channel] = false;
    midiChannelAssign[_channel] = 1;
    midiNumberAssign[_channel] = 60;
  }
  switch (_channel) {
    case 0:
      synthAssign[0] = UNASSIGNED;
      midiMessageAssign[0] = UNASSIGNED;
      break;
    case 1:
      synthAssign[1] = UNASSIGNED;
      midiMessageAssign[1] = UNASSIGNED;
      break;
    case 2:
      synthAssign[2] = UNASSIGNED;
      midiMessageAssign[2] = UNASSIGNED;
      break;
    case 3:
      synthAssign[3] = UNASSIGNED;
      midiMessageAssign[3] = UNASSIGNED;
      break;
    case 4:
      synthAssign[4] = UNASSIGNED;
      midiMessageAssign[4] = UNASSIGNED;
      break;
    case 5:
      synthAssign[5] = UNASSIGNED;
      midiMessageAssign[5] = UNASSIGNED;
      break;
    case 6:
      synthAssign[6] = UNASSIGNED;
      midiMessageAssign[6] = UNASSIGNED;
      break;
    case 7:
      synthAssign[7] = UNASSIGNED;
      midiMessageAssign[7] = UNASSIGNED;
      break;
  }
}
void formatMemory() {
  //Serial.println("formatin memory");
  for (uint8_t i = 0; i < 8; i++) {
    resetChannel(i);
  }
  /*
    for (uint8_t i = 0; i < 8; i++) { //set all variables to default
    invertState[i] = false;
    channelMin[i] = 0;
    channelMax[i] = 1023;
    channelThreshold[i] = 512;
    channelThreshold2[i] = 1023;
    extractionSettings[i] = 0;
    muteState[i] = false;
    midiChannelAssign[i] = 1;
    midiNumberAssign[i] = 64;

    }


    synthAssign[0] = SYNTH_MOD;
    synthAssign[1] = SYNTH_FREQUENCY;
    synthAssign[2] = SYNTH_TIMBRE;
    synthAssign[3] = SYNTH_MOD;
    synthAssign[4] = SYNTH_MOD;
    synthAssign[5] = SYNTH_DECAY;
    synthAssign[6] = 4;
    synthAssign[7] = 1;

    midiMessageAssign[0] = 5;
    midiMessageAssign[1] = 0;
    midiMessageAssign[2] = 1;
    midiMessageAssign[3] = 5;

    midiMessageAssign[4] = 5;
    midiMessageAssign[5] = 3;
    midiMessageAssign[6] = 4;
    midiMessageAssign[7] = 1;
  */
  for (uint8_t i = 0; i < 8; i++) synthParameter[0];
  assignedCV = 6;
  assignedCV2 = ASSIGNED_TO_SYNTH;
  assignedGate = 7;
  digitalSensorType = 0;


  for (uint8_t i = 0; i < 5; i++) save(i); //save to all presets to eeprom
  
  for (uint8_t i = 0; i < 8; i++) {
    formatPreset6(i);
  }
  
  save(5);

  //write confirmation bytes
  EEPROM.write(CHECK_ADDRESS_1, CHECK_BYTE_1);
  EEPROM.write(CHECK_ADDRESS_2, CHECK_BYTE_2);
  EEPROM.write(CHECK_ADDRESS_3, CHECK_BYTE_3);

  currentPreset = 0;
  EEPROM.write(CURRENT_PRESET_OFFSET, currentPreset);

}

void load(uint8_t _preset) {

  if (EEPROM.read(CHECK_ADDRESS_1) == CHECK_BYTE_1 && EEPROM.read(CHECK_ADDRESS_2) == CHECK_BYTE_2 && EEPROM.read(CHECK_ADDRESS_3) == CHECK_BYTE_3) { //look if memory was correctly formated
    // Serial.println("loading");
    //if yes load from memory
    assignedCV = EEPROM.read(_preset * PRESET_OFFSET + CVASSIGN_OFFSET);
    assignedCV2 = EEPROM.read(_preset * PRESET_OFFSET + CV2ASSIGN_OFFSET);
    assignedGate = EEPROM.read(_preset * PRESET_OFFSET + GATEASSIGN_OFFSET);
    assignedGate2 = EEPROM.read(_preset * PRESET_OFFSET + GATEASSIGN2_OFFSET);
    ch7capacitive = EEPROM.read(_preset * PRESET_OFFSET + PAD1MODE_OFFSET);
    ch8capacitive = EEPROM.read(_preset * PRESET_OFFSET + PAD2MODE_OFFSET);



    for (uint8_t i = 0; i < 8; i++) {
      invertState[i] = EEPROM.read(_preset * PRESET_OFFSET + i + INVERT_OFFSET);
      extractionSettings[i] = EEPROM.read(_preset * PRESET_OFFSET + i + EXTR_OFFSET);
      synthAssign[i] = EEPROM.read(_preset * PRESET_OFFSET + i + SYNTHASSIGN_OFFSET);
      //loading mute states is deactivated to avoid congussion - uncomment to load them from memory (they are always saved in the preset)
      // muteState[i] = EEPROM.read(_preset * PRESET_OFFSET + i + MUTE_OFFSET);
      midiChannelAssign[i] = EEPROM.read(_preset * PRESET_OFFSET + i + CHANNEL_OFFSET);
      midiMessageAssign[i] = EEPROM.read(_preset * PRESET_OFFSET + i + MESSAGE_OFFSET);
      midiNumberAssign[i] = EEPROM.read(_preset * PRESET_OFFSET + i + NUMBER_OFFSET);
    }

    for (uint8_t i = 0; i < 8; i++) {
      channelMin[i] = word(EEPROM.read(_preset * PRESET_OFFSET + (2 * i) + MIN_OFFSET + 1), EEPROM.read((2 * i) + MIN_OFFSET ));
      channelMax[i] = word(EEPROM.read(_preset * PRESET_OFFSET + (2 * i) + MAX_OFFSET + 1), EEPROM.read((2 * i) + MAX_OFFSET ));
      channelThreshold[i] = word(EEPROM.read(_preset * PRESET_OFFSET + (2 * i) + THR_OFFSET + 1), EEPROM.read((2 * i) + THR_OFFSET ));
      channelThreshold2[i] = word(EEPROM.read(_preset * PRESET_OFFSET + (2 * i) + THR_2_OFFSET + 1), EEPROM.read((2 * i) + THR_2_OFFSET ));
      synthParameter[i] = word(EEPROM.read(_preset * PRESET_OFFSET + (2 * i) + SYNTH_PARAMETER_OFFSET + 1), EEPROM.read((2 * i) + SYNTH_PARAMETER_OFFSET ));
    }
    digitalSensorType = EEPROM.read(DIGITAL_SENSOR_TYPE_OFFSET);

    currentPreset = _preset;
    EEPROM.write(CURRENT_PRESET_OFFSET, currentPreset);


  }
  else { //if not format memory
    formatMemory();
  }

}

void save(uint8_t _preset) {

  EEPROM.write ( _preset * PRESET_OFFSET + CVASSIGN_OFFSET , assignedCV);
  EEPROM.write ( _preset * PRESET_OFFSET + CV2ASSIGN_OFFSET , assignedCV2);
  EEPROM.write( _preset * PRESET_OFFSET + GATEASSIGN_OFFSET, assignedGate);
  EEPROM.write( _preset * PRESET_OFFSET + GATEASSIGN2_OFFSET, assignedGate2);
  EEPROM.write(_preset * PRESET_OFFSET + PAD1MODE_OFFSET, ch7capacitive);
  EEPROM.write(_preset * PRESET_OFFSET + PAD2MODE_OFFSET, ch8capacitive);

  for (uint8_t i = 0; i < 8; i++) {
    EEPROM.write( _preset * PRESET_OFFSET + i + INVERT_OFFSET, invertState[i]);
    EEPROM.write( _preset * PRESET_OFFSET + i + EXTR_OFFSET, extractionSettings[i]);
    EEPROM.write( _preset * PRESET_OFFSET + i + SYNTHASSIGN_OFFSET, synthAssign[i]);
    EEPROM.write( _preset * PRESET_OFFSET + i + MUTE_OFFSET, muteState[i]);

    EEPROM.write( _preset * PRESET_OFFSET + i + CHANNEL_OFFSET, midiChannelAssign[i]);
    EEPROM.write( _preset * PRESET_OFFSET + i + MESSAGE_OFFSET, midiMessageAssign[i]);
    EEPROM.write( _preset * PRESET_OFFSET + i + NUMBER_OFFSET, midiNumberAssign[i]);
  }

  for (uint8_t i = 0; i < 8; i++) {
    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + MIN_OFFSET + 1, highByte(channelMin[i]));
    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + MIN_OFFSET , lowByte(channelMin[i]));

    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + MAX_OFFSET + 1, highByte(channelMax[i]));
    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + MAX_OFFSET , lowByte(channelMax[i]));

    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + THR_OFFSET + 1, highByte(channelThreshold[i]));
    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + THR_OFFSET , lowByte(channelThreshold[i]));

    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + THR_2_OFFSET + 1, highByte(channelThreshold2[i]));
    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + THR_2_OFFSET , lowByte(channelThreshold2[i]));

    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + SYNTH_PARAMETER_OFFSET + 1, highByte(synthParameter[i]));
    EEPROM.write( (_preset * PRESET_OFFSET) + (2 * i) + SYNTH_PARAMETER_OFFSET , lowByte(synthParameter[i]));
  }

  EEPROM.write(DIGITAL_SENSOR_TYPE_OFFSET, digitalSensorType);

}
