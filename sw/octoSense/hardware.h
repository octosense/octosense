#ifndef HARDWARE_LIB
#define HARDWARE_LIB

#include <Arduino.h>
#include <TimerOne.h>
#ifdef ARDUINO_TEENSY31
#include "pinsTeensy.h" 
#else
#include "pinsArduino.h"
#endif



const uint8_t analogMap[8] = {0, 1, 2, 3, 4, 5, 8, 11};
const uint8_t readOrder[8] = {0,6,1,3,4,5,7,2}; // changing order of readout of analog channels to avoid crosstalk between touchpads
//const uint8_t readOrder[8] = {0,1,2,3,4,5,6,7};
//defines of the common aliases
#define CH_A 0
#define CH_B 1
#define CH_C 2
#define CH_D 3
#define CH_E 4
#define CH_R 5
#define CAL_B 6
#define INVERT_B 7
#define SHIFT_B 8 
#define SYNTH_B 11 //simons version 10 - originally 11
#define SELECT_8 10  //simons version 11 - originally 10
#define SELECT_7 9 //simons version - originally 9
#define MUTE_B 12
#define CV_B 13
#define GATE_B 14
#define MIDI_B 15

//#include <ShiftRegister74HC595.h>

// create a global shift register object
// parameters: <number of shift registers> (data pin, clock pin, latch pin)
//ShiftRegister74HC595<1> sr(PIN_SERIALDATA, PIN_CLOCK, PIN_LATCH);

//


#define NUMBER_OF_BUTTONS 16
#define NUMBER_OF_ANALOG_CHANNELS 8


class hw
{
  private:
    uint8_t unOrderedIndex;
    uint8_t shiftOutBytes[4];
    uint8_t readIndex;
    uint16_t buttonHash;
    uint16_t lastButtonHash;

    uint16_t ledStateHash;
    uint8_t voltLedStateHash;
    bool pad1capacitive;
    bool pad2capacitive;
    uint16_t analogValues[NUMBER_OF_ANALOG_CHANNELS];
    uint16_t lastAnalogValues[NUMBER_OF_ANALOG_CHANNELS];

    void shiftOutFast(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint8_t val);
    int digiSensorValue;
  public:
    void updateShiftRegisters();
    void displaySynthParameter(uint16_t _voltage);
    int getDigitalSensorValue(){ return digiSensorValue;};
    void blinkNumber(uint8_t _number);
    void setTouchpad1Mode(bool _mode);
    void setTouchpad2Mode(bool _mode);
    void setDigitalSensorValue(uint16_t _value);
    void setAssignableGate(bool _state);
     void setAssignableGate2(bool _state);
    void init();
    void update();
    void setLed(uint8_t _led, bool _state);
    void setGate(uint8_t _gate, bool _state);
    bool buttonState(uint8_t _button);
    bool justPressed(uint8_t _button);
    bool justReleased(uint8_t _button);
    uint8_t getReadIndex() {
      return readIndex;
    };
    uint16_t getAnalogValue(uint8_t _channel);
    uint16_t getThresholdKnobValue();
    uint16_t getLastThresholdKnobValue();

    void displayVoltage(uint16_t _voltage);
    void displayNumber(uint8_t _number);


    void setCV(uint16_t value);

};
#endif
