/*
  #include "DHT.h"

  /* ultrasonic proximity distance sensor
   encoder
   DHT=temperature and humidity
    BH1750=light resistor?
    accelerometer/gyroscope
    GAS SENSOR/
    COLOR SENSOR
*/
int readOutRate = 0;


DHT11 dht11(D2);  // creating the object sensor on pin 'D2/SDA'
int C;   // temperature C readings are integers
float F; // temperature F readings are returned in float format
int H;   // humidity readings are integers



int h;
const int trigPin = 2;
const int echoPin = 3;
// defines variables
long duration;
int distance;


void initDigitalSensor() {

  switch (digitalSensorType) {
    case 0: //encoder
      break;

    case 1: //dht
      readOutRate = 300; //read every 300ms
      break;

    case 2: //ultrasonic distance sensor
      readOutRate = 100;
      pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
      pinMode(echoPin, INPUT); // Sets the echoPin as an Input
      break;

    case 3: //accelerometer
      break;

    case 4: //gas sensor?
      break;

    case 5: //color sensor?
      break;

    case 6: //custom
      break;

    case 7: //custom
      break;
  }

}
long lastTime;
int digitalSensorValue = 0;
void readDigitalSensor() {

  if (millis() - lastTime > readOutRate) { // defined how often each sensor is being read
    lastTime = millis();

    switch (digitalSensorType) {
      case 0: //encoder
        break;

      case 1: //dht
        dht11.update();
        digitalSensorValue = dht11.readHumidity() * 10;
        break;

      case 2: //ultrasonic distance sensor
        digitalWrite(trigPin, LOW);
        delayMicroseconds(2);
        // Sets the trigPin on HIGH state for 10 micro seconds
        digitalWrite(trigPin, HIGH);
        delayMicroseconds(10);
        digitalWrite(trigPin, LOW);
        // Reads the echoPin, returns the sound wave travel time in microseconds
        duration = pulseIn(echoPin, HIGH, 8200); //timeout for 2 meters (8.2ms)
        if(duration==0) duration = 8200;
        // Calculating the distance
        distance = duration * 0.034 / 2;
        distance = constrain(distance, 1, 200); //make sure it is withing range
        //Serial.print("Distance: ");
        //Serial.println(distance);
        digitalSensorValue = distance;
        break;

      case 3: //accelerometer
        break;

      case 4: //gas sensor?
        break;

      case 5: //color sensor?
        break;

      case 6: //custom
        break;

      case 7: //custom
      digitalSensorValue = hw.getThresholdKnobValue();
        break;
    }
  }

  hw.setDigitalSensorValue(digitalSensorValue);
}
/*
  #define DHTPIN 2     // Digital pin connected to the DHT sensor
  #define DHTTYPE DHT11
  //#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
  //#define DHTTYPE DHT21   // DHT 21 (AM2301)
  DHT dht(DHTPIN, DHTTYPE);

  dht.begin();
  }

  void readDigitalSensor(){

    float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
   float t = dht.readTemperature();

   hw.setDigitalSensorValue(t);
  }
*/


/*
  #include <Wire.h>
  #include <BH1750.h>

  BH1750 lightMeter;
  void initDigitalSensor(){
  Wire.begin();
  lightMeter.begin();
  }

  void readDigitalSensor(){
   hw.setDigitalSensorValue(lightMeter.readLightLevel()/10);
  }
*/
